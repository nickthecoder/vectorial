package uk.co.nickthecoder.vectorial.renderer

import java.util.*

/**
 * A rectangle using Float
 */
data class Rect(val left: Float, val top: Float, val right: Float, val bottom: Float) {

    val width get() = right - left
    val height get() = bottom - top

    constructor() : this(0f, 0f, 0f, 0f)

    fun plus(dx: Float, dy: Float) = Rect(
        left + dx,
        top + dy,
        right + dx,
        bottom + dy,
    )

    fun contains(other: Rect): Boolean =
        left <= other.left && right >= other.right &&
        top <= other.bottom && bottom >= other.top

    fun overlaps(other: Rect): Boolean =
        left < other.right && right > other.left &&
        top < other.bottom && bottom > other.top

    fun grow(deltaX: Float, deltaY: Float) = Rect(left - deltaX, top - deltaY, right + deltaX, bottom + deltaY)
    fun grow(delta: Float) = grow(delta, delta)

    override fun equals(other: Any?): Boolean {
        if (other !is Rect) {
            return false
        }
        return other.left == left && other.bottom == bottom && other.right == right && other.top == top
    }

    override fun hashCode() = Objects.hash(left, bottom, top, right)


    override fun toString(): String = "($left , $bottom , $right , $top)"

    companion object {
        val UNIT = Rect(0f, 0f, 1f, 1f)
    }
}
