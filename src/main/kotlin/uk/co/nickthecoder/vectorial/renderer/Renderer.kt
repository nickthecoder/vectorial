package uk.co.nickthecoder.vectorial.renderer

import org.joml.Matrix3x2f
import org.lwjgl.opengl.EXTFramebufferObject.*
import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL32.GL_TEXTURE_2D_MULTISAMPLE
import org.lwjgl.system.MemoryUtil
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.shape.GeometryShape
import uk.co.nickthecoder.vectorial.core.shape.Group
import uk.co.nickthecoder.vectorial.core.shape.Shape
import java.nio.FloatBuffer

lateinit var renderer : Renderer

/**
 * The place that all Vectorial's graphics operations feed through.
 *
 * Rendering is performed by various [ShaderProgram]s.
 * The [ShaderProgram]s are `internal` classes, and therefore are not part of the API.
 *
 * This is a `class` rather than an `object`, because we need to control when it is created
 * (for the Vectorial application, this is created in the `launch` method).
 */
class Renderer {

    /**
     * A matrix which converts Vectorial's world coordinates to OpenGL's output coordinates
     * (where 0,0 is the center of the output, and the height and width of the output is 2).
     */
    val viewMatrix = Matrix3x2f()

    private val flatColorProgram = FlatColorProgram()

    private val subs = listOf(
        flatColorProgram
    )

    var outputFBO = 0
        private set

    /**
     * Render output to a [VectorialTexture], or set to null to output to the display device.
     * You must NOT change this while rendering a scene. i.e. not between calls to [beginView] and [endView].
     */
    var outputTexture: VectorialTexture? = null
        set(v) {
            if (field !== v) {

                reset()

                if (v == null) {
                    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0)

                    // Delete now unused FBO
                    if (outputFBO != 0) {
                        glDeleteFramebuffersEXT(outputFBO)
                        outputFBO = 0
                    }
                } else {
                    outputFBO = glGenFramebuffersEXT()
                    glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, outputFBO)
                    glFramebufferTexture2DEXT(
                        GL_FRAMEBUFFER_EXT,
                        GL_COLOR_ATTACHMENT0_EXT,
                        if (v.multiSample == 1) GL_TEXTURE_2D else GL_TEXTURE_2D_MULTISAMPLE,
                        v.handle,
                        0
                    )
                }
                field = v
            }
        }

    init {
        if (::renderer.isInitialized) throw RuntimeException("Renderer has already been initialised" )
        renderer = this

        vertexBuffer = VertexBuffer()
        vertexBuffer.bind()
        /* Upload null data to allocate storage for the VBO */
        val size = (floatBuffer.capacity() * java.lang.Float.BYTES).toLong()
        vertexBuffer.uploadData(size, Usage.DYNAMIC_DRAW)
    }

    // region ==== Setup view etc ====

    fun reportError() : Boolean {
        val error = glGetError()
        if (error != 0) {
            severe("GL Error $error")
            return true
        }
        return false
    }

    fun reset() {
        vertexBuffer.bind()
        currentSubRenderer?.unuse()
        currentSubRenderer = null
        glEnable(GL_BLEND)
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA)
    }

    fun clear(color: Color) {
        glClearColor(color.red, color.green, color.blue, color.alpha)
        glClear(GL_COLOR_BUFFER_BIT)
    }

    fun view(left: Float, top: Float, right: Float, bottom: Float, block : ()->Unit ) {
        beginView(left, top, right, bottom)
        block()
        endView()
    }

    fun beginView(viewMatrix: Matrix3x2f) {
        reset()
        this.viewMatrix.set(viewMatrix)
    }

    fun beginView(left: Float, top: Float, right: Float, bottom: Float) {
        reset()
        viewMatrix.set(
            2f / (right - left), 0f,
            0f, 2f / (bottom - top),
            -(right + left) / (right - left), -(top + bottom) / (bottom - top)
        )
    }

    fun endView() {
        reset()
    }
    // endregion Setup view etc.

    // region ==== Low level drawing ====

    fun fillTriangles(vertices: List<Vector2>, color: Color, modelMatrix: Matrix3x2f? = null) {
        with(flatColorProgram) {
            use(viewMatrix)
            setup(color, modelMatrix)
            draw(vertices)
            unuse()
        }
    }

    /**
     * Draws a solid rectangle.
     */
    fun fillRect(rect: Rect, color: Color, modelMatrix: Matrix3x2f? = null) {
        with(flatColorProgram) {
            use(viewMatrix)
            setup(color, modelMatrix)
            fillRect(rect)
            unuse()
        }
    }

    /**
     * Draws a rectangular outline. The [thickness] is *outside* the rect.
     * Assuming that [rect] left <= right and top <= bottom.
     * If they are the other way round, then the thickness will be *inside* the rect.
     */
    fun strokeRect(rect: Rect, thickness: Float, color: Color, modelMatrix: Matrix3x2f? = null) {
        with(flatColorProgram) {
            use(viewMatrix)
            setup(color, modelMatrix)
            strokeRect(rect, thickness)
            unuse()
        }
    }


    fun line(from: Vector2, to: Vector2, thickness: Float, color: Color, modelMatrix: Matrix3x2f? = null) {
        with(flatColorProgram) {
            use(viewMatrix)
            setup(color, modelMatrix)
            drawLine(from, to, thickness)
            unuse()
        }
    }


    fun stroke(shape: GeometryShape, modelMatrix: Matrix3x2f? = null) {
        val sub = flatColorProgram
        with(sub) {
            use(viewMatrix)
            setup(shape.style.stroke(), modelMatrix)
            draw(shape.strokeMesh.points)
            unuse()
        }
    }

    fun fill(shape: GeometryShape, modelMatrix: Matrix3x2f? = null) {
        if (shape.fillMesh.points.isEmpty()) return
        val sub = flatColorProgram
        with(sub) {
            use(viewMatrix)
            setup(shape.style.fill(), modelMatrix)

            draw(shape.fillMesh.points)
            unuse()
        }
    }

    // endregion low level drawing

    // region ==== Draw Shapes ====

    private var noPriorErrors = true

    fun drawShape(shape: Shape) {
        try {
            when (shape) {
                is Group -> {
                    for (child in shape.shapes) {
                        drawShape(child)
                    }
                }

                is GeometryShape -> {
                    fill(shape)
                    stroke(shape)
                }
            }
        } catch (e: Exception) {
            println("Failed to render shape $shape : $e")
            if (noPriorErrors) {
                e.printStackTrace()
                noPriorErrors = false
            }
        }
    }

    /**
     * See [LayerParent] for details on the correct ordering of drawing.
     */
    fun drawLayer(layer: Layer) {
        if (layer.isVisible) {
            for (shape in layer.shapes) {
                drawShape(shape)
            }
            for (child in layer.layers) {
                drawLayer(child)
            }
        }
    }

    // endregion draw shapes

    private fun delete() {
        for (sub in subs) {
            sub.delete()
        }
        floatBuffer.clear()
        vertexBuffer.delete()
    }

    companion object {
        internal var currentSubRenderer: ShaderProgram? = null

        // We use one float buffer for all rendering.
        internal var floatBuffer: FloatBuffer = MemoryUtil.memAllocFloat(40960)
        lateinit var vertexBuffer: VertexBuffer
    }

}
