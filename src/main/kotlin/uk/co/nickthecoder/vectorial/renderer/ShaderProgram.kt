/*
Tickle
Copyright (C) 2017 Nick Robinson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/
package uk.co.nickthecoder.vectorial.renderer

import org.joml.Matrix3f
import org.joml.Matrix3x2f
import org.joml.Matrix4f
import org.lwjgl.opengl.GL20.*
import org.lwjgl.system.MemoryStack
import uk.co.nickthecoder.vectorial.core.*

/**
 * [Renderer] has a simplified API to use all the [ShaderProgram]s, so there is little need to interact
 * with them directly.
 *
 */
internal abstract class ShaderProgram(
    vertex: String,
    fragment: String,
    val floatsPerVertex: Int
) {

    private val handle = glCreateProgram()

    protected val floatsPerTriangle = floatsPerVertex * 3

    init {
        try {
            val vertexShader = Shader(ShaderType.VERTEX_SHADER, vertex)
            val fragmentShader = Shader(ShaderType.FRAGMENT_SHADER, fragment)
            glAttachShader(handle, vertexShader.handle)
            glAttachShader(handle, fragmentShader.handle)
            glLinkProgram(handle)
            checkStatus()
            // We can delete the shaders, now that the program has been linked.
            vertexShader.delete()
            fragmentShader.delete()

        } catch (e: Exception) {
            severe(e)
        }
    }

    val positionLocation = getAttributeLocation("position")
    val uvLocation = getAttributeLocation("uv", false)
    val uLocation = getAttributeLocation("u", false)

    val viewMatrixLocation = getUniformLocation("viewMatrix")
    val modelMatrixLocation = getUniformLocation("modelMatrix", false)

    private var vertexCount = 0

    open fun printLocations() {
        info("position : $positionLocation")
        info("uv       : $uvLocation")
        info("u        : $uLocation")
        info("")
        info("view     : $viewMatrixLocation")
        info("model    : $modelMatrixLocation")
    }

    fun use(viewMatrix: Matrix3x2f) {
        val current = Renderer.currentSubRenderer
        if (current != this) {
            current?.unuse()
        }
        glUseProgram(handle)
        Renderer.currentSubRenderer = this
        setUniform(viewMatrixLocation, viewMatrix)
    }

    fun setModelMatrix(modelMatrix: Matrix3x2f?) {
        setUniform(modelMatrixLocation, modelMatrix ?: identity)
    }

    fun unuse() {
        flush()
        glUseProgram(0)
        Renderer.currentSubRenderer = null
    }

    protected fun enablePosition() {
        vertexAttribute(positionLocation, 2, 0, 0)
        glEnableVertexAttribArray(positionLocation)
    }

    protected fun enablePositionNormal(normalLocation: Int) {
        vertexAttribute(positionLocation, 2, 4 * java.lang.Float.BYTES, 0)
        vertexAttribute(normalLocation, 2, 4 * java.lang.Float.BYTES, 2L * java.lang.Float.BYTES)
        glEnableVertexAttribArray(positionLocation)
        glEnableVertexAttribArray(normalLocation)
    }

    protected fun enablePositionUV() {
        vertexAttribute(positionLocation, 2, 4 * java.lang.Float.BYTES, 0)
        vertexAttribute(uvLocation, 2, 4 * java.lang.Float.BYTES, 2L * java.lang.Float.BYTES)
        glEnableVertexAttribArray(positionLocation)
        glEnableVertexAttribArray(uvLocation)
    }

    protected fun enablePositionU() {
        vertexAttribute(positionLocation, 2, 3 * java.lang.Float.BYTES, 0)
        vertexAttribute(uLocation, 1, 3 * java.lang.Float.BYTES, 2L * java.lang.Float.BYTES)
        glEnableVertexAttribArray(positionLocation)
        glEnableVertexAttribArray(uLocation)
    }

    protected fun disablePosition() {
        glDisableVertexAttribArray(positionLocation)
    }

    protected fun disablePositionNormal(normalLocation: Int) {
        glDisableVertexAttribArray(positionLocation)
        glDisableVertexAttribArray(normalLocation)
    }

    protected fun disablePositionUV() {
        glDisableVertexAttribArray(positionLocation)
        glDisableVertexAttribArray(uvLocation)
    }

    fun getAttributeLocation(name: CharSequence, warn: Boolean = true): Int {
        val result = glGetAttribLocation(handle, name)
        if (warn && result < 0) {
            warn("Uniform $name not found in ${javaClass.simpleName}")
        }
        return result
    }

    fun getUniformLocation(name: CharSequence, warn: Boolean = true): Int {
        val result = glGetUniformLocation(handle, name)
        if (warn && result < 0) {
            warn("Uniform $name not found in ${javaClass.simpleName}")
        }
        return result
    }

    fun enableVertexAttribute(index: Int) {
        glEnableVertexAttribArray(index)
    }

    fun disableVertexAttribute(index: Int) {
        glDisableVertexAttribArray(index)
    }

    fun vertexAttribute(index: Int, size: Int, stride: Int, offset: Long) {
        glVertexAttribPointer(index, size, GL_FLOAT, false, stride, offset)
    }

    fun setUniform(location: Int, value: Int) {
        glUniform1i(location, value)
    }

    fun setUniform(location: Int, value: Float) {
        glUniform1f(location, value)
    }

    fun setUniform(location: Int, value: Vector2) {
        MemoryStack.stackPush().use { stack ->
            val buffer = stack.mallocFloat(2 * 4)
            buffer.put(value.x).put(value.y)
            buffer.flip()
            glUniform2fv(location, buffer)
        }
    }

    fun setUniform(location: Int, value: Matrix4f) {
        MemoryStack.stackPush().use { stack ->
            val buffer = stack.mallocFloat(4 * 4)
            value[0, buffer]
            glUniformMatrix4fv(location, false, buffer)
        }
    }

    fun setUniform(location: Int, value: Matrix3f) {
        MemoryStack.stackPush().use { stack ->
            val buffer = stack.mallocFloat(9)
            value[0, buffer]
            glUniformMatrix3fv(location, false, buffer)
        }
    }

    /**
     * Set a 3x3 matrix from a 3x2 matrix.
     */
    fun setUniform(location: Int, value: Matrix3x2f) {
        MemoryStack.stackPush().use { stack ->
            with(value) {
                glUniformMatrix3fv(
                    location, false,
                    stack.mallocFloat(9)
                        .put(m00).put(m01).put(0f)
                        .put(m10).put(m11).put(0f)
                        .put(m20).put(m21).put(1f)
                        .flip()

                )
            }
        }
    }

    fun setUniform(location: Int, value: Color) {
        glUniform4f(location, value.red, value.green, value.blue, value.alpha)
    }

    protected fun requiredTriangles(triangles: Int) {
        if (Renderer.floatBuffer.remaining() < triangles * floatsPerTriangle) {
            flush()
        }
        vertexCount += triangles * 3
    }

    protected fun requiredVertices(vertices: Int) {
        if (Renderer.floatBuffer.remaining() < vertices * floatsPerVertex) {
            flush()
        }
        vertexCount += vertices
    }

    fun flush() {
        if (vertexCount > 0) {
            Renderer.floatBuffer.flip()
            Renderer.vertexBuffer.bind()
            Renderer.vertexBuffer.uploadSubData(0, Renderer.floatBuffer)

            // Draw the batch
            glDrawArrays(GL_TRIANGLES, 0, vertexCount)

            // Clear vertex data for next batch
            Renderer.floatBuffer.clear()
            vertexCount = 0
        }

    }

    fun checkStatus() {
        val status = glGetProgrami(handle, GL_LINK_STATUS)
        if (status != GL_TRUE) {
            throw RuntimeException(glGetProgramInfoLog(handle))
        }
    }

    fun delete() {
        glDeleteProgram(handle)
    }

    override fun toString(): String = javaClass.simpleName

    companion object {
        val identity = Matrix3x2f()
        val white = Colors.WHITE
    }
}
