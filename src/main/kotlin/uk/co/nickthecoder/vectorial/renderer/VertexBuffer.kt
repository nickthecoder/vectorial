package uk.co.nickthecoder.vectorial.renderer

import org.lwjgl.opengl.GL15.*
import java.nio.FloatBuffer
import java.nio.IntBuffer

class VertexBuffer {

    val handle: Int = glGenBuffers()

    fun bind() {
        glBindBuffer(GL_ARRAY_BUFFER, handle)
    }

    fun delete() {
        glDeleteBuffers(handle)
    }

    fun uploadData(data: FloatBuffer, usage: Usage) {
        glBufferData(GL_ARRAY_BUFFER, data, usage.value)
    }

    fun uploadData(size: Long, usage: Usage) {
        glBufferData(GL_ARRAY_BUFFER, size, usage.value)
    }

    fun uploadSubData(offset: Long, data: FloatBuffer) {
        glBufferSubData(GL_ARRAY_BUFFER, offset, data)
    }

    fun uploadData(data: IntBuffer, usage: Usage) {
        glBufferData(GL_ARRAY_BUFFER, data, usage.value)
    }

}
