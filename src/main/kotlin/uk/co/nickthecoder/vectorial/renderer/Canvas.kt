/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.renderer

import org.lwjgl.opengl.EXTFramebufferObject.*
import org.lwjgl.opengl.GL30.*
import uk.co.nickthecoder.glok.backend.Texture
import uk.co.nickthecoder.glok.util.log
import uk.co.nickthecoder.vectorial.core.expression.RecursionLimitExceeded
import uk.co.nickthecoder.vectorial.core.expression.logExpressions
import uk.co.nickthecoder.vectorial.core.severe

/**
 * Helps to draw to an off-screen buffer, with the option of using multi-sampling for anti-aliasing.
 * `DiagramView` draws the diagram to a Canvas, and then draws the cavas to the screen.
 */
class Canvas {

    var width: Int= 0
        private set

    var height: Int = 0
        private set

    var multiSample : Int = 1
        private set

    private var drawingTexture : VectorialTexture? = null

    /**
     * An intermediate texture used when [multiSample] != 1. This texture and the associated
     * FBO [intermediateFBO] are NOT multi-sampled, whereas [drawingTexture] is.
     */
    private var intermediateTexture: VectorialTexture? = null

    private var intermediateFBO = 0

    val texture : Texture get() = intermediateTexture ?: drawingTexture ?: throw RuntimeException( "Canvas has not been drawn yet" )

    fun resize(width: Int, height: Int, multiSample: Int) {
        if (width <= 0 || height <= 0 || multiSample < 1) throw IllegalArgumentException()
        if (width == this.width && height == this.height && multiSample == this.multiSample) return

        this.width = width
        this.height = height
        this.multiSample = multiSample

        log.debug("Canvas resized to $width, $height : $multiSample")

        if (intermediateFBO != 0) {
            glDeleteFramebuffersEXT(intermediateFBO)
        }
        intermediateTexture?.destroy()
        drawingTexture?.destroy()

        drawingTexture = VectorialTexture(width, height, null, multiSample)

        if (multiSample == 1) {
            // Not multi-sampled, therefore intermediateTexture and intermediateFBO are not used.
            intermediateTexture = null
            intermediateFBO = 0
        } else {
            intermediateTexture = VectorialTexture(width, height, null)
            intermediateFBO = glGenFramebuffersEXT()
            glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, intermediateFBO)
            glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_2D, intermediateTexture!!.handle, 0)
        }
    }

    fun draw(width: Int, height: Int, multiSample: Int, action: () -> Unit) {
        // Report any previous GLErrors.
        renderer.reportError()

        VectorialTexture.unbind()
        resize(width, height, multiSample)
        renderer.outputTexture = drawingTexture

        glViewport(0, 0, width, height)
        try {
            action()
        } catch (e: RecursionLimitExceeded) {
            logExpressions("Canvas.draw() RecursionLimitExceeded redrawing with logging on") {
                action()
            }
        } catch (e: Exception) {
            severe("Exception thrown during Canvas.draw(). Image will be incomplete.")
            severe(e)
        }

        if (multiSample != 1) {
            // [texture] is multi-sampled, therefore we have to `resolve` it to a non-multi-sampled
            // texture [intermediateTexture] first, using its associated FBO [intermediateFBO].
            glBindFramebuffer(GL_READ_FRAMEBUFFER, renderer.outputFBO)
            glBindFramebuffer(GL_DRAW_FRAMEBUFFER, intermediateFBO)
            glBlitFramebuffer(0, 0, width, height, 0, 0, width, height, GL_COLOR_BUFFER_BIT, GL_NEAREST)
            glBindFramebuffer(GL_FRAMEBUFFER, 0)
        }
        renderer.outputTexture = null

        renderer.reportError()
    }

}
