package uk.co.nickthecoder.vectorial.renderer

import org.joml.Matrix3x2f
import uk.co.nickthecoder.vectorial.core.Color
import uk.co.nickthecoder.vectorial.core.Vector2

internal class FlatColorProgram : ShaderProgram(SG_VERTEX, SG_FRAGMENT, 2) {

    private val colorLocation = getUniformLocation("color")

    fun setup(color: Color, modelMatrix: Matrix3x2f? = null) {
        enablePosition()
        setModelMatrix(modelMatrix)
        setUniform(colorLocation, color)
    }

    fun draw(vertices: List<Vector2>) {
        requiredVertices(vertices.size)

        for (point in vertices) {
            Renderer.floatBuffer.put(point.x).put(point.y)
        }
    }


    fun drawLine(from: Vector2, to: Vector2, thickness: Float) {
        val delta = (to - from).unit().perpendicular() * (thickness / 2f)

        requiredTriangles(2)
        with(Renderer.floatBuffer) {

            put(from.x + delta.x).put(from.y + delta.y)
            put(from.x - delta.x).put(from.y - delta.y)
            put(to.x + delta.x).put(to.y + delta.y)

            put(to.x + delta.x).put(to.y + delta.y)
            put(to.x - delta.x).put(to.y - delta.y)
            put(from.x - delta.x).put(from.y - delta.y)
        }
    }

    fun setupAndStrokeRect(rect: Rect, thickness: Float, color: Color, modelMatrix: Matrix3x2f? = null) {
        setup(color, modelMatrix)
        strokeRect(rect, thickness)
    }

    fun strokeRect(rect: Rect, thickness: Float) {
        val left = rect.left
        val top = rect.top
        val right = rect.right
        val bottom = rect.bottom

        val outerLeft = left - thickness
        val outerTop = top - thickness
        val outerBottom = bottom + thickness
        val outerRight = right + thickness

        requiredTriangles(8)
        with(Renderer.floatBuffer) {

            // Left edge
            put(left).put(bottom)
            put(outerLeft).put(outerBottom)
            put(outerLeft).put(outerTop)

            put(outerLeft).put(outerTop)
            put(left).put(top)
            put(left).put(bottom)

            // Top edge
            put(outerLeft).put(outerTop)
            put(outerRight).put(outerTop)
            put(left).put(top)

            put(left).put(top)
            put(outerRight).put(outerTop)
            put(right).put(top)


            // Right Edge
            put(right).put(top)
            put(outerRight).put(outerTop)
            put(right).put(bottom)

            put(right).put(bottom)
            put(outerRight).put(outerTop)
            put(outerRight).put(outerBottom)

            // Bottom Edge
            put(outerRight).put(outerBottom)
            put(outerLeft).put(outerBottom)
            put(left).put(bottom)

            put(left).put(bottom)
            put(right).put(bottom)
            put(outerRight).put(outerBottom)
        }
    }

    fun setupAndFillRect(rect: Rect, color: Color, modelMatrix: Matrix3x2f? = null) {
        setup(color, modelMatrix)
        fillRect(rect)
    }

    fun fillRect(rect: Rect) {

        val left = rect.left
        val bottom = rect.bottom
        val right = rect.right
        val top = rect.top

        requiredTriangles(2)
        with(Renderer.floatBuffer) {
            put(left).put(bottom)
            put(right).put(bottom)
            put(right).put(top)

            put(right).put(top)
            put(left).put(top)
            put(left).put(bottom)
        }
    }

    companion object {

        private val SG_VERTEX = """
            #version 120

            attribute vec2 position;
            
            uniform mat3 viewMatrix;
            uniform mat3 modelMatrix;
            
            void main() {
                gl_Position = vec4( viewMatrix * modelMatrix * vec3(position, 1.0), 1.0);
            }
            
            """.trimIndent()

        private val SG_FRAGMENT = """
            #version 120
            
            uniform vec4 color;

            void main() {
                gl_FragColor = color;
            }
            """.trimIndent()
    }

}
