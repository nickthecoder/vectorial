package uk.co.nickthecoder.vectorial.renderer

import org.lwjgl.opengl.GL11.*
import org.lwjgl.opengl.GL32.GL_TEXTURE_2D_MULTISAMPLE
import org.lwjgl.opengl.GL32.glTexImage2DMultisample
import org.lwjgl.stb.STBImage.stbi_load
import org.lwjgl.system.MemoryStack
import uk.co.nickthecoder.glok.backend.Texture
import uk.co.nickthecoder.vectorial.core.Color
import uk.co.nickthecoder.vectorial.core.info
import uk.co.nickthecoder.vectorial.core.warn
import java.io.File
import java.io.IOException
import java.nio.ByteBuffer

class VectorialTexture(
    override val width: Int,
    override val height: Int,
    buffer: ByteBuffer? = null,
    val multiSample: Int = 1,
) : Texture {

    override val name get() = "VectorialTexture"

    var handle: Int = glGenTextures()
        private set

    init {
        bind()
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST)
        if (multiSample == 1) {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer)
        } else {
            glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, multiSample, GL_RGBA, width, height, true)
        }
    }

    override fun bind() {
        if (multiSample == 1) {
            glBindTexture(GL_TEXTURE_2D, handle)
        } else {
            glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, handle)
        }
    }

    /**
     * Transfers the texture from the GPU back to main memory (which is SLOW).
     * The result is a ByteArray of size width * height * 4, and the format is RGBA.
     * i.e. to get the alpha value at x,y :
     *
     *     read()[ (y * height + x) * 4 + 3 ]
     *
     * and then deal with the annoyance of java's lack of unsigned bytes. Grr :
     *
     *     .toInt() & 0xFF
     *
     * Does not work when [multiSample] != 1
     */
    fun toByteArray(): ByteArray {
        bind()
        val pixels = ByteArray(width * height * 4)
        val buffer = ByteBuffer.allocateDirect(pixels.size)
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer)
        buffer.get(pixels)
        unbind()
        return pixels
    }

    fun destroy() {
        if (handle != 0) {
            glDeleteTextures(handle)
            handle = 0
        }
    }

    /**
     * Ascii-art style image of the texture.
     * This is used for debugging only. It dumps the alpha channel of the texture, showing values in the range 0..ff
     * I used it a lot when debugging the PixelOverlap code.
     *
     * Does not work when [multiSample] != 1
     */
    fun dumpAlpha() {
        info("Texture. Dumping alpha channel")
        try {
            val pixels = toByteArray()
            for (y in 0 until height) {
                for (x in 0 until width) {
                    val alpha = pixels[(x + (y * width)) * 4 + 3]
                    print(String.format("%02x", alpha.toInt() and 0xff))
                }
                info("")
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * This is used for debugging only. It dumps texture, showing values in the range 0..ff
     *
     * Does not work when [multiSample] != 1
     */
    fun dumpRGBA(label: String = "") {
        println("Texture#$handle $label $width,$height")
        try {
            val pixels = toByteArray()
            for (y in 0 until height) {
                for (x in 0 until width) {
                    for (b in 0 until 4) {
                        val byte = pixels[(x + (y * width)) * 4 + b]
                        print(String.format("%02x", byte.toInt() and 0xff))
                    }
                    print(" ")
                }
                println()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    /**
     * This is used for debugging only. It dumps texture, showing values in the range 0..ff
     *
     * Does not work when [multiSample] != 1
     */
    fun dumpRGB(label: String = "") {
        println("Texture#$handle $label $width,$height")
        try {
            val pixels = toByteArray()
            if (glGetError() != 0) {
                warn("GL Error ${glGetError()}")
            } else {
                for (y in 0 until height) {
                    for (x in 0 until width) {
                        for (b in 0 until 3) {
                            val byte = pixels[(x + (y * width)) * 4 + b]
                            print(String.format("%02x", byte.toInt() and 0xff))
                        }
                        print(" ")
                    }
                    println()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun toString() = "Texture $width x $height handle=$handle" + if (multiSample==1) "" else " multiSample=$multiSample"

    companion object {

        fun unbind() {
            glBindTexture(GL_TEXTURE_2D, 0)
        }

        fun load(file: File): VectorialTexture {

            MemoryStack.stackPush().use { stack ->

                val width = stack.mallocInt(1)
                val height = stack.mallocInt(1)
                val channels = stack.mallocInt(1)

                // STBImage.stbi_set_flip_vertically_on_load(true)
                val buffer = stbi_load(file.path, width, height, channels, 4)
                buffer ?: throw IOException("Failed to load texture from ${file.absoluteFile}")

                return VectorialTexture(width.get(), height.get(), buffer)
            }
        }

        /**
         * Creates a texture from pixel data in bytes in RGBA order.
         */
        fun fromByteArray(width: Int, height: Int, pixels: ByteArray): VectorialTexture {
            if (pixels.size != width * height * 4) throw IllegalArgumentException("Expected an array of size ${width * height * 4}")
            val bb = ByteBuffer.allocateDirect(width * height * 4)
            bb.mark()
            for (element in pixels) {
                bb.put(element)
            }
            bb.reset()
            return VectorialTexture(width, height, bb)
        }

        fun filled(name: String, width: Int, height: Int, color: Color): VectorialTexture {
            val red = (color.red * 255).toInt().toByte()
            val green = (color.green * 255).toInt().toByte()
            val blue = (color.blue * 255).toInt().toByte()
            val alpha = (color.alpha * 255).toInt().toByte()

            val buffer = ByteBuffer.allocateDirect(width * height * 4)
            for (y in 0 until height) {
                for (x in 0 until width) {
                    with(buffer) {
                        put(red)
                        put(green)
                        put(blue)
                        put(alpha)
                    }
                }
            }
            buffer.flip()
            return VectorialTexture(width, height, buffer)
        }
    }

}
