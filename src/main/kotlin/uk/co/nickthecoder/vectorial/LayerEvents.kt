/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial
import uk.co.nickthecoder.glok.control.ButtonType
import uk.co.nickthecoder.glok.control.ChoiceBox
import uk.co.nickthecoder.glok.control.TextField
import uk.co.nickthecoder.glok.dialog.dialog
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.StageType
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.Layer
import uk.co.nickthecoder.vectorial.core.NewLayerPosition
import uk.co.nickthecoder.vectorial.core.Reorder
import uk.co.nickthecoder.vectorial.core.change.SelectionChange
import uk.co.nickthecoder.vectorial.core.shape.Shape

internal fun onDeleteCurrentLayer(diagram: Diagram?) {
    (diagram?.currentLayerOrGroup as? Layer)?.delete()?.now()
}

internal fun onReorderCurrentLayer(diagram: Diagram?, reorder: Reorder) {
    (diagram?.currentLayerOrGroup as? Layer)?.reorder(reorder)?.now()
}

internal fun onNewLayer(diagram: Diagram?, fromStage: Stage) {
    diagram ?: return

    lateinit var layerName : TextField
    lateinit var position : ChoiceBox<NewLayerPosition>

    dialog {
        title = "New Layer"
        buttonTypes(ButtonType.OK, ButtonType.CANCEL) { reply ->
            if (reply == ButtonType.OK) {
                val layer = Layer.create(layerName.text)
                val newPosition = position.value ?: NewLayerPosition.ABOVE
                val currentLayer = diagram.currentLayerOrGroup as? Layer
                if (currentLayer == null) {
                    diagram.addLayer(layer, newPosition)
                } else {
                    currentLayer.parent.addLayer(layer, newPosition)
                }
            }
        }
        content = form {
            padding( 10 )
            + row( "Layer Name" ) {
                right = textField(diagram.document.generateName("layer")) {
                    layerName = this
                }
            }
            + row( "Position" ) {
                right = choiceBox<NewLayerPosition> {
                    position = this
                    items.addAll( NewLayerPosition.values() )
                    selection.selectedIndex = 0
                }
            }
        }
    }.createStage( fromStage, StageType.MODAL ) {
        show()
        layerName.selectAll()
        layerName.requestFocus()
    }

}

fun onLockUnlockCurrentLayer(diagram: Diagram?, lock : Boolean? = null) {
    val layer = diagram?.currentLayerOrGroup as? Layer ?: return
    val newState = lock ?: !layer.isLocked
    layer.diagram.document.history.batch(
        layer.changeLock(newState),
        SelectionChange.select(layer.diagram, selectionExcludingLayer(layer))
    )
}

fun onLockUnlockLayer(layer: Layer, lock : Boolean? = null) {
    val newState = lock ?: !layer.isLocked
    layer.diagram.document.history.batch(
        layer.changeLock(newState),
        SelectionChange.select(layer.diagram, selectionExcludingLayer(layer))
    )
}

fun onHideShowCurrentLayer(diagram: Diagram?, show : Boolean? = null) {
    val layer = diagram?.currentLayerOrGroup as? Layer ?: return
    val newState = show ?: !layer.isVisible

    layer.diagram.document.history.batch(
        layer.changeVisibility(newState),
        SelectionChange.select(layer.diagram, selectionExcludingLayer(layer))
    )
}
fun onHideShowLayer(layer: Layer, show : Boolean? = null) {
    val newState = show ?: !layer.isVisible

    layer.diagram.document.history.batch(
        layer.changeVisibility(newState),
        SelectionChange.select(layer.diagram, selectionExcludingLayer(layer))
    )
}

/**
 * When we hide/lock a layer, we should remove items from the selection which are on that layer.
 * This filters the selection, excluding shapes on [layer].
 */
private fun selectionExcludingLayer(layer: Layer): List<Shape> {
    val result = mutableListOf<Shape>()
    val selection = layer.diagram.selection
    for (selected in selection.shapes) {
        if (!selected.ancestors().contains(layer)) {
            result.add(selected)
        }
    }
    return result
}
