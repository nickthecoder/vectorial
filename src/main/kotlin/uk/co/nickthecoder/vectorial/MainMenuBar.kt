/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.control.MenuBar
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.scene.dsl.spacer

internal fun mainMenuBar(commands: Commands) = MenuBar().apply {
    id = "MenuBar"

    visibleProperty.bindTo(VectorialSettings.showMenuBarProperty)

    commands.build(VectorialSettings.menuIconSizeProperty) {

        with(VectorialActions) {

            + menu(FILE) {
                + menuItem(FILE_SETTINGS)
                + Separator()
                + menuItem(FILE_CLOSE)
                + menuItem(FILE_QUIT)
            }

            + menu(EDIT) {
                + menuItem(EDIT_UNDO)
                + menuItem(EDIT_REDO)
                + Separator()

                + menuItem(EDIT_CUT)
                + menuItem(EDIT_COPY)
                + menuItem(EDIT_PASTE)
                + Separator()

                + menuItem(EDIT_DUPLICATE)
                + Separator()

                + menuItem(EDIT_DELETE)
                + Separator()

                + menuItem(EDIT_SELECT_ALL)
                + menuItem(EDIT_SELECT_EVERYTHING)
                + menuItem(EDIT_SELECT_NONE)
            }

            + menu(VIEW) {
                + subMenu( VIEW_DOCKS ) {
                    + checkMenuItem( DOCK_LAYERS )
                    + checkMenuItem( DOCK_DEBUG )
                    +checkMenuItem( DOCK_NODE_INSPECTOR )
                }
                + subMenu( VIEW_ZOOM ) {
                    + menuItem( ZOOM_RESET )
                    + menuItem( ZOOM_IN )
                    + menuItem( ZOOM_OUT )
                }
                + Separator()
                + checkMenuItem( VIEW_SHOW_MENUBAR )
                + checkMenuItem( VIEW_SHOW_FIXED_TOOLBAR )
                + checkMenuItem( VIEW_SHOW_STATUS_BAR )
                + Separator()
                + checkMenuItem( THEME_DARK )
            }

            + menu(LAYER) {
                + menuItem( LAYER_TOP )
                + menuItem( LAYER_RAISE )
                + menuItem( LAYER_LOWER )
                + menuItem( LAYER_BOTTOM )
                + Separator()
                + checkMenuItem( DOCK_LAYERS )
            }

            + menu(DEBUG) {
                + checkMenuItem(DOCK_DEBUG)
                + menuItem(DEBUG_DUMP_SCENE)
            }

            + spacer()

            button( FILE_SETTINGS )
        }
    }
}
