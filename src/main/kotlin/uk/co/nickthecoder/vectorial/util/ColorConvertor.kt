/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.util

import uk.co.nickthecoder.glok.util.Converter
import uk.co.nickthecoder.glok.scene.Color as GlokColor
import uk.co.nickthecoder.vectorial.core.Color as VectorialColor

/**
 * Converts between Glok's colors and Vectorial's colors.
 */
object ColorConvertor : Converter<GlokColor, VectorialColor> {
    override fun forwards(value: GlokColor) = VectorialColor(value.red, value.blue, value.green, value.alpha)
    override fun backwards(value: VectorialColor) = GlokColor(value.red, value.blue, value.green, value.alpha)
}
