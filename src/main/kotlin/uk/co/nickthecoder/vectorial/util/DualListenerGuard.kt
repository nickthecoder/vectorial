/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.util


/**
 * Prevents mutual recursion when there are listeners on the GUI and the model,
 * which both attempt to update the other.
 *
 */
class DualListenerGuard {

    private var allowGUIChanges = true
    private var allowModelChanges = true

    fun updateModel(action: () -> Unit) {
        if (allowModelChanges) {
            allowGUIChanges = false
            try {
                action()
            } finally {
                allowGUIChanges = true
            }
        }
    }

    fun updateGUI(action: () -> Unit) {
        if (allowGUIChanges) {
            allowModelChanges = false
            try {
                action()
            } finally {
                allowModelChanges = true
            }
        }
    }
}
