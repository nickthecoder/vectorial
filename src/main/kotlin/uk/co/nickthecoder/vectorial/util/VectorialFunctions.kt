package uk.co.nickthecoder.vectorial.util

import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.shape.layer

/*
    Extension methods to Vectorial `core` classes, specifically designed to help the Glok based GUI.
*/

/**
 * Used to enable/disable the  layer "raise" and "top" buttons.
 */
fun Diagram?.disableRaiseLayer() : Boolean {
    if (this == null) return true
    return !canRaiseLayer()
}

/**
 * Used to enable/disable the  layer "lower" and "bottom" buttons.
 */
fun Diagram?.disableLowerLayer() : Boolean {
    if (this == null) return true
    return !canLowerLayer()
}

// TODO These could be placed on Diagram itself.
fun Diagram.canRaiseLayer() : Boolean {
    val selectedShape = selection.shapes.firstOrNull() ?: return false
    val layer = selectedShape.parent.layer()
    val parentLayer = layer.parent
    return parentLayer.layers.firstOrNull() !== layer
}


fun Diagram.canLowerLayer() : Boolean {
    val selectedShape = selection.shapes.firstOrNull() ?: return false
    val layer = selectedShape.parent.layer()
    val parentLayer = layer.parent
    return parentLayer.layers.lastOrNull() !== layer
}
