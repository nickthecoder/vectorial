/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.control.Tab
import uk.co.nickthecoder.glok.property.boilerplate.StringBinaryFunction
import uk.co.nickthecoder.glok.property.boilerplate.booleanProperty
import uk.co.nickthecoder.glok.property.boilerplate.optionalFileProperty
import uk.co.nickthecoder.vectorial.control.DiagramView
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.Change
import uk.co.nickthecoder.vectorial.core.change.DiagramChange
import uk.co.nickthecoder.vectorial.core.shape.*

class DiagramTab : Tab("Diagram"), DocumentListener {

    val document = Document.create()
    val diagram = document.diagram

    val diagramView = DiagramView(document.diagram).apply {
        id="DiagramView"
        multiSampleProperty.bindTo( VectorialSettings.multiSampleProperty )
        scaleProperty.bindTo( VectorialSettings.diagramScaleProperty )
    }

    private val mutableFileProperty by optionalFileProperty(null)
    val fileProperty = mutableFileProperty.asReadOnly()
    var file by  mutableFileProperty 
        private set

    private val mutableSavedProperty by booleanProperty(document.isSaved())
    val savedProperty = mutableSavedProperty.asReadOnly()
    var saved by mutableSavedProperty
        private set

    private val tabTitleProperty = StringBinaryFunction( fileProperty, savedProperty) { file, saved ->
        val filename = file?.name ?: "New Document"
        if (saved) {
            filename
        } else {
            "* $filename"
        }
    }


    /**
     * Setting this to true, will cause the diagram to be redrawn, and will be reset back to false
     * when the draw has finished. Note, It uses `runLater` so that setting to true more than once
     * will batch them together, and redraw only once.
     * It shouldn't be necessary to use this property though, as a built-in [DocumentListener] will
     * set this to true when [DiagramChange]s are fired.
     */
    val dirtyProperty by booleanProperty(true)
    var dirty by  dirtyProperty 

    init {
        document.addListener(this)
        bindPreferences()

        addTestShapes()

        textProperty.bindTo(tabTitleProperty)
        content = diagramView
    }

    private fun addTestShapes() {

        val layer = Layer.create("default")

        val line0 = Line.create(Vector2(100f, 100f), Vector2(200f, 200f))
        val line0s = line0.createShape("line0", ShapeStyle.create(document, stroke = Colors["red"]))
        val line1 = Line.create(line0.end.eval(), Vector2(100f, 300f))
        val line1s = line1.createShape("line1", ShapeStyle.create(document, stroke = Colors["purple"]))
        val line2 = Line.create(line1.end.eval(), (line0.start.eval() + line0.end.eval()) / 2f)
        val line2s = line2.createShape("line2", ShapeStyle.create(document))

        val guideLine0 = GuideLine.create(50f, Angle.degrees(90.0))
        val guideLine0s = guideLine0.createShape("guide0", ShapeStyle.guideLineStyle(document))
        val guideLine1 = GuideLine.create(50f, Angle.degrees(0.0))
        val guideLine1s = guideLine1.createShape("guide1", ShapeStyle.guideLineStyle(document))

        val square = Polygon.createCentered(4, Vector2(30f, 30f), Vector2(10f, 10f))
        // val bezier = Bezier.create(line0.start.eval(), line1.start.eval(), line2.start.eval(), line2.end.eval())

        //val bezierShape = bezier.createShape("bezier", ShapeStyle.create(stroke = Colors["green"]))
        val squareShape = square.createShape("square", ShapeStyle.create(document, stroke = Colors["green"]))

        diagram.addLayer(layer)
        diagram.document.history.clear()
        document.history.batch("Add dummy shapes") {
            layer.addShape(guideLine0s)
            layer.addShape(guideLine1s)
            layer.addShape(line0s)
            layer.addShape(line1s)
            layer.addShape(line2s)
            layer.addShape(squareShape)
            //diagram.addShape(bezierShape)
        }

    }

    private fun bindPreferences() {
        with(VectorialSettings) {
            diagramView.combineBoundingBoxesProperty.bindTo(combineBoundingBoxesProperty)
            diagramView.handleBorderColorProperty.bindTo(handleBorderColorProperty)
            diagramView.handleColorProperty.bindTo(handleColorProperty)
            diagramView.fixedHandleColorProperty.bindTo(fixedHandleColorProperty)
            diagramView.selectedHandleColorProperty.bindTo(selectedHandleColorProperty)
            diagramView.selectionColorProperty.bindTo(selectionColorProperty)
            diagramView.pageBorderColorProperty.bindTo(borderColorProperty)
            diagramView.diagramBackgroundColorProperty.bindTo(backgroundColorProperty)
            diagramView.paperColorProperty.bindTo(paperColorProperty)
            diagramView.polygonSidesProperty.bindTo(polygonSidesProperty)
            diagramView.polygonTypeProperty.bindTo(polygonTypeProperty)
            diagramView.quadrilateralTypeProperty.bindTo(quadrilateralTypeProperty)
            diagramView.ellipseTypeProperty.bindTo(ellipseTypeProperty)

            diagramView.handleSizeProperty.bindTo(handleSizeProperty)
            diagramView.lineThresholdProperty.bindTo(lineThresholdProperty)
            diagramView.handleThresholdProperty.bindTo(handleThresholdProperty)
            diagramView.constructionLineThicknessProperty.bindTo(constructionLineThicknessProperty)
            diagramView.selectionLineThicknessProperty.bindTo(selectionLineThicknessProperty)
        }
    }

    override fun documentChanged(document: Document, change: Change, isUndo: Boolean) {
        if (!dirty) {
            dirty = true
        }
        saved = document.history.isDocumentSaved()
    }

    fun onZoomIn() {
        if (diagramView.zoom < 0.1f) {
            diagramView.zoom = 0.1f
        } else {
            diagramView.zoom += if (diagramView.zoom >= 1.99f) {
                .50f
            } else if (diagramView.zoom > 0.99f) {
                .20f
            } else {
                .10f
            }
        }
    }

    fun onZoomOut() {
        if (diagramView.zoom < 0.02f) {
            diagramView.zoom = 0.01f
        } else {
            diagramView.zoom -= if (diagramView.zoom <= 0.1f) {
                .01f
            } else if (diagramView.zoom <= 1f) {
                .1f
            } else if (diagramView.zoom <= 2f) {
                .2f
            } else {
                .5f
            }
        }
    }

    fun onZoomReset() {
        // If we reset zoom twice, then pan to the center.
        if (diagramView.zoom == 1.0f) {
            diagramView.centerX = diagram.width() / 2
            diagramView.centerY = diagram.height() / 2
        } else {
            diagramView.zoom = 1f
        }
    }

    fun onUndo(skipSelectionChanges: Boolean) {
        document.history.apply {
            if (!isBatchStarted()) {
                undo(skipSelectionChanges)
            }
        }
    }

    fun onRedo(skipSelectionChanges: Boolean) {
        document.history.apply {
            if (!isBatchStarted()) {
                redo(skipSelectionChanges)
            }
        }
    }


    /**
     * [GuideLine]s are not selected.
     */
    fun onSelectAll() {
        val toSelect = mutableListOf<Shape>()
        for (layer in diagram.layers) {
            toSelect.addAll(
                layer.shapes.filter { it !is GeometryShape || it.geometry !is GuideLine }
            )
        }

        if (!diagram.document.history.isBatchStarted()) {
            diagram.select(toSelect)
        }
    }

    /**
     * Select everything, including [GuideLine]s.
     */
    fun onSelectEverything() {
        val toSelect = mutableListOf<Shape>()
        for (layer in diagram.layers) {
            toSelect.addAll(layer.shapes)
        }

        if (!diagram.document.history.isBatchStarted()) {
            diagram.select(toSelect)
        }
    }

    fun onSelectNone() {
        if (!diagram.document.history.isBatchStarted()) {
            diagram.clearSelection()
        }
    }

    fun onGroup() {
        diagram.group()?.now()
    }

    fun onUngroup() {
        diagram.ungroup()?.now()
    }

}
