/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.property.boilerplate.FloatProperty
import uk.co.nickthecoder.glok.property.boilerplate.ObservableFloat
import uk.co.nickthecoder.glok.property.boilerplate.ObservableString
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.dsl.spacer
import uk.co.nickthecoder.glok.scene.dsl.toolBar
import uk.co.nickthecoder.vectorial.control.PositionIndicator
import uk.co.nickthecoder.vectorial.control.ZoomIndicator
import uk.co.nickthecoder.vectorial.control.formattedText

fun statusBar(
    tipProperty : ObservableString,
    mouseXProperty : ObservableFloat,
    mouseYProperty : ObservableFloat,
    zoomProperty : FloatProperty

) = toolBar( Side.BOTTOM ) {
    id = "StatusBar"
    visibleProperty.bindTo(VectorialSettings.showStatusBarProperty)

    + formattedText(tipProperty) { overrideMinWidth = 0f }
    + spacer()
    + Separator()
    + PositionIndicator(mouseXProperty, mouseYProperty)
    + Separator()
    + ZoomIndicator(zoomProperty)
}
