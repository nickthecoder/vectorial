/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.application.launch
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.dock.load
import uk.co.nickthecoder.glok.dock.save
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.row
import uk.co.nickthecoder.glok.scene.dsl.texture
import uk.co.nickthecoder.glok.scene.icons
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.combineWith
import uk.co.nickthecoder.glok.util.log
import uk.co.nickthecoder.vectorial.control.DiagramView
import uk.co.nickthecoder.vectorial.renderer.Renderer
import java.util.prefs.Preferences

/**
 * The entry point for the Vectorial application.
 * This does very little :
 *
 * 1. Parses command line arguments
 * 1. Initialises `Glok`
 * 1. Loads persisted state (via Java [Preferences] used by [VectorialSettings], `glok-dock`)
 * 1. Creating a [MainWindow] and then letting it control things from then on.
 * 1. When the [MainWindow] is closed, the state is saved.
 *
 * Most of the GUI's heavy-lifting is performed by just two classes, [MainWindow] and [DiagramView].
 */
class Vectorial : Application() {

    override fun start(primaryStage: Stage) {

        Renderer()

        try {
            VectorialSettings.load()
        } catch (e: Exception) {
            log.warn("Failed to load preferences : $e")
        }

        Tantalum.darkProperty.bidirectionalBind(VectorialSettings.darkThemeProperty)
        GlokSettings.defaultThemeProperty.unbind()
        GlokSettings.defaultThemeProperty.bindTo(Tantalum combineWith VectorialTheme )

        val mainWindow = MainWindow(primaryStage)
        primaryStage.show()
        mainWindow.harbour.load(VectorialSettings.preferences)

        primaryStage.onClosed {
            with(VectorialSettings) {
                primaryStage.scene?.let { scene ->
                    sceneWidth = scene.width
                    sceneHeight = scene.height
                }
                save()
            }
            mainWindow.harbour.save(VectorialSettings.preferences)
        }

    }

    companion object {

        @JvmStatic
        fun main(vararg args: String) {
            launch(Vectorial::class.java, args)
        }

        // lazy, because backend isn't set yet.
        val icons by lazy {
            icons(backend.resources("uk/co/nickthecoder/vectorial")) {
                texture("vectorialIcons.png", 64) {
                    row(
                        "mode_", 2, 2, 4,
                        "select", "rectangle", "ellipse", "polygon", "path", "line", "curve"
                    )
                    row(
                        "draw_", 2, 70, 4,
                        "ellipse_whole", "ellipse_arc", "ellipse_chord", "ellipse_slice",
                        "rectangle", "parallelogram", "diamond", "polygon", "star"
                    )
                    row(
                        2, 2 + 68 * 2, 4,
                        "edit_duplicate", "edit_copy", "edit_undo", "edit_select_all", "edit_select_none", "edit_redo"
                    )
                    row(
                        2, 2 + 68 * 3, 4,
                        "layer_bottom", "layer_lower", "layer_raise", "layer_top", "layer_delete", "layer_new",
                        "draw_centered", "draw_corner_based", "break_connection"
                    )
                    row(
                        2, 2 + 68 * 4, 4,
                        "selection_group", "selection_ungroup", "visible", "hidden", "locked", "unlocked"
                    )
                    row(
                        2, 2 + 68 * 5, 4,
                        "zoom_in", "zoom_out", "zoom_reset"
                    )
                    row(
                        2, 2 + 68 * 6, 4,
                        "refresh", "file_settings", "dock_layers", "dock_debug", "dock_node_inspector"
                    )
                }
            }
        }

        val resizableIcons by lazy {
            icons.resizableIcons(VectorialSettings.defaultIconSizeProperty)
        }
    }

}
