/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.GlokSettings
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.ifElse

import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.vectorial.VectorialSettings.load
import uk.co.nickthecoder.vectorial.VectorialSettings.save
import uk.co.nickthecoder.vectorial.boilerplate.*
import uk.co.nickthecoder.vectorial.boilerplate.ColorProperty
import uk.co.nickthecoder.vectorial.control.DiagramView
import uk.co.nickthecoder.vectorial.core.Color
import uk.co.nickthecoder.vectorial.core.Colors
import uk.co.nickthecoder.vectorial.core.shape.Centered
import uk.co.nickthecoder.vectorial.core.shape.EllipseType
import uk.co.nickthecoder.vectorial.core.shape.PolygonType
import uk.co.nickthecoder.vectorial.core.shape.QuadrilateralType
import java.util.prefs.Preferences
import uk.co.nickthecoder.glok.scene.Color as GlokColor

/**
 * Various preferences stored as JavaFX Properties.
 *
 * [load] and [save] uses Java's [Preferences].
 *
 * On Windows, that may mean the registry???
 *
 * On linux, they are stored in `~/.java/.userPrefs/uk/co/nickthecoder/vectorial/prefs.xml`
 * If you wish to reset, just close the application, then delete the file before restarting.
 */
object VectorialSettings : AbstractSettings(Preferences.userNodeForPackage(Vectorial::class.java)) {

    // region ========= RENDERING ==========

    /**
     * See [DiagramView.multiSample]
     */
    val multiSampleProperty by intProperty(4)
    var multiSample by multiSampleProperty

    /**
     * Is [Application.globalScale] used when rendering [DiagramView].
     *
     */
    val highDPIAwareProperty by booleanProperty(false)
    var highDPIAware by highDPIAwareProperty

    val diagramScaleProperty = highDPIAwareProperty.ifElse(GlokSettings.globalScale,1f).apply {
        addDependent(GlokSettings.globalScaleProperty)
    }
    val diagramScale by diagramScaleProperty

    // endregion

    // region ========= COLORS =========

    /**
     * See [DiagramView.handleBorderColorProperty]
     */
    val handleBorderColorProperty by colorProperty( Colors["#000"] )
    var handleBorderColor by handleBorderColorProperty

    /**
     * See [DiagramView.handleColorProperty]
     */
    val handleColorProperty by colorProperty( Colors["#fff"])
    var handleColor by  handleColorProperty 

    /**
     * See [DiagramView.fixedHandleColorProperty]
     */
    val fixedHandleColorProperty by colorProperty( Colors["#00d"])
    var fixedHandleColor by fixedHandleColorProperty

    /**
     * See [DiagramView.selectedHandleColorProperty]
     */
    val selectedHandleColorProperty by colorProperty( Colors["#c00"])
    var selectedHandleColor by selectedHandleColorProperty

    /**
     * See [DiagramView.selectionColorProperty]
     */
    val selectionColorProperty by colorProperty( Colors["#88d"])
    var selectionColor by selectedHandleColorProperty

    /**
     * See [DiagramView.pageBorderColorProperty]
     */
    val borderColorProperty by colorProperty( Colors["#888"])
    var borderColor by borderColorProperty

    /**
     * See [DiagramView.diagramBackgroundColorProperty]
     */
    val backgroundColorProperty by colorProperty( Colors["#ddd"])
    var backgroundColor by backgroundColorProperty

    /**
     * See [DiagramView.paperColorProperty]
     */
    val paperColorProperty by colorProperty( Colors["#fff"])
    var paperColor by paperColorProperty

    // endregion

    // region ========== SIZES ==========

    /**
     * See [DiagramView.selectionLineThicknessProperty]
     */
    val selectionLineThicknessProperty by floatProperty( 1f)
    var selectionLineThickness by selectionLineThicknessProperty

    /**
     * See [DiagramView.constructionLineThicknessProperty]
     */
    val constructionLineThicknessProperty by floatProperty( 1f)
    var constructionLineThickness by  constructionLineThicknessProperty

    /**
     * See [DiagramView.handleSizeProperty]
     */
    val handleSizeProperty by floatProperty( 6f)
    var handleSize by handleSizeProperty

    // endregion

    // region ========= THRESHOLDS ETC =========

    /**
     * See [DiagramView.lineThresholdProperty]
     */
    val lineThresholdProperty by floatProperty( 4f)
    var lineThreshold by lineThresholdProperty

    /**
     * See [DiagramView.lineThresholdProperty]
     */
    val handleThresholdProperty by floatProperty( 10f)
    var handleThreshold by handleThresholdProperty

    /**
     * The maximum number of items in the Undo/Redo menu.
     */
    val maxHistoryMenuItemsProperty by intProperty( 30)
    var maxHistoryMenuItems by maxHistoryMenuItemsProperty

    // endregion

    // region ========= FEATURES =========

    /**
     * Should changes to the selection be skipped over when doing undo/redo?
     */
    val skipSelectionChangesProperty by booleanProperty( false)
    var skipSelectionChanges by  skipSelectionChangesProperty

    /**
     * Should changes to the selection be shown in the undo/redo menus?
     */
    val showSelectionChangesProperty by booleanProperty( false)
    var showSelectionChanges by showSelectionChangesProperty

    /**
     * Should there be only 1 big bounding box when multiple shapes are selected?
     */
    val combineBoundingBoxesProperty by booleanProperty( true)
    var combineBoundingBoxes by combineBoundingBoxesProperty

    // endregion

    // region ========== PREFERRED VALUES / STATES =========
    /**
     * The number of sides when creating a new Polygon / Star
     */
    val polygonSidesProperty by intProperty( 5)
    var polygonSides by polygonSidesProperty

    /**
     * Should the polygon mode draw polygons or stars?
     */
    val polygonTypeProperty by polygonTypeProperty(PolygonType.POLYGON)
    var polygonType by polygonTypeProperty

    /**
     * Which type of quadrilateral to draw when using the "rectangle" mode?
     */
    val quadrilateralTypeProperty by quadrilateralTypeProperty( QuadrilateralType.RECTANGLE)
    var quadrilateralType by quadrilateralTypeProperty

    /**
     * Which type of ellipse to draw when using the "ellipse" mode?
     */
    val ellipseTypeProperty by ellipseTypeProperty(EllipseType.WHOLE)
    var ellipseType by ellipseTypeProperty

    /**
     * Rectangles / Parallelograms / Diamonds - Define them based of their `corner` and `size`?
     * The alternative is to be defined by two opposite corners.
     */
    val centeredProperty by centeredProperty(Centered.CENTERED)
    var centered by centeredProperty

    // endregion

    // region ========== GUI PREFERENCES ==========

    val darkThemeProperty by booleanProperty(false)
    var darkTheme by darkThemeProperty

    val fontSizeProperty by floatProperty(14f)
    var fontSize by fontSizeProperty

    val darkAccentColorProperty by colorProperty(GlokColor.WHITE)
    var darkAccentColor by darkAccentColorProperty

    val lightAccentColorProperty by colorProperty(GlokColor.WHITE)
    var lightAaccentColor by lightAccentColorProperty

    val showMenuBarProperty by booleanProperty(true)
    var showMenuBar by showMenuBarProperty

    val showFixedToolBarProperty by booleanProperty(true)
    var showFixedToolBar by showFixedToolBarProperty

    val showStatusBarProperty by booleanProperty(true)
    var showStatusBar by showStatusBarProperty

    val sceneWidthProperty by floatProperty( 1000f)
    var sceneWidth by sceneWidthProperty

    val sceneHeightProperty by floatProperty( 800f)
    var sceneHeight by sceneHeightProperty

    val maximisedProperty by booleanProperty( false)
    var maximised by maximisedProperty

    val defaultIconSizeProperty by intProperty(32)
    val defaultIconSize by  defaultIconSizeProperty 
    val defaultIcons = Vectorial.icons.resizableIcons(defaultIconSizeProperty)

    val toolIconSizeProperty by intProperty(48)
    val toolIconSize by  toolIconSizeProperty 
    val toolIcons = Vectorial.icons.resizableIcons(toolIconSizeProperty)

    val secondaryIconSizeProperty by intProperty(24)
    val secondaryIconSize by  secondaryIconSizeProperty 
    val secondaryIcons = Vectorial.icons.resizableIcons(secondaryIconSizeProperty)

    val menuIconSizeProperty by intProperty(24)
    val menuIconSize by  menuIconSizeProperty 
    val menuIcons = Vectorial.icons.resizableIcons(menuIconSizeProperty)

    // endregion

    // ========== END ==========

    init {
        lightAccentColorProperty.bidirectionalBind(Tantalum.lightColorScheme["accent"] !!)
        darkAccentColorProperty.bidirectionalBind(Tantalum.darkColorScheme["accent"] !!)
        fontSizeProperty.bidirectionalBind(Tantalum.fontSizeProperty)
    }

    // region ==== Methods ====

    // endregion Methods

    init {
        rememberDefaultValues()
        autoSaveChanged()
    }

    override fun autoSaveIgnoredProperties(): Set<Property<*>> = setOf(
        sceneWidthProperty, sceneHeightProperty, maximisedProperty,
        polygonSidesProperty, polygonTypeProperty, quadrilateralTypeProperty, centeredProperty, ellipseTypeProperty
    )

    override fun saveProperty(preferences: Preferences, name: String?, value: Any?) {
        when (value) {
            is Color -> preferences.put(name, value.toHashRGBA())

            else -> super.saveProperty(preferences, name, value)

        }
    }

    override fun loadProperty(property: Property<*>) {
        val name = property.beanName

        when (property) {
            is ColorProperty -> property.value = Colors[preferences.get(name, property.value.toHashRGBA())]

            is CenteredProperty -> property.value = Centered.valueOf(preferences.get(name, property.value.name))
            is PolygonTypeProperty -> property.value = PolygonType.valueOf(preferences.get(name, property.value.name))
            is EllipseTypeProperty -> property.value = EllipseType.valueOf(preferences.get(name, property.value.name))
            is QuadrilateralTypeProperty -> property.value =
                QuadrilateralType.valueOf(preferences.get(name, property.value.name))

            else -> super.loadProperty(property)
        }
    }
}
