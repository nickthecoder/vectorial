/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.control.ContentDisplay
import uk.co.nickthecoder.glok.control.FormGrid
import uk.co.nickthecoder.glok.control.HBox
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.dialog.Dialog
import uk.co.nickthecoder.glok.property.Property
import uk.co.nickthecoder.glok.property.functions.and
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.styles.TINTED
import uk.co.nickthecoder.vectorial.util.ColorConvertor

class SettingsDialog : Dialog() {

    init {
        content = buildContent()
        title = "Settings"
        buttonBar.visible = false
    }

    private fun buildContent() = singleContainer {
        overridePrefWidth = 800f
        overridePrefHeight = 600f
        style("SettingsDialog")
        content = tabPane {
            side = Side.LEFT

            + tab("Look and Feel") {
                content = settingsForm { lookAndFeel() }
            }
            + tab("Colors") {
                content = settingsForm { colors() }
            }
            + tab("Sizes") {
                content = settingsForm { sizes() }
            }
            + tab( "Thresholds" ) {
                content = settingsForm { thresholds() }
            }
            + tab( "Features" ) {
                content = settingsForm { features() }
            }
            + tab( "Rendering" ) {
                content = settingsForm { rendering() }
            }
        }
    }

    private fun settingsForm(block: FormGrid.() -> Unit) = scrollPane {
        content = formGrid {
            style(".settings_form")
            block()
        }
    }


    private fun FormGrid.lookAndFeel() {

        + row( "Font Size" ) {
            right = floatSpinner {
                editor.prefColumnCount = 4
                valueProperty.bidirectionalBind(VectorialSettings.fontSizeProperty)
            }.withResetSettings(VectorialSettings.fontSizeProperty)
        }

        + row( "Default Icon Size" ) {
            right = intSpinner {
                editor.prefColumnCount = 3
                valueProperty.bidirectionalBind(VectorialSettings.defaultIconSizeProperty)
            }.withResetSettings(VectorialSettings.defaultIconSizeProperty)
            below = label( "(toolbar buttons)") { style(".information" ) }
        }
        + row( "Tool Icon Size" ) {
            right = intSpinner {
                editor.prefColumnCount = 3
                valueProperty.bidirectionalBind(VectorialSettings.defaultIconSizeProperty)
            }.withResetSettings(VectorialSettings.defaultIconSizeProperty)
            below = label( "(tool buttons to the left of the diagram)") { style(".information" ) }
        }
        + row( "Secondary Icon Size" ) {
            right = intSpinner {
                editor.prefColumnCount = 3
                valueProperty.bidirectionalBind(VectorialSettings.secondaryIconSizeProperty)
            }.withResetSettings(VectorialSettings.secondaryIconSizeProperty)
            below = label( "(menu items, docks etc)") { style(".information" ) }
        }

        + row("Show Menu Bar") {
            right = checkBox { selectedProperty.bidirectionalBind(VectorialSettings.showMenuBarProperty) }
                .withResetSettings(VectorialSettings.showMenuBarProperty)
        }
        + row("Show Fixed Toolbar") {
            right = checkBox { selectedProperty.bidirectionalBind(VectorialSettings.showFixedToolBarProperty) }
                .withResetSettings(VectorialSettings.showFixedToolBarProperty)
        }
        + row("Show Status Bar") {
            right = checkBox { selectedProperty.bidirectionalBind(VectorialSettings.showStatusBarProperty) }
                .withResetSettings(VectorialSettings.showStatusBarProperty)
        }
        + row("Dark Theme") {
            right = checkBox { selectedProperty.bidirectionalBind(VectorialSettings.darkThemeProperty) }
                .withResetSettings(VectorialSettings.darkThemeProperty)
        }

        + row("Accent Color") {
            visibleProperty.bindTo(! VectorialSettings.darkThemeProperty)
            right = colorButton {
                title = "Accent Color"
                colorProperty.bidirectionalBind(VectorialSettings.lightAccentColorProperty)
            }.withResetSettings(VectorialSettings.lightAccentColorProperty)
        }
        + row("Accent Color") {
            visibleProperty.bindTo(VectorialSettings.darkThemeProperty)
            right = colorButton {
                title = "Accent Color"
                colorProperty.bidirectionalBind(VectorialSettings.darkAccentColorProperty)
            }.withResetSettings(VectorialSettings.darkAccentColorProperty)
        }

    }

    private fun FormGrid.colors() {
        + row( "Handle Border Color" ) {
            right = colorButton {
                title = "Handle Border Color"
                colorProperty.bidirectionalBind(VectorialSettings.handleBorderColorProperty, ColorConvertor)
            }.withResetSettings(VectorialSettings.handleBorderColorProperty)
        }
        + row( "Handle Color" ) {
            right = colorButton {
                title = "Handle Color"
                colorProperty.bidirectionalBind(VectorialSettings.handleColorProperty, ColorConvertor)
            }.withResetSettings(VectorialSettings.handleColorProperty)
        }
        + row( "Fixed Handle Color" ) {
            right = colorButton {
                title = "Fixed Handle Color"
                colorProperty.bidirectionalBind(VectorialSettings.fixedHandleColorProperty, ColorConvertor)
            }.withResetSettings(VectorialSettings.fixedHandleColorProperty)
        }
        + row( "Selected Handle Color" ) {
            right = colorButton {
                title = "Selected Handle Color"
                colorProperty.bidirectionalBind(VectorialSettings.selectedHandleColorProperty, ColorConvertor)
            }.withResetSettings(VectorialSettings.selectedHandleColorProperty)
        }
        + row( "Border Color" ) {
            right = colorButton {
                title = "Border Color"
                colorProperty.bidirectionalBind(VectorialSettings.borderColorProperty, ColorConvertor)
            }.withResetSettings(VectorialSettings.borderColorProperty)
        }
        + row( "Background Color" ) {
            right = colorButton {
                title = "Border Color"
                colorProperty.bidirectionalBind(VectorialSettings.backgroundColorProperty, ColorConvertor)
            }.withResetSettings(VectorialSettings.backgroundColorProperty)
        }
        + row( "Paper Color" ) {
            right = colorButton {
                title = "Border Color"
                colorProperty.bidirectionalBind(VectorialSettings.paperColorProperty, ColorConvertor)
            }.withResetSettings(VectorialSettings.paperColorProperty)
        }
    }

    private fun FormGrid.sizes() {
        + row( "Selection Line Thickness" ) {
            right = floatSpinner {
                editor.prefColumnCount = 3
                valueProperty.bidirectionalBind(VectorialSettings.selectionLineThicknessProperty)
            }.withResetSettings(VectorialSettings.selectionLineThicknessProperty)
        }
        + row( "Construction Line Thickness" ) {
            right = floatSpinner {
                editor.prefColumnCount = 3
                valueProperty.bidirectionalBind(VectorialSettings.constructionLineThicknessProperty)
            }.withResetSettings(VectorialSettings.constructionLineThicknessProperty)
        }
        + row( "Handle Size" ) {
            right = floatSpinner {
                editor.prefColumnCount = 3
                valueProperty.bidirectionalBind(VectorialSettings.handleSizeProperty)
            }.withResetSettings(VectorialSettings.handleSizeProperty)
        }
        + row( "Selection Line Thickness" ) {
            right = floatSpinner {
                editor.prefColumnCount = 3
                valueProperty.bidirectionalBind(VectorialSettings.selectionLineThicknessProperty)
            }.withResetSettings(VectorialSettings.selectionLineThicknessProperty)
        }
    }

    private fun FormGrid.thresholds() {
        + row( "Line Threshold" ) {
            right = floatSpinner {
                editor.prefColumnCount = 3
                valueProperty.bidirectionalBind(VectorialSettings.lineThresholdProperty)
            }.withResetSettings(VectorialSettings.lineThresholdProperty)
        }
        + row( "Control Point Threshold" ) {
            right = floatSpinner {
                editor.prefColumnCount = 3
                valueProperty.bidirectionalBind(VectorialSettings.handleThresholdProperty)
            }.withResetSettings(VectorialSettings.handleThresholdProperty)
        }
    }

    private fun FormGrid.features() {
        + row( "Skip Selection Changes" ) {
            right = checkBox {
                selectedProperty.bidirectionalBind(VectorialSettings.skipSelectionChangesProperty)
            }.withResetSettings(VectorialSettings.skipSelectionChangesProperty)
        }
        + row( "Combine Bounding Boxes" ) {
            right = checkBox {
                selectedProperty.bidirectionalBind(VectorialSettings.combineBoundingBoxesProperty)
            }.withResetSettings(VectorialSettings.combineBoundingBoxesProperty)
        }
    }

    private fun FormGrid.rendering() {
        + row( "Multi-Sample" ) {
            right = intSpinner {
                editor.prefColumnCount = 3
                min = 1
                max = 6
                valueProperty.bidirectionalBind(VectorialSettings.multiSampleProperty)
            }.withResetSettings(VectorialSettings.multiSampleProperty)
        }
        + row( "High DPI Aware?" ) {
            right = checkBox {
                selectedProperty.bidirectionalBind(VectorialSettings.highDPIAwareProperty)
            }.withResetSettings(VectorialSettings.highDPIAwareProperty)
            below = Label( "(For low DPI monitors, this makes no difference)" )
        }
    }

    companion object {
        private var dialog : SettingsDialog? = null

        fun show( fromStage : Stage) {
            if (dialog != null) {
                dialog!!.stage.show()
            } else {
                dialog = SettingsDialog().apply {
                    createStage(fromStage) {
                        onClosed { dialog = null }
                        show()
                    }
                }
            }
        }
    }
}


private fun Node.withResetSettings(vararg properties: Property<*>) =
    withResetSettings(true, *properties)

private fun Node.withResetSettings(grow: Boolean, vararg properties: Property<*>, block: (() -> Unit)? = null): HBox {
    var disabled = VectorialSettings.hasDefaultValueProperty(properties.first())
    for (i in 1 until properties.size) {
        disabled = disabled and VectorialSettings.hasDefaultValueProperty(properties[i])
    }

    return hBox {
        if (grow) {
            growPriority = 1f
        }
        fillHeight = false
        alignment = Alignment.TOP_CENTER
        spacing(4)

        + this@withResetSettings
        + spacer()
        + button("Reset") {
            style(TINTED)
            disabledProperty.bindTo(disabled)
            graphic = imageView(Vectorial.resizableIcons.getResizable("refresh"))
            tooltip = TextTooltip("Reset to the default value")
            contentDisplay = ContentDisplay.GRAPHIC_ONLY
            onAction {
                for (property in properties) {
                    VectorialSettings.restoreProperty(property)
                }
                block?.invoke()
            }
        }
    }
}
