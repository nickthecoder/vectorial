package uk.co.nickthecoder.vectorial.boilerplate

import kotlin.reflect.KClass
import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.shape.*
import uk.co.nickthecoder.vectorial.control.*


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ObservableValue<Centered>`, we can simply use `ObservableCentered`.
 */
interface ObservableCentered: ObservableValue<Centered> {}

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ReadOnlyProperty<Centered>`, we can simply use `ReadOnlyCenteredProperty`.
 */
interface ReadOnlyCenteredProperty : ObservableCentered, ReadOnlyProperty<Centered>

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `Property<Centered>`, we can simply use `CenteredProperty`.
 */
interface CenteredProperty : Property<Centered>, ReadOnlyCenteredProperty {

    /**
     * Returns a read-only view of this mutable CenteredProperty.
     * Typical usage :
     *
     *     private val _fooProperty by centeredProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
     fun asReadOnly() : ReadOnlyCenteredProperty = ReadOnlyCenteredPropertyWrapper( this )
}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `SimpleProperty<Centered>`, we can use `SimpleCenteredProperty`.
 */
open class SimpleCenteredProperty(initialValue: Centered, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Centered>(initialValue, bean, beanName), CenteredProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleCenteredProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableCenteredProperty( initialValue: Centered, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<Centered>(initialValue, bean, beanName), CenteredProperty {
    override fun kclass() : KClass<*> = Centered::class
}

/**
 * Never use this class directly. Use [CenteredProperty.asReadOnly] to obtain a read-only version of a mutable [CenteredProperty].
 *
 * Boilerplate which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<Centered, Property<Centered>>`, we can simply use `ReadOnlyCenteredPropertyWrapper`.
 */
class ReadOnlyCenteredPropertyWrapper(wraps: CenteredProperty) : ReadOnlyPropertyWrapper<Centered, Property<Centered>>(wraps),
    ReadOnlyCenteredProperty


// Functions

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableCentered] which is calculated from 1 (unary) other [ObservableValue].
 *
 * See [UnaryFunction].
 */
class CenteredUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Centered) :
    UnaryFunction<Centered, A, OA>(argA, lambda), ObservableCentered

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableCentered] which is calculated from 2 (binary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class CenteredBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Centered) :
    BinaryFunction<Centered, A, OA, B, OB>(argA, argB, lambda), ObservableCentered

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableCentered] which is calculated from 3 (ternary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class CenteredTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Centered
) : TernaryFunction<Centered, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableCentered

// Delegate

/**
 * A Kotlin `delegate` to create a [CenteredProperty] (the implementation will be a [SimpleCenteredProperty].
 * Typical usage :
 *
 *     val fooProperty by centeredProperty( initialValue )
 *     var foo by myFooProperty
 *
 */
fun centeredProperty(initialValue: Centered) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleCenteredProperty(value, bean, name)
}

fun stylableCenteredProperty(initialValue: Centered) = PropertyDelegate<Centered,StylableCenteredProperty>(initialValue) { bean, name, value ->
    StylableCenteredProperty(value, bean, name)
}

// ============================
// Optional (nullable) versions
// ============================


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ObservableCentered], but the [value] can also be `null`.
 */
interface ObservableOptionalCentered : ObservableValue<Centered?> {
    fun defaultOf( defaultValue : Centered ) : ObservableCentered = CenteredUnaryFunction( this ) { a -> a ?: defaultValue }
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyCenteredProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalCenteredProperty : ObservableOptionalCentered, ReadOnlyProperty<Centered?>

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [CenteredProperty], but the [value] can also be `null`.
 */
interface OptionalCenteredProperty : Property<Centered?>, ReadOnlyOptionalCenteredProperty {

    /**
     * Returns a read-only view of this mutable OptionalCenteredProperty.
     * Typical usage :
     *
     *     private val _fooProperty by optionalCenteredProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalCenteredProperty = ReadOnlyOptionalCenteredPropertyWrapper( this )

}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [SimpleCenteredProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalCenteredProperty(initialValue: Centered?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Centered?>(initialValue, bean, beanName), OptionalCenteredProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleOptionalCenteredProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableOptionalCenteredProperty(initialValue: Centered?, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<Centered?>(initialValue, bean, beanName), OptionalCenteredProperty {
    override fun kclass() : KClass<*> = Centered::class
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyCenteredPropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalCenteredPropertyWrapper(wraps: OptionalCenteredProperty) :
    ReadOnlyPropertyWrapper<Centered?, Property<Centered?>>(wraps), ReadOnlyOptionalCenteredProperty

// Functions

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [CenteredUnaryFunction], but the [value] can also be `null`.
 */
class OptionalCenteredUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Centered?) :
    UnaryFunction<Centered?, A, OA>(argA, lambda), ObservableOptionalCentered

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [CenteredBinaryFunction], but the [value] can also be `null`.
 */
class OptionalCenteredBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Centered?) :
    BinaryFunction<Centered?, A, OA, B, OB>(argA, argB, lambda), ObservableOptionalCentered

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [CenteredTernaryFunction], but the [value] can also be `null`.
 */
class OptionalCenteredTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Centered?
) : TernaryFunction<Centered?, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableOptionalCentered

// Delegate

/**
 * A Kotlin `delegate` to create an [OptionalCenteredProperty] (the implementation will be a [SimpleOptionalCenteredProperty]).
 * Typical usage :
 *
 *     val fooProperty by optionalCenteredProperty( initialValue )
 *     var foo by  myFooProperty
 *
 */
fun optionalCenteredProperty(initialValue: Centered?) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleOptionalCenteredProperty(value, bean, name)
}


fun stylableOptionalCenteredProperty(initialValue: Centered?) = PropertyDelegate<Centered?,StylableOptionalCenteredProperty>(initialValue) { bean, name, value ->
    StylableOptionalCenteredProperty(value, bean, name)
}
