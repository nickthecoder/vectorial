package uk.co.nickthecoder.vectorial.boilerplate

import kotlin.reflect.KClass
import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.vectorial.control.*


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ObservableValue<ModeType>`, we can simply use `ObservableModeType`.
 */
interface ObservableModeType: ObservableValue<ToolType> {}

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ReadOnlyProperty<ModeType>`, we can simply use `ReadOnlyModeTypeProperty`.
 */
interface ReadOnlyModeTypeProperty : ObservableModeType, ReadOnlyProperty<ToolType>

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `Property<ModeType>`, we can simply use `ModeTypeProperty`.
 */
interface ModeTypeProperty : Property<ToolType>, ReadOnlyModeTypeProperty {

    /**
     * Returns a read-only view of this mutable ModeTypeProperty.
     * Typical usage :
     *
     *     private val _fooProperty by modeTypeProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
     fun asReadOnly() : ReadOnlyModeTypeProperty = ReadOnlyModeTypePropertyWrapper( this )
}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `SimpleProperty<ModeType>`, we can use `SimpleModeTypeProperty`.
 */
open class SimpleModeTypeProperty(initialValue: ToolType, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<ToolType>(initialValue, bean, beanName), ModeTypeProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleModeTypeProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableModeTypeProperty(initialValue: ToolType, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<ToolType>(initialValue, bean, beanName), ModeTypeProperty {
    override fun kclass() : KClass<*> = ToolType::class
}

/**
 * Never use this class directly. Use [ModeTypeProperty.asReadOnly] to obtain a read-only version of a mutable [ModeTypeProperty].
 *
 * Boilerplate which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<ModeType, Property<ModeType>>`, we can simply use `ReadOnlyModeTypePropertyWrapper`.
 */
class ReadOnlyModeTypePropertyWrapper(wraps: ModeTypeProperty) : ReadOnlyPropertyWrapper<ToolType, Property<ToolType>>(wraps),
    ReadOnlyModeTypeProperty


// Functions

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableModeType] which is calculated from 1 (unary) other [ObservableValue].
 *
 * See [UnaryFunction].
 */
class ModeTypeUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> ToolType) :
    UnaryFunction<ToolType, A, OA>(argA, lambda), ObservableModeType

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableModeType] which is calculated from 2 (binary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class ModeTypeBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> ToolType) :
    BinaryFunction<ToolType, A, OA, B, OB>(argA, argB, lambda), ObservableModeType

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableModeType] which is calculated from 3 (ternary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class ModeTypeTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> ToolType
) : TernaryFunction<ToolType, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableModeType

// Delegate

/**
 * A Kotlin `delegate` to create a [ModeTypeProperty] (the implementation will be a [SimpleModeTypeProperty].
 * Typical usage :
 *
 *     val fooProperty by modeTypeProperty( initialValue )
 *     var foo by myFooProperty
 *
 */
fun toolTypeProperty(initialValue: ToolType) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleModeTypeProperty(value, bean, name)
}

fun stylableModeTypeProperty(initialValue: ToolType) = PropertyDelegate<ToolType,StylableModeTypeProperty>(initialValue) { bean, name, value ->
    StylableModeTypeProperty(value, bean, name)
}

// ============================
// Optional (nullable) versions
// ============================


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ObservableModeType], but the [value] can also be `null`.
 */
interface ObservableOptionalModeType : ObservableValue<ToolType?> {
    fun defaultOf( defaultValue : ToolType ) : ObservableModeType = ModeTypeUnaryFunction( this ) { a -> a ?: defaultValue }
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyModeTypeProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalModeTypeProperty : ObservableOptionalModeType, ReadOnlyProperty<ToolType?>

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ModeTypeProperty], but the [value] can also be `null`.
 */
interface OptionalModeTypeProperty : Property<ToolType?>, ReadOnlyOptionalModeTypeProperty {

    /**
     * Returns a read-only view of this mutable OptionalModeTypeProperty.
     * Typical usage :
     *
     *     private val _fooProperty by optionalModeTypeProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalModeTypeProperty = ReadOnlyOptionalModeTypePropertyWrapper( this )

}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [SimpleModeTypeProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalModeTypeProperty(initialValue: ToolType?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<ToolType?>(initialValue, bean, beanName), OptionalModeTypeProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleOptionalModeTypeProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableOptionalModeTypeProperty(initialValue: ToolType?, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<ToolType?>(initialValue, bean, beanName), OptionalModeTypeProperty {
    override fun kclass() : KClass<*> = ToolType::class
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyModeTypePropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalModeTypePropertyWrapper(wraps: OptionalModeTypeProperty) :
    ReadOnlyPropertyWrapper<ToolType?, Property<ToolType?>>(wraps), ReadOnlyOptionalModeTypeProperty

// Functions

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ModeTypeUnaryFunction], but the [value] can also be `null`.
 */
class OptionalModeTypeUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> ToolType?) :
    UnaryFunction<ToolType?, A, OA>(argA, lambda), ObservableOptionalModeType

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ModeTypeBinaryFunction], but the [value] can also be `null`.
 */
class OptionalModeTypeBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> ToolType?) :
    BinaryFunction<ToolType?, A, OA, B, OB>(argA, argB, lambda), ObservableOptionalModeType

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ModeTypeTernaryFunction], but the [value] can also be `null`.
 */
class OptionalModeTypeTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> ToolType?
) : TernaryFunction<ToolType?, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableOptionalModeType

// Delegate

/**
 * A Kotlin `delegate` to create an [OptionalModeTypeProperty] (the implementation will be a [SimpleOptionalModeTypeProperty]).
 * Typical usage :
 *
 *     val fooProperty by optionalModeTypeProperty( initialValue )
 *     var foo by  myFooProperty
 *
 */
fun optionalModeTypeProperty(initialValue: ToolType?) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleOptionalModeTypeProperty(value, bean, name)
}


fun stylableOptionalModeTypeProperty(initialValue: ToolType?) = PropertyDelegate<ToolType?,StylableOptionalModeTypeProperty>(initialValue) { bean, name, value ->
    StylableOptionalModeTypeProperty(value, bean, name)
}
