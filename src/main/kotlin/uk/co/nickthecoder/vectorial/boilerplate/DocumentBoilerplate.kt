package uk.co.nickthecoder.vectorial.boilerplate

import kotlin.reflect.KClass
import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.shape.*
import uk.co.nickthecoder.vectorial.control.*


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ObservableValue<Document>`, we can simply use `ObservableDocument`.
 */
interface ObservableDocument: ObservableValue<Document> {}

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ReadOnlyProperty<Document>`, we can simply use `ReadOnlyDocumentProperty`.
 */
interface ReadOnlyDocumentProperty : ObservableDocument, ReadOnlyProperty<Document>

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `Property<Document>`, we can simply use `DocumentProperty`.
 */
interface DocumentProperty : Property<Document>, ReadOnlyDocumentProperty {

    /**
     * Returns a read-only view of this mutable DocumentProperty.
     * Typical usage :
     *
     *     private val _fooProperty by documentProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
     fun asReadOnly() : ReadOnlyDocumentProperty = ReadOnlyDocumentPropertyWrapper( this )
}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `SimpleProperty<Document>`, we can use `SimpleDocumentProperty`.
 */
open class SimpleDocumentProperty(initialValue: Document, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Document>(initialValue, bean, beanName), DocumentProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleDocumentProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableDocumentProperty( initialValue: Document, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<Document>(initialValue, bean, beanName), DocumentProperty {
    override fun kclass() : KClass<*> = Document::class
}

/**
 * Never use this class directly. Use [DocumentProperty.asReadOnly] to obtain a read-only version of a mutable [DocumentProperty].
 *
 * Boilerplate which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<Document, Property<Document>>`, we can simply use `ReadOnlyDocumentPropertyWrapper`.
 */
class ReadOnlyDocumentPropertyWrapper(wraps: DocumentProperty) : ReadOnlyPropertyWrapper<Document, Property<Document>>(wraps),
    ReadOnlyDocumentProperty


// Functions

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableDocument] which is calculated from 1 (unary) other [ObservableValue].
 *
 * See [UnaryFunction].
 */
class DocumentUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Document) :
    UnaryFunction<Document, A, OA>(argA, lambda), ObservableDocument

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableDocument] which is calculated from 2 (binary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class DocumentBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Document) :
    BinaryFunction<Document, A, OA, B, OB>(argA, argB, lambda), ObservableDocument

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableDocument] which is calculated from 3 (ternary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class DocumentTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Document
) : TernaryFunction<Document, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableDocument

// Delegate

/**
 * A Kotlin `delegate` to create a [DocumentProperty] (the implementation will be a [SimpleDocumentProperty].
 * Typical usage :
 *
 *     val fooProperty by documentProperty( initialValue )
 *     var foo by myFooProperty
 *
 */
fun documentProperty(initialValue: Document) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleDocumentProperty(value, bean, name)
}

fun stylableDocumentProperty(initialValue: Document) = PropertyDelegate<Document,StylableDocumentProperty>(initialValue) { bean, name, value ->
    StylableDocumentProperty(value, bean, name)
}

// ============================
// Optional (nullable) versions
// ============================


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ObservableDocument], but the [value] can also be `null`.
 */
interface ObservableOptionalDocument : ObservableValue<Document?> {
    fun defaultOf( defaultValue : Document ) : ObservableDocument = DocumentUnaryFunction( this ) { a -> a ?: defaultValue }
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyDocumentProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalDocumentProperty : ObservableOptionalDocument, ReadOnlyProperty<Document?>

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [DocumentProperty], but the [value] can also be `null`.
 */
interface OptionalDocumentProperty : Property<Document?>, ReadOnlyOptionalDocumentProperty {

    /**
     * Returns a read-only view of this mutable OptionalDocumentProperty.
     * Typical usage :
     *
     *     private val _fooProperty by optionalDocumentProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalDocumentProperty = ReadOnlyOptionalDocumentPropertyWrapper( this )

}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [SimpleDocumentProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalDocumentProperty(initialValue: Document?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Document?>(initialValue, bean, beanName), OptionalDocumentProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleOptionalDocumentProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableOptionalDocumentProperty(initialValue: Document?, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<Document?>(initialValue, bean, beanName), OptionalDocumentProperty {
    override fun kclass() : KClass<*> = Document::class
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyDocumentPropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalDocumentPropertyWrapper(wraps: OptionalDocumentProperty) :
    ReadOnlyPropertyWrapper<Document?, Property<Document?>>(wraps), ReadOnlyOptionalDocumentProperty

// Functions

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [DocumentUnaryFunction], but the [value] can also be `null`.
 */
class OptionalDocumentUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Document?) :
    UnaryFunction<Document?, A, OA>(argA, lambda), ObservableOptionalDocument

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [DocumentBinaryFunction], but the [value] can also be `null`.
 */
class OptionalDocumentBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Document?) :
    BinaryFunction<Document?, A, OA, B, OB>(argA, argB, lambda), ObservableOptionalDocument

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [DocumentTernaryFunction], but the [value] can also be `null`.
 */
class OptionalDocumentTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Document?
) : TernaryFunction<Document?, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableOptionalDocument

// Delegate

/**
 * A Kotlin `delegate` to create an [OptionalDocumentProperty] (the implementation will be a [SimpleOptionalDocumentProperty]).
 * Typical usage :
 *
 *     val fooProperty by optionalDocumentProperty( initialValue )
 *     var foo by  myFooProperty
 *
 */
fun optionalDocumentProperty(initialValue: Document?) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleOptionalDocumentProperty(value, bean, name)
}


fun stylableOptionalDocumentProperty(initialValue: Document?) = PropertyDelegate<Document?,StylableOptionalDocumentProperty>(initialValue) { bean, name, value ->
    StylableOptionalDocumentProperty(value, bean, name)
}
