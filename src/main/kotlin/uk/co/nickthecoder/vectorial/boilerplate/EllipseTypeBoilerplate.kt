package uk.co.nickthecoder.vectorial.boilerplate

import kotlin.reflect.KClass
import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.shape.*
import uk.co.nickthecoder.vectorial.control.*


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ObservableValue<EllipseType>`, we can simply use `ObservableEllipseType`.
 */
interface ObservableEllipseType: ObservableValue<EllipseType> {}

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ReadOnlyProperty<EllipseType>`, we can simply use `ReadOnlyEllipseTypeProperty`.
 */
interface ReadOnlyEllipseTypeProperty : ObservableEllipseType, ReadOnlyProperty<EllipseType>

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `Property<EllipseType>`, we can simply use `EllipseTypeProperty`.
 */
interface EllipseTypeProperty : Property<EllipseType>, ReadOnlyEllipseTypeProperty {

    /**
     * Returns a read-only view of this mutable EllipseTypeProperty.
     * Typical usage :
     *
     *     private val _fooProperty by ellipseTypeProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
     fun asReadOnly() : ReadOnlyEllipseTypeProperty = ReadOnlyEllipseTypePropertyWrapper( this )
}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `SimpleProperty<EllipseType>`, we can use `SimpleEllipseTypeProperty`.
 */
open class SimpleEllipseTypeProperty(initialValue: EllipseType, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<EllipseType>(initialValue, bean, beanName), EllipseTypeProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleEllipseTypeProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableEllipseTypeProperty( initialValue: EllipseType, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<EllipseType>(initialValue, bean, beanName), EllipseTypeProperty {
    override fun kclass() : KClass<*> = EllipseType::class
}

/**
 * Never use this class directly. Use [EllipseTypeProperty.asReadOnly] to obtain a read-only version of a mutable [EllipseTypeProperty].
 *
 * Boilerplate which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<EllipseType, Property<EllipseType>>`, we can simply use `ReadOnlyEllipseTypePropertyWrapper`.
 */
class ReadOnlyEllipseTypePropertyWrapper(wraps: EllipseTypeProperty) : ReadOnlyPropertyWrapper<EllipseType, Property<EllipseType>>(wraps),
    ReadOnlyEllipseTypeProperty


// Functions

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableEllipseType] which is calculated from 1 (unary) other [ObservableValue].
 *
 * See [UnaryFunction].
 */
class EllipseTypeUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> EllipseType) :
    UnaryFunction<EllipseType, A, OA>(argA, lambda), ObservableEllipseType

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableEllipseType] which is calculated from 2 (binary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class EllipseTypeBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> EllipseType) :
    BinaryFunction<EllipseType, A, OA, B, OB>(argA, argB, lambda), ObservableEllipseType

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableEllipseType] which is calculated from 3 (ternary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class EllipseTypeTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> EllipseType
) : TernaryFunction<EllipseType, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableEllipseType

// Delegate

/**
 * A Kotlin `delegate` to create a [EllipseTypeProperty] (the implementation will be a [SimpleEllipseTypeProperty].
 * Typical usage :
 *
 *     val fooProperty by ellipseTypeProperty( initialValue )
 *     var foo by myFooProperty
 *
 */
fun ellipseTypeProperty(initialValue: EllipseType) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleEllipseTypeProperty(value, bean, name)
}

fun stylableEllipseTypeProperty(initialValue: EllipseType) = PropertyDelegate<EllipseType,StylableEllipseTypeProperty>(initialValue) { bean, name, value ->
    StylableEllipseTypeProperty(value, bean, name)
}

// ============================
// Optional (nullable) versions
// ============================


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ObservableEllipseType], but the [value] can also be `null`.
 */
interface ObservableOptionalEllipseType : ObservableValue<EllipseType?> {
    fun defaultOf( defaultValue : EllipseType ) : ObservableEllipseType = EllipseTypeUnaryFunction( this ) { a -> a ?: defaultValue }
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyEllipseTypeProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalEllipseTypeProperty : ObservableOptionalEllipseType, ReadOnlyProperty<EllipseType?>

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [EllipseTypeProperty], but the [value] can also be `null`.
 */
interface OptionalEllipseTypeProperty : Property<EllipseType?>, ReadOnlyOptionalEllipseTypeProperty {

    /**
     * Returns a read-only view of this mutable OptionalEllipseTypeProperty.
     * Typical usage :
     *
     *     private val _fooProperty by optionalEllipseTypeProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalEllipseTypeProperty = ReadOnlyOptionalEllipseTypePropertyWrapper( this )

}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [SimpleEllipseTypeProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalEllipseTypeProperty(initialValue: EllipseType?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<EllipseType?>(initialValue, bean, beanName), OptionalEllipseTypeProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleOptionalEllipseTypeProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableOptionalEllipseTypeProperty(initialValue: EllipseType?, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<EllipseType?>(initialValue, bean, beanName), OptionalEllipseTypeProperty {
    override fun kclass() : KClass<*> = EllipseType::class
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyEllipseTypePropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalEllipseTypePropertyWrapper(wraps: OptionalEllipseTypeProperty) :
    ReadOnlyPropertyWrapper<EllipseType?, Property<EllipseType?>>(wraps), ReadOnlyOptionalEllipseTypeProperty

// Functions

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [EllipseTypeUnaryFunction], but the [value] can also be `null`.
 */
class OptionalEllipseTypeUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> EllipseType?) :
    UnaryFunction<EllipseType?, A, OA>(argA, lambda), ObservableOptionalEllipseType

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [EllipseTypeBinaryFunction], but the [value] can also be `null`.
 */
class OptionalEllipseTypeBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> EllipseType?) :
    BinaryFunction<EllipseType?, A, OA, B, OB>(argA, argB, lambda), ObservableOptionalEllipseType

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [EllipseTypeTernaryFunction], but the [value] can also be `null`.
 */
class OptionalEllipseTypeTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> EllipseType?
) : TernaryFunction<EllipseType?, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableOptionalEllipseType

// Delegate

/**
 * A Kotlin `delegate` to create an [OptionalEllipseTypeProperty] (the implementation will be a [SimpleOptionalEllipseTypeProperty]).
 * Typical usage :
 *
 *     val fooProperty by optionalEllipseTypeProperty( initialValue )
 *     var foo by  myFooProperty
 *
 */
fun optionalEllipseTypeProperty(initialValue: EllipseType?) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleOptionalEllipseTypeProperty(value, bean, name)
}


fun stylableOptionalEllipseTypeProperty(initialValue: EllipseType?) = PropertyDelegate<EllipseType?,StylableOptionalEllipseTypeProperty>(initialValue) { bean, name, value ->
    StylableOptionalEllipseTypeProperty(value, bean, name)
}
