package uk.co.nickthecoder.vectorial.boilerplate

import kotlin.reflect.KClass
import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.shape.*
import uk.co.nickthecoder.vectorial.control.*


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ObservableValue<QuadrilateralType>`, we can simply use `ObservableQuadrilateralType`.
 */
interface ObservableQuadrilateralType: ObservableValue<QuadrilateralType> {}

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ReadOnlyProperty<QuadrilateralType>`, we can simply use `ReadOnlyQuadrilateralTypeProperty`.
 */
interface ReadOnlyQuadrilateralTypeProperty : ObservableQuadrilateralType, ReadOnlyProperty<QuadrilateralType>

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `Property<QuadrilateralType>`, we can simply use `QuadrilateralTypeProperty`.
 */
interface QuadrilateralTypeProperty : Property<QuadrilateralType>, ReadOnlyQuadrilateralTypeProperty {

    /**
     * Returns a read-only view of this mutable QuadrilateralTypeProperty.
     * Typical usage :
     *
     *     private val _fooProperty by quadrilateralTypeProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
     fun asReadOnly() : ReadOnlyQuadrilateralTypeProperty = ReadOnlyQuadrilateralTypePropertyWrapper( this )
}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `SimpleProperty<QuadrilateralType>`, we can use `SimpleQuadrilateralTypeProperty`.
 */
open class SimpleQuadrilateralTypeProperty(initialValue: QuadrilateralType, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<QuadrilateralType>(initialValue, bean, beanName), QuadrilateralTypeProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleQuadrilateralTypeProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableQuadrilateralTypeProperty( initialValue: QuadrilateralType, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<QuadrilateralType>(initialValue, bean, beanName), QuadrilateralTypeProperty {
    override fun kclass() : KClass<*> = QuadrilateralType::class
}

/**
 * Never use this class directly. Use [QuadrilateralTypeProperty.asReadOnly] to obtain a read-only version of a mutable [QuadrilateralTypeProperty].
 *
 * Boilerplate which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<QuadrilateralType, Property<QuadrilateralType>>`, we can simply use `ReadOnlyQuadrilateralTypePropertyWrapper`.
 */
class ReadOnlyQuadrilateralTypePropertyWrapper(wraps: QuadrilateralTypeProperty) : ReadOnlyPropertyWrapper<QuadrilateralType, Property<QuadrilateralType>>(wraps),
    ReadOnlyQuadrilateralTypeProperty


// Functions

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableQuadrilateralType] which is calculated from 1 (unary) other [ObservableValue].
 *
 * See [UnaryFunction].
 */
class QuadrilateralTypeUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> QuadrilateralType) :
    UnaryFunction<QuadrilateralType, A, OA>(argA, lambda), ObservableQuadrilateralType

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableQuadrilateralType] which is calculated from 2 (binary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class QuadrilateralTypeBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> QuadrilateralType) :
    BinaryFunction<QuadrilateralType, A, OA, B, OB>(argA, argB, lambda), ObservableQuadrilateralType

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableQuadrilateralType] which is calculated from 3 (ternary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class QuadrilateralTypeTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> QuadrilateralType
) : TernaryFunction<QuadrilateralType, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableQuadrilateralType

// Delegate

/**
 * A Kotlin `delegate` to create a [QuadrilateralTypeProperty] (the implementation will be a [SimpleQuadrilateralTypeProperty].
 * Typical usage :
 *
 *     val fooProperty by quadrilateralTypeProperty( initialValue )
 *     var foo by myFooProperty
 *
 */
fun quadrilateralTypeProperty(initialValue: QuadrilateralType) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleQuadrilateralTypeProperty(value, bean, name)
}

fun stylableQuadrilateralTypeProperty(initialValue: QuadrilateralType) = PropertyDelegate<QuadrilateralType,StylableQuadrilateralTypeProperty>(initialValue) { bean, name, value ->
    StylableQuadrilateralTypeProperty(value, bean, name)
}

// ============================
// Optional (nullable) versions
// ============================


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ObservableQuadrilateralType], but the [value] can also be `null`.
 */
interface ObservableOptionalQuadrilateralType : ObservableValue<QuadrilateralType?> {
    fun defaultOf( defaultValue : QuadrilateralType ) : ObservableQuadrilateralType = QuadrilateralTypeUnaryFunction( this ) { a -> a ?: defaultValue }
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyQuadrilateralTypeProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalQuadrilateralTypeProperty : ObservableOptionalQuadrilateralType, ReadOnlyProperty<QuadrilateralType?>

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [QuadrilateralTypeProperty], but the [value] can also be `null`.
 */
interface OptionalQuadrilateralTypeProperty : Property<QuadrilateralType?>, ReadOnlyOptionalQuadrilateralTypeProperty {

    /**
     * Returns a read-only view of this mutable OptionalQuadrilateralTypeProperty.
     * Typical usage :
     *
     *     private val _fooProperty by optionalQuadrilateralTypeProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalQuadrilateralTypeProperty = ReadOnlyOptionalQuadrilateralTypePropertyWrapper( this )

}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [SimpleQuadrilateralTypeProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalQuadrilateralTypeProperty(initialValue: QuadrilateralType?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<QuadrilateralType?>(initialValue, bean, beanName), OptionalQuadrilateralTypeProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleOptionalQuadrilateralTypeProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableOptionalQuadrilateralTypeProperty(initialValue: QuadrilateralType?, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<QuadrilateralType?>(initialValue, bean, beanName), OptionalQuadrilateralTypeProperty {
    override fun kclass() : KClass<*> = QuadrilateralType::class
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyQuadrilateralTypePropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalQuadrilateralTypePropertyWrapper(wraps: OptionalQuadrilateralTypeProperty) :
    ReadOnlyPropertyWrapper<QuadrilateralType?, Property<QuadrilateralType?>>(wraps), ReadOnlyOptionalQuadrilateralTypeProperty

// Functions

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [QuadrilateralTypeUnaryFunction], but the [value] can also be `null`.
 */
class OptionalQuadrilateralTypeUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> QuadrilateralType?) :
    UnaryFunction<QuadrilateralType?, A, OA>(argA, lambda), ObservableOptionalQuadrilateralType

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [QuadrilateralTypeBinaryFunction], but the [value] can also be `null`.
 */
class OptionalQuadrilateralTypeBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> QuadrilateralType?) :
    BinaryFunction<QuadrilateralType?, A, OA, B, OB>(argA, argB, lambda), ObservableOptionalQuadrilateralType

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [QuadrilateralTypeTernaryFunction], but the [value] can also be `null`.
 */
class OptionalQuadrilateralTypeTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> QuadrilateralType?
) : TernaryFunction<QuadrilateralType?, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableOptionalQuadrilateralType

// Delegate

/**
 * A Kotlin `delegate` to create an [OptionalQuadrilateralTypeProperty] (the implementation will be a [SimpleOptionalQuadrilateralTypeProperty]).
 * Typical usage :
 *
 *     val fooProperty by optionalQuadrilateralTypeProperty( initialValue )
 *     var foo by  myFooProperty
 *
 */
fun optionalQuadrilateralTypeProperty(initialValue: QuadrilateralType?) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleOptionalQuadrilateralTypeProperty(value, bean, name)
}


fun stylableOptionalQuadrilateralTypeProperty(initialValue: QuadrilateralType?) = PropertyDelegate<QuadrilateralType?,StylableOptionalQuadrilateralTypeProperty>(initialValue) { bean, name, value ->
    StylableOptionalQuadrilateralTypeProperty(value, bean, name)
}
