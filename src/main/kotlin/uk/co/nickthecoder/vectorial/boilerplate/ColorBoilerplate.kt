package uk.co.nickthecoder.vectorial.boilerplate

import kotlin.reflect.KClass
import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.shape.*
import uk.co.nickthecoder.vectorial.control.*


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ObservableValue<Color>`, we can simply use `ObservableColor`.
 */
interface ObservableColor: ObservableValue<Color> {}

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ReadOnlyProperty<Color>`, we can simply use `ReadOnlyColorProperty`.
 */
interface ReadOnlyColorProperty : ObservableColor, ReadOnlyProperty<Color>

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `Property<Color>`, we can simply use `ColorProperty`.
 */
interface ColorProperty : Property<Color>, ReadOnlyColorProperty {

    /**
     * Returns a read-only view of this mutable ColorProperty.
     * Typical usage :
     *
     *     private val _fooProperty by colorProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
     fun asReadOnly() : ReadOnlyColorProperty = ReadOnlyColorPropertyWrapper( this )
}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `SimpleProperty<Color>`, we can use `SimpleColorProperty`.
 */
open class SimpleColorProperty(initialValue: Color, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Color>(initialValue, bean, beanName), ColorProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleColorProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableColorProperty( initialValue: Color, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<Color>(initialValue, bean, beanName), ColorProperty {
    override fun kclass() : KClass<*> = Color::class
}

/**
 * Never use this class directly. Use [ColorProperty.asReadOnly] to obtain a read-only version of a mutable [ColorProperty].
 *
 * Boilerplate which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<Color, Property<Color>>`, we can simply use `ReadOnlyColorPropertyWrapper`.
 */
class ReadOnlyColorPropertyWrapper(wraps: ColorProperty) : ReadOnlyPropertyWrapper<Color, Property<Color>>(wraps),
    ReadOnlyColorProperty


// Functions

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableColor] which is calculated from 1 (unary) other [ObservableValue].
 *
 * See [UnaryFunction].
 */
class ColorUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Color) :
    UnaryFunction<Color, A, OA>(argA, lambda), ObservableColor

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableColor] which is calculated from 2 (binary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class ColorBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Color) :
    BinaryFunction<Color, A, OA, B, OB>(argA, argB, lambda), ObservableColor

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableColor] which is calculated from 3 (ternary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class ColorTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Color
) : TernaryFunction<Color, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableColor

// Delegate

/**
 * A Kotlin `delegate` to create a [ColorProperty] (the implementation will be a [SimpleColorProperty].
 * Typical usage :
 *
 *     val fooProperty by colorProperty( initialValue )
 *     var foo by myFooProperty
 *
 */
fun colorProperty(initialValue: Color) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleColorProperty(value, bean, name)
}

fun stylableColorProperty(initialValue: Color) = PropertyDelegate<Color,StylableColorProperty>(initialValue) { bean, name, value ->
    StylableColorProperty(value, bean, name)
}

// ============================
// Optional (nullable) versions
// ============================


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ObservableColor], but the [value] can also be `null`.
 */
interface ObservableOptionalColor : ObservableValue<Color?> {
    fun defaultOf( defaultValue : Color ) : ObservableColor = ColorUnaryFunction( this ) { a -> a ?: defaultValue }
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyColorProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalColorProperty : ObservableOptionalColor, ReadOnlyProperty<Color?>

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ColorProperty], but the [value] can also be `null`.
 */
interface OptionalColorProperty : Property<Color?>, ReadOnlyOptionalColorProperty {

    /**
     * Returns a read-only view of this mutable OptionalColorProperty.
     * Typical usage :
     *
     *     private val _fooProperty by optionalColorProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalColorProperty = ReadOnlyOptionalColorPropertyWrapper( this )

}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [SimpleColorProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalColorProperty(initialValue: Color?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Color?>(initialValue, bean, beanName), OptionalColorProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleOptionalColorProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableOptionalColorProperty(initialValue: Color?, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<Color?>(initialValue, bean, beanName), OptionalColorProperty {
    override fun kclass() : KClass<*> = Color::class
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyColorPropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalColorPropertyWrapper(wraps: OptionalColorProperty) :
    ReadOnlyPropertyWrapper<Color?, Property<Color?>>(wraps), ReadOnlyOptionalColorProperty

// Functions

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ColorUnaryFunction], but the [value] can also be `null`.
 */
class OptionalColorUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Color?) :
    UnaryFunction<Color?, A, OA>(argA, lambda), ObservableOptionalColor

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ColorBinaryFunction], but the [value] can also be `null`.
 */
class OptionalColorBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Color?) :
    BinaryFunction<Color?, A, OA, B, OB>(argA, argB, lambda), ObservableOptionalColor

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ColorTernaryFunction], but the [value] can also be `null`.
 */
class OptionalColorTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Color?
) : TernaryFunction<Color?, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableOptionalColor

// Delegate

/**
 * A Kotlin `delegate` to create an [OptionalColorProperty] (the implementation will be a [SimpleOptionalColorProperty]).
 * Typical usage :
 *
 *     val fooProperty by optionalColorProperty( initialValue )
 *     var foo by  myFooProperty
 *
 */
fun optionalColorProperty(initialValue: Color?) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleOptionalColorProperty(value, bean, name)
}


fun stylableOptionalColorProperty(initialValue: Color?) = PropertyDelegate<Color?,StylableOptionalColorProperty>(initialValue) { bean, name, value ->
    StylableOptionalColorProperty(value, bean, name)
}
