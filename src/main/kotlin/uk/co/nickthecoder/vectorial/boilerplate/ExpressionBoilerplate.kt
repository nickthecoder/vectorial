package uk.co.nickthecoder.vectorial.boilerplate

import kotlin.reflect.KClass
import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.shape.*
import uk.co.nickthecoder.vectorial.control.*
import uk.co.nickthecoder.vectorial.core.expression.Expression


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ObservableValue<Expression>`, we can simply use `ObservableExpression`.
 */
interface ObservableExpression: ObservableValue<Expression<*>> {}

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ReadOnlyProperty<Expression>`, we can simply use `ReadOnlyExpressionProperty`.
 */
interface ReadOnlyExpressionProperty : ObservableExpression, ReadOnlyProperty<Expression<*>>

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `Property<Expression>`, we can simply use `ExpressionProperty`.
 */
interface ExpressionProperty : Property<Expression<*>>, ReadOnlyExpressionProperty {

    /**
     * Returns a read-only view of this mutable ExpressionProperty.
     * Typical usage :
     *
     *     private val _fooProperty by expressionProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
     fun asReadOnly() : ReadOnlyExpressionProperty = ReadOnlyExpressionPropertyWrapper( this )
}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `SimpleProperty<Expression>`, we can use `SimpleExpressionProperty`.
 */
open class SimpleExpressionProperty(initialValue: Expression<*>, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Expression<*>>(initialValue, bean, beanName), ExpressionProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleExpressionProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableExpressionProperty( initialValue: Expression<*>, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<Expression<*>>(initialValue, bean, beanName), ExpressionProperty {
    override fun kclass() : KClass<*> = Expression::class
}

/**
 * Never use this class directly. Use [ExpressionProperty.asReadOnly] to obtain a read-only version of a mutable [ExpressionProperty].
 *
 * Boilerplate which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<Expression, Property<Expression>>`, we can simply use `ReadOnlyExpressionPropertyWrapper`.
 */
class ReadOnlyExpressionPropertyWrapper(wraps: ExpressionProperty) : ReadOnlyPropertyWrapper<Expression<*>, Property<Expression<*>>>(wraps),
    ReadOnlyExpressionProperty


// Functions

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableExpression] which is calculated from 1 (unary) other [ObservableValue].
 *
 * See [UnaryFunction].
 */
class ExpressionUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Expression<*>) :
    UnaryFunction<Expression<*>, A, OA>(argA, lambda), ObservableExpression

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableExpression] which is calculated from 2 (binary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class ExpressionBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Expression<*>) :
    BinaryFunction<Expression<*>, A, OA, B, OB>(argA, argB, lambda), ObservableExpression

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableExpression] which is calculated from 3 (ternary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class ExpressionTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Expression<*>
) : TernaryFunction<Expression<*>, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableExpression

// Delegate

/**
 * A Kotlin `delegate` to create a [ExpressionProperty] (the implementation will be a [SimpleExpressionProperty].
 * Typical usage :
 *
 *     val fooProperty by expressionProperty( initialValue )
 *     var foo by myFooProperty
 *
 */
fun expressionProperty(initialValue: Expression<*>) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleExpressionProperty(value, bean, name)
}

fun stylableExpressionProperty(initialValue: Expression<*>) = PropertyDelegate<Expression<*>,StylableExpressionProperty>(initialValue) { bean, name, value ->
    StylableExpressionProperty(value, bean, name)
}

// ============================
// Optional (nullable) versions
// ============================


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ObservableExpression], but the [value] can also be `null`.
 */
interface ObservableOptionalExpression : ObservableValue<Expression<*>?> {
    fun defaultOf( defaultValue : Expression<*> ) : ObservableExpression = ExpressionUnaryFunction( this ) { a -> a ?: defaultValue }
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyExpressionProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalExpressionProperty : ObservableOptionalExpression, ReadOnlyProperty<Expression<*>?>

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ExpressionProperty], but the [value] can also be `null`.
 */
interface OptionalExpressionProperty : Property<Expression<*>?>, ReadOnlyOptionalExpressionProperty {

    /**
     * Returns a read-only view of this mutable OptionalExpressionProperty.
     * Typical usage :
     *
     *     private val _fooProperty by optionalExpressionProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalExpressionProperty = ReadOnlyOptionalExpressionPropertyWrapper( this )

}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [SimpleExpressionProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalExpressionProperty(initialValue: Expression<*>?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Expression<*>?>(initialValue, bean, beanName), OptionalExpressionProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleOptionalExpressionProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableOptionalExpressionProperty(initialValue: Expression<*>?, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<Expression<*>?>(initialValue, bean, beanName), OptionalExpressionProperty {
    override fun kclass() : KClass<*> = Expression::class
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyExpressionPropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalExpressionPropertyWrapper(wraps: OptionalExpressionProperty) :
    ReadOnlyPropertyWrapper<Expression<*>?, Property<Expression<*>?>>(wraps), ReadOnlyOptionalExpressionProperty

// Functions

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ExpressionUnaryFunction], but the [value] can also be `null`.
 */
class OptionalExpressionUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Expression<*>?) :
    UnaryFunction<Expression<*>?, A, OA>(argA, lambda), ObservableOptionalExpression

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ExpressionBinaryFunction], but the [value] can also be `null`.
 */
class OptionalExpressionBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Expression<*>?) :
    BinaryFunction<Expression<*>?, A, OA, B, OB>(argA, argB, lambda), ObservableOptionalExpression

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ExpressionTernaryFunction], but the [value] can also be `null`.
 */
class OptionalExpressionTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Expression<*>?
) : TernaryFunction<Expression<*>?, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableOptionalExpression

// Delegate

/**
 * A Kotlin `delegate` to create an [OptionalExpressionProperty] (the implementation will be a [SimpleOptionalExpressionProperty]).
 * Typical usage :
 *
 *     val fooProperty by optionalExpressionProperty( initialValue )
 *     var foo by  myFooProperty
 *
 */
fun optionalExpressionProperty(initialValue: Expression<*>?) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleOptionalExpressionProperty(value, bean, name)
}


fun stylableOptionalExpressionProperty(initialValue: Expression<*>?) = PropertyDelegate<Expression<*>?,StylableOptionalExpressionProperty>(initialValue) { bean, name, value ->
    StylableOptionalExpressionProperty(value, bean, name)
}
