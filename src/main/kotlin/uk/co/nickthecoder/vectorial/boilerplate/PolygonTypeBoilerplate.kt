package uk.co.nickthecoder.vectorial.boilerplate

import kotlin.reflect.KClass
import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.shape.*
import uk.co.nickthecoder.vectorial.control.*


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ObservableValue<PolygonType>`, we can simply use `ObservablePolygonType`.
 */
interface ObservablePolygonType: ObservableValue<PolygonType> {}

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ReadOnlyProperty<PolygonType>`, we can simply use `ReadOnlyPolygonTypeProperty`.
 */
interface ReadOnlyPolygonTypeProperty : ObservablePolygonType, ReadOnlyProperty<PolygonType>

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `Property<PolygonType>`, we can simply use `PolygonTypeProperty`.
 */
interface PolygonTypeProperty : Property<PolygonType>, ReadOnlyPolygonTypeProperty {

    /**
     * Returns a read-only view of this mutable PolygonTypeProperty.
     * Typical usage :
     *
     *     private val _fooProperty by polygonTypeProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
     fun asReadOnly() : ReadOnlyPolygonTypeProperty = ReadOnlyPolygonTypePropertyWrapper( this )
}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `SimpleProperty<PolygonType>`, we can use `SimplePolygonTypeProperty`.
 */
open class SimplePolygonTypeProperty(initialValue: PolygonType, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<PolygonType>(initialValue, bean, beanName), PolygonTypeProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimplePolygonTypeProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylablePolygonTypeProperty( initialValue: PolygonType, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<PolygonType>(initialValue, bean, beanName), PolygonTypeProperty {
    override fun kclass() : KClass<*> = PolygonType::class
}

/**
 * Never use this class directly. Use [PolygonTypeProperty.asReadOnly] to obtain a read-only version of a mutable [PolygonTypeProperty].
 *
 * Boilerplate which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<PolygonType, Property<PolygonType>>`, we can simply use `ReadOnlyPolygonTypePropertyWrapper`.
 */
class ReadOnlyPolygonTypePropertyWrapper(wraps: PolygonTypeProperty) : ReadOnlyPropertyWrapper<PolygonType, Property<PolygonType>>(wraps),
    ReadOnlyPolygonTypeProperty


// Functions

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservablePolygonType] which is calculated from 1 (unary) other [ObservableValue].
 *
 * See [UnaryFunction].
 */
class PolygonTypeUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> PolygonType) :
    UnaryFunction<PolygonType, A, OA>(argA, lambda), ObservablePolygonType

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservablePolygonType] which is calculated from 2 (binary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class PolygonTypeBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> PolygonType) :
    BinaryFunction<PolygonType, A, OA, B, OB>(argA, argB, lambda), ObservablePolygonType

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservablePolygonType] which is calculated from 3 (ternary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class PolygonTypeTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> PolygonType
) : TernaryFunction<PolygonType, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservablePolygonType

// Delegate

/**
 * A Kotlin `delegate` to create a [PolygonTypeProperty] (the implementation will be a [SimplePolygonTypeProperty].
 * Typical usage :
 *
 *     val fooProperty by polygonTypeProperty( initialValue )
 *     var foo by myFooProperty
 *
 */
fun polygonTypeProperty(initialValue: PolygonType) = PropertyDelegate(initialValue) { bean, name, value ->
    SimplePolygonTypeProperty(value, bean, name)
}

fun stylablePolygonTypeProperty(initialValue: PolygonType) = PropertyDelegate<PolygonType,StylablePolygonTypeProperty>(initialValue) { bean, name, value ->
    StylablePolygonTypeProperty(value, bean, name)
}

// ============================
// Optional (nullable) versions
// ============================


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ObservablePolygonType], but the [value] can also be `null`.
 */
interface ObservableOptionalPolygonType : ObservableValue<PolygonType?> {
    fun defaultOf( defaultValue : PolygonType ) : ObservablePolygonType = PolygonTypeUnaryFunction( this ) { a -> a ?: defaultValue }
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyPolygonTypeProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalPolygonTypeProperty : ObservableOptionalPolygonType, ReadOnlyProperty<PolygonType?>

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [PolygonTypeProperty], but the [value] can also be `null`.
 */
interface OptionalPolygonTypeProperty : Property<PolygonType?>, ReadOnlyOptionalPolygonTypeProperty {

    /**
     * Returns a read-only view of this mutable OptionalPolygonTypeProperty.
     * Typical usage :
     *
     *     private val _fooProperty by optionalPolygonTypeProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalPolygonTypeProperty = ReadOnlyOptionalPolygonTypePropertyWrapper( this )

}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [SimplePolygonTypeProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalPolygonTypeProperty(initialValue: PolygonType?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<PolygonType?>(initialValue, bean, beanName), OptionalPolygonTypeProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleOptionalPolygonTypeProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableOptionalPolygonTypeProperty(initialValue: PolygonType?, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<PolygonType?>(initialValue, bean, beanName), OptionalPolygonTypeProperty {
    override fun kclass() : KClass<*> = PolygonType::class
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyPolygonTypePropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalPolygonTypePropertyWrapper(wraps: OptionalPolygonTypeProperty) :
    ReadOnlyPropertyWrapper<PolygonType?, Property<PolygonType?>>(wraps), ReadOnlyOptionalPolygonTypeProperty

// Functions

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [PolygonTypeUnaryFunction], but the [value] can also be `null`.
 */
class OptionalPolygonTypeUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> PolygonType?) :
    UnaryFunction<PolygonType?, A, OA>(argA, lambda), ObservableOptionalPolygonType

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [PolygonTypeBinaryFunction], but the [value] can also be `null`.
 */
class OptionalPolygonTypeBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> PolygonType?) :
    BinaryFunction<PolygonType?, A, OA, B, OB>(argA, argB, lambda), ObservableOptionalPolygonType

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [PolygonTypeTernaryFunction], but the [value] can also be `null`.
 */
class OptionalPolygonTypeTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> PolygonType?
) : TernaryFunction<PolygonType?, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableOptionalPolygonType

// Delegate

/**
 * A Kotlin `delegate` to create an [OptionalPolygonTypeProperty] (the implementation will be a [SimpleOptionalPolygonTypeProperty]).
 * Typical usage :
 *
 *     val fooProperty by optionalPolygonTypeProperty( initialValue )
 *     var foo by  myFooProperty
 *
 */
fun optionalPolygonTypeProperty(initialValue: PolygonType?) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleOptionalPolygonTypeProperty(value, bean, name)
}


fun stylableOptionalPolygonTypeProperty(initialValue: PolygonType?) = PropertyDelegate<PolygonType?,StylableOptionalPolygonTypeProperty>(initialValue) { bean, name, value ->
    StylableOptionalPolygonTypeProperty(value, bean, name)
}
