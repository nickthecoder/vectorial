package uk.co.nickthecoder.vectorial.boilerplate

import kotlin.reflect.KClass
import uk.co.nickthecoder.glok.property.*
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.shape.*
import uk.co.nickthecoder.vectorial.control.*


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ObservableValue<Diagram>`, we can simply use `ObservableDiagram`.
 */
interface ObservableDiagram: ObservableValue<Diagram> {}

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `ReadOnlyProperty<Diagram>`, we can simply use `ReadOnlyDiagramProperty`.
 */
interface ReadOnlyDiagramProperty : ObservableDiagram, ReadOnlyProperty<Diagram>

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `Property<Diagram>`, we can simply use `DiagramProperty`.
 */
interface DiagramProperty : Property<Diagram>, ReadOnlyDiagramProperty {

    /**
     * Returns a read-only view of this mutable DiagramProperty.
     * Typical usage :
     *
     *     private val _fooProperty by diagramProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
     fun asReadOnly() : ReadOnlyDiagramProperty = ReadOnlyDiagramPropertyWrapper( this )
}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 * Instead of `SimpleProperty<Diagram>`, we can use `SimpleDiagramProperty`.
 */
open class SimpleDiagramProperty(initialValue: Diagram, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Diagram>(initialValue, bean, beanName), DiagramProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleDiagramProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableDiagramProperty( initialValue: Diagram, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<Diagram>(initialValue, bean, beanName), DiagramProperty {
    override fun kclass() : KClass<*> = Diagram::class
}

/**
 * Never use this class directly. Use [DiagramProperty.asReadOnly] to obtain a read-only version of a mutable [DiagramProperty].
 *
 * Boilerplate which avoids having to use generic.
 * Instead of `ReadOnlyPropertyWrapper<Diagram, Property<Diagram>>`, we can simply use `ReadOnlyDiagramPropertyWrapper`.
 */
class ReadOnlyDiagramPropertyWrapper(wraps: DiagramProperty) : ReadOnlyPropertyWrapper<Diagram, Property<Diagram>>(wraps),
    ReadOnlyDiagramProperty


// Functions

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableDiagram] which is calculated from 1 (unary) other [ObservableValue].
 *
 * See [UnaryFunction].
 */
class DiagramUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Diagram) :
    UnaryFunction<Diagram, A, OA>(argA, lambda), ObservableDiagram

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableDiagram] which is calculated from 2 (binary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class DiagramBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Diagram) :
    BinaryFunction<Diagram, A, OA, B, OB>(argA, argB, lambda), ObservableDiagram

/**
 * Boilerplate which avoids having to use generics.
 * An [ObservableDiagram] which is calculated from 3 (ternary) other [ObservableValue]s.
 *
 * See [BinaryFunction].
 */
class DiagramTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Diagram
) : TernaryFunction<Diagram, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableDiagram

// Delegate

/**
 * A Kotlin `delegate` to create a [DiagramProperty] (the implementation will be a [SimpleDiagramProperty].
 * Typical usage :
 *
 *     val fooProperty by diagramProperty( initialValue )
 *     var foo by myFooProperty
 *
 */
fun diagramProperty(initialValue: Diagram) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleDiagramProperty(value, bean, name)
}

fun stylableDiagramProperty(initialValue: Diagram) = PropertyDelegate<Diagram,StylableDiagramProperty>(initialValue) { bean, name, value ->
    StylableDiagramProperty(value, bean, name)
}

// ============================
// Optional (nullable) versions
// ============================


// Interfaces

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ObservableDiagram], but the [value] can also be `null`.
 */
interface ObservableOptionalDiagram : ObservableValue<Diagram?> {
    fun defaultOf( defaultValue : Diagram ) : ObservableDiagram = DiagramUnaryFunction( this ) { a -> a ?: defaultValue }
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyDiagramProperty], but the [value] can also be `null`.
 */
interface ReadOnlyOptionalDiagramProperty : ObservableOptionalDiagram, ReadOnlyProperty<Diagram?>

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [DiagramProperty], but the [value] can also be `null`.
 */
interface OptionalDiagramProperty : Property<Diagram?>, ReadOnlyOptionalDiagramProperty {

    /**
     * Returns a read-only view of this mutable OptionalDiagramProperty.
     * Typical usage :
     *
     *     private val _fooProperty by optionalDiagramProperty( initialValue )
     *     val fooProperty = _fooProperty.asReadOnly()
     *     val foo by _fooProperty
     *         private set
     *
     * This give `private` access to the mutable property, and the mutable `var`, but the public
     * only has read-only access.
     */
    fun asReadOnly() : ReadOnlyOptionalDiagramProperty = ReadOnlyOptionalDiagramPropertyWrapper( this )

}

// Classes

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [SimpleDiagramProperty], but the [value] can also be `null`.
 */
open class SimpleOptionalDiagramProperty(initialValue: Diagram?, bean: Any? = null, beanName: String? = null) :
    SimpleProperty<Diagram?>(initialValue, bean, beanName), OptionalDiagramProperty

/**
 * Boilerplate which avoids having to use generics.
 *
 * A subclass of [SimpleOptionalDiagramProperty], which also stores the pre-styled value, so that if the style is removed,
 * then the pre-styled value can be reinstated.
 */
class StylableOptionalDiagramProperty(initialValue: Diagram?, bean: Any? = null, beanName: String? = null) :
    SimpleStylableProperty<Diagram?>(initialValue, bean, beanName), OptionalDiagramProperty {
    override fun kclass() : KClass<*> = Diagram::class
}

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [ReadOnlyDiagramPropertyWrapper], but the [value] can also be `null`.
 */
class ReadOnlyOptionalDiagramPropertyWrapper(wraps: OptionalDiagramProperty) :
    ReadOnlyPropertyWrapper<Diagram?, Property<Diagram?>>(wraps), ReadOnlyOptionalDiagramProperty

// Functions

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [DiagramUnaryFunction], but the [value] can also be `null`.
 */
class OptionalDiagramUnaryFunction<A, OA : ObservableValue<A>>(argA: OA, lambda: (A) -> Diagram?) :
    UnaryFunction<Diagram?, A, OA>(argA, lambda), ObservableOptionalDiagram

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [DiagramBinaryFunction], but the [value] can also be `null`.
 */
class OptionalDiagramBinaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>>(argA: OA, argB: OB, lambda: (A, B) -> Diagram?) :
    BinaryFunction<Diagram?, A, OA, B, OB>(argA, argB, lambda), ObservableOptionalDiagram

/**
 * Boilerplate which avoids having to use generics.
 *
 * Similar to [DiagramTernaryFunction], but the [value] can also be `null`.
 */
class OptionalDiagramTernaryFunction<A, OA : ObservableValue<A>, B, OB : ObservableValue<B>, C, OC : ObservableValue<C>>(
    argA: OA,
    argB: OB,
    argC: OC,
    lambda: (A, B, C) -> Diagram?
) : TernaryFunction<Diagram?, A, OA, B, OB, C, OC>(argA, argB, argC, lambda), ObservableOptionalDiagram

// Delegate

/**
 * A Kotlin `delegate` to create an [OptionalDiagramProperty] (the implementation will be a [SimpleOptionalDiagramProperty]).
 * Typical usage :
 *
 *     val fooProperty by optionalDiagramProperty( initialValue )
 *     var foo by  myFooProperty
 *
 */
fun optionalDiagramProperty(initialValue: Diagram?) = PropertyDelegate(initialValue) { bean, name, value ->
    SimpleOptionalDiagramProperty(value, bean, name)
}


fun stylableOptionalDiagramProperty(initialValue: Diagram?) = PropertyDelegate<Diagram?,StylableOptionalDiagramProperty>(initialValue) { bean, name, value ->
    StylableOptionalDiagramProperty(value, bean, name)
}
