/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.ToolBar
import uk.co.nickthecoder.glok.control.WrappedNode
import uk.co.nickthecoder.vectorial.boilerplate.OptionalDiagramProperty
import uk.co.nickthecoder.vectorial.control.*
import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.Document
import uk.co.nickthecoder.vectorial.core.DocumentListener
import uk.co.nickthecoder.vectorial.core.change.AltersSelection
import uk.co.nickthecoder.vectorial.core.change.Change
import uk.co.nickthecoder.vectorial.core.change.ChangeGeometry
import uk.co.nickthecoder.vectorial.core.shape.*


class SelectionToolBar(

    private val diagramProperty : OptionalDiagramProperty

) : WrappedNode<ToolBar>(ToolBar()), DocumentListener {


    private val commands = Commands().apply {
        with(VectorialActions) {
            radioChoices(VectorialSettings.centeredProperty) {
                choice(DRAW_CENTERED, Centered.CENTERED)
                choice(DRAW_CORNER_BASED, Centered.CORNER_BASED)
            }
        }
    }

    val diagram: Diagram?
        get() = diagramProperty.value


    private val descriptionLabel = Label("")

    private val distanceView = FloatExpressionView()

    /**
     * For the start, opposite corner, or center of the selected Shape
     */
    private val pointView1 = Vector2ExpressionView()

    private val angleView = AngleExpressionView()

    /**
     * For the end, of opposite corner of the selected Shape
     */
    private val pointView2 = Vector2ExpressionView()

    /**
     * For the start, opposite corner, or center of the selected Shape
     */
    private val polarView = PolarExpressionView()

    /**
     * For the size of the selected Shape
     */
    private val sizeView = Vector2ExpressionView().apply {
        name = "Size"
        separator = "x"
    }

    init {
        id = "SelectionToolBar"

        selectionChanged()
        diagramProperty.addChangeListener { _, oldValue, newValue ->
            oldValue?.document?.removeListener(this)
            newValue?.document?.addListener(this)
        }
    }

    override fun documentChanged(document: Document, change: Change, isUndo: Boolean) {
        if (change is AltersSelection || change is ChangeGeometry) {
            selectionChanged()
        }
    }

    private fun selectionChanged() {
        inner.items.clear()
        val shapes = diagram?.selection?.shapes ?: return

        inner.apply {
            if (shapes.size == 1) {
                val shape = shapes.first()

                + shapeButton(shape)

                if (shape is GeometryShape) {
                    when (val geometry = shape.geometry) {
                        is Line -> {
                            pointView1.name = "start"
                            pointView1.vector2Expression = geometry.start
                            pointView2.name = "end"
                            pointView2.vector2Expression = geometry.end

                            + pointView1
                            + pointView2
                        }

                        is Quadrilateral -> {
                            if (geometry.isCentered()) {
                                pointView1.name = "Center"
                                pointView1.vector2Expression = geometry.center
                                sizeView.vector2Expression = geometry.size

                                + pointView1
                                + sizeView
                            } else {
                                pointView1.vector2Expression = geometry.corner0
                                pointView2.vector2Expression = geometry.corner2

                                + pointView1
                                + pointView2
                            }
                            commands.build(VectorialSettings.defaultIconSizeProperty) {

                                + Label("Centered?")
                                + propertyRadioButton2(VectorialActions.DRAW_CENTERED)
                                + propertyRadioButton2(VectorialActions.DRAW_CORNER_BASED)
                            }
                        }

                        is Star -> {
                            pointView1.vector2Expression = geometry.center
                            pointView1.name = "Center"
                            polarView.polarExpression = geometry.ratioAndTurn
                            polarView.name = "Ratio and turn"
                            // TODO Use a new PolarView to show/edit the radius and angle from `delta`.

                            + pointView1
                            + polarView
                        }

                        is PolygonOrStar -> {
                            pointView1.vector2Expression = geometry.center
                            // TODO Use a new PolarView to show/edit the radius and angle from `delta`.
                            +pointView1
                        }

                        is Bezier -> {
                        }

                        is GuideLine -> {
                            distanceView.floatExpression = geometry.distance
                            angleView.angleExpression = geometry.angle
                            + distanceView
                            + angleView
                        }
                    }
                }

            } else {
                + descriptionLabel
                descriptionLabel.text = "${shapes.size} shapes selected"
                for (shape in shapes) {
                    + shapeButton(shape)
                }
            }
        }
    }
}
