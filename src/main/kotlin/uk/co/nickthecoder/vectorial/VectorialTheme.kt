/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.Theme
import uk.co.nickthecoder.glok.theme.ThemeBuilder
import uk.co.nickthecoder.glok.theme.dsl.or
import uk.co.nickthecoder.glok.theme.dsl.theme
import uk.co.nickthecoder.glok.theme.styles.*


object VectorialTheme : ThemeBuilder() {

    init {
        theme = buildTheme()
    }

    override fun buildTheme(): Theme = theme {

        "SettingsDialog" {
            padding( 10 )
        }

        LABEL {
            ".information" {
                font(Tantalum.font.italic())
                textColor( Tantalum.fontColor2.opacity(0.8f))
            }
        }

        LABEL {
            ".key_combination" {
                font(Tantalum.font.italic())
                textColor( Tantalum.fontColor2.opacity(0.8f))
            }
        }

        // Bug fix. Fixed after glok version 0.6alpha8
        TOOL_BAR {
            child(CONTAINER) {
                child(RADIO_BUTTON2) {
                    HAS_GRAPHIC {
                        SELECTED { backgroundColor(Tantalum.accentColor.opacity(0.5f)) }
                    }
                }
            }
        }
    }

}
