package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.control.ToolBar
import uk.co.nickthecoder.glok.control.WrappedNode

class PathToolBar : WrappedNode<ToolBar>(ToolBar()) {

    init {
        id = "PathToolBar"
    }

}
