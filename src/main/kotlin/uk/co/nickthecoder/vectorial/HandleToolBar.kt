/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.property.SimpleProperty
import uk.co.nickthecoder.glok.property.UnaryFunction
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.scene.dsl.radioChoices
import uk.co.nickthecoder.vectorial.control.FloatExpressionView
import uk.co.nickthecoder.vectorial.control.Vector2ExpressionView
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.shape.Anchor
import uk.co.nickthecoder.vectorial.core.shape.CornerType
import uk.co.nickthecoder.vectorial.core.shape.LineEdge
import uk.co.nickthecoder.vectorial.core.shape.RatioAlongBezier
import uk.co.nickthecoder.vectorial.util.DualListenerGuard

/**
 * Shows data related to a single [Handle] :
 *
 * * Name
 * * Position
 *
 * If its value is a [Connection] :
 *
 * * Connection Type (Ratio / From Start / From End)
 * * The distance or ratio
 *
 * If the handle is an Anchor (part of a Path), then we also allow the PathTransition to be changed
 * (Angled, Smooth, Symmetric)
 */
class HandleToolBar(

    private val handle: Handle

) : WrappedNode<ToolBar>(ToolBar()) {

    private val diagram = handle.shape.diagram()

    private val guard = DualListenerGuard()

    private val attributeExpressionProperty =
        SimpleProperty<Expression<*>?>((handle as? AttributeHandle<*, *>)?.attribute)

    private val connectionProperty = SimpleProperty<Connection<*>?>(null)

    private val connectorProperty = SimpleProperty<Connector?>(null).apply {
        addListener {
            guard.updateGUI {
                populateConnectorTypes()
                updateGUIConnector()
            }
        }
    }

    private val connectorToEdgeProperty = UnaryFunction(connectorProperty) { it as? ConnectorToEdge }

    private val lineEdgeProperty = UnaryFunction(connectorToEdgeProperty) { it?.edge as? LineEdge }

    private val attribute = (handle as? AttributeHandle<*, *>)?.attribute


    private val attributeListener = attribute?.let {
        ExpressionListener(it) {
            attributeExpressionProperty.value = attribute.expression
            connectionProperty.value = attribute.expression as? Connection<*>
            connectorProperty.value = connectionProperty.value?.connector
        }
    }

    private val distanceOrRatio = FloatExpressionView().apply {
        visibleProperty.bindTo(connectorToEdgeProperty.isNotNull())
    }

    private val connectorTypeComboBox = ChoiceBox<ConnectorType?>().apply {
        selection.selectedItemProperty.addListener {
            guard.updateModel { updateModelConnector() }
        }
    }

    private val isBoundedCheckBox = CheckBox("Is Bounded").apply {
        visibleProperty.bindTo(lineEdgeProperty.isNotNull())
        tooltip = TextTooltip("Prevent extending beyond the ends of the edge")
        selectedProperty.addListener {
            guard.updateModel { updateModelConnector() }
        }
    }

    private val cornerTypeProperty = SimpleProperty<CornerType?>((handle as? Anchor)?.pathPart?.endCornerType)
    private var cornerType by cornerTypeProperty

    init {
        id = "HandleToolBar"

        attributeExpressionProperty.value = attribute?.expression
        connectionProperty.value = attribute?.expression as? Connection<*>
        connectorProperty.value = connectionProperty.value?.connector

        with(inner) {
            + label(handle.name)

            if (handle is AttributeHandle<*, *>) {

                val attribute = handle.attribute
                attributeExpressionProperty.value = attribute.expression

                when (attribute) {
                    is Vector2Attribute -> {
                        + Vector2ExpressionView().also { it.vector2Expression = attribute }
                    }

                    is FloatAttribute -> {
                        + FloatExpressionView().also { it.floatExpression = attribute }
                    }
                }

                if (handle is Anchor) {
                    radioChoices(cornerTypeProperty) {
                        + propertyRadioButton(CornerType.ANGLED, "Angled")
                        + propertyRadioButton(CornerType.SMOOTH, "Smooth")
                        + propertyRadioButton(CornerType.SYMMETRIC, "Symmetric")
                    }
                }

                + label("") {
                    visibleProperty.bindTo(connectionProperty.isNotNull())
                    textProperty.bindTo(
                        UnaryFunction(connectorProperty) { connector ->
                            when (connector) {
                                is ConnectorToHandle -> "Control Point Connection"
                                is ConnectorToEdge -> "Edge Connection"
                                else -> ""
                            }
                        }
                    )
                }

                + connectorTypeComboBox
                + distanceOrRatio
                + isBoundedCheckBox

            }
        }

        populateConnectorTypes()
        guard.updateGUI { updateGUIConnector() }
        guard.updateGUI { updateGUICornerType() }
        cornerTypeProperty.addListener { guard.updateModel { updateModelCornerType() } }
    }

    private fun populateConnectorTypes() {
        val connector = connectorProperty.value as? ConnectorToEdge
        if (connector == null) {
            connectorTypeComboBox.visible = false
        } else {
            val edge = connector.edge
            connectorTypeComboBox.items.clear()
            for (type in edge.supportedConnectorTypes) {
                connectorTypeComboBox.items.add(type)
            }
            connectorTypeComboBox.selection.selectedItem = connector.connectorType
            connectorTypeComboBox.visible = connectorTypeComboBox.items.size > 1
        }
    }

    /**
     * Updates the GUI [connectorTypeComboBox] and [isBoundedCheckBox] from the model.
     */
    private fun updateGUIConnector() {

        val connector = connectorProperty.value

        if (connector is ConnectorToEdge) {
            connectorTypeComboBox.selection.selectedItem = connector.connectorType
            isBoundedCheckBox.selected = connector.isBounded
            distanceOrRatio.floatExpression = when (connector) {
                is RatioAlongLine -> connector.ratio
                is DistanceAlongLine -> connector.distance
                is RatioAlongBezier -> connector.ratio
                else -> null
            }

        } else {
            connectorTypeComboBox.selection.selectedItem = null
            isBoundedCheckBox.selected = false
        }
    }

    /**
     * Updates the model based on the state of the GUI : [connectorTypeComboBox] and [isBoundedCheckBox].
     */
    private fun updateModelConnector() {
        val newConnectorType = connectorTypeComboBox.selection.selectedItem ?: return

        connectionProperty.value?.changeConnector(newConnectorType, isBoundedCheckBox.selected)
        updateGUIConnector()
    }

    private fun updateModelCornerType() {
        cornerType?.let {
            (handle as? Anchor)?.pathPart?.changeCornerType(it)
        }
    }


    private fun updateGUICornerType() {
        (handle as? Anchor)?.let {
            cornerType = it.pathPart.endCornerType
        }
    }

}
