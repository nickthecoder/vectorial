/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.control.ToolBar

internal fun fixedToolBar(commands : Commands) = ToolBar().apply {
    id = "FixedToolBar"

    visibleProperty.bindTo(VectorialSettings.showFixedToolBarProperty)

    commands.build(VectorialSettings.defaultIconSizeProperty) {

        with(VectorialActions) {
            + button(EDIT_UNDO)
            + button(EDIT_REDO)
            + Separator()
        }
    }
}
