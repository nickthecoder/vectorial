/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.property.boilerplate.intProperty
import uk.co.nickthecoder.vectorial.VectorialActions.DRAW_CENTERED
import uk.co.nickthecoder.vectorial.VectorialActions.DRAW_CORNER_BASED
import uk.co.nickthecoder.vectorial.core.shape.Centered
import uk.co.nickthecoder.vectorial.core.shape.PolygonType
import uk.co.nickthecoder.vectorial.util.DualListenerGuard

class PolygonToolBar : WrappedNode<ToolBar>(ToolBar()) {

    private val guard = DualListenerGuard()

    val sidesProperty by intProperty(3)
    var sides by sidesProperty

    private val sidesSpinner = IntSpinner().apply {
        min = 3
        max = 30
        value = VectorialSettings.polygonSides
        editor.prefColumnCount = 4
    }
    private val sidesTextListener = sidesSpinner.editor.textProperty.addChangeListener { _, _, newText ->
        guard.updateModel {
            newText.toIntOrNull()?.let { sidesProperty.value = it }
        }
    }

    private val commands = Commands().apply {
        radioChoices(VectorialSettings.polygonTypeProperty) {
            with(VectorialActions) {
                choice(DRAW_POLYGON, PolygonType.POLYGON)
                choice(DRAW_STAR, PolygonType.STAR)
            }
        }
        radioChoices(VectorialSettings.centeredProperty) {
            choice(DRAW_CENTERED, Centered.CENTERED)
            choice(DRAW_CORNER_BASED, Centered.CORNER_BASED)
        }

    }
    private val sidesListener = sidesProperty.addChangeListener { _, _, newValue ->
        guard.updateGUI { sidesSpinner.editor.text = newValue.toString() }
    }

    init {
        id = "PolygonToolBar"

        commands.build(VectorialSettings.defaultIconSizeProperty) {

            inner.apply {
                with(VectorialActions) {
                    + Label("Sides")
                    + sidesSpinner
                    + Separator()
                    + Label("Type")
                    + propertyRadioButton2(DRAW_POLYGON)
                    + propertyRadioButton2(DRAW_STAR)
                    + Label("Centered?")
                    + propertyRadioButton2(DRAW_CENTERED)
                    + propertyRadioButton2(DRAW_CORNER_BASED)

                }
            }
        }

        // TODO whenSceneSet { actionGroup.attachTo(scene.root) }
    }

}
