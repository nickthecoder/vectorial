/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.dock

import uk.co.nickthecoder.glok.collections.ObservableListSize
import uk.co.nickthecoder.glok.collections.asMutableObservableList
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dock.Dock
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.property.ObservableValue
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.changeListener
import uk.co.nickthecoder.glok.property.functions.greaterThan
import uk.co.nickthecoder.glok.property.functions.lessThan
import uk.co.nickthecoder.glok.property.functions.minus
import uk.co.nickthecoder.glok.property.functions.not
import uk.co.nickthecoder.glok.scene.*
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.Tantalum
import uk.co.nickthecoder.glok.theme.styles.TINTED
import uk.co.nickthecoder.vectorial.Vectorial
import uk.co.nickthecoder.vectorial.VectorialSettings
import uk.co.nickthecoder.vectorial.boilerplate.OptionalExpressionUnaryFunction
import uk.co.nickthecoder.vectorial.boilerplate.optionalDocumentProperty
import uk.co.nickthecoder.vectorial.control.handleButton
import uk.co.nickthecoder.vectorial.control.shapeButton
import uk.co.nickthecoder.vectorial.control.typeLabel
import uk.co.nickthecoder.vectorial.control.typeRow
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.AddShape
import uk.co.nickthecoder.vectorial.core.change.AltersSelection
import uk.co.nickthecoder.vectorial.core.change.Change
import uk.co.nickthecoder.vectorial.core.change.PathChange
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.shape.GeometryShape
import uk.co.nickthecoder.vectorial.core.shape.Group
import uk.co.nickthecoder.vectorial.core.shape.Shape

class DebugDock(harbour: Harbour)

    : Dock(ID, harbour) {

    // region ==== Fields ====
    private val treeView = MixedTreeView<DebugTreeItem>()

    /**
     * A list expressions, that we can go backwards and forwards through from buttons in the
     * `Expression` panel's toolBar.
     */
    private val expressionHistory = ExpressionHistory()
    // endregion fields

    // region ==== Properties ====

    val documentProperty by optionalDocumentProperty(null)
    val document by documentProperty
    private val documentPropertyListener = documentProperty.addChangeListener { _, old, new ->
        old?.removeListener(documentListener)
        new?.addListener(documentListener)
        treeView.root = if (new == null) {
            null
        } else {
            DocumentTreeItem(new)
        }
    }

    /**
     * When the diagram's selection changes, should the tree view's selection be changed?
     * When true, the tree view items will be expanded as needed, so that the selected item is visible.
     */
    val syncProperty by booleanProperty(true)
    var sync by syncProperty

    private val documentListener = object : DocumentListener {
        override fun documentChanged(document: Document, change: Change, isUndo: Boolean) {
            if (sync && change is AltersSelection) {
                selectTreeViewFromSelection(change.diagram.selection)
            }
            if (change is AddShape) {
                val parent = change.parent
                findShapeParentTreeItem(parent)?.refresh()
            }
            if (change is PathChange) {
                val parent = change.shape.parent
                findShapeParentTreeItem(parent)?.refresh()
            }
        }
    }

    private val selectedItemListener = changeListener<DebugTreeItem?, ObservableValue<DebugTreeItem?>> { _, _, selectedItem ->
        selectedItem?.selected()
    }

    /**
     * Incremented to cause [expressionPanelProperty] and [detailsPanelProperty] to be reevaluated.
     */
    private val refreshProperty = SimpleIntProperty(0)
    private var refresh by refreshProperty

    /**
     * The main content of the "Expression" panel (the center of the border pane)
     */
    private val expressionPanelProperty: ObservableOptionalNode = OptionalNodeBinaryFunction(expressionHistory.currentExpressionProperty, refreshProperty) { expression, _ ->
        if (expression == null) null else expressionPanel(expression)
    }

    /**
     * The main content of the "Details" panel (the center of the border pane)
     */
    private val detailsPanelProperty: ObservableOptionalNode = OptionalNodeBinaryFunction(treeView.selection.selectedItemProperty, refreshProperty) { item, _ ->
        item?.createDetails()
    }

    // endregion properties

    // region ==== init ==== (including scene graph)
    init {
        title = "Debug"
        id = "DebugDock"
        icons = Vectorial.icons
        iconName = "dock_debug"

        defaultSide = Side.RIGHT

        titleButtons.addAll(

            button("Update from Selection") {
                tooltip = TextTooltip("Update from Selection")
                style(TINTED)
                graphic = Tantalum.icon("sync_once")
                disabledProperty.bindTo(syncProperty)
                onAction { document?.diagram?.selection?.let { selectTreeViewFromSelection(it) } }
            },

            toggleButton("Sync") {
                tooltip = TextTooltip("Keep in Sync with Selection")
                style(TINTED)
                graphic = Tantalum.icon("sync")
                selectedProperty.bidirectionalBind(syncProperty)
            }
        )

        content = splitPane(Orientation.VERTICAL) {
            + treeView.apply {
                selection.selectedItemProperty.addChangeListener(selectedItemListener)
                document?.let { root = DocumentTreeItem(it) }
            }
            // Contains the Details panel, or the Expression panel (one is always invisible).
            + vBox {
                fillWidth = true

                // Details Panel
                + borderPane {
                    growPriority = 1f
                    visibleProperty.bindTo(expressionPanelProperty.isNull())
                    centerProperty.bindTo(detailsPanelProperty)
                    top = toolBar {
                        styles.add(".secondary")
                        + Label("Details")
                        + spacer()
                        + button("Refresh") {
                            tooltip = TextTooltip("Refresh")
                            style(TINTED)
                            graphic = ImageView(VectorialSettings.secondaryIcons.getResizable("refresh"))
                            onAction { refresh++ }
                        }
                    }
                }

                // Expression Panel
                + borderPane {
                    growPriority = 1f
                    visibleProperty.bindTo(expressionPanelProperty.isNotNull())
                    centerProperty.bindTo(expressionPanelProperty)
                    top = toolBar {
                        styles.add(".secondary")

                        + Label("Expression")
                        + spacer()
                        + button("Back") {
                            disabledProperty.bindTo(!expressionHistory.canGoBackProperty)
                            styles.add(TINTED)
                            graphic = Tantalum.icon("back")
                            onAction { expressionHistory.back() }
                        }
                        + button("Forward") {
                            disabledProperty.bindTo(!expressionHistory.canGoForwardProperty)
                            styles.add(TINTED)
                            graphic = Tantalum.icon("forward")
                            onAction { expressionHistory.forward() }
                        }
                        + button("Refresh") {
                            styles.add(TINTED)
                            graphic = Tantalum.icon("refresh")
                            onAction { refresh++ }
                        }
                    }
                }
            }
        }
    }
    // endregion init

    // region ==== expression Panel ====
    private fun expressionPanel(expression: Expression<*>) = ScrollPane().apply {
        content = formGrid {
            padding(10)

            typeRow(expression)
            + row("Value") {
                right = Label(expression.evalToString())
            }
            when (expression) {
                is Attribute<*, *> -> attribute(expression)
                is Connection -> connection(expression)
            }
        }
    }

    private fun FormGrid.attribute(expression: Attribute<*, *>) {
        + row("Attribute") {
            right = Label(expression.key)
        }
        + row("Expression") {
            right = expressionButton(expression.expression)
        }
    }

    private fun FormGrid.connection(connection: Connection<*>) {

        when (val connector = connection.connector) {
            is ConnectorToHandle -> connectorToHandle(connector)
            is ConnectorToEdge -> connectorToEdge(connector)

        }
    }

    private fun FormGrid.connectorToHandle(connector: ConnectorToHandle) {
        + row {
            above = Label("Connection to Handle")
            left = shapeButton(connector.handle.shape)
            right = handleButton(connector.handle)
        }
    }

    private fun FormGrid.connectorToEdge(connector: ConnectorToEdge) {
        + row {
            below = Label("Connector to Edge (${connector.connectorType.name})")
        }
        + row("Shape") {
            right = shapeButton(connector.edge.shape)
        }

        val along = connector

        typeRow(along)
        when (along) {
            is RatioAlongLine -> {
                + row("Start") {
                    right = expressionButton(along.start)
                }
                + row("End") {
                    right = expressionButton(along.end)
                }
                + row("Ratio") {
                    right = expressionButton(along.ratio)
                }
            }

            is DistanceAlongLine -> {
                + row("Start") {
                    right = expressionButton(along.start)
                }
                + row("End") {
                    right = expressionButton(along.end)
                }
                + row("Distance") {
                    right = expressionButton(along.distance)
                }
            }
        }
    }
    // endregion expression panel

    // region ==== Find TreeItems ====
    private fun selectTreeViewFromSelection(selection: Selection) {
        val shape = selection.shapes.firstOrNull() ?: return
        findShapeTreeItem(shape)?.let { shapeTreeItem ->
            treeView.selection.selectedItem = shapeTreeItem
            treeView.ensureVisible(shapeTreeItem)

            val handle = selection.handle ?: return
            shapeTreeItem.findControlPointTreeItem(handle)?.let { handleTreeItem ->
                treeView.selection.selectedItem = handleTreeItem
                treeView.ensureVisible(handleTreeItem)
            }
        }
    }

    private fun findControlPointTreeItem(handle: Handle): ControlPointTreeItem? {
        return findShapeTreeItem(handle.shape)?.findControlPointTreeItem(handle)
    }

    private fun findShapeTreeItem(shape: Shape): ShapeTreeItem? {
        val ownerItem = findShapeParentTreeItem(shape.parent)
        return ownerItem?.findShapeTreeItem(shape)
    }

    private fun findShapeParentTreeItem(shapeParent: ShapeParent): ShapeParentTreeItem? {
        return when (shapeParent) {
            is Layer -> findLayerTreeItem(shapeParent)
            is Group -> findShapeTreeItem(shapeParent)
            else -> null
        }
    }

    private fun findLayerTreeItem(layer: Layer): LayerTreeItem? {
        val ownerItem = findLayerParentTreeItem(layer.parent)
        return ownerItem?.findLayerTreeItem(layer)
    }

    private fun findLayerParentTreeItem(layerParent: LayerParent): LayerParentTreeItem? {
        return when (layerParent) {
            is Layer -> findLayerTreeItem(layerParent)
            is Diagram -> findDiagramTreeItem(layerParent)
            else -> null
        }
    }

    private fun findDiagramTreeItem(diagram: Diagram): DiagramTreeItem? {
        val documentTreeItem = treeView.root as? DocumentTreeItem ?: return null
        return documentTreeItem.findDiagramTreeItem(diagram)
    }


    private fun expressionButton(expression: Expression<*>) = button(expression.evalToString()) {
        onAction {
            expressionHistory.add(expression)
        }
    }

    // endregion ==== find tree items ====

    companion object {
        const val ID = "DEBUG"
    }

    // region == inner DebugTreeItem ==
    private abstract inner class DebugTreeItem : MixedTreeItem<DebugTreeItem>() {

        abstract fun createChildren(children: MutableList<DebugTreeItem>)

        open fun selected() {
            expressionHistory.clear()
        }

        open fun refresh() {
            val newChildren = mutableListOf<DebugTreeItem>()
            createChildren(newChildren)
            children.clear()
            children.addAll(newChildren)
            if (expanded) {
                for (child in children) {
                    child.refresh()
                }
            }
        }

        protected fun MutableList<DebugTreeItem>.addReuse(item: DebugTreeItem) {
            this.add(children.firstOrNull { it == item } ?: item)
        }

        open fun createDetails(): Node? = null
    }
    // endregion == inner DebugTreeItem ==

    // region == ShapeParentTreeItem ==
    private interface ShapeParentTreeItem {
        fun findShapeTreeItem(shape: Shape): ShapeTreeItem?
        fun refresh()
    }
    // endregion

    // region == LayerParentTreeItem ==
    private interface LayerParentTreeItem {
        fun findLayerTreeItem(layer: Layer): LayerTreeItem?
    }
    // endregion

    // region == inner DocumentTreeItem ==
    private inner class DocumentTreeItem(val document: Document) : DebugTreeItem() {
        init {
            expanded = true
            createChildren(children)
        }

        override fun createChildren(children: MutableList<DebugTreeItem>) {
            children.addReuse(DiagramTreeItem(document.diagram))
        }

        fun findDiagramTreeItem(diagram: Diagram): DiagramTreeItem? {
            return children.firstOrNull { (it as? DiagramTreeItem)?.diagram === diagram } as? DiagramTreeItem
        }

        override fun createCell(treeView : MixedTreeView<DebugTreeItem>) = SingleNodeMixedTreeCell<DebugTreeItem, Label>(treeView, this, Label("Document"))

        override fun createDetails() = ScrollPane().apply {
            content = formGrid {
                padding(10)
                + row("File") {
                    right = Label( document.name ) // TODO Should be filename, but not implemented yet.
                }
                + row("Saved") {
                    right = Label( document.isSaved().toString() )
                }
            }
        }
    }
    // endregion == inner DiagramTreeItem ==

    // region == inner DiagramTreeItem ==
    private inner class DiagramTreeItem(val diagram: Diagram) : DebugTreeItem(), LayerParentTreeItem {

        init {
            expanded = true
            createChildren(children)
        }

        override fun createChildren(children: MutableList<DebugTreeItem>) {
            children.addReuse(AttributesTreeItem(diagram.attributes, "Attributes"))
            children.addReuse(LayersTreeItem(diagram))
        }

        override fun findLayerTreeItem(layer: Layer): LayerTreeItem? {
            val layersTreeItem = children.firstOrNull { it is LayersTreeItem }
            return layersTreeItem?.children?.firstOrNull { (it as? LayerTreeItem)?.layer === layer } as? LayerTreeItem
        }

        override fun createCell(treeView : MixedTreeView<DebugTreeItem>) = SingleNodeMixedTreeCell<DebugTreeItem, Label>(treeView, this, Label("Diagram"))

        override fun createDetails() = ScrollPane().apply {
            content = formGrid {
                padding(10)
                + row("Diagram") {
                    right = Label( diagram.name )
                }
                + row("Size") {
                    right = Label( diagram.size().toString() )
                }
            }
        }
    }
    // endregion == inner DiagramTreeItem ==

    // region == inner AttributesTreeItem ==
    private inner class AttributesTreeItem(val attributes: Attributes, val title: String = "Attributes") : DebugTreeItem() {

        init {
            createChildren(children)
        }

        override fun createChildren(children: MutableList<DebugTreeItem>) {
            for (key in attributes.keys()) {
                children.addReuse(AttributeTreeItem(attributes[key]))
            }
        }

        override fun createCell(treeView : MixedTreeView<DebugTreeItem>) = SingleNodeMixedTreeCell<DebugTreeItem, Label>(treeView, this, Label(title))

    }
    // endregion == inner AttributesTreeItem ==

    // region == inner AttributeTreeItem ==
    private inner class AttributeTreeItem(val attribute: Attribute<*, *>) : DebugTreeItem() {

        init {
            leaf = true
        }

        override fun selected() {
            expressionHistory.clear()
            expressionHistory.add(attribute)
        }

        override fun createChildren(children: MutableList<DebugTreeItem>) {}

        override fun createCell(treeView : MixedTreeView<DebugTreeItem>) = SingleNodeMixedTreeCell<DebugTreeItem, Label>(treeView, this, Label(attribute.key))

    }
    // endregion == inner AttributeTreeItem ==

    // region == inner LayersTreeItem ==
    private inner class LayersTreeItem(val layerParent: LayerParent) : DebugTreeItem() {

        init {
            leaf = false
            expanded = true
            createChildren(children)
        }

        override fun createChildren(children: MutableList<DebugTreeItem>) {
            for (layer in layerParent.layers) {
                children.addReuse(LayerTreeItem(layer))
            }
        }

        override fun createCell(treeView : MixedTreeView<DebugTreeItem>) = SingleNodeMixedTreeCell<DebugTreeItem, Label>(treeView, this, Label("Layers"))
    }
    // endregion == inner LayersTreeItem ==

    // region == inner LayerTreeItem ==
    private inner class LayerTreeItem(val layer: Layer) : DebugTreeItem(), ShapeParentTreeItem, LayerParentTreeItem {

        init {
            leaf = false
            expanded = true
            createChildren(children)
        }

        override fun createChildren(children: MutableList<DebugTreeItem>) {
            if (layer.layers.isNotEmpty()) {
                children.addReuse(LayersTreeItem(layer))
            }
            for (shape in layer.shapes) {
                children.addReuse(ShapeTreeItem(shape))
            }
        }

        override fun findShapeTreeItem(shape: Shape): ShapeTreeItem? {
            return children.firstOrNull { (it as? ShapeTreeItem)?.shape === shape } as? ShapeTreeItem
        }

        override fun findLayerTreeItem(layer: Layer): LayerTreeItem? {
            val layersTreeItem = children.firstOrNull { it is LayersTreeItem }
            return layersTreeItem?.children?.firstOrNull { (it as? LayerTreeItem)?.layer === layer } as? LayerTreeItem
        }

        override fun createCell(treeView : MixedTreeView<DebugTreeItem>) = SingleNodeMixedTreeCell<DebugTreeItem, Label>(treeView, this, Label(layer.name))

        override fun createDetails() = ScrollPane().apply {
            content = formGrid {
                padding(10)
                + row("Layer") {
                    right = Label( layer.name )
                }
                + row("Depth") {
                    right = Label( layer.depth.toString() )
                }
                + row("Locked") {
                    right = Label( layer.isLocked.toString() )
                }
                + row("Visible") {
                    right = Label( layer.isVisible.toString() )
                }
            }
        }
    }
    // endregion == inner LayerTreeItem ==

    // region == inner ShapeTreeItem ==
    private inner class ShapeTreeItem(val shape: Shape) : DebugTreeItem(), ShapeParentTreeItem {

        init {
            createChildren(children)
        }

        override fun createChildren(children: MutableList<DebugTreeItem>) {
            if (shape is GeometryShape) {
                children.addReuse(AttributesTreeItem(shape.style.attributes, "Style"))
                children.addReuse(AttributesTreeItem(shape.geometry.attributes, "Geometry"))
                children.addReuse(ControlPointsTreeItem(shape))
            }
            if (shape is Group) {
                children.addReuse(GroupedShapesTreeItem(shape))
            }
        }

        override fun findShapeTreeItem(shape: Shape): ShapeTreeItem? {
            val layersTreeItem = children.firstOrNull { it is LayersTreeItem }
            return layersTreeItem?.children?.firstOrNull { (it as? ShapeTreeItem)?.shape === shape } as? ShapeTreeItem
        }

        fun findControlPointTreeItem(handle: Handle): ControlPointTreeItem? {
            val handleTreeItem = children.firstOrNull { it is ControlPointsTreeItem }
            return handleTreeItem?.children?.firstOrNull { (it as? ControlPointTreeItem)?.handle === handle } as? ControlPointTreeItem
        }

        override fun createCell(treeView : MixedTreeView<DebugTreeItem>) = SingleNodeMixedTreeCell<DebugTreeItem, Label>(treeView, this, Label(shape.name))

        override fun createDetails() = scrollPane {
            content = formGrid {
                padding(10)
                + row("Shape") {
                    right = shapeButton( shape )
                }
                typeRow(shape)
                + row("Bounding Box", shape.boundingBox.toString())
                + row("Z Order", shape.zOrder().toString())

                (shape.parent as? Shape)?.let { parentShape ->
                    + row("Parent") {
                        right = shapeButton( parentShape )
                    }
                }

                if (shape is GeometryShape) {
                    val geometry = shape.geometry

                    + row("Geometry") {
                        right = typeLabel( geometry )
                    }
                    + row("Closed? ") {
                        right = Label( geometry.isClosed().toString() )
                    }
                }
            }
        }

    }
    // endregion == inner ShapeTreeItem ==

    // region == inner GroupedShapesTreeItem ==
    private inner class GroupedShapesTreeItem(val group: Group) : DebugTreeItem() {

        init {
            createChildren(children)
        }

        override fun createChildren(children: MutableList<DebugTreeItem>) {
            for (shape in group.shapes) {
                children.addReuse(ShapeTreeItem(shape))
            }
        }

        override fun createCell(treeView : MixedTreeView<DebugTreeItem>) = SingleNodeMixedTreeCell<DebugTreeItem, Label>(treeView, this, Label("Grouped Shapes"))

    }
    // endregion == inner GroupedShapesTreeItem ==

    // region == inner ControlPointsTreeItem ==
    private inner class ControlPointsTreeItem(val shape: Shape) : DebugTreeItem() {

        init {
            createChildren(children)
        }

        override fun createChildren(children: MutableList<DebugTreeItem>) {
            for (cp in shape.handles) {
                children.addReuse(ControlPointTreeItem(cp))
            }
        }

        override fun createCell(treeView : MixedTreeView<DebugTreeItem>) = SingleNodeMixedTreeCell<DebugTreeItem, Label>(treeView, this, Label("Control Points"))

    }
    // endregion == inner ControlPointsTreeItem ==

    // region == inner ControlPointTreeItem ==
    private inner class ControlPointTreeItem(val handle: Handle) : DebugTreeItem() {

        init {
            leaf = true
        }

        override fun createChildren(children: MutableList<DebugTreeItem>) {}

        override fun createCell(treeView : MixedTreeView<DebugTreeItem>) = SingleNodeMixedTreeCell<DebugTreeItem, Label>(treeView, this, Label(handle.name))

        override fun createDetails() = ScrollPane().apply {
            content = formGrid {
                padding(10)
                + row("Control Point") {
                    right = handleButton(handle)
                }
                + row("Position") {
                    right = Label(handle.position().toString())
                }
                + row("isFree") {
                    right = Label(handle.isFree().toString())
                }
                + row("isFixed") {
                    right = Label(handle.isFixed().toString())
                }
                + row("isConnectable") {
                    right = Label(handle.isConnectable().toString())
                }

                if (handle is AttributeHandle<*, *>) {
                    val attribute = handle.attribute
                    + row("Attribute") {
                        right = expressionButton(attribute)
                    }
                }
            }
        }
    }
    // endregion == inner ControlPointTreeItem ==

}



// region ==== ExpressionHistory ====
private class ExpressionHistory {

    val expressionHistory = mutableListOf<Expression<*>>().asMutableObservableList()

    /**
     * The index for the current expression.
     */
    val indexProperty by intProperty(-1)
    var index by indexProperty

    val sizeProperty = ObservableListSize(expressionHistory)
    val size by sizeProperty

    val canGoBackProperty = indexProperty.greaterThan(0)
    val canGoBack by canGoBackProperty

    val canGoForwardProperty = indexProperty.lessThan(sizeProperty - 1)
    val canGoForward by canGoForwardProperty

    val currentExpressionProperty = OptionalExpressionUnaryFunction(indexProperty) {
        if (it >= 0) {
            expressionHistory[it]
        } else {
            null
        }
    }
    val currentExpression by currentExpressionProperty

    fun clear() {
        expressionHistory.clear()
        index = -1
    }

    fun add(expression: Expression<*>) {
        val foundIndex = expressionHistory.indexOf(expression)
        if (foundIndex >= 0) {
            index = foundIndex
        } else {
            while (expressionHistory.size > index + 1) {
                expressionHistory.removeLast()
            }
            expressionHistory.add(expression)
            index = expressionHistory.size - 1
        }
    }

    fun back(): Expression<*>? {
        return if (index > 0) {
            index--
            return expressionHistory[index]
        } else {
            null
        }
    }

    fun forward(): Expression<*>? {
        return if (index < expressionHistory.size - 1) {
            index++
            expressionHistory[index]
        } else {
            null
        }
    }

}
// endregion ExpressionHistory
