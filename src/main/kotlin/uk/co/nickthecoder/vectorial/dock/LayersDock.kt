/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.dock

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.control.ContentDisplay
import uk.co.nickthecoder.glok.control.ListView
import uk.co.nickthecoder.glok.control.TextListCell
import uk.co.nickthecoder.glok.dock.Dock
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.property.boilerplate.SimpleBooleanProperty
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.glok.theme.styles.TINTED
import uk.co.nickthecoder.vectorial.*
import uk.co.nickthecoder.vectorial.boilerplate.optionalDiagramProperty
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.AdjustLayer
import uk.co.nickthecoder.vectorial.core.change.Change
import uk.co.nickthecoder.vectorial.core.change.LayerChange
import uk.co.nickthecoder.vectorial.core.change.SetCurrentLayerOrGroup
import uk.co.nickthecoder.vectorial.util.DualListenerGuard

class LayersDock(

    harbour : Harbour

) : Dock(ID, harbour), DocumentListener {

    val diagramProperty by optionalDiagramProperty(null)
    val diagram by diagramProperty
    private val diagramListener = diagramProperty.addChangeListener{ _, old, new ->
        refresh( old, new )
    }

    private val guard = DualListenerGuard()

    private val listView = ListView<Layer>().apply {
        cellFactory = { listView : ListView<Layer>, layer : Layer -> LayerCell(listView, layer) }
    }
    private val selectionListener = listView.selection.selectedItemProperty.addChangeListener { _, _, layer ->
        guard.updateModel {
            if (diagram?.currentLayerOrGroup != layer) {
                layer?.makeCurrentLayerOrGroup()?.now()
            }
        }
    }

    private val disableLowerProperty = SimpleBooleanProperty(false)
    private var disableLower by disableLowerProperty

    private val disableRaiseProperty = SimpleBooleanProperty(false)
    private var disableRaise by disableRaiseProperty

    val commands = Commands().apply {
        with (VectorialActions) {
            LAYER_NEW { onNewLayer(diagram, scene?.stage!!) }
            LAYER_TOP { onReorderCurrentLayer(diagram, Reorder.TOP) }.apply {
                disableProperty = disableRaiseProperty
            }
            LAYER_RAISE { onReorderCurrentLayer(diagram, Reorder.RAISE) }.apply {
                disableProperty = disableRaiseProperty
            }
            LAYER_LOWER { onReorderCurrentLayer(diagram, Reorder.LOWER) }.apply {
                disableProperty = disableLowerProperty
            }
            LAYER_BOTTOM { onReorderCurrentLayer(diagram, Reorder.BOTTOM) }.apply {
                disableProperty = disableLowerProperty
            }
            LAYER_DELETE { onDeleteCurrentLayer(diagram) }
        }
    }

    init {
        title = "Layers"
        id = "LayersDock"
        icons = Vectorial.icons
        iconName = "dock_layers"

        defaultSide = Side.RIGHT

        titleButtons.addAll(

            button("Refresh") {
                tooltip = TextTooltip("Refresh")
                style(TINTED)
                graphic = ImageView(VectorialSettings.secondaryIcons.getResizable("refresh"))
                onAction { refresh(diagram, diagram) }
            }
        )

        content = borderPane {
            style("layers")

            center= listView.apply {
                growPriority = 1f
            }
            commands.build(VectorialSettings.secondaryIconSizeProperty) {

                with(VectorialActions) {
                    bottom = toolBar {
                        + button(LAYER_NEW)
                        + button(LAYER_DELETE)
                        + spacer()
                        + button(LAYER_TOP)
                        + button(LAYER_RAISE)
                        + button(LAYER_LOWER)
                         +button(LAYER_BOTTOM)
                    }
                }
            }
        }
        refresh(null, diagram)
    }

    private fun refresh(oldDiagram: Diagram?, newDiagram: Diagram?) {

        fun addItems(parent: LayerParent) {
            for (layer in parent.layers.asReversed()) {
                listView.items.add(layer)
                addItems(layer)
            }
        }
        guard.updateGUI {
            if (oldDiagram != newDiagram) {
                oldDiagram?.document?.removeListener(this)
                newDiagram?.document?.addListener(this)
            }
            listView.items.clear()
            if (newDiagram != null) {
                addItems(newDiagram)
            }
            (diagram?.currentLayerOrGroup as? Layer)?.let { layer ->
                listView.selection.selectedItem = layer
                // TODO listView.scrollTo(layer)
            }
            updateCanReorder()
        }
    }

    override fun documentChanged(document: Document, change: Change, isUndo: Boolean) {
        diagram?.let { diagram ->
            if (change is LayerChange) {
                refresh(diagram, diagram)
            }
            if (change is SetCurrentLayerOrGroup) {
                val currentLayer = change.newLayerOrGroup as? Layer
                if (currentLayer == null) {
                    guard.updateGUI { listView.selection.selectedItem = null }
                } else {
                    guard.updateGUI { listView.selection.selectedItem = currentLayer }
                }
                updateCanReorder()
            }
        }
    }

    private fun updateCanReorder() {
        val currentLayer = listView.selection.selectedItem
        if (currentLayer == null) {
            disableLower = true
            disableRaise = true
        } else {
            val layers = currentLayer.parent.layers
            val index = layers.indexOf(currentLayer)
            disableLower = index <= 0
            disableRaise = index >= layers.size - 1
        }
    }

    private inner class LayerCell( listView : ListView<Layer>, layer : Layer ) : TextListCell<Layer>( listView, layer ), DocumentListener {

        private val layerLockedProperty = SimpleBooleanProperty(false)
        private var layerLocked by layerLockedProperty

        private val layerVisibleProperty = SimpleBooleanProperty(false)
        private var layerVisible by layerVisibleProperty

        override fun documentChanged(document: Document, change: Change, isUndo: Boolean) {
            if (change is AdjustLayer && change.layer === item) {
                layerLocked = change.layer.isLocked
                layerVisible = change.layer.isVisible
            }
        }

        init {
            item.diagram.document.addListener(this)

            layerLocked = item.isLocked
            layerVisible = item.isVisible

            commands.build(VectorialSettings.secondaryIconSizeProperty) {

                with (VectorialActions ) {

                    node.contentDisplay = ContentDisplay.RIGHT
                    node.text = item.name
                    // TODO When layers change level, this would need to be dynamic.
                    // i.e. we would need to listen for that Change.
                    node.padding( 0,10,0, 10 + item.depth * 20 )
                    node.graphic = hBox {
                        + toggleButton( LAYER_HIDE_SHOW ).apply {
                            style(".plain")
                            onAction { onHideShowLayer(layer) }
                            selectedProperty.bidirectionalBind(layerVisibleProperty)
                        }
                        + toggleButton ( LAYER_LOCK_UNLOCK ).apply {
                            style(".plain")
                            onAction{ onLockUnlockLayer(layer) }
                            selectedProperty.bidirectionalBind(layerLockedProperty)
                        }
                    }
                }
            }
        }
    }

    companion object {
        const val ID = "LAYERS"
    }

}
