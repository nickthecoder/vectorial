/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.application.Application
import uk.co.nickthecoder.glok.application.ApplicationStatus
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.dialog.alertDialog
import uk.co.nickthecoder.glok.dock.Harbour
import uk.co.nickthecoder.glok.dock.MapDockFactory
import uk.co.nickthecoder.glok.dock.inspector.NodeInspectorDock
import uk.co.nickthecoder.glok.event.ActionEvent
import uk.co.nickthecoder.glok.property.BidirectionalBind
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.Stage
import uk.co.nickthecoder.glok.scene.dsl.*
import uk.co.nickthecoder.vectorial.boilerplate.OptionalDocumentUnaryFunction
import uk.co.nickthecoder.vectorial.boilerplate.SimpleModeTypeProperty
import uk.co.nickthecoder.vectorial.boilerplate.SimpleOptionalDiagramProperty
import uk.co.nickthecoder.vectorial.control.DiagramView
import uk.co.nickthecoder.vectorial.control.ToolType
import uk.co.nickthecoder.vectorial.control.PositionIndicator
import uk.co.nickthecoder.vectorial.control.ZoomIndicator
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.AltersSelection
import uk.co.nickthecoder.vectorial.core.change.Change
import uk.co.nickthecoder.vectorial.core.shape.*
import uk.co.nickthecoder.vectorial.dock.DebugDock
import uk.co.nickthecoder.vectorial.dock.LayersDock
import uk.co.nickthecoder.vectorial.util.disableLowerLayer
import uk.co.nickthecoder.vectorial.util.disableRaiseLayer

/**
 * [MainWindow], defines the large scale structure of the GUI elements of the application, and is a conduit
 * which allow the small components to work together.
 * This is done through two mechanisms
 *
 * 1. Glok `Properties`.
 * 2. Vectorial [Change]s via [DocumentListener]s
 *
 * For example, we have a [diagramProperty] whose value is the [Diagram] being edited in the selected
 * tab of a TabPane.
 *
 * The [DiagramView] and `Docks` also have a `diagramProperty`, and these are _bound_ together, such that
 * all the small components agree on the same value.
 *
 * Editing a diagram in the [DiagramView] causes [Change] messages to be sent to [DocumentListener]s,
 * such as the [LayersDock], which can update themselves based on the contents of the [Change].
 *
 */
class MainWindow(val stage: Stage) : DocumentListener {

    private val rootBorderPane = BorderPane()

    private var diagramView: DiagramView? = null
    private var diagramTab: DiagramTab? = null

    private val commands: Commands = Commands()

    /**
     * Which [Diagram] is currently on show? From the [DiagramView] of the current [DiagramTab].
     * Note, this can be null, when there are no tabs open, or if we open a tab which isn't a [DiagramTab].
     */
    private val diagramProperty = SimpleOptionalDiagramProperty(null)
    private var diagram by diagramProperty

    /**
     * The [Document] currently on show, derived from [diagramProperty].
     */
    private val documentProperty = OptionalDocumentUnaryFunction(diagramProperty) { it?.document }
    private val document by documentProperty

    private val debugDockVisibleProperty = SimpleBooleanProperty(false)
    private val nodeInspectorDockVisibleProperty = SimpleBooleanProperty(false)
    private val layersDockVisibleProperty = SimpleBooleanProperty(false)

    private val dockFactory = MapDockFactory()

    val harbour = Harbour().apply {
        id = "Harbour"
        iconSizeProperty.bindTo(VectorialSettings.secondaryIconSizeProperty)
        dockFactory = this@MainWindow.dockFactory
    }

    private val dynamicToolBarProperty = SimpleOptionalNodeProperty(null)
    private var dynamicToolBar by dynamicToolBarProperty

    /**
     * At the time of writing, multiple tabs aren't being used.
     */
    private val tabPane = documentTabPane {
        id = "TabPane"
    }

    /**
     * Used to give the user context-sensitive help from [DiagramView], and shown in the statusBar.
     */
    private val tipProperty = SimpleStringProperty("")

    // These properties will be bound to the current DiagramView
    // When we change tabs, they will be unbound and bound to the new tab's DiagramView.

    /**
     * Communicates the zoom factor between the [DiagramView] and the [ZoomIndicator] in the statusBar.
     */
    private val zoomProperty = SimpleFloatProperty(1f)

    /**
     * Increments every time the document changes.
     * We can use this as a parameter property functions
     */
    private val changeCounterProperty = SimpleIntProperty(0)
    private var changeCounter by changeCounterProperty

    /**
     *
     */
    private val disableRaiseLayerProperty = BooleanUnaryFunction(changeCounterProperty) {
        diagram.disableRaiseLayer()
    }

    private val disableLowerLayerProperty = BooleanUnaryFunction(changeCounterProperty) {
        diagram.disableLowerLayer()
    }

    /**
     * Communicates the position of the mouse between the [DiagramView] and the [PositionIndicator] in the statusBar.
     */
    private val mouseXProperty = SimpleFloatProperty(0f)
    private val mouseYProperty = SimpleFloatProperty(0f)

    /**
     * When the user changes `mode` e.g. by clicking a button in the [modeToolBar],
     * [DiagramView] hears about it via this property.
     */
    private val toolTypeProperty = SimpleModeTypeProperty(ToolType.SELECT)
    private var modeType by toolTypeProperty

    /**
     * Used to enable/disable the `undo` button.
     */
    private val disableUndoProperty = SimpleBooleanProperty(false)
    private val disableRedoProperty = SimpleBooleanProperty(false)

    /**
     * The title as it appears in the native window's title bar.
     * When we change tabs, this is updated to include the document's filename.
     */
    private val windowTitleProperty = SimpleStringProperty("")

    // region ==== Commands ====
    init {
        commands.apply {
            with(VectorialActions) {
                ESCAPE { onEscapePressed() }

                radioChoices(toolTypeProperty) {
                    choice(MODE_SELECT, ToolType.SELECT)
                    choice(MODE_RECTANGLE, ToolType.RECTANGLE)
                    choice(MODE_POLYGON, ToolType.POLYGON)
                    choice(MODE_ELLIPSE, ToolType.ELLIPSE)
                    choice(MODE_PATH, ToolType.PATH)
                    choice(MODE_LINE, ToolType.LINE)
                    choice(MODE_CURVE, ToolType.CURVE)
                }
                DRAW_END {
                    diagramView?.endDrawing()
                }

                FILE_SAVE { onSave() }
                FILE_CLOSE { stage.close() }
                FILE_QUIT { Application.instance.status = ApplicationStatus.REQUEST_QUIT }
                TAB_CLOSE { onCloseTab() }
                FILE_SETTINGS { SettingsDialog.show(stage) }

                EDIT_UNDO {
                    if (diagram?.document?.history?.isBatchStarted() == false) {
                        diagramTab?.onUndo(VectorialSettings.skipSelectionChanges)
                    }
                }.disable(disableUndoProperty)
                EDIT_REDO {
                    if (diagram?.document?.history?.isBatchStarted() == false) {
                        diagramTab?.onRedo(VectorialSettings.skipSelectionChanges)
                    }
                }.disable(disableRedoProperty)

                EDIT_SELECT_ALL { diagramTab?.onSelectAll() }
                EDIT_SELECT_EVERYTHING { diagramTab?.onSelectEverything() }
                EDIT_SELECT_NONE { diagramTab?.onSelectNone() }
                EDIT_DELETE { onDelete() }

                OBJECT_GROUP { diagramTab?.onGroup() }
                OBJECT_UNGROUP { diagramTab?.onUngroup() }

                toggle(VIEW_SHOW_MENUBAR, VectorialSettings.showMenuBarProperty)
                toggle(VIEW_SHOW_FIXED_TOOLBAR, VectorialSettings.showFixedToolBarProperty)
                toggle(VIEW_SHOW_STATUS_BAR, VectorialSettings.showStatusBarProperty)

                toggle(THEME_DARK, VectorialSettings.darkThemeProperty)

                toggle(DOCK_DEBUG, debugDockVisibleProperty)
                toggle(DOCK_LAYERS, layersDockVisibleProperty)
                toggle(DOCK_NODE_INSPECTOR, nodeInspectorDockVisibleProperty)

                LAYER_NEW { onNewLayer(diagram, stage) }
                LAYER_HIDE_SHOW { onHideShowCurrentLayer(diagram) }
                LAYER_HIDE { onHideShowCurrentLayer(diagram, false) }
                LAYER_SHOW { onHideShowCurrentLayer(diagram, true) }
                LAYER_LOCK_UNLOCK { onLockUnlockCurrentLayer(diagram) }
                LAYER_LOCK { onLockUnlockCurrentLayer(diagram, true) }
                LAYER_UNLOCK { onLockUnlockCurrentLayer(diagram, false) }
                LAYER_TOP { onReorderCurrentLayer(diagram, Reorder.TOP) }.apply {
                    disableProperty = disableRaiseLayerProperty
                }
                LAYER_RAISE { onReorderCurrentLayer(diagram, Reorder.RAISE) }.apply {
                    disableProperty = disableRaiseLayerProperty
                }
                LAYER_LOWER { onReorderCurrentLayer(diagram, Reorder.LOWER) }.apply {
                    disableProperty = disableLowerLayerProperty
                }
                LAYER_BOTTOM { onReorderCurrentLayer(diagram, Reorder.BOTTOM) }.apply {
                    disableProperty = disableLowerLayerProperty
                }
                LAYER_DELETE { onDeleteCurrentLayer(diagram) }

                ZOOM_RESET { diagramTab?.onZoomReset() }
                ZOOM_IN { diagramTab?.onZoomIn() }
                ZOOM_OUT { diagramTab?.onZoomOut() }

                DEBUG_DUMP_SCENE { stage.scene?.dump(borders = true) }
            }
        }
    }
    // endregion

    // region ==== Docks ====

    init {
        LayersDock(harbour).apply {
            dockFactory.add(this)
            visibleProperty.bidirectionalBind(layersDockVisibleProperty)
            diagramProperty.bindTo(this@MainWindow.diagramProperty)
        }

        DebugDock(harbour).apply {
            dockFactory.add(this)
            visibleProperty.bidirectionalBind(debugDockVisibleProperty)
            documentProperty.bindTo(this@MainWindow.documentProperty)
        }
        NodeInspectorDock(harbour).apply {
            dockFactory.add(this)
            visibleProperty.bidirectionalBind(nodeInspectorDockVisibleProperty)
            rootNode = rootBorderPane
        }
    }
    // endregion docks

    private val defaultToolBar = defaultToolBar(commands)
    private val selectionToolBar = SelectionToolBar(diagramProperty)
    private val modeToolBar = modeToolBar(commands)
    private val quadrilateralToolBar = QuadrilateralToolBar()
    private val ellipseToolBar = EllipseToolBar()
    private val polygonToolBar = PolygonToolBar()
    private val pathToolBar = PathToolBar()

    // region ==== Listeners ====
    init {
        tabPane.selection.selectedItemProperty.addChangeListener { _, old, tab ->
            onTabChanged(old, tab)
        }
        toolTypeProperty.addListener { adjustToolBars() }

        with(VectorialSettings) {
            polygonSidesProperty.addChangeListener { _, _, sides -> changePolygonSides(sides) }
            polygonTypeProperty.addChangeListener { _, _, type -> changePolygonType(type) }
            quadrilateralTypeProperty.addChangeListener { _, _, type -> changeQuadrilateralType(type) }
            centeredProperty.addChangeListener { _, _, centered -> changeCentered(centered) }
            ellipseTypeProperty.addChangeListener { _, _, type -> changeEllipseType(type) }
            maximisedProperty
        }
        stage.maximizedProperty.addChangeListener { _, _, value -> VectorialSettings.maximised = value }
    }
    // endregion

    // region ==== Scene Graph ====
    init {
        with(stage) {
            titleProperty.bindTo(windowTitleProperty)
            scene = scene(VectorialSettings.sceneWidth, VectorialSettings.sceneHeight) {
                root = rootBorderPane.apply {
                    commands.attachTo(this)

                    top = vBox {
                        fillWidth = false

                        + mainMenuBar(commands)
                        + hBox {
                            fillWidth = true
                            + fixedToolBar(commands)
                            + singleContainer(dynamicToolBarProperty) {
                                growPriority = 1f
                            }
                            + toolBar {
                                commands.build(VectorialSettings.defaultIconSizeProperty) {
                                    + button(VectorialActions.FILE_SETTINGS)
                                }
                            }
                        }
                    }
                    center = harbour.apply {
                        harbour.apply {
                            content = borderPane {
                                left = modeToolBar
                                center = tabPane
                            }
                        }
                    }
                    bottom = statusBar(tipProperty,mouseXProperty,mouseYProperty,zoomProperty )
                }
            }
        }
    }

    // endregion

    init {

        adjustToolBars()

        tabPane.tabs.add(DiagramTab())

        stage.onCloseRequested { onCloseRequested(it) }
    }

    // region ==== Listeners ====
    private fun changePolygonSides(sides: Int) {
        val diagram = diagram ?: return
        diagram.document.history.batch {
            for (shape in diagram.selection.shapes) {
                if (shape is GeometryShape) {
                    val geometry = shape.geometry
                    if (geometry is PolygonOrStar) {
                        geometry.setSides( sides )
                    }
                }
            }
        }
    }

    private fun changePolygonType(type: PolygonType) {
        val diagram = diagram ?: return
        diagram.document.history.batch {
            for (shape in diagram.selection.shapes) {
                if (shape is GeometryShape) {
                    val geometry = shape.geometry
                    if (geometry is PolygonOrStar) {
                        geometry.changePolygonType(type)
                    }
                }
            }
        }
    }

    private fun changeQuadrilateralType(type: QuadrilateralType) {
        val diagram = diagram ?: return
        val shape = diagram.selection.singleShape() ?: return

        if (shape is GeometryShape) {
            val geometry = shape.geometry
            if (geometry is Quadrilateral) {
                geometry.changeQuadrilateralType(type)
            }
        }
    }

    private fun changeCentered(centered: Centered) {
        val diagram = diagram ?: return
        val shape = diagram.selection.singleShape() ?: return
        if (shape is GeometryShape) {
            val geometry = shape.geometry
            if (geometry is Centerable) {
                geometry.changeCentered(centered)
            }
        }
    }

    private fun changeEllipseType(type: EllipseType) {
        val diagram = diagram ?: return
        val shape = diagram.selection.singleShape() ?: return
        if (shape is GeometryShape) {
            val geometry = shape.geometry
            if (geometry is Ellipse) {
                geometry.changeEllipseType(type)?.now()
            }
        }
    }

    private fun selectionChanged() {
        val diagram = diagram ?: return
        val shape = diagram.selection.singleShape()
        if (shape is GeometryShape) {
            val geometry = shape.geometry
            if (geometry is Centerable) {
                VectorialSettings.centered = geometry.centered
            }
            if (geometry is Quadrilateral) {
                VectorialSettings.quadrilateralType = geometry.quadrilateralType
            }
        }
    }

    // endregion

    // region ==== Action methods ====

    private fun onEscapePressed() {
        val diagram = diagram ?: return
        val diagramView = diagramView ?: return

        if (diagram.selection.isEmpty() || diagram.document.history.isBatchStarted()) {
            diagramView.toolType = ToolType.NONE
        } else {
            diagram.clearSelection()
        }
    }

    private fun onSave() {
        diagram?.let { diagram ->
            val doc = diagram.document
            ToXML().toXMLDocument(doc)

        }
    }

    private fun onDelete() {
        diagram?.let { diagram ->
            diagram.document.history.batch {
                for (shape in diagram.selection.shapes.toList()) {
                    shape.delete()
                }
                diagram.clearSelection()
            }
        }
    }

    private fun onCloseTab() {
        // TODO Check if file needs saving, and there isn't another view of this document open.
        tabPane.selection.selectedItem?.let { tabPane.tabs.remove(it) }
    }

    // endregion Actions

    // region ==== DocumentListener ====
    override fun documentChanged(document: Document, change: Change, isUndo: Boolean) {
        changeCounter ++
        if (document !== this.document) return
        if (change is AltersSelection) {
            adjustToolBars()
            selectionChanged()
        }

        disableUndoProperty.value = !document.history.canUndo()
        disableRedoProperty.value = !document.history.canRedo()
    }
    // endregion DocumentListener

    // region ==== methods ====

    private var ignoreUnsavedFiles = true // TODO ignoreUnsavedFiles should be false, but is annoying while developing!!!
    private fun onCloseRequested(event : ActionEvent) {
        if (!ignoreUnsavedFiles) {
            val unsavedTabs = tabPane.tabs.filterIsInstance<DiagramTab>().filter{ !it.saved }
            if (unsavedTabs.isNotEmpty()) {
                // Prevent the stage from closing.
                event.consume()

                // Show the first unsaved tab
                tabPane.selection.selectedItem = unsavedTabs.first()

                alertDialog(AlertType.WARNING) {
                    title="Unsaved Changed"
                    heading="Unsaved Changes"
                    message = unsavedTabs.joinToString(separator = "\n") {it.document.name}
                    buttonTypes(ButtonType.CANCEL)
                    buttons {
                        meaning(ButtonMeaning.OK) {
                            + button("Close without Saving") {
                                onAction {
                                    stage.close()
                                    ignoreUnsavedFiles = true
                                    this@MainWindow.stage.close()
                                }
                            }
                        }
                        meaning(ButtonMeaning.APPLY) {
                            + button( "Save" ) {
                                defaultButton = true
                                onAction {
                                    stage.close()
                                    for ( tab in unsavedTabs ) {
                                        tabPane.selection.selectedItem = tab
                                        // TODO tab.save()
                                    }
                                    // TODO If all were saved.
                                    // this@MainView.stage.close()
                                }
                            }
                        }
                    }
                }.createStage(stage) {
                    show()
                }
            }
        }
    }

    private fun fallbackToolbar(): Node {
        val selection = diagram?.selection
        val handle = selection?.handle

        return if (handle != null) {
            HandleToolBar(handle)
        } else if (selection?.isEmpty() == true) {
            defaultToolBar
        } else {
            selectionToolBar
        }
    }

    private fun adjustToolBars() {
        val selection = diagram?.selection
        val singleShape = selection?.singleShape()
        val nothingSelected = selection?.isEmpty() == true
        val geometry = (singleShape as? GeometryShape)?.geometry

        dynamicToolBar = when (modeType) {
            ToolType.RECTANGLE -> if (nothingSelected || geometry is Quadrilateral) quadrilateralToolBar else fallbackToolbar()
            ToolType.ELLIPSE -> if (nothingSelected || geometry is Ellipse) ellipseToolBar else fallbackToolbar()
            ToolType.POLYGON -> if (nothingSelected || geometry is PolygonOrStar) polygonToolBar else fallbackToolbar()
            ToolType.PATH -> if (nothingSelected || geometry is Path) pathToolBar else fallbackToolbar()
            else -> fallbackToolbar()
        }
    }

    private var modeTypeBind: BidirectionalBind? = null
    private var zoomBind: BidirectionalBind? = null

    private fun onTabChanged(oldTab: Tab?, tab: Tab?) {

        (oldTab as? DiagramTab)?.document?.removeListener(this)

        windowTitleProperty.unbind()

        zoomBind?.unbind()
        modeTypeBind?.unbind()

        tipProperty.unbind()
        toolTypeProperty.unbind()
        mouseXProperty.unbind()
        mouseYProperty.unbind()
        zoomProperty.unbind()
        document?.removeListener(this)

        val diagramTab = tab as? DiagramTab
        if (diagramTab == null) {
            diagramProperty.value = null
            return
        }
        val diagram = diagramTab.diagram
        diagramProperty.value = diagram
        val document = diagram.document
        val diagramView = diagramTab.diagramView

        this.diagramTab = diagramTab
        this.diagram = diagram
        this.diagramView = diagramView

        tipProperty.bindTo(diagramView.tipProperty)
        mouseXProperty.bindTo(diagramView.worldMouseXProperty)
        mouseYProperty.bindTo(diagramView.worldMouseYProperty)

        modeTypeBind = toolTypeProperty.bidirectionalBind(diagramView.toolTypeProperty)
        zoomBind = zoomProperty.bidirectionalBind(diagramView.zoomProperty)

        windowTitleProperty.bindTo(StringBinaryFunction(diagramTab.fileProperty, diagramTab.savedProperty) { file, saved ->
            StringBuilder().apply {
                if (!saved) append("*")
                if (file == null) {
                    append("New Document")
                } else {
                    append(file.name)
                }
                append(" - ")
                append("Glok")
            }.toString()
        })
        document.addListener(this)
        changeCounter++
    }
    // endregion
}
