/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.Separator
import uk.co.nickthecoder.glok.control.ToolBar
import uk.co.nickthecoder.glok.control.WrappedNode
import uk.co.nickthecoder.vectorial.core.shape.Centered
import uk.co.nickthecoder.vectorial.core.shape.QuadrilateralType

class QuadrilateralToolBar : WrappedNode<ToolBar>(ToolBar()) {

    private val commands = Commands().apply {
        with(VectorialActions) {
            radioChoices(VectorialSettings.quadrilateralTypeProperty) {
                choice(DRAW_RECTANGLE, QuadrilateralType.RECTANGLE)
                choice(DRAW_DIAMOND, QuadrilateralType.DIAMOND)
                choice(DRAW_PARALLELOGRAM, QuadrilateralType.PARALLELOGRAM)
            }
            radioChoices(VectorialSettings.centeredProperty) {
                choice(DRAW_CENTERED, Centered.CENTERED)
                choice(DRAW_CORNER_BASED, Centered.CORNER_BASED)
            }
        }
    }

    init {
        id = "RectangularToolBar"

        commands.build(VectorialSettings.defaultIconSizeProperty) {
            inner.apply {

                with (VectorialActions) {
                    + Label("Type")
                    + propertyRadioButton2(DRAW_RECTANGLE)
                    + propertyRadioButton2(DRAW_DIAMOND)
                    + propertyRadioButton2(DRAW_PARALLELOGRAM)
                    + Separator()
                    + Label("Centered?")
                    + propertyRadioButton2(DRAW_CENTERED)
                    + propertyRadioButton2(DRAW_CORNER_BASED)
                }
            }

        }
        // TODO actionGroup.attachTo(this)
    }
}
