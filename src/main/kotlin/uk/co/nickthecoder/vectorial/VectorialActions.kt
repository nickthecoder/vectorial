/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.action.Actions
import uk.co.nickthecoder.glok.event.Key

object VectorialActions : Actions(Vectorial.icons) {

    val ESCAPE = define( "escape", "Escape", Key.ESCAPE.noMods() )

    val FILE = define("file", "File") // TODO _File
    val FILE_NEW = define("file_ew", "New", Key.N.control(), tooltip = "Create a New Diagram")
    val FILE_SAVE = define("file_ave", "Save", Key.S.control())
    val FILE_CLOSE = define("file_close", "Close", Key.F4.control())
    val FILE_QUIT = define("file_quit", "Quit", Key.Q.control())
    val FILE_SETTINGS = define( "file_settings", "Settings", Key.S.control().alt()).apply {
        tinted=true
    }

    val EDIT = define("edit", "Edit")
    val EDIT_CUT = define("edit_cut", "Cut", Key.X.control())
    val EDIT_COPY = define("edit_copy", "Copy", Key.C.control())
    val EDIT_PASTE = define("edit_paste", "Paste", Key.V.control())
    val EDIT_DELETE = define("edit_delete", "Delete", Key.DELETE.noMods())
    val EDIT_DUPLICATE = define("edit_duplicate", "Duplicate", Key.D.control())
    val EDIT_UNDO = define("edit_undo", "Undo", Key.Z.control())
    val EDIT_REDO = define("edit_redo", "Redo", Key.Z.control().shift()).apply {
        additionalKeyCombinations.add(Key.Y.control())
    }
    val EDIT_SELECT_ALL = define("edit_select_all", "Select All", Key.A.control())
    val EDIT_SELECT_EVERYTHING = define("edit_select_everything", "Select Everything", Key.A.shift())
    val EDIT_SELECT_NONE = define("edit_select_none", "Select None", Key.A.control().shift())

    val OBJECT_GROUP = define("object_group", "Group", Key.G.control(), tooltip = "Group selected objects")
    val OBJECT_UNGROUP = define("object_ungroup", "Ungroup", Key.U.control(), tooltip = "Ungroup selected groups")

    val VIEW = define("view", "View")
    val VIEW_SHOW_MENUBAR = define("view_show_menubar", "Show Menubar", Key.M.control())
    val VIEW_SHOW_FIXED_TOOLBAR = define("view_show_fixed_toolbar", "Show Fixed Toolbar")
    val VIEW_SHOW_STATUS_BAR = define("view_show_status_bar", "Show Status Bar")
    val VIEW_DOCKS = define("view_docks", "Tool Windows")

    val DOCK_DEBUG = define("dock_debug", "Debug...", Key.D.control().shift()).apply {
        tinted = true
    }
    val DOCK_LAYERS = define("dock_layers", "Layers...", Key.L.control().shift()).apply {
        tinted = true
    }
    val DOCK_NODE_INSPECTOR = define("dock_node_inspector", "Node Inspector...", Key.N.control().shift()).apply {
        tinted = true
    }

    val VIEW_ZOOM = define("zoom", "Zoom")
    val ZOOM_RESET = define("zoom_reset", "Reset Zoom", Key.DIGIT_0.noMods())
    val ZOOM_IN = define("zoom_in", "Zoom In", Key.EQUALS.maybeShift())
    val ZOOM_OUT = define("zoom_out", "Zoom Out", Key.MINUS.noMods())
    val THEME_DARK = define( "theme_dark", "Dark Theme" )

    val LAYER = define("layer", "Layer")
    val LAYER_HIDE_SHOW = define("layer_toggle_visibility", "Layer Hide/Show").apply {
        iconName = "visible"
        alternateIconName = "hidden"
    }
    val LAYER_HIDE = define("layer_hide", "Hide Layer" )
    val LAYER_SHOW = define("layer_show", "Show Layer")
    val LAYER_LOCK_UNLOCK = define("layer_toggle_lock", "Layer Lock/Unlock").apply {
        iconName = "locked"
        alternateIconName = "unlocked"
    }
    val LAYER_LOCK = define("layer_lock", "Lock Layer")
    val LAYER_UNLOCK = define("layer_unlock", "Unlock Layer")
    val LAYER_NEW = define("layer_new", "New Layer")
    val LAYER_DELETE = define("layer_delete", "Delete Layer")
    val LAYER_RENAME = define("layer_rename", "Rename Layer...")
    val LAYER_RAISE = define("layer_raise", "Raise Layer", tooltip = "Lower the current layer")
    val LAYER_LOWER = define("layer_lower", "Lower Layer", tooltip = "Lower the current layer")
    val LAYER_TOP = define("layer_top", "Layer to Top", tooltip = "Raise the current layer to the top")
    val LAYER_BOTTOM = define("layer_bottom", "Layer to Bottom", tooltip = "Lower the current layer to the bottom")

    val MODE_SELECT = define("mode_select", "Select Mode", Key.S.noMods(), tooltip = "Select and transform objects")
    val MODE_RECTANGLE = define("mode_rectangle", "Rectangle Mode", Key.R.noMods(), tooltip = "Create rectangles and squares")
    val MODE_POLYGON = define("mode_polygon", "Polygon Mode", Key.DIGIT_8.shift(), tooltip = "Create polygons and stars")
    val MODE_ELLIPSE = define("mode_ellipse", "Ellipse Mode", Key.E.noMods(), tooltip = "Create circles, ellipses and arcs")
    val MODE_PATH = define("mode_path", "Path Mode", Key.P.noMods(), tooltip = "Create lines, bezier curves and arcs")
    val MODE_LINE = define("mode_line", "Line Mode", Key.L.noMods(), tooltip = "Create lines")
    val MODE_CURVE = define("mode_curve", "Curve Mode", Key.C.noMods(), tooltip = "Create curves")

    val DRAW_END = define("drawing_end", "End Drawing", Key.ENTER.noMods())
    val DRAW_RECTANGLE = define("draw_rectangle", "Rectangle", tooltip = "Draw rectangles / squares")
    val DRAW_DIAMOND = define("draw_diamond", "Diamond", tooltip = "Draw diamonds")
    val DRAW_PARALLELOGRAM = define("draw_parallelogram", "Parallelogram", tooltip = "Draw parallelograms")
    val DRAW_POLYGON = define("draw_polygon", "Polygon", tooltip = "Draw regular polygons")
    val DRAW_STAR = define("draw_star", "Star", tooltip = "Draw stars")
    val DRAW_CENTERED = define("draw_centered", "Centered", tooltip = "Defined by its center and size")
    val DRAW_CORNER_BASED = define("draw_corner_based", "Corner Based", tooltip = "Defined by its corners")
    val DRAW_ELLIPSE_WHOLE = define("draw_ellipse_whole", "Whole Ellipse", tooltip = "Make the shape a whole ellipse")
    val DRAW_ELLIPSE_SLICE = define("draw_ellipse_slice", "Slice", tooltip = "Switch to slice (closed shape with two radii)")
    val DRAW_ELLIPSE_CHORD = define("draw_ellipse_chord", "Chord", tooltip = "Switch to chord (closed shape)")
    val DRAW_ELLIPSE_ARC = define("draw_ellipse_arc", "Arc", tooltip = "Switch to arc (unclosed shape)")

    val SNAP_TO_GRID_TOGGLE = define("snapGridToggle", "Snap to Grid").apply {
        // The Pound/Hash key
        additionalKeyCombinations.add(Key.WORLD_1.control()) // UK Keyboards (left of the Enter Key)
        additionalKeyCombinations.add(Key.DIGIT_3.shift()) // US keyboards
    }

    val SNAP_TO_GUIDES_TOGGLE = define("snapGuidesToggle", "Snap to Guides", Key.G.control())
    val SNAP_TO_OTHERS_TOGGLE = define("snapOthersToggle", "Snap to Others", Key.O.control())
    val SNAP_ROTATION_TOGGLE = define("snapRotationToggle", "Snap to Rotation", Key.R.control())

    val TAB_CLOSE = define("tab_close", "Close Tab", Key.W.control())

    val DEBUG = define( "debug", "Debug" )
    val DEBUG_DUMP_SCENE = define( "debug_dump_scene", "Dump Scene")

}
