/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial

import uk.co.nickthecoder.glok.action.Commands
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.ToolBar
import uk.co.nickthecoder.glok.control.WrappedNode
import uk.co.nickthecoder.vectorial.core.shape.EllipseType

class EllipseToolBar : WrappedNode<ToolBar>(ToolBar()) {

    private val commands = Commands().apply {
        radioChoices(VectorialSettings.ellipseTypeProperty) {
            with( VectorialActions) {
                choice(DRAW_ELLIPSE_WHOLE, EllipseType.WHOLE)
                choice(DRAW_ELLIPSE_SLICE, EllipseType.SLICE)
                choice(DRAW_ELLIPSE_ARC, EllipseType.ARC)
                choice(DRAW_ELLIPSE_CHORD, EllipseType.CHORD)
            }
        }
    }

    init {
        id = "EllipseToolBar"

        commands.build(VectorialSettings.defaultIconSizeProperty) {

            with(VectorialActions) {
                inner.apply {
                    + Label("Type")
                    + propertyRadioButton2(DRAW_ELLIPSE_WHOLE)
                    + propertyRadioButton2(DRAW_ELLIPSE_SLICE)
                    + propertyRadioButton2(DRAW_ELLIPSE_ARC)
                    + propertyRadioButton2(DRAW_ELLIPSE_CHORD)
                }
            }
        }
        commands.attachTo(this)
    }
}
