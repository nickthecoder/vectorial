/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.control

import uk.co.nickthecoder.glok.control.TextField
import uk.co.nickthecoder.glok.event.Key
import uk.co.nickthecoder.glok.event.KeyEvent
import uk.co.nickthecoder.glok.property.boilerplate.floatProperty
import uk.co.nickthecoder.vectorial.core.expression.Expression
import kotlin.math.roundToInt

abstract class NumericExpressionView<V : Any, E : Expression<V>> : ExpressionView<V, E>() {

    /**
     * How much to increment/decrement the x or y values when the `up` and `down` keys are pressed, while `ctrl` is NOT held down.
     */
    val smallStepProperty by floatProperty(1f)
    var smallStep by smallStepProperty

    /**
     * How much to increment/decrement the x or y values when the `up` and `down` keys are pressed, while `ctrl` is held down.
     */
    val largeStepProperty by floatProperty(10f)
    var largeStep by largeStepProperty


    protected fun filterKeyDown(textField: TextField, event: KeyEvent) {
        if (event.key == Key.UP || event.key == Key.DOWN) {
            val step = if (event.isControlDown) largeStep else smallStep
            val current = textField.text.toFloatOrNull() ?: return

            var newValue = if (event.key == Key.DOWN) current - step else current + step
            if (step >= 1f) newValue = newValue.roundToInt().toFloat()
            textField.text = newValue.toString()
            event.consume()
        }
    }

}
