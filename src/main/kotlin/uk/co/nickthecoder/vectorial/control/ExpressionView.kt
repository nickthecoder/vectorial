/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.control

import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.control.HBox
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.ImageView
import uk.co.nickthecoder.glok.scene.Node
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.vectorial.Vectorial
import uk.co.nickthecoder.vectorial.boilerplate.optionalExpressionProperty
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.util.DualListenerGuard


/**
 * A JavaFX node, showing the value of an [Expression] as [TextField]s etc.
 * If [expression] is an [Attribute] or [Variable], whose expression is a [Constant],
 * then the text fields are editable. Otherwise, they aren't, and a `break` button is enabled,
 * which when pressed, changes the [Attribute]'s or [Variable]'s expression into a [Constant]
 * (replacing the previous expression).
 *
 * If [expression] is not an [Attribute] or [Variable], then the text fields are always disabled,
 * and the break button is not displayed.
 */
abstract class ExpressionView<V : Any, E : Expression<V>> : WrappedNode<HBox>(HBox()) {

    /**
     * A label before the expressions value. May be blank.
     */
    val nameProperty by stringProperty("")
    var name by nameProperty

    /**
     * The [Expression] to be edited/viewed.
     */
    val expressionProperty by optionalExpressionProperty(null)
    protected var expression by expressionProperty

    private var valueListener: ExpressionListener<V>? = null

    protected val nameLabel = Label("").apply {
        textProperty.bindTo(nameProperty)
        visibleProperty.bindTo(nameProperty.isNotNull())
    }

    protected val breakButton = Button("X").apply {
        visible = false
        if (breakConnectionIcon != null) {
            graphic = ImageView(breakConnectionIcon)
            text = ""
        }
        // TODO tooltip = Tooltip("Break Connection")
        onAction {
            expression?.let { exp ->
                if (exp is Vector2Attribute) {
                    exp.set(Vector2Constant(exp.eval()))
                }
            }
        }
    }

    val expressionListener = expressionProperty.addChangeListener { _, _, exp ->
        @Suppress("UNCHECKED_CAST" )
        exp as Expression<V>?
        valueListener?.removeListener()
        if (exp == null) {
            valueListener = null
        } else {
            valueListener = ExpressionListener<V>(exp) { guard.updateGUI { updateGUI() } }
        }
        guard.updateGUI { updateGUI() }
    }

    protected val guard = DualListenerGuard()

    init {
        style("expression_view")
        inner.alignment = Alignment.CENTER_LEFT
        inner.spacing = 6f
        inner.children.add(nameLabel)
    }

    protected fun enableControls(vararg nodes: Node) {
        val exp = expression
        val inner =
            if (exp is Attribute<*, *>) exp.expression else if (exp is Variable<*, *>) exp.innerExpression else null

        val isModifiable = inner is Constant

        for (node in nodes) {
            if (node is HasDisabled) {
                node.disabled = !isModifiable
            } else if (node is HasReadOnly) {
                node.readOnly = !isModifiable
            }
        }

        if (exp is Attribute<*, *>) {
            breakButton.disabled = isModifiable
            breakButton.visible = true
        } else {
            breakButton.visible = false
        }
    }

    abstract fun updateGUI()
    abstract fun updateModel()

    companion object {
        val breakConnectionIcon by lazy {
            Vectorial.icons.get("break_connection", 24)
        }
    }
}

