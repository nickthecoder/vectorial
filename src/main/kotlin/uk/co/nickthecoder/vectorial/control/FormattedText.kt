/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.control

import uk.co.nickthecoder.glok.control.HBox
import uk.co.nickthecoder.glok.control.Text
import uk.co.nickthecoder.glok.control.WrappedNode
import uk.co.nickthecoder.glok.property.boilerplate.ObservableString
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty

/**
 * A HBox, composed of a series of [Text] built from the value of [text],
 * which may contain square brackets to indicate that the text should be bold.
 *
 * e.g. The word Hello is bold :
 *
 *     "[Hello] World"
 *
 * This is used for the `tips` in the status-bar, which I've shamelessly copied from Inkscape,
 * which uses `bold` for actions such as `Drag`, `Shift-Click` etc. with the rest of the text
 * being plain (not bold).
 */
class FormattedText( text : String ): WrappedNode<HBox>(HBox()) {

    constructor( textProperty : ObservableString) : this( "" ) {
        this.textProperty.bindTo(textProperty)
    }

    val textProperty by stringProperty(text)
    var text by  textProperty 
    private val textListener = textProperty.addChangeListener { _, _, newValue -> update(newValue) }

    init {
        update(text)
    }

    private fun update(fromIndex: Int, str: String) {
        val openIndex = str.indexOf("[", fromIndex)
        if (openIndex >= 0) {
            val closeIndex = str.indexOf("]", fromIndex + 1)
            if (closeIndex > 0) {
                if (openIndex != fromIndex) {
                    inner.children.add(Text(str.substring(fromIndex, openIndex)))
                }
                inner.children.add(
                    Text(str.substring(openIndex+1, closeIndex)).apply {
                        styles.add(".bold" )
                    }
                )
                update(closeIndex + 1, str)
                return
            }
        }
        val remainder = str.substring(fromIndex)
        if (remainder.isNotBlank()) {
            inner.children.add(Text(remainder))
        }
        children.lastOrNull()?.let {
            shrinkPriority = 1f
        }
    }

    private fun update( text : String ) {
        inner.children.clear()
        update(0, text)
    }

}

fun formattedText( text : String, block : FormattedText.() -> Unit ) = FormattedText(text).apply(block)
fun formattedText( textProperty : ObservableString, block : FormattedText.() -> Unit ) = FormattedText(textProperty).apply(block)
