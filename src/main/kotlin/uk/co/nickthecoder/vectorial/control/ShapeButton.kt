/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.control

import uk.co.nickthecoder.glok.scene.dsl.menuItem
import uk.co.nickthecoder.glok.scene.dsl.splitMenuButton
import uk.co.nickthecoder.vectorial.core.change.SelectionChange
import uk.co.nickthecoder.vectorial.core.shape.Shape

fun shapeButton(shape: Shape) = splitMenuButton(shape.name) {
    + menuItem( "Select" ) {
        onAction {
            shape.select()
        }
    }
    + menuItem( "Remove From Selection" ) {
        onAction { shape.removeFromSelection() }
    }
    + menuItem( "Delete" ) {
        onAction {
            shape.delete()
        }
    }
    // TODO Copy, Cut, Duplicated ...

    onAction { shape.select() }
}
