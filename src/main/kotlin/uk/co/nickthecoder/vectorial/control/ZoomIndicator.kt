/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.control

import uk.co.nickthecoder.glok.control.FloatSpinner
import uk.co.nickthecoder.glok.control.HBox
import uk.co.nickthecoder.glok.control.WrappedNode
import uk.co.nickthecoder.glok.property.boilerplate.FloatProperty
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.label
import uk.co.nickthecoder.glok.util.Converter

class ZoomIndicator(zoomProperty: FloatProperty): WrappedNode<HBox>(HBox()) {

    private val percentageConverter = object : Converter<Float,Float> {
        override fun forwards(value: Float) = value / 100f
        override fun backwards(value: Float) = value * 100f
    }

    private val floatSpinner = FloatSpinner().apply {
        valueProperty.bidirectionalBind(zoomProperty, percentageConverter)
        smallStep = 10f
        largeStep = 100f
        editor.prefColumnCount = 4
        min = 10f
        max = 1000f
    }

    init {
        with(inner) {
            spacing = 4f
            alignment = Alignment.TOP_CENTER
            fillHeight = true
            + label("Z %") {
               tooltip = TextTooltip( "Zoom" )
            }
            + floatSpinner
        }
    }
}
