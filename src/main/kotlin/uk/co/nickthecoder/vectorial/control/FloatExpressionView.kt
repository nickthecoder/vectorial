/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.control

import uk.co.nickthecoder.glok.control.TextField
import uk.co.nickthecoder.glok.event.EventType
import uk.co.nickthecoder.vectorial.core.expression.Expression
import uk.co.nickthecoder.vectorial.core.expression.FloatAttribute
import uk.co.nickthecoder.vectorial.core.expression.FloatConstant
import uk.co.nickthecoder.vectorial.core.expression.FloatExpression

/**
 * A node, showing the value of a [FloatExpression] as a [TextField].
 * If [expression] is a [FloatAttribute], whose expression is a [FloatConstant],
 * then the text field is editable. Otherwise, they aren't, and a `break` button is enabled,
 * which when pressed, changes the [FloatAttribute]'s expression into a [FloatConstant]
 * (replacing the previous expression).
 *
 * If [expression] is not a [FloatAttribute], then the text field is always disabled,
 * and the break button is not displayed.
 */
class FloatExpressionView : NumericExpressionView<Float, FloatExpression>() {

    var floatExpression: Expression<Float>?
        @Suppress("UNCHECKED_CAST")
        get() = expression as Expression<Float>
        set(v) {
            expression = v
        }

    private val floatField = TextField("")

    private val textListener = floatField.textProperty.addChangeListener { _, _, _ ->
        guard.updateModel { updateModel() }
    }

    init {
        styles.add("float_expression")

        floatField.prefColumnCount = 6

        inner.children.addAll( floatField, breakButton )

        floatField.addEventFilter(EventType.KEY_PRESSED) { filterKeyDown(floatField, it) }
    }

    /**
     * Called when the [expression]'s value changes, or if the [expression] is replaced by a different one.
     */
    override fun updateGUI() {
        floatField.text = expression?.eval()?.toString() ?: ""
        enableControls(floatField)
    }

    override fun updateModel() {
        val exp = expression as? FloatAttribute ?: return
        val value = floatField.text.toFloatOrNull() ?: return
        exp.set(FloatConstant(value))
    }

}
