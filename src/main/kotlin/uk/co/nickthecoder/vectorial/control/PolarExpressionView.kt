/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.control

import uk.co.nickthecoder.glok.control.Text
import uk.co.nickthecoder.glok.control.TextField
import uk.co.nickthecoder.glok.event.EventType
import uk.co.nickthecoder.vectorial.core.Angle
import uk.co.nickthecoder.vectorial.core.Polar
import uk.co.nickthecoder.vectorial.core.expression.Expression
import uk.co.nickthecoder.vectorial.core.expression.PolarAttribute
import uk.co.nickthecoder.vectorial.core.expression.PolarConstant
import uk.co.nickthecoder.vectorial.core.expression.PolarExpression

/**
 * A JavaFX node, showing the value of a [PolarExpression] as two [TextField]s.
 * If [expression] is a [PolarAttribute], whose expression is a [PolarConstant],
 * then the text fields are editable. Otherwise, they aren't, and a `break` button is enabled,
 * which when pressed, changes the [PolarAttribute]'s expression into a [PolarConstant]
 * (replacing the previous expression).
 *
 * If [expression] is not a [PolarAttribute], then the text fields are always disabled,
 * and the break button is not displayed.
 */
class PolarExpressionView : NumericExpressionView<Polar, PolarExpression>() {

    var polarExpression: Expression<Polar>?
        @Suppress("UNCHECKED_CAST") get() = expression as Expression<Polar>?
        set(v) {
            expression = v
        }

    private val magnitudeField = TextField("")
    private val angleField = TextField("")
    private val separatorLabel = Text(",")

    private val mafListener = magnitudeField.textProperty.addChangeListener { _, _, _ ->
        guard.updateModel { updateModel() }
    }
    private val angleListener = angleField.textProperty.addChangeListener { _, _, _ ->
        guard.updateModel { updateModel() }
    }

    init {
        styles.add("polar_expression")

        magnitudeField.prefColumnCount = 6
        angleField.prefColumnCount = 6

        inner.children.addAll(magnitudeField, separatorLabel, angleField, breakButton)

        magnitudeField.addEventFilter(EventType.KEY_PRESSED) { filterKeyDown(magnitudeField, it) }
        angleField.addEventFilter(EventType.KEY_PRESSED) { filterKeyDown(angleField, it) }
    }

    /**
     * Called when the [expression]'s value changes, or if the [expression] is replaced by a different one.
     */
    override fun updateGUI() {
        val newValue = polarExpression?.eval()

        magnitudeField.text = newValue?.magnitude?.toString() ?: ""
        angleField.text = newValue?.angle?.degrees?.toString() ?: ""
        enableControls(magnitudeField, angleField)
    }

    override fun updateModel() {
        val exp = expression as? PolarAttribute ?: return
        val magnitude = magnitudeField.text.toFloatOrNull() ?: return
        val degrees = angleField.text.toDoubleOrNull() ?: return
        exp.set(PolarConstant(Polar(magnitude, Angle.degrees(degrees))))
    }

}

