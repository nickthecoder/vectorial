/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.control

import uk.co.nickthecoder.glok.control.Text
import uk.co.nickthecoder.glok.control.VBox
import uk.co.nickthecoder.glok.property.ChangeListener
import uk.co.nickthecoder.glok.property.boilerplate.ObservableFloat
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.hBox
import uk.co.nickthecoder.glok.scene.dsl.text
import uk.co.nickthecoder.vectorial.MainWindow
import java.text.DecimalFormat

/**
 * Used to show the position of the mouse in the [MainWindow]'s status bar.
 */
class PositionIndicator(

    mouseXProperty: ObservableFloat,
    mouseYProperty: ObservableFloat,
    val format: DecimalFormat = DecimalFormat("0.0")

) : VBox() {

    private val listeners = mutableListOf<Any>()
    private fun ChangeListener<*, *>.remember() {
        listeners.add(this)
    }

    init {
        tooltip = TextTooltip( "Position" )
        fillWidth = true
        + hBox {
            spacing = 5f
            + Text("X:")
            + text("0") {
                alignment = Alignment.CENTER_RIGHT
                overridePrefWidth = 60f
                mouseXProperty.addChangeListener { _, _, value ->
                    text = format.format(value)
                }.remember()
                textProperty.addListener(requestRedrawListener)
                textProperty.removeListener(requestLayoutListener)
            }
        }

        + hBox {
            spacing = 5f
            + Text("Y:")
            + text("0") {
                alignment = Alignment.CENTER_RIGHT
                overridePrefWidth = 60f
                mouseYProperty.addChangeListener { _, _, value ->
                    text = format.format(value)
                }.remember()
                textProperty.addListener(requestRedrawListener)
                textProperty.removeListener(requestLayoutListener)
            }
        }
    }
}
