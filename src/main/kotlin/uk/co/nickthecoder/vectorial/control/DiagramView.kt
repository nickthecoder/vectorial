/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.control

import org.lwjgl.opengl.GL11.GL_SCISSOR_TEST
import org.lwjgl.opengl.GL11.glDisable
import uk.co.nickthecoder.glok.application.Platform
import uk.co.nickthecoder.glok.backend.backend
import uk.co.nickthecoder.glok.collections.asObservableList
import uk.co.nickthecoder.glok.control.*
import uk.co.nickthecoder.glok.event.Event
import uk.co.nickthecoder.glok.event.MouseEvent
import uk.co.nickthecoder.glok.property.boilerplate.*
import uk.co.nickthecoder.glok.property.functions.plus
import uk.co.nickthecoder.glok.scene.Side
import uk.co.nickthecoder.glok.scene.TextTooltip
import uk.co.nickthecoder.glok.scene.dsl.toggleMenuItem
import uk.co.nickthecoder.glok.util.Converter
import uk.co.nickthecoder.vectorial.boilerplate.*
import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.*
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.shape.*
import uk.co.nickthecoder.vectorial.renderer.Canvas
import uk.co.nickthecoder.vectorial.renderer.Rect
import uk.co.nickthecoder.vectorial.renderer.renderer
import kotlin.math.abs
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt


/**
 * Displays a [Diagram], and allows the user to edit it.
 *
 * ## Implementation Details
 *
 * A DiagramView is composed of four parts in a 2x2 grid structure :
 *
 * * [corner] , [hRuler]
 * * [vRuler] , [scrollPane]
 *
 * The [scrollPane]'s content is _virtual_ - it isn't drawn, and instead, we draw the visible part of diagram
 * programmatically inside [drawChildren].
 *
 * Here's an ASCII art diagram explaining various X coordinates.
 * Exactly the same logic applies to the Y values.
 *
 *
 *     0 (pixels)
 *     |<----------------------scrollableWidth----------------------->| (pixels)
 *                          0 (world)
 *                          |<---diagramWidth--->|                      (world)
 *     |<---diagramWidth--->|                    |<---diagramWidth--->| (world)
 *
 *              |<-------visibleWidth------->|                          (pixels)
 *              |              |
 *              |           centerX                                     (world units)
 *              |
 *     |<------>| scrollPane.hScrollValue                               (pixels)
 *
 * [scrollableWidth] is the width (in Glok pixels) of the scrollBar's content ([fakeContent]).
 * It is always 3 times wider than the diagram.
 * The zoom factor does not affect the area available to edit (if we scroll around).
 *
 * [visibleWidth] is the width (in Glok pixels) of the viewport
 *
 * So, if we change the zoom, or resize the window, [visibleWidth] will change,
 * but [scrollableWidth] and [diagramWidth] stay the same.
 * FYI, when changing zoom in/out via shortcuts, menus, or buttons, [centerX] remains constant.
 * When zooming by double-clicking, (not implemented yet), [centerX] will also change.
 *
 * To convert a between widths in pixels and world coordinates :
 *
 * * pixelWidth = worldWidth * [zoom]
 * * worldWidth = pixelWidth / [zoom]
 *
 * As the two `zero` coordinates are offset from each other by [diagramWidth] (in Glok pixels), we can
 * convert between positions in pixels and positions world coordinates by :
 *
 * * pixelX = ([diagramWidth] + worldX) * [zoom]
 * * worldX = pixelX / [zoom] - [diagramWidth]
 *
 * See [contentToWorldX], [worldToContentX].
 */

class DiagramView(val diagram: Diagram, initialMultiSample: Int = 4) : Region(), DocumentListener {

    // region ==== Properties ====

    /**
     * The position of the mouse in pixel coordinates relative to [fakeContent].
     */
    private val contentMouseXProperty = SimpleFloatProperty(0f)
    private var contentMouseX by contentMouseXProperty

    /**
     * The position of the mouse in world coordinates relative to [fakeContent]..
     */
    private val contentMouseYProperty = SimpleFloatProperty(0f)
    private var contentMouseY by contentMouseYProperty

    private val dirtyProperty = SimpleBooleanProperty(true)
    private var dirty by dirtyProperty
    private val dirtyListener = dirtyProperty.addChangeListener { _, _, isDirty ->
        if (isDirty) requestRedraw()
    }

    val scaleProperty by floatProperty(1f)
    var scale by scaleProperty
    private val scaleListener = scaleProperty.addListener { dirty = true }

    /**
     * Used to anti-alias the diagram. A value of 1 will be quick, but edges will be pixelated.
     */
    val multiSampleProperty by intProperty(initialMultiSample)
    var multiSample by multiSampleProperty
    private val multiSampleListener = multiSampleProperty.addListener{ dirty = true }

    /**
     * The scaling factor for the view.  1 = No Zoom, 2 = Twice as big as normal etc.
     */
    val zoomProperty by floatProperty(1f)
    var zoom by zoomProperty
    private val zoomListener = zoomProperty.addListener {
        // Keep the center of the view the same.
        scrollPane.hScrollValue = centerXToHScrollValueConverter.forwards(centerX)
        scrollPane.vScrollValue = centerYToVScrollValueConverter.forwards(centerY)
        dirty = true
    }

    val toolTypeProperty by toolTypeProperty(ToolType.SELECT)
    var toolType by toolTypeProperty
    private val toolTypeListener = toolTypeProperty.addChangeListener{ _, _, newValue ->
        if (tool.toolType != newValue) {
            when (newValue) {
                ToolType.NONE -> tool = SelectTool()
                ToolType.SELECT -> tool = SelectTool()
                ToolType.RECTANGLE -> tool = QuadrilateralTool()
                ToolType.POLYGON -> tool = PolygonTool()
                ToolType.ELLIPSE -> tool = EllipseTool()
                ToolType.PATH -> tool = PathTool()
                ToolType.LINE -> tool = LineTool()
                ToolType.CURVE -> tool = SelectTool() // TODO CurveTool()
                else -> {
                    warn("Tool $toolType not currently implemented!")
                    tool = SelectTool()
                }
            }
        }
    }


    /**
     * Help for the user (for the status bar) on how to use the currently selected tool.
     */
    private val mutableTipProperty by stringProperty("")
    val tipProperty = mutableTipProperty.asReadOnly()
    var tip by mutableTipProperty

    /**
     * The border color around `drag handles`. (default=black)
     */
    val handleBorderColorProperty by stylableColorProperty(Colors["#000"])
    var handleBorderColor by handleBorderColorProperty

    val fixedHandleColorProperty by stylableColorProperty(Colors["#d00"])
    var fixedHandleColor by fixedHandleColorProperty


    /**
     * The fill color of `drag handles`, when not selected. (default=white)
     */
    val handleColorProperty by stylableColorProperty(Colors["#fff"])
    var handleColor by handleColorProperty

    /**
     * The fill color of selected `drag handles`. (default=red)
     */
    val selectedHandleColorProperty by stylableColorProperty(Colors["#c00"])
    var selectedHandleColor by selectedHandleColorProperty

    /**
     * The color of the bounding box around selected shapes. (default=red).
     *
     * See [selectionLineThickness].
     */
    val selectionColorProperty by stylableColorProperty(Colors["#f88"])
    var selectionColor by selectionColorProperty

    /**
     * The color of the bounding box around selected shapes when the mouse is hovering, or dragging. (default=green).
     *
     * See [selectionLineThickness].
     */
    val hoverColorProperty by stylableColorProperty(Colors["#8f8"])
    var hoverColor by hoverColorProperty

    /**
     * The thin page border (default=grey)
     */
    val pageBorderColorProperty by stylableColorProperty(Colors["#888"])
    var pageBorderColor by pageBorderColorProperty

    /**
     * The background color outside the diagram's bounds.
     */
    val diagramBackgroundColorProperty by stylableColorProperty(Colors["#ddd"])
    var diagramBackgroundColor by diagramBackgroundColorProperty

    /**
     * The background color of the diagram.
     */
    val paperColorProperty by stylableColorProperty(Colors["#fff"])
    var paperColor by paperColorProperty

    /**
     * The thickness of the bounding rectangle around selected shapes.
     *
     * See [selectionColorProperty]
     */
    val selectionLineThicknessProperty by stylableFloatProperty(2f)
    var selectionLineThickness by selectionLineThicknessProperty

    /**
     * The thickness of construction lines and horizontal/vertical guides.
     */
    val constructionLineThicknessProperty by stylableFloatProperty(1f)
    var constructionLineThickness by constructionLineThicknessProperty

    /**
     * Half the width/height of `drag handles`.
     *
     * See [handleColorProperty] and [selectedHandleColorProperty]
     */
    val handleSizeProperty by stylableFloatProperty(6f)
    var handleSize by handleSizeProperty


    /**
     * When clicking on thin lines, how far away can you be (in pixels), and still select the shape.
     */
    val lineThresholdProperty by stylableFloatProperty(4f)
    var lineThreshold by lineThresholdProperty

    /**
     * When clicking on thin lines, how far away can you be (in pixels), and still select the shape.
     */
    val handleThresholdProperty by stylableFloatProperty(10f)
    var handleThreshold by handleThresholdProperty

    val combineBoundingBoxesProperty by booleanProperty(true)
    var combineBoundingBoxes by combineBoundingBoxesProperty

    val polygonSidesProperty by intProperty(6)
    var polygonSides by polygonSidesProperty

    val polygonTypeProperty by polygonTypeProperty(PolygonType.POLYGON)
    var polygonType by polygonTypeProperty

    val quadrilateralTypeProperty by quadrilateralTypeProperty(QuadrilateralType.RECTANGLE)
    var quadrilateralType by quadrilateralTypeProperty

    val ellipseTypeProperty by ellipseTypeProperty(EllipseType.WHOLE)
    var ellipseType by ellipseTypeProperty

    // endregion properties

    // region ==== Fields ====

    private val document = diagram.document

    private val history = document.history

    private val selection: Selection get() = diagram.selection

    private val diagramWidthProperty = SimpleFloatProperty(diagram.width())
    private val diagramWidth by diagramWidthProperty
    private val diagramHeightProperty = SimpleFloatProperty(diagram.height())
    private val diagramHeight by diagramHeightProperty

    /**
     * The width of the scrollable area (in glok pixels) - three times the width of the diagram.
     */
    private val scrollableWidthProperty = FloatBinaryFunction(diagramWidthProperty, zoomProperty) { width, zoom ->
        width * zoom * 3
    }
    private val scrollableWidth by scrollableWidthProperty

    /**
     * The height of the scrollable area (in glok pixels) - three times the height of the diagram.
     */
    private val scrollableHeightProperty = FloatBinaryFunction(diagramHeightProperty, zoomProperty) { width, zoom ->
        width * zoom * 3
    }
    private val scrollableHeight by scrollableHeightProperty

    /**
     * The width of the viewport. Set by [layoutChildren]
     */
    private val visibleWidthProperty = SimpleFloatProperty(1f)
    private var visibleWidth by visibleWidthProperty
    private val visibleWidthListener = visibleWidthProperty.addListener {
        // Keep the center of the view the same.
        scrollPane.hScrollValue = centerXToHScrollValueConverter.forwards(centerX)
    }


    /**
     * The height of the viewport. Set by [layoutChildren]
     */
    private val visibleHeightProperty = SimpleFloatProperty(1f)
    private var visibleHeight by visibleHeightProperty
    private val visibleHeightListener = visibleHeightProperty.addListener {
        // Keep the center of the view the same.
        scrollPane.vScrollValue = centerYToVScrollValueConverter.forwards(centerY)
    }

    // region ==== Children ====

    private val fakeContent = Region().apply {
        overridePrefWidthProperty.bindCastTo(scrollableWidthProperty)
        overridePrefHeightProperty.bindCastTo(scrollableHeightProperty)
        onMouseClicked { attempt(it) { mouseClicked(it) } }
        onMousePressed { attempt(it) { mousePressed(it) } }
        onMouseMoved { attempt(it) { mouseMoved(it) } }
        onMouseDragged { attempt(it) { mouseDragged(it) } }
        onMouseReleased { attempt(it) { mouseReleased(it) } }
    }

    private val scrollPane = ScrollPane(fakeContent)

    private val scrollPaneListener = scrollPane.hScrollValueProperty.addListener{ dirty = true }.apply {
        scrollPane.vScrollValueProperty.addListener(this)
    }

    private val hRuler = Ruler().apply {
        tooltip = TextTooltip( "Drag to add a horizontal guide" )
        side = Side.TOP
        lowerBoundProperty.bindTo(contentToWorldX(scrollPane.hScrollValueProperty))
        upperBoundProperty.bindTo(contentToWorldX(scrollPane.hScrollValueProperty + visibleWidthProperty))
        onMousePressed { dragRuler(it, isVertical = false) }
        onMouseDragged { mouseDragged(it) }
        onMouseReleased { mouseReleased(it) }
    }

    private val vRuler = Ruler().apply {
        tooltip = TextTooltip( "Drag to add a vertical guide" )
        side = Side.LEFT
        lowerBoundProperty.bindTo(contentToWorldY(scrollPane.vScrollValueProperty))
        upperBoundProperty.bindTo(contentToWorldY(scrollPane.vScrollValueProperty + visibleHeightProperty))
        onMousePressed { dragRuler(it, isVertical = true) }
        onMouseDragged { mouseDragged(it) }
        onMouseReleased { mouseReleased(it) }
    }

    private val corner = Pane().apply {
        backgroundProperty.bindTo( hRuler.backgroundProperty )
    }

    override val children = listOf(corner, hRuler, vRuler, scrollPane).asObservableList()

    // endregion children

    /**
     * The world point at the center of the view.
     */
    val centerXProperty by floatProperty(0f)
    var centerX by centerXProperty

    /**
     * Converts [centerX] (in world units) to [ScrollPane.hScrollValue] (Glok pixels).
     * backwards : Converts [ScrollPane.hScrollValue] to [centerX].
     */
    private val centerXToHScrollValueConverter = object : Converter<Float, Float> {
        override fun forwards(value: Float): Float {
            return worldToContentX(value) - visibleWidth / 2
        }
        override fun backwards(value: Float): Float {
            return contentToWorldX(value + visibleWidth / 2)
        }
    }

    // BEWARE! When visibleWidth or zoom is changed, neither properties are updated automatically,
    // so the scrollValue should be updated manually in visibleWidth and zoom listeners.
    private val centerXBind = centerXProperty.bidirectionalBind(scrollPane.hScrollValueProperty, centerXToHScrollValueConverter)

    /**
     * The world point at the center of the view.
     */
    val centerYProperty by floatProperty(0f)
    var centerY by centerYProperty

    /**
     * [centerYBind] is a bidirectional bind between [centerY] and [ScrollPane.vScrollValue].
     * It uses this converter
     * Converts [centerY] (in world units) to [ScrollPane.vScrollValue] (Glok pixels).
     * backwards : Converts [ScrollPane.vScrollValue] to [centerY].
     */
     private val centerYToVScrollValueConverter = object : Converter<Float, Float> {
        override fun forwards(value: Float): Float {
            return worldToContentY(value) - visibleHeight / 2
        }
        override fun backwards(value: Float): Float {
            return contentToWorldY(value + visibleHeight / 2)
        }
    }

    // BEWARE! When visibleWidth or zoom are changed, neither properties are updated automatically, so the scrollValue
    // should be updated manually in visibleWidth and zoom listeners.
    private val centerYBind = centerYProperty.bidirectionalBind(scrollPane.vScrollValueProperty, centerYToVScrollValueConverter)


    /**
     * How much error is allowed when clicking on a line. (in world units).
     * This is dependent on the [DiagramView.zoom] facter, as well as [DiagramView.lineThreshold].
     */
    private val zoomedLineThresholdProperty = FloatBinaryFunction(lineThresholdProperty, zoomProperty) { thresh, zoom ->
        thresh / zoom
    }
    private val zoomedLineThreshold by zoomedLineThresholdProperty

    private val zoomedLineThreshold2Property = FloatUnaryFunction(zoomedLineThresholdProperty) { foo -> foo * foo }
    private val zoomedLineThreshold2 by zoomedLineThreshold2Property

    private val zoomedControlPointThreshold2Property =
        FloatBinaryFunction(lineThresholdProperty, zoomProperty) { thresh, zoom ->
            val dist = thresh / zoom
            dist * dist
        }
    private val zoomedControlPointThreshold2 by zoomedControlPointThreshold2Property

    private var tool: Tool = SelectTool()
        set(v) {
            hoverOverShape = null
            hoverOverEdge = null
            hoverOverHandle = null
            history.abortBatch()
            field.end()
            field = v
            v.begin()
            toolType = v.toolType
            redraw()
            tip = v.tip()
        }

    private var dragging = false
    private var dragStartWorld = Vector2(0f, 0f)

    private var handles = mutableListOf<Handle>()

    /**
     * In some [Tool]s, when you hover over a [Handle],
     */
    private var hoverOverHandle: Handle? = null
        set(v) {
            if (field != v) {
                field = v
                redraw()
            }
        }

    private var hoverOverShape: Shape? = null
        set(v) {
            if (field != v) {
                field = v
                redraw()
            }
        }
    private var hoverOverEdge: Edge? = null
        set(v) {
            if (field != v) {
                field = v
                redraw()
            }
        }

    private var combinedBoundingBox: BoundingBox? = null

    /**
     * Null when not dragging a ruler. True for a vertical [GuideLine], false for horizontal.
     */
    private var draggingRulerIsVertical: Boolean? = null

    /**
     * We draw the diagram to the canvas, not directly to the screen's frame buffer.
     * In [drawChildren] we *may* redraw the canvas, but always draw the contents of the canvas to the
     * screen's frame buffer.
     *
     * This has a few advantages :
     *
     * * If the diagram has not changed, and the scene is redrawn, then we don't need to redraw the diagram.
     * * We can multi-sample - effectively drawing x2 or x4 bigger than required, and then scaling back down
     *   This is an easy antialiasing solution. Note, old hardware does not support multi-sample,
     *   but we could _fake_ it, by making the canvas x2 or x4 bigger than required.
     * * We don't need any extra scissor tests while rendering the canvas (I'm not sure if that affects speed).
     *
     * The disadvantages :
     *
     * * We need an addition texture (video memory)
     * * We need an addition draw operation (or 2 when using multi-samples).
     */
    private val canvas = Canvas()

    // endregion

    /**
     * The position of the mouse in world coordinates.
     */
    val worldMouseXProperty: ObservableFloat = contentToWorldX(contentMouseXProperty)
    val mouseX by worldMouseXProperty

    /**
     * The position of the mouse in world coordinates.
     */
    val worldMouseYProperty: ObservableFloat = contentToWorldY(contentMouseYProperty)
    val mouseY by worldMouseYProperty

    // region ==== init ====
    init {
        focusAcceptable = true
        focusTraversable = true
        claimChildren()

        document.addListener(this)

        Platform.runLater {
            centerX = diagram.width() / 2
            centerY = diagram.height() / 2
        }
    }
    // endregion init


    // region ==== Conversions ====
    private fun contentToWorldX(pixelX: Float) = pixelX / zoom - diagramWidth
    private fun contentToWorldY(pixelY: Float) = pixelY / zoom - diagramHeight
    private fun worldToContentX(worldX: Float) = (diagramWidth + worldX) * zoom
    private fun worldToContentY(worldY: Float) = (diagramHeight + worldY) * zoom

    private fun contentToWorldX(pixelXProperty: ObservableFloat) = FloatUnaryFunction(pixelXProperty) { pixelX ->
        contentToWorldX(pixelX)
    }.apply {
        addDependent(diagramWidthProperty)
        addDependent(zoomProperty)
    }

    private fun contentToWorldY(pixelYProperty: ObservableFloat) = FloatUnaryFunction(pixelYProperty) { pixelY ->
        contentToWorldY(pixelY)
    }.apply {
        addDependent(diagramWidthProperty)
        addDependent(zoomProperty)
    }

    // TODO NOT used
    private fun worldToContentPropertyX(worldXProperty: ObservableFloat) = FloatUnaryFunction(worldXProperty) { worldX ->
        worldToContentX(worldX)
    }.apply {
        addDependent(diagramWidthProperty)
        addDependent(zoomProperty)
    }

    // TODO Not used
    private fun worldToContentPropertyY(worldYProperty: ObservableFloat) = FloatUnaryFunction(worldYProperty) { worldY ->
        worldToContentY(worldY)
    }.apply {
        addDependent(diagramWidthProperty)
        addDependent(zoomProperty)
    }

    /**
     * Converts a mouse position to a world position.
     */
    private fun mouseToWorld(event: MouseEvent): Vector2 {
        contentMouseX = event.sceneX - fakeContent.sceneX
        contentMouseY = event.sceneY - fakeContent.sceneY

        // Find the mouse position relative to [fakeContent] first, and then convert as normal.
        val localX = event.sceneX - fakeContent.sceneX
        val localY = event.sceneY - fakeContent.sceneY
        return Vector2(contentToWorldX(localX), contentToWorldY(localY))
    }

    private fun worldToContent( vector : Vector2 ) = Vector2( worldToContentX(vector.x), worldToContentY(vector.y) )

    private fun worldToContent( box : BoundingBox ) = Rect(
        worldToContentX(box.left),
        worldToContentY(box.top),
        worldToContentX(box.right),
        worldToContentY(box.bottom)
    )

    // endregion conversions

    // region ==== Layout ====
    override fun nodePrefWidth() = vRuler.evalPrefWidth() + scrollPane.evalPrefWidth() + surroundX()
    override fun nodePrefHeight() = hRuler.evalPrefWidth() + scrollPane.evalPrefHeight() + surroundY()
    override fun layoutChildren() {
        var x = surroundLeft()
        var y = surroundTop()
        var availableX = width - surroundX()
        var availableY = height - surroundY()
        val rulerHeight = hRuler.evalPrefHeight()
        val rulerWidth = vRuler.evalPrefWidth()
        setChildBounds(corner, x, y, rulerWidth, rulerHeight)
        setChildBounds(hRuler, x + rulerWidth, y, availableX - rulerWidth, rulerHeight)
        y += rulerHeight
        availableY -= rulerHeight
        setChildBounds(vRuler, x, y, rulerWidth, availableY)
        x += rulerWidth
        availableX -= rulerWidth
        setChildBounds(scrollPane, x, y, availableX, availableY)
        visibleWidth = availableX
        visibleHeight = availableY
    }
    // endregion Layout

    // region ==== Draw ====

    private fun redraw() {
        // If the view is already dirty, we don't need to do anything.
        // (as it will be redrawn soon anyway)
        if (!dirty) {
            dirty = true

            requestRedraw()
        }
    }

    override fun drawChildren() {
        // Does the canvas need to be redrawn?
        if (dirty) {
            backend.saveRestoreState {
                drawDiagramToCanvas()
            }
            dirty = false
        }

        val canvasTexture = canvas.texture
        backend.drawTexture(
            canvasTexture,
            0f, 0f, canvasTexture.width.toFloat(), canvasTexture.height.toFloat(),
            scrollPane.sceneX, scrollPane.sceneY, scrollPane.width, scrollPane.height
        )

        super.drawChildren()
    }

    private fun drawDiagramToCanvas() {

        val width = hRuler.width // Size in Glok logical units
        val height = vRuler.height
        val pixelWidth = width * scale // Size in physical pixels
        val pixelHeight = height * scale

        // Draw using vectorial
        canvas.draw(pixelWidth.toInt(), pixelHeight.toInt(), multiSample) {
            with(renderer) {
                glDisable(GL_SCISSOR_TEST)

                clear(diagramBackgroundColor)
                view(hRuler.lowerBound, vRuler.lowerBound, hRuler.upperBound, vRuler.upperBound) {

                    // PAPER/OUTLINE
                    val rect = Rect(0f, 0f, diagram.width(), diagram.height())
                    strokeRect(rect, constructionLineThickness, pageBorderColor)
                    fillRect(rect, paperColor)

                    // SHAPES
                    for (layer in diagram.layers) {
                        drawLayer(layer)
                    }
                }

                val offsetX = scrollPane.hScrollValue
                val offsetY = scrollPane.vScrollValue
                view(offsetX, offsetY, offsetX+width, offsetY+height) {
                    // From here onwards, we will be using Glok's units relative to fakeContent.
                    // i.e. 0,0 is the top left of the scrollable node, NOT the top left of the scroll pane viewport.
                    // This is so that control point sizes will be the same size regardless of the zoom factor.
                    // So we need to use worldToContent methods for each world coordinate used.

                    // SELECTION BOUNDING BOXES
                    if (selection.handle == null) {
                        if (combineBoundingBoxes) {
                            combinedBoundingBox?.let {
                                val viewportRect = worldToContent(it)
                                strokeRect(viewportRect, selectionLineThickness, selectionColor)
                            }

                        } else {

                            for (shape in selection) {
                                if (shape === hoverOverShape) continue // Drawn after this loop
                                when (shape) {
                                    is GeometryShape -> {
                                        val viewportRect = worldToContent(shape.boundingBox)
                                        val color = if (shape === hoverOverShape) {
                                            hoverColor
                                        } else {
                                            selectionColor
                                        }
                                        strokeRect(viewportRect, selectionLineThickness, color)
                                    }
                                }
                            }
                        }
                    }

                    // HOVERED OVER SHAPE's bounding box.
                    hoverOverShape?.let { shape ->
                        val viewportRect = worldToContent(shape.boundingBox)
                        strokeRect(viewportRect, selectionLineThickness, hoverColor)
                    }

                    // EDGE CONNECTIONS
                    hoverOverEdge?.let { edge ->
                        var prev: Vector2? = null
                        for (point in edge.points) {
                            val local = worldToContent(point)
                            prev?.let {
                                line(it, local, selectionLineThickness, hoverColor)
                            }
                            prev = local
                        }
                    }

                    // HANDLES
                    for (handle in handles.asReversed()) {
                        if (handle !== hoverOverHandle) {
                            val color = if (handle === selection.handle) {
                                selectedHandleColor
                            } else {
                                handleColor
                            }
                            drawControlPoint(handle, color)
                        }
                    }
                    hoverOverHandle?.let {
                        drawControlPoint(it, hoverColor)
                    }

                    // Each tool can draw addition stuff.
                    tool.draw()
                }
            }
        }
    }

    /**
     * All world coordinates must be converted using `worldToContent` methods.
     */
    private fun drawControlPoint(handle: Handle, color: Color) {
        val local = worldToContent(handle.position())
        val halfSize = 4f
        var rect = Rect(local.x - halfSize, local.y - halfSize, local.x + halfSize, local.y + halfSize)
        var borderRect = rect.grow(1f)

        with(renderer) {

            if (handle is BezierHandle) {
                val toLocal = worldToContent(handle.startEnd.attribute.eval())
                line(local, toLocal, 1f, handleBorderColor)
            }

            if (handle is PathBezierHandle) {
                val toLocal = worldToContent(handle.anchor.position())
                line(local, toLocal, 1f, handleBorderColor)
            }

            if (handle.isFixed()) {
                val thickness = 2f
                val margin = 1f
                // Outline
                line(
                    Vector2(rect.left, rect.top),
                    Vector2(rect.right, rect.bottom),
                    thickness + margin * 2,
                    Colors.WHITE
                )
                line(
                    Vector2(rect.right, rect.top),
                    Vector2(rect.left, rect.bottom),
                    thickness + margin * 2,
                    Colors.WHITE
                )
                // Small cross
                line(
                    Vector2(rect.left + margin, rect.top + margin),
                    Vector2(rect.right - margin, rect.bottom - margin),
                    thickness,
                    fixedHandleColor
                )
                line(
                    Vector2(rect.right - margin, rect.top + margin),
                    Vector2(rect.left + margin, rect.bottom - margin),
                    thickness,
                    fixedHandleColor
                )

            } else {
                if (handle is Anchor && handle.pathPart.endCornerType == CornerType.ANGLED) {
                    // Diamond shapes for angled anchors
                    rect = rect.grow(halfSize / 4)
                    borderRect = borderRect.grow(halfSize / 4)

                    val a1 = Vector2(local.x, borderRect.top)
                    val b1 = Vector2(borderRect.right, local.y)
                    val c1 = Vector2(local.x, borderRect.bottom)
                    val d1 = Vector2(borderRect.left, local.y)
                    val a2 = Vector2(local.x, rect.top)
                    val b2 = Vector2(rect.right, local.y)
                    val c2 = Vector2(local.x, rect.bottom)
                    val d2 = Vector2(rect.left, local.y)
                    fillTriangles(listOf(a1, b1, c1, a1, d1, c1), handleBorderColor)
                    fillTriangles(listOf(a2, b2, c2, a2, d2, c2), color)
                } else if (handle is Anchor) {
                    // Hexagons (Circular-ish) smooth/symmetric anchor points
                    rect = rect.grow(0f, halfSize / 4)
                    borderRect = borderRect.grow(0f, halfSize / 4)

                    var deltaY = (halfSize + 1) * 0.6f
                    val a1 = Vector2(local.x, borderRect.top)
                    val b1 = Vector2(borderRect.right, local.y - deltaY)
                    val c1 = Vector2(borderRect.right, local.y + deltaY)
                    val d1 = Vector2(local.x, borderRect.bottom)
                    val e1 = Vector2(borderRect.left, local.y + deltaY)
                    val f1 = Vector2(borderRect.left, local.y - deltaY)
                    deltaY = halfSize * 0.6f
                    val a2 = Vector2(local.x, rect.top)
                    val b2 = Vector2(rect.right, local.y - deltaY)
                    val c2 = Vector2(rect.right, local.y + deltaY)
                    val d2 = Vector2(local.x, rect.bottom)
                    val e2 = Vector2(rect.left, local.y + deltaY)
                    val f2 = Vector2(rect.left, local.y - deltaY)

                    fillTriangles(listOf(a1, b1, d1, b1, c1, d1, d1, e1, f1, d1, f1, a1), handleBorderColor)
                    fillTriangles(listOf(a2, b2, d2, b2, c2, d2, d2, e2, f2, d2, f2, a2), color)
                } else {
                    if (handle.isFree()) {
                        fillRect(borderRect, handleBorderColor)
                        fillRect(rect, color)
                    } else {
                        fillRect(borderRect, handleBorderColor)
                        val thickness = 2f
                        val margin = 1f
                        line(
                            Vector2(rect.left + margin, rect.top + margin),
                            Vector2(rect.right - margin, rect.bottom - margin),
                            thickness,
                            color
                        )
                        line(
                            Vector2(rect.right - margin, rect.top + margin),
                            Vector2(rect.left + margin, rect.bottom - margin),
                            thickness,
                            color
                        )
                    }
                }
            }
        }
    }

    // endregion

    // region ==== Methods ====

    /**
     * See `VectorialActions.DRAW_END`
     */
    fun endDrawing() {
        tool.endDrawing()
    }

    private fun attempt(event: Event, action: () -> Unit) {
        try {
            action()
        } catch (e: Exception) {
            event.consume()
            tool = SelectTool()
            severe(e)
        }
    }

    private fun createHandlesForSelection() {
        handles.clear()
        // Show handles when ONE shape is selected.
        if (tool is SelectToolBase && selection.shapes.size == 1) {
            val selected = selection.shapes.first()
            if (selected is GeometryShape) {
                for (handle in selected.handles) {
                    handles.add(handle)
                }
            }
        }
    }

    override fun documentChanged(document: Document, change: Change, isUndo: Boolean) {

        if (change is SelectionChange || change is ChangeGeometry || change is ControlPointsChange) {
            createHandlesForSelection()
        }

        if (change is SelectControlPoint || change is SelectionChange) {
            hoverOverHandle = null
        }

        if (change is SelectControlPoint) {
            handles.removeIf { it is PathBezierHandle }
            val pcp = selection.handle as? Anchor
            if (pcp != null) {
                pcp.handleBefore?.let { handles.add(it) }
                pcp.handleAfter?.let { handles.add(it) }
            }
        }

        if (change is DiagramChange && change.diagram === diagram) {

            if (combineBoundingBoxes) {
                combinedBoundingBox = null
                for (shape in selection) {
                    combinedBoundingBox = if (combinedBoundingBox == null) {
                        shape.boundingBox
                    } else {
                        combinedBoundingBox!!.combineWith(shape.boundingBox)
                    }
                }
            }

            redraw()
        }
    }

    private fun dragRuler(event: MouseEvent, isVertical: Boolean) {
        draggingRulerIsVertical = isVertical
        dragStartWorld = mouseToWorld(event)
    }

    fun defaultStyle() = ShapeStyle.create(document)

    private fun buildContextMenu(world: Vector2) {

        val popupMenu = PopupMenu().apply {
            selection.handle?.let { handle ->
                if (!handle.isFree()) {
                    items.add(SubMenu("Connection : ${handle.name}").apply {
                        items.add(MenuItem("Break Connection").apply {
                            onAction { handle.moveTo(handle.position()) }
                        })
                        if (handle is SimpleAttributeHandle) {
                            val exp = handle.attribute
                            if (exp is Connection<*>) {
                                val connector = exp.connector
                                if (connector is ConnectorToEdge) {
                                    items.add(Separator())
                                    for (type in connector.edge.supportedConnectorTypes) {
                                        + toggleMenuItem(type.name) {
                                            selected = connector.connectorType === type
                                            onAction {
                                                exp.changeConnector(type, connector.isBounded)
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    })
                }
            }
            items.add(Menu("Select Shape").apply {
                val shapes = diagram.findShapesAt(world, zoomedLineThreshold, zoomedLineThreshold2)
                if (shapes.isEmpty()) {
                    disabled = true
                } else {
                    for (shape in shapes) {
                        items.add(MenuItem(shape.name).apply {
                            onAction {
                                shape.select()
                                tool = SelectTool()
                            }
                        })
                    }
                }
            })
            val nearControlPoints = handles.filter { it.isNear(world, zoomedControlPointThreshold2) }
            if (nearControlPoints.size > 1) {
                items.add(Menu("Select Handle").apply {
                    for (cp in nearControlPoints) {
                        items.add(MenuItem("${cp.name} of ${cp.shape.name}").apply {
                            onAction {
                                cp.select()
                                tool = SelectTool()
                            }
                        })
                    }
                })
            }
        }
        // TODO Show the menu at point [world].
        popupMenu.show(this)
    }

    // endregion methods

    // region ==== Events ====

    private fun mouseClicked(event: MouseEvent) {
        requestFocus()
        // NOTE. JavaFX sends onMouseClicked events even when the mouse is pressed, then dragged, then released.
        // I don't want this behaviour, and therefore I check for `dragging`,
        // and only forward onMouseClicked events which it IS a click (not a drag!).
        if (! dragging && event.isPrimary) {
            tool.onMouseClicked(event, mouseToWorld(event))
        }
        dragging = false
        tip = tool.tip()
    }

    private fun mousePressed(event: MouseEvent) {

        val world = mouseToWorld(event)
        if (event.isPopupTrigger() && event.mods == 0) {
            buildContextMenu(world)
            return
        }
        if (event.isPrimary) {
            dragStartWorld = world
            tool.onMousePressed(event, dragStartWorld)
        }

        if (event.isPanButton() && ! dragging) {
            tool = PanTool(event)
        }

        tip = tool.tip()
    }

    private fun mouseMoved(event: MouseEvent) {
        val world = mouseToWorld(event)

        if (dragging) {
            tool.onMouseDragged(event, world)
        } else {
            tool.onMouseMoved(event, world)
        }
        tip = tool.tip()
    }

    private fun mouseDragged(event: MouseEvent) {
        val world = mouseToWorld(event)
        if (draggingRulerIsVertical == null) {

            tool.onMouseDragged(event, world)
            event.consume()
            dragging = true

        } else {
            dragging = true
            val guideGeometry = if (draggingRulerIsVertical == true) {
                GuideLine.create(dragStartWorld.y, Angle.degrees(90.0))
            } else {
                GuideLine.create(dragStartWorld.x, Angle.degrees(0.0))
            }
            val guide = guideGeometry.createShape(document.generateName("guide"), ShapeStyle.guideLineStyle(document))
            draggingRulerIsVertical = null

            tool = MoveTool(ToolType.SELECT, "Create Guide Line") {
                // Add as the first shape, so it is at the bottom of the z-order.
                diagram.currentLayerOrGroup.addShape(0, guide)
                guide.select()
            }
        }
        tip = tool.tip()
    }

    private fun mouseReleased(event: MouseEvent) {
        val world = mouseToWorld(event)
        if (event.isPopupTrigger() && ! dragging) {
            buildContextMenu(world)
            return
        }
        if (event.isPrimary) {
            tool.onMouseReleased(event, mouseToWorld(event))
            dragging = false
        }
        if (event.isPanButton() && tool is PanTool) {
            tool.onMouseReleased(event, mouseToWorld(event))
            dragging = false
        }

        tip = tool.tip()
    }

    // endregion Events

    // region ==== Tool ====
    /**
     * Tools are different ways that the user can interact with the [Diagram].
     *
     * The most common [Tool] is [SelectTool], where shapes can be clicked to select them.
     *
     * Drawing new shapes, moving shapes, and moving [Handle]s have their own tools.
     */
    interface Tool {
        val toolType: ToolType

        fun tip(): String
        fun begin() {}
        fun onMouseClicked(event: MouseEvent, world: Vector2) {}
        fun onMousePressed(event: MouseEvent, world: Vector2) {}
        fun onMouseMoved(event: MouseEvent, world: Vector2) {}
        fun onMouseDragged(event: MouseEvent, world: Vector2) {}
        fun onMouseReleased(event: MouseEvent, world: Vector2) {}
        fun endDrawing() {}

        /**
         * A Tool may draw additional things.
         * The coordinates must be translated using `worldToContent` methods.
         */
        fun draw() {}
        fun end() {}

    }
    // endregion Tool

    // region ==== PanTool ====
    internal inner class PanTool(event: MouseEvent) : Tool {
        override val toolType: ToolType get() = ToolType.PAN
        override fun tip() = "Pan"

        private var prevX = event.sceneX
        private var prevY = event.sceneY
        private val beforePanTool: Tool = tool

        init {
            event.capture()
        }

        override fun onMouseReleased(event: MouseEvent, world: Vector2) {
            if (event.isPanButton()) {
                tool = beforePanTool
            }
        }

        override fun onMouseDragged(event: MouseEvent, world: Vector2) {
            scrollPane.hScrollValue += prevX - event.sceneX
            scrollPane.vScrollValue += prevY - event.sceneY
            prevX = event.sceneX
            prevY = event.sceneY
        }

        override fun onMouseMoved(event: MouseEvent, world: Vector2) {
            onMouseDragged(event, world)
        }
    }
    // endregion PanTool

    // region ==== SelectAreaTool ====
    /**
     * Drag a bounding rectangle to select multiple shapes
     */
    internal inner class SelectAreaTool : Tool {

        override val toolType
            get() = ToolType.SELECT

        private var selectionRectangle = BoundingBox(dragStartWorld.x, dragStartWorld.y, dragStartWorld.x, dragStartWorld.y)

        init {
            history.beginBatch("Bounding box select")
        }

        override fun tip() = "[Drag] to select shapes within a rectangular area"

        override fun begin() {
            history.beginBatch()
        }

        override fun onMouseDragged(event: MouseEvent, world: Vector2) {
            val left = min(world.x, dragStartWorld.x)
            val top = min(world.y, dragStartWorld.y)
            val right = max(world.x, dragStartWorld.x)
            val bottom = max(world.y, dragStartWorld.y)

            selectionRectangle = BoundingBox(left, top, right, bottom)

            val toSelect = mutableListOf<Shape>()
            for (layer in diagram.layers) {
                for (shapes in layer.shapes) {
                    if (selectionRectangle.contains(shapes.boundingBox)) {
                        toSelect.add(shapes)
                    }
                }
            }
            diagram.select(toSelect)
            redraw()
        }

        override fun onMouseReleased(event: MouseEvent, world: Vector2) {
            history.endBatch()
            tool = SelectTool()
        }

        override fun draw() {
            // Draw the selection rectangle currently being dragged.
            with(renderer) {
                strokeRect(
                    worldToContent(selectionRectangle),
                    selectionLineThickness,
                    selectionColor
                )
            }
        }
    }

    // endregion SelectAreaTool

    // region ==== SelectToolBase ====

    /**
     * The base class for any [Tool]s which can also select shapes, or drag handles of the currently selected shape.
     * [SelectTool] only uses the features here, whereas [QuadrilateralTool] uses these features, as well as
     * creating rectangles when a drag is started.
     */
    internal abstract inner class SelectToolBase : Tool, ShapeFilter {
        /**
         * When set, we are dragging a [Handle].
         */
        protected var draggingHandle: Handle? = null

        /**
         * Which shapes can be selected.
         * The default allows ANY shapes to be selected.
         * Subclasses can select based on their type. e.g. [PathTool] only [Path]s to be selected.
         */
        override fun acceptShape(shape: Shape): Boolean = true

        protected open fun specialTip(): String = if (draggingHandle == null) {
            if (selection.isEmpty()) {
                "[Click] to select. [Drag] to select shapes within a rectangular area"
            } else {
                "[$adjustSelectionMod+Click]: Add/Remove shapes from the selection. [$lowerShapesMod+Click]: Select lower shapes."
            }
        } else {
            ""
        }

        override fun begin() {
            createHandlesForSelection()
        }

        override fun tip(): String {
            val draggingHandle = draggingHandle
            val hoverOverHandle = hoverOverHandle
            val hoverOverEdge = hoverOverEdge

            return if (draggingHandle == null) {
                if (hoverOverHandle == null) {
                    specialTip()
                } else {
                    val suffix = if (hoverOverHandle is Anchor &&
                        hoverOverHandle.position() == hoverOverHandle.handleAfter?.position()
                    ) {
                        " [${pullOutHandleMod}+Drag] to pull out curve handle."
                    } else {
                        ""
                    }
                    "[Drag] to move [${hoverOverHandle.name}].$suffix"
                }
            } else {
                // We are dragging a control point
                val cpTip = draggingHandle.tip()

                if (draggingHandle.isConnectable()) {

                    if (hoverOverHandle != null) {
                        "Connect [${draggingHandle.name}] to [${hoverOverHandle.shape.name}.${hoverOverHandle.name}]"
                    } else if (hoverOverEdge != null) {
                        "Connect [${draggingHandle.name}] to [${hoverOverEdge.shape.name}]"
                    } else {
                        if (draggingHandle.isFree()) {
                            "Dragging [${draggingHandle.name}]. [${connectMod}+Drag] to make a connection. $cpTip"
                        } else {
                            "Dragging [${draggingHandle.name}]. [${connectMod}+Drag] to break the connection. $cpTip"
                        }
                    }
                } else {
                    "[Drag] to move [${draggingHandle.name}]. The control point cannot be connected. $cpTip"
                }
            }
        }

        protected fun selectBaseMousePressed(event: MouseEvent, world: Vector2): Boolean {
            // If there is already a handle selected, check that one first.
            // This is important when 2 handles overlap, and we have selected one using the context menu.
            val selectedCP = selection.handle
            if (selectedCP != null && ! event.isPullOutHandle()) {
                val isNear = selectedCP.isNear(world, zoomedControlPointThreshold2)
                if (handles.contains(selectedCP) && isNear && (! selectedCP.isFixed() || event.isConnect())) {
                    // This *might* be a start of a drag of the handle
                    draggingHandle = selectedCP
                    history.beginBatch("Move ${selectedCP.shape.name}.${selectedCP.name}")
                    event.capture()
                    return true
                }
            }

            // Now check to see if we have pressed any of the other Handles (i.e. previously unselected Handles).
            for (otherControlPoint in handles) {
                if ((! otherControlPoint.isFixed() || event.isConnect()) && otherControlPoint.isNear(
                        world,
                        zoomedControlPointThreshold2
                    )
                ) {
                    // This *might* be a start of a drag of the handle

                    // If (shift) modifier is held down, then drag out the `handleAfter` Handle, rather than the Anchor.
                    val cp1Handle = (otherControlPoint as? Anchor)?.handleAfter
                    draggingHandle =
                        if (event.isPullOutHandle() && cp1Handle != null && cp1Handle.position() == otherControlPoint.position()) {
                            cp1Handle
                        } else {
                            otherControlPoint
                        }

                    history.beginBatch("Move ${otherControlPoint.shape.name}.${otherControlPoint.name}")
                    // We do NOT select PathBezierControlPoints, we keep the BezierAnchor ControlPoint selected.
                    if (otherControlPoint !is PathBezierHandle) {
                        otherControlPoint.select()
                    }
                    event.capture()
                    return true
                }
            }

            // Ok, so a Handle has NOT been pressed, let's see if we have pressed a Shape.

            val shapesAtPoint = diagram.findShapesAt(world, zoomedLineThreshold, zoomedLineThreshold2, this)
            if (shapesAtPoint.isEmpty()) {
                // We've pressed an empty part of the diagram.
                // This might be the start of bounding box selection when tool is [SelectTool],
                // or the creation of a Rectangle when tool is [QuadrilateralTool] etc.
                return false
            }

            val highestShapeClicked = shapesAtPoint.first()

            if (event.isAdjustSelection()) {
                // Add or remove the top-most shape to/from the selection
                if (selection.contains(highestShapeClicked)) {
                    highestShapeClicked.removeFromSelection()
                } else {
                    highestShapeClicked.addToSelection()
                }
            } else if (event.isSelectLowerShape()) {
                val latest = selection.shapes.lastOrNull()
                if (latest != null && shapesAtPoint.contains(latest)) {
                    // Select the shape below the currently selected actor (i.e. with a higher index)
                    // This is useful when there are many shapes on top of each other.
                    // We can select to any of them, by repeatedly clicking with the modifier held down.
                    val i = shapesAtPoint.indexOf(latest)
                    if (i == shapesAtPoint.size - 1) {
                        shapesAtPoint.first().select()
                    } else {
                        shapesAtPoint[i + 1].select()
                    }
                }
            } else {
                // Not adjust nor selectLower, i.e. a regular mouse press.

                // Have we pressed a shape from the current selection?
                for (shape in selection.shapes) {
                    if (shape.touching(world, zoomedLineThreshold, zoomedLineThreshold2)) {
                        tool = MoveTool(toolType, "Move ${summariseShapes(selection.shapes)}") {
                            diagram.clearControlPointSelection()
                        }
                        event.capture()
                        return true
                    }
                }

                // No we haven't pressed a currently selected item.
                tool = MoveTool(toolType, "Select ${highestShapeClicked.name}") {
                    // Note, if this shape is already selected, then the selection won't change.
                    highestShapeClicked.select()
                    diagram.clearControlPointSelection()
                }
                return true
            }

            return true
        }

        protected fun selectBaseMouseMoved(world: Vector2) {
            // Assume at first that we aren't over a Handle or Shape
            hoverOverHandle = null
            hoverOverShape = null

            for (handle in handles) {
                if (handle.isNear(world, zoomedControlPointThreshold2)) {
                    hoverOverHandle = handle
                    // When hovering over a Handle, hoverOverShape will remain null.
                    return
                }
            }

            hoverOverShape = diagram.findTopShapeAt(world, zoomedLineThreshold, zoomedLineThreshold2, this)
        }

        protected fun selectBaseMouseDragged(event: MouseEvent, world: Vector2): Boolean {
            draggingHandle?.let { dragging ->
                if (event.isConnect() && dragging.isConnectable()) {
                    if (dragging.isFree()) {

                        // Highlight the edge or control point that we could connect to.
                        // Assume none until we find otherwise
                        hoverOverHandle = null
                        hoverOverEdge = null
                        val touchingEdges = mutableListOf<Edge>()
                        val nearShapes = diagram.findShapesAt(world, zoomedLineThreshold, zoomedLineThreshold2)
                        for (nearShape in nearShapes) {
                            if (nearShape !== dragging.shape) {

                                for (cp in nearShape.handles) {
                                    if (cp !== dragging && cp.isNear(world, zoomedControlPointThreshold2)) {
                                        hoverOverHandle = cp
                                        break
                                    }
                                }
                                if (hoverOverHandle != null) break

                                nearShape.touchingEdges(world, zoomedLineThreshold, zoomedLineThreshold2, touchingEdges)
                                if (touchingEdges.isNotEmpty()) {
                                    hoverOverEdge = touchingEdges.first()
                                    break
                                }
                            }
                        }

                    } else {
                        val breakConnection = if (dragging.isFree()) {
                            null
                        } else {
                            dragging.breakConnection(dragging.shape, world)
                        }
                        (breakConnection ?: dragging.moveTo(world))
                    }
                }
                val snapped = if (event.isConstrainMove()) dragging.snap(world) else world
                dragging.moveTo(snapped)
                return true
            }
            return false
        }

        protected fun selectBaseMouseReleased(event: MouseEvent, world: Vector2): Boolean {
            draggingHandle?.let { handle ->
                if (event.isConnect()) {

                    try {
                        if (hoverOverHandle != null) {
                            val connector = hoverOverHandle!!.connect()
                            if (! handle.makeConnection(handle.shape, connector)) {
                                handle.moveTo(world)
                            }

                        } else if (hoverOverEdge != null) {
                            val connector = hoverOverEdge !!.createConnector(world)
                            if (! handle.makeConnection(handle.shape, connector)) {
                                handle.moveTo(world)
                            }
                        }
                    } catch (e: RecursionLimitExceeded) {
                        info("Recursive definition?")
                        history.abortBatch()
                        draggingHandle = null
                        hoverOverEdge = null
                        hoverOverHandle = null
                        info("Batch aborted")
                    }

                }
                draggingHandle = null
                hoverOverEdge = null
                hoverOverHandle = null
                history.endBatch()
                return true
            }
            return false
        }

    }

    // endregion SelectToolBase

    // region ==== SelectTool ====

    internal inner class SelectTool : SelectToolBase() {

        override val toolType
            get() = ToolType.SELECT

        override fun onMousePressed(event: MouseEvent, world: Vector2) {
            if (!event.isPrimary) return

            if (!selectBaseMousePressed(event, world)) {
                diagram.clearSelection()
                tool = SelectAreaTool()
                event.capture()
            }
        }

        override fun onMouseMoved(event: MouseEvent, world: Vector2) {
            selectBaseMouseMoved(world)
        }

        override fun onMouseDragged(event: MouseEvent, world: Vector2) {
            selectBaseMouseDragged(event, world)
        }

        override fun onMouseReleased(event: MouseEvent, world: Vector2) {
            selectBaseMouseReleased(event, world)
        }
    }

    // endregion SelectTool

    // region ==== GrowShapeTool ====

    internal abstract inner class GrowShapeTool : SelectToolBase() {

        protected var shape: GeometryShape? = null

        /**
         * Set to true, then the mouse has been dragged far enough.
         */
        private var keep = false

        private var start = Vector2(0f, 0f)

        protected abstract fun createGeometry(isCentered: Boolean, world: Vector2): Geometry

        open fun createShape(isCentered: Boolean, world: Vector2): GeometryShape {
            val geometry = createGeometry(isCentered, world)
            return geometry.createShape(document.generateName(geometry.baseName), defaultStyle())
        }

        open fun postCreateShapeChange(shape: Shape): Change? = null

        override fun onMousePressed(event: MouseEvent, world: Vector2) {
            if (!event.isPrimary) return

            if (!selectBaseMousePressed(event, world)) {
                val shape = createShape(event.isCentered(), world)
                history.beginBatch("Create Rectangle")
                diagram.clearSelection()
                diagram.currentLayerOrGroup.addShape(shape)
                postCreateShapeChange(shape)?.addToCurrentBatch()

                this.shape = shape
                start = world
                event.capture()
            }
        }

        override fun onMouseDragged(event: MouseEvent, world: Vector2) {
            val shape = shape
            if (shape == null) {
                if (selectBaseMouseDragged(event, world)) return
                super.onMouseMoved(event, world)
                return
            }

            if (!keep) {
                if (abs(world.x - start.x) > DRAG_THRESHOLD) {
                    keep = true
                } else {
                    return
                }
            }
            val geometry = shape.geometry
            val cp = geometry.handles()[1]
            shape.select()
            cp.select()
            draggingHandle = cp
            val snapped = if (event.isSnapControl()) cp.snap(world) else world
            cp.moveTo(snapped)
        }

        override fun onMouseReleased(event: MouseEvent, world: Vector2) {
            val shape = shape
            if (shape == null) {
                if (selectBaseMouseReleased(event, world)) return
            } else {
                if (keep) {
                    shape.select()
                    history.endBatch()
                } else {
                    history.abortBatch()
                    diagram.clearSelection()
                }
            }
            keep = false
            this.shape = null
            draggingHandle = null
        }
    }

    // endregion GrowShapeTool

    // region ==== PolygonTool ====

    internal inner class PolygonTool : GrowShapeTool() {

        override val toolType get() = ToolType.POLYGON

        override fun acceptShape(shape: Shape) = shape is GeometryShape && shape.geometry is PolygonOrStar

        override fun specialTip() = "[Drag] to create a polygon. [$centeredMod+Drag] Centered."

        override fun createGeometry(isCentered: Boolean, world: Vector2): PolygonOrStar {

            val ratioAndTurn = Polar(0.5f, Angle.radians(Math.PI / polygonSides))

            return when (polygonType) {

                PolygonType.POLYGON -> {
                    if (isCentered) {
                        Polygon.createCentered(polygonSides, world, Vector2(1f, 0f))
                    } else {
                        Polygon.createCornerBased(polygonSides, world, world + Vector2(1f, 0f))
                    }
                }

                PolygonType.STAR -> {
                    if (isCentered) {
                        Star.createCentered(polygonSides, world, Vector2(1f, 0f), ratioAndTurn)
                    } else {
                        Star.createCornerBased(polygonSides, world, world + Vector2(1f, 0f), ratioAndTurn)
                    }
                }
            }
        }

        override fun onMouseMoved(event: MouseEvent, world: Vector2) {
            selectBaseMouseMoved(world)
        }
    }

    // endregion PolygonTool

    // region ==== QuadrilateralTool ====

    internal inner class QuadrilateralTool : GrowShapeTool() {

        override val toolType
            get() = ToolType.RECTANGLE

        override fun acceptShape(shape: Shape) = shape is GeometryShape && shape.geometry is Quadrilateral

        override fun specialTip() = "[Drag] to create a rectangle. [$centeredMod+Drag] Centered."

        override fun createGeometry(isCentered: Boolean, world: Vector2): Quadrilateral {
            val zero = Vector2(0f, 0f)

            return if (isCentered) {

                when (quadrilateralType) {
                    QuadrilateralType.DIAMOND -> Diamond.createCentered(world, zero)
                    QuadrilateralType.PARALLELOGRAM -> Parallelogram.createCentered(world, zero, 0.25f)
                    else -> Rectangle.createCentered(world, Vector2(0f, 0f))
                }
            } else {
                when (quadrilateralType) {
                    QuadrilateralType.DIAMOND -> Diamond.create(world, world)
                    QuadrilateralType.PARALLELOGRAM -> Parallelogram.create(world, world, 0.25f)
                    else -> Rectangle.create(world, world)
                }
            }
        }


        override fun onMouseMoved(event: MouseEvent, world: Vector2) {
            selectBaseMouseMoved(world)
        }

    }

    // endregion QuadrilateralTool

    // region ==== EllipseTool ====

    internal inner class EllipseTool : GrowShapeTool() {

        override val toolType
            get() = ToolType.ELLIPSE

        override fun acceptShape(shape: Shape) = shape is GeometryShape && shape.geometry is Ellipse

        override fun specialTip() = "[Drag] to create a circle. [$centeredMod+Drag] Centered."

        override fun createGeometry(isCentered: Boolean, world: Vector2): Ellipse {

            return if (isCentered) {
                Ellipse.createCenterEdge(world, world)
            } else {
                Ellipse.createCenterRadius(world, 0f)
            }
        }

        override fun postCreateShapeChange(shape: Shape): Change? = ((shape as? GeometryShape)?.geometry as? Ellipse)?.changeEllipseType(ellipseType)


        override fun onMouseMoved(event: MouseEvent, world: Vector2) {
            selectBaseMouseMoved(world)
        }

    }

    // endregion EllipseTool

    // region ==== PathTool ====

    /**
     * Draw new [Path]s, and adjust [Handle]s of the currently selected shape (if a single shape is selected).
     * Once a new [Path] has been created, the tool remains unchanged, and the new [Path] is selected.
     * Therefore, the user can either :
     *
     * 1. Adjust the [Handle]s of the existing path.
     * 2. Start drawing another path.
     *
     * This tool is complicated because it can add line-segments and bezier curve segments.
     * If it further complicated because it inherits [SelectToolBase], which allows us to move [Handle]s
     * of the currently selected shape.
     *
     * ## Line Segments
     *
     * Adding line-segments is easy. Click, move click. Each click denotes the start/end of a line segment.
     * Between each click, a construction line is drawn (see [draw]) between the end of the [newPath], and
     * the mouse's position.
     *
     * The very first click creates a [newPath] (with no parts).
     * All subsequent clicks add a [LinePathPart] to [newPath]
     *
     * ## Bezier Curve Segments
     *
     * Each bezier curve segment is defined by an initial click to [BezierPathPart.start],
     * a mouse movement (not a drag) to [BezierPathPart.end] followed by dragging the mouse to
     * [BezierPathPart.controlPoint2].
     * Note. [BezierPathPart.controlPoint1] is NOT defined directly by the user.
     * If the previous [PathPart] is a [BezierPathPart], then [BezierPathPart.controlPoint1] is a smooth symmetrical
     * control point. i.e. [BezierPathPart.controlPoint1] - [BezierPathPart.start] =
     * [BezierPathPart.end] - [BezierPathPart.controlPoint2] of the _previous_ [BezierPathPart].
     *
     * Otherwise [BezierPathPart.controlPoint1] = [BezierPathPart.start]
     *
     * ## Other [PathPart] types.
     * No other [PathPart] types are implemented. However, I'd like `rounded corners` of some kind.
     * (either using a `Circular/Elliptical Arc` as seen in SVG paths, or a `RoundedCorner`, which must be
     * before and/or after a [LinePathPart]).
     *
     * ## Combining All Use-Cases
     *
     * Each mouse event first checks if it should be handled by [SelectToolBase] (i.e. selection/adjusting [Handle]s).
     * If not, then we need to distinguish between creating lines and bezier curves.
     * This is done using [newCurve], which not null when adjusting a bezier curve, and null otherwise.
     *
     * The user interactions for a line and curve start the same. i.e. click then move.
     *
     * ### Mouse Movement
     *
     * * [newCurve] == `null` : We are expecting a _click_ to add a line-segment.
     * * [newCurve] != `null` : We are expecting a _click_ to set `controlPoint2`, which completes [newCurve].
     *
     * ### Mouse Drag
     *
     * * [newCurve] == `null` : The previous mouse position is [BezierPathPart.end].
     * * [newCurve] != `null` : Moving `controlPoint2`
     *
     * ### Mouse Press
     *
     * We always expect [newCurve] == `null`.
     * The user may be intending to create a line (in which case, this is part of a _click_).
     * Or part of a curve (in which case, this is the start of a drag).
     *
     * ### Mouse Released
     *
     * * [newCurve] == `null`. Part of a _click_. We do nothing here, and instead we do the processing in onClicked.
     * * [newCurve] != `null`. The end of the _drag_ of `controlPoint2`.
     *
     */
    internal inner class PathTool : SelectToolBase() {

        override val toolType: ToolType get() = ToolType.PATH

        private var newPath: Path? = null

        private var mouseWorldPosition = Vector2(0f, 0f)

        private var newCurve: BezierPathPart? = null

        override fun acceptShape(shape: Shape) = shape is GeometryShape && shape.geometry is Path

        /**
         * Only used when a path is closed with a bezier curve.
         * [onMouseDragged] adjusts the first part's curve's `controlPoint1` and the last part's `controlPoint2`
         */
        private var draggingLastControlPoint: Boolean = false

        override fun specialTip(): String = if (newPath == null) {
            "[Lines] : [Click], move, [click].   [Curves] : [Click], move, [drag]. [ENTER] to finish path"
        } else {
            if (dragging) {
                "[Drag] to change the direction of the [curve]. [ENTER] to finish path"
            } else {
                "[Click] for a [Line], [Drag] for a [Curve]. [ENTER] to finish path"
            }
        }

        override fun onMousePressed(event: MouseEvent, world: Vector2) {
            if (! event.isPrimary) return

            if (newPath == null && selectBaseMousePressed(event, world)) {
                return
            }

            if (newPath != null) {
                event.capture()
            }
        }

        override fun onMouseClicked(event: MouseEvent, world: Vector2) {

            if (! event.isPrimary) return

            val history = document.history
            val existingPath = newPath
            mouseWorldPosition = world

            if (event.clickCount == 2) {
                endDrawing()
                return
            }

            if (existingPath == null) {
                // The very first click.
                val geometry = Path.create(world)
                newPath = geometry
                val shape = geometry.createShape(document.generateName(geometry.baseName), defaultStyle())
                history.beginBatch("Create Path")
                diagram.clearSelection()
                diagram.currentLayerOrGroup.addShape(shape)
                diagram.select(shape)
            } else {
                if (nearStartPoint(world)) {
                    existingPath.closePath()
                    endDrawing()
                } else {
                    existingPath.appendLineSegment(world)
                }
            }
        }

        override fun onMouseDragged(event: MouseEvent, world: Vector2) {
            if (newPath == null && selectBaseMouseDragged(event, world)) return

            val curve = newCurve

            val path = newPath
            if (path != null) {
                if (curve == null) {
                    val prevCurve = path.pathParts.lastOrNull() as? BezierPathPart
                    val cp1 = if (prevCurve == null) {
                        // Previous part isn't a bezier curve, so make controlPoint1 the same position as the old end.
                        path.end.eval()
                    } else {
                        // Previous part is a bezier curve, so make controlPoint1 symmetric with the previous controlPoint2.
                        prevCurve.changeCornerType(CornerType.SYMMETRIC)
                        prevCurve.end.eval() - (prevCurve.controlPoint2.eval() - prevCurve.end.eval())
                    }
                    if (nearStartPoint(dragStartWorld)) {
                        // Joining back up to the start of the path to form a closed path.
                        path.closePath(true)
                        newCurve = path.pathParts.lastOrNull() as? BezierPathPart
                        draggingLastControlPoint = true
                    } else {
                        newCurve = path.appendBezierCurve(cp1, dragStartWorld, dragStartWorld)
                    }
                } else {
                    if (draggingLastControlPoint) {
                        val cp1 = (path.pathParts.firstOrNull() as? BezierPathPart)?.controlPoint1
                        val cp2 = (path.pathParts.lastOrNull() as? BezierPathPart)?.controlPoint2
                        cp1?.set(Vector2Constant(world))
                        cp2?.set(Vector2Constant(path.start.eval() - (world - path.start.eval())))
                    } else {
                        val cp2 = curve.end.eval() - (world - curve.end.eval())
                        curve.controlPoint2.set(Vector2Constant(cp2))
                    }
                }
            }

            mouseWorldPosition = world
        }

        override fun onMouseReleased(event: MouseEvent, world: Vector2) {
            if (newPath == null && selectBaseMouseReleased(event, world)) return

            val curve = newCurve
            if (curve != null) {
                newCurve = null
                if (draggingLastControlPoint) {
                    endDrawing()
                }
            }
            return
        }

        override fun endDrawing() {
            val shape = newPath !!.shape
            diagram.clearSelection()
            diagram.currentLayerOrGroup.addShape(shape)
            diagram.select(shape)
            document.history.endBatch()
            draggingLastControlPoint = false
            newPath = null
            newCurve = null
            redraw()
        }

        private fun nearStartPoint(mouseWorldPoint: Vector2): Boolean {
            val path = newPath ?: return false
            return (path.start.eval() - mouseWorldPoint).magnitudeSquared() < zoomedControlPointThreshold2
        }

        override fun onMouseMoved(event: MouseEvent, world: Vector2) {
            selectBaseMouseMoved(world)

            val curve = newCurve
            if (curve == null) {
                mouseWorldPosition = world
                redraw()
            } else {
                curve.controlPoint2.set(Vector2Constant(world))
            }
        }

        override fun draw() {
            val path = newPath
            val curve = newCurve
            if (path != null) {
                with(renderer) {

                    if (nearStartPoint(mouseWorldPosition)) {
                        path.handles().firstOrNull()?.let {
                            drawControlPoint(it, hoverColor)
                        }
                    }

                    if (curve == null) {
                        val end = path.endPoint()
                        // Draw a thin line where the next line segment will appear (if the user _clicks_ the mouse).
                        line(
                            worldToContent(end), //(path.pathParts.lastOrNull()?.end ?: path.start).eval()),
                            worldToContent(mouseWorldPosition),
                            selectionLineThickness,
                            selectionColor
                        )
                        val prevCurve = path.pathParts.lastOrNull() as? BezierPathPart
                        if (prevCurve != null) {
                            val bezier = Bezier.create(
                                end,
                                end - (prevCurve.controlPoint2.eval() - end),
                                mouseWorldPosition,
                                mouseWorldPosition
                            )
                            // Draw a bezier curve that would be produced if the user _drags_ the mouse.
                            val points = bezier.pointsForPolyline(1f, 0.1)
                            var prev = end
                            for (point in points) {
                                line(
                                    worldToContent(prev),
                                    worldToContent(point),
                                    selectionLineThickness,
                                    selectionColor
                                )
                                prev = point
                            }
                        }

                    } else {
                        // Draw the tangent from the controlPoint2 through BezierPathPart.end to a symmetric control
                        // point of the next (yet to be created) bezier path part.
                        val nextCp1 = mouseWorldPosition
                        val cp2 = curve.end.eval() - (nextCp1 - curve.end.eval())

                        line(
                            worldToContent(cp2),
                            worldToContent(nextCp1),
                            selectionLineThickness,
                            selectionColor
                        )
                    }
                }
            }
        }
    }

    // endregion PathTool

    // region ==== LineTool ====

    internal inner class LineTool : SelectToolBase() {

        override val toolType get() = ToolType.LINE

        private var startWorldPoint: Vector2? = null
        private var endWorldPoint: Vector2? = null

        override fun specialTip() = "[Drag] to create a line."

        override fun acceptShape(shape: Shape) = shape is GeometryShape && shape.geometry is Line

        override fun onMousePressed(event: MouseEvent, world: Vector2) {
            if (! event.isPrimary) return

            if (! selectBaseMousePressed(event, world)) {
                startWorldPoint = world
                event.capture()
            }
        }

        override fun onMouseDragged(event: MouseEvent, world: Vector2) {

            if (startWorldPoint != null) {
                val snapped = if (event.isConstrainMove()) world.constrainAngle(startWorldPoint !!) else world
                endWorldPoint = snapped
                redraw()
            }

        }

        override fun onMouseReleased(event: MouseEvent, world: Vector2) {
            if (startWorldPoint != null && endWorldPoint != null) {
                val line = Line.create(startWorldPoint !!, endWorldPoint !!)
                val shape = line.createShape(document.generateName(line.baseName), defaultStyle())
                history.batch("Create Line") {
                    diagram.clearSelection()
                    diagram.currentLayerOrGroup.addShape(shape)
                    diagram.select(shape)
                }
                startWorldPoint = null
                endWorldPoint = null
            }
        }

        override fun draw() {
            if (startWorldPoint != null && endWorldPoint != null) {
                with(renderer) {
                    line(
                        worldToContent(startWorldPoint !!),
                        worldToContent(endWorldPoint !!),
                        selectionLineThickness,
                        selectionColor
                    )
                }
            }
        }
    }

    // endregion LineTool

    // region ==== MoveTool ====

    /**
     * Allows the selected shapes to be dragged to move them.
     * [makeSelection] defines which shapes in the selection when this tool starts.
     *
     * This class assumes that the primary mouse button is down, when this tool begins.
     *
     * NOTE, [MoveTool] does NOT have its own [ToolType]. Instead, it uses one
     * of [SelectToolBase]'s [ToolType]s. i.e. [ToolType.SELECT], [ToolType.RECTANGLE] etc.
     * depending on how the move was started. Most of the time it is [ToolType.SELECT].
     */
    internal inner class MoveTool(
        override val toolType: ToolType,
        private val batchLabel: String,
        private val makeSelection: Batch.() -> Unit,
    ) : Tool {

        private var dragThresholdPassed = false

        private var previousWorld = dragStartWorld

        override fun tip() =
            "[Drag] to move. [$constrainMoveMod+Drag] snap angle. [$connectMod+Drag] to break connections"

        override fun begin() {
            history.beginBatch(batchLabel).makeSelection()
            handles.clear()
        }

        override fun onMouseDragged(event: MouseEvent, world: Vector2) {

            val snapped = if (event.isConstrainMove()) world.constrainAngle(dragStartWorld, 90f) else world

            val diff = snapped - previousWorld
            if (! dragThresholdPassed) {
                if (abs(diff.x) > DRAG_THRESHOLD || abs(diff.y) > DRAG_THRESHOLD) {
                    dragThresholdPassed = true
                } else {
                    return
                }
            }

            val onlyShape = if (selection.shapes.size == 1) selection.shapes.first() else null
            if (onlyShape is GeometryShape && onlyShape.geometry is GuideLine) {
                // Special case for dragging GuideLines.
                (onlyShape.geometry as GuideLine).moveTo(world)
            } else {
                // General case for dragging any other shapes.
                history.renameCurrentBatch("Move ${summariseShapes(selection.shapes)}") {
                    MoveShapes.moveShapes(selection.shapes.toList(), diff, event.isConnect())
                }
            }
            previousWorld = snapped

        }

        override fun onMouseReleased(event: MouseEvent, world: Vector2) {
            history.endBatch()
            // Return to the previous tool, which is usually [SelectTool],
            // but when we clicked a shape from e.g. [QuadrilateralTool], then we
            // will return to [QuadrilateralTool].
            this@DiagramView.toolType = ToolType.NONE
            this@DiagramView.toolType = toolType
        }
    }
    // endregion MoveTool


    // region ==== Companion ====

    private companion object {

        private const val SNAP_ANGLE = 15f
        /**
         * The amount we need to move, before a drag is really considered to be real.
         */
        const val DRAG_THRESHOLD = 5f

        val snapControlMod = "Ctrl" // Keep in sync with `ControlPoint.snapControlMod`
        private fun MouseEvent.isSnapControl() = isControlDown

        private val constrainMoveMod = "Ctrl"
        private fun MouseEvent.isConstrainMove() = isControlDown


        val adjustSelectionMod = "Shift"
        private fun MouseEvent.isAdjustSelection() = isShiftDown

        val pullOutHandleMod = "Shift"
        private fun MouseEvent.isPullOutHandle() = isShiftDown

        val centeredMod = "Shift"
        private fun MouseEvent.isCentered() = isShiftDown


        val connectMod = "Alt"
        private fun MouseEvent.isConnect() = isAltDown

        private val lowerShapesMod = "Alt"
        private fun MouseEvent.isSelectLowerShape() = isAltDown

        fun MouseEvent.isPanButton() = isMiddle || (isSecondary && this.isControlDown)

        init {
            // The label in the `core` subproject must be kept in sync.
            // It has to be in `core`, because Geometries and ControlPoints use this label.
            Handle.snapControlMod = snapControlMod
        }

        private fun Vector2.constrainAngle(start: Vector2): Vector2 = constrainAngle(start, SNAP_ANGLE)

        private fun Vector2.constrainAngle(start: Vector2, constrainAngle: Float): Vector2 {
            val angle = (this - start).angleDegrees()
            if (angle.isNaN()) return this
            val snappedAngle = (angle / constrainAngle).roundToInt() * constrainAngle
            val distance = start.distanceTo(this)
            return start + Vector2(distance, 0f).rotateDegrees(snappedAngle.toDouble())
        }

    }
    // endregion Companion

}
