/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.control

import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.TextField
import uk.co.nickthecoder.glok.event.EventType
import uk.co.nickthecoder.vectorial.core.Angle
import uk.co.nickthecoder.vectorial.core.expression.*

/**
 * A JavaFX node, showing the value of a [AngleExpression] as a [TextField].
 * If [expression] is a [AngleAttribute], whose expression is a [AngleConstant],
 * then the text field is editable. Otherwise, they aren't, and a `break` button is enabled,
 * which when pressed, changes the [FloatAttribute]'s expression into a [AngleConstant]
 * (replacing the previous expression).
 *
 * If [expression] is not a [AngleAttribute], then the text field is always disabled,
 * and the break button is not displayed.
 */
class AngleExpressionView : NumericExpressionView<Angle, AngleExpression>() {

    var angleExpression: Expression<Angle>?
        @Suppress("UNCHECKED_CAST") get() = expression as Expression<Angle>?
        set(v) {
            expression = v
        }

    private val angleField = TextField("")
    private val degreesLabel = Label("°")

    init {
        styles.add("angle_expression")

        name = "Angle"
        angleField.prefColumnCount = 6

        inner.children.addAll( angleField, degreesLabel, breakButton )

        angleField.addEventFilter(EventType.KEY_PRESSED) { filterKeyDown(angleField, it) }

        angleField.textProperty.addChangeListener { _, _, _ -> guard.updateModel { updateModel() } }

    }

    /**
     * Called when the [expression]'s value changes, or if the [expression] is replaced by a different one.
     */
    override fun updateGUI() {
        angleField.text = angleExpression?.eval()?.degrees?.toString() ?: ""
        enableControls(angleField)
    }

    override fun updateModel() {
        val exp = expression as? AngleAttribute ?: return
        val degrees = angleField.text.toDoubleOrNull() ?: return
        exp.set(AngleConstant(Angle.degrees(degrees)))
    }

}
