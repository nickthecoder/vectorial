/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.control

import uk.co.nickthecoder.glok.control.FloatSpinner
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.control.TextField
import uk.co.nickthecoder.glok.property.boilerplate.stringProperty
import uk.co.nickthecoder.glok.scene.Alignment
import uk.co.nickthecoder.glok.scene.dsl.style
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.Expression
import uk.co.nickthecoder.vectorial.core.expression.Vector2Attribute
import uk.co.nickthecoder.vectorial.core.expression.Vector2Constant
import uk.co.nickthecoder.vectorial.core.expression.Vector2Expression


/**
 * A node, showing the value of a [Vector2Expression] as two [TextField]s.
 * If [expression] is a [Vector2Attribute], whose expression is a [Vector2Constant],
 * then the text fields are editable. Otherwise, they aren't, and a `break` button is enabled,
 * which when pressed, changes the [Vector2Attribute]'s expression into a [Vector2Constant]
 * (replacing the previous expression).
 *
 * If [expression] is not a [Vector2Attribute], then the text fields are always disabled,
 * and the break button is not displayed.
 */
class Vector2ExpressionView : NumericExpressionView<Vector2, Vector2Expression>() {

    var vector2Expression: Expression<Vector2>?
        @Suppress("UNCHECKED_CAST")
        get() = expression as Expression<Vector2>?
        set(v) {
            expression = v
        }

    /**
     * Used for the [Label] between the X and Y [TextField]s.
     * For positions, the default value of `,` is suitable, but for sizes, I suggest this is changed to `x`.
     */
    val separatorProperty by stringProperty(",")
    var separator: String
        get() = separatorProperty.value
        set(v) {
            separatorProperty.value = v
        }

    private val xField = FloatSpinner().apply {
        editor.prefColumnCount = 5
        editor.minColumnCount = 3
        min = -Float.MAX_VALUE
        max = Float.MAX_VALUE
    }
    private val yField = FloatSpinner().apply {
        editor.prefColumnCount = 5
        editor.minColumnCount = 3
        min = -Float.MAX_VALUE
        max = Float.MAX_VALUE
    }
    private val separatorLabel = Label("").apply {
        textProperty.bindTo(separatorProperty)
        alignment = Alignment.BOTTOM_LEFT
    }

    private val valuesListener = xField.valueProperty.addChangeListener { _, _, _ ->
        guard.updateModel { updateModel() }
    }.apply {
        yField.valueProperty.addChangeListener(this)
    }

    init {
        style("vector2_expression")
        inner.children.addAll(xField, separatorLabel, yField, breakButton)
    }

    /**
     * Called when the [expression]'s value changes, or if the [expression] is replaced by a different one.
     */
    override fun updateGUI() {
        val newValue = vector2Expression?.eval()

        xField.value = newValue?.x ?: 0f
        yField.value = newValue?.y?: 0f
        enableControls(xField, yField)
    }

    override fun updateModel() {
        val exp = expression as? Vector2Attribute ?: return
        exp.set(Vector2Constant(Vector2(xField.value, yField.value)))
    }

}
