/*
 * Copyright (C) 2023 Nick Robinson
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package uk.co.nickthecoder.vectorial.control

import uk.co.nickthecoder.glok.control.FormGrid
import uk.co.nickthecoder.glok.control.Label
import uk.co.nickthecoder.glok.scene.dsl.row

fun FormGrid.typeRow(obj : Any? ) {
    + row( "Type" ) {
        right = typeLabel(obj)
    }
}

// At a later date, we could change this to a button, which shows the source code / Dokka page or both.
fun typeLabel(obj : Any? ) : Label {
    val text = if (obj == null) {
        "<null>"
    } else {
        val simpleName = obj.javaClass.simpleName
        if (simpleName.endsWith("Impl")) {
            simpleName.substring(0, simpleName.length - 4)
        } else {
            simpleName
        }
    }
    return Label( text )
}
