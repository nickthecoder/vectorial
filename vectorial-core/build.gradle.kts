// vectorial-core
import org.jetbrains.dokka.DokkaConfiguration.Visibility
import org.jetbrains.dokka.gradle.DokkaTaskPartial

plugins {
    kotlin("jvm")
    `maven-publish`
}

tasks.withType<DokkaTaskPartial>().configureEach {
    dokkaSourceSets {
        configureEach {
            // used as project name in the header
            moduleName.set("Vectorial Core")

            // contains descriptions for the module and the packages
            includes.from("packages.md")

            documentedVisibilities.set(setOf(Visibility.PUBLIC))
            suppressInheritedMembers.set(true)
            suppressObviousFunctions.set(true)
        }
    }
}

publishing {
    publications {
        create<MavenPublication>("maven") {
            groupId = "uk.co.nickthecoder"
            artifactId = "vectorial-core"
            version = "0.1"
            from(components["java"])
        }
    }
    repositories {
        maven {
            // 42006798 is the GitLab project ID of project nickthecoder/vectorial
            url = uri("https://gitlab.com/api/v4/projects/42006798/packages/maven")
            credentials(HttpHeaderCredentials::class) {
                name = "Private-Token"
                value = providers.gradleProperty("gitLabPrivateToken").get()
                // The password is stored in ~/.gradle/gradle.properties
            }
            authentication {
                create<HttpHeaderAuthentication>("header")
            }
        }
    }
}

dependencies {
    val jsonVersion: String by project

    implementation("org.joml:joml:${Version.joml}")
    //implementation("com.eclipsesource.minimal-json:minimal-json:$jsonVersion")
    testImplementation("junit:junit:${Version.junit}")
}

