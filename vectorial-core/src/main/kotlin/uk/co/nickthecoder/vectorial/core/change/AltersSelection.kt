package uk.co.nickthecoder.vectorial.core.change

/**
 * Indicates that this [Change] alters the selection.
 * It is implemented by [SelectionChange], [SelectControlPoint] (which both ONLY alter the selection),
 * and also by changes which alter the selection as only PART of the change, such as [GroupShapes].
 *
 * NOTE. This does NOT implement [SkippableChange] because changes such as [GroupShapes] are not skippable.
 */
interface AltersSelection : DiagramChange
