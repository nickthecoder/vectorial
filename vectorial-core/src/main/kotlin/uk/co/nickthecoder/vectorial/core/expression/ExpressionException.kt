package uk.co.nickthecoder.vectorial.core.expression

import java.lang.Exception

open class ExpressionException(val expression: Expression<*>, message: String) : Exception(message) {

    init {
        latest = this
    }

    companion object {
        /**
         * When an ExpressionException is created, it is stored here.
         * This can aid debugging. The GUI doesn't need special handling at every possible code path,
         * and instead, when an [ExpressionException] is thrown, we can bring up the diagnostic tool
         * to look at it.
         */
        var latest: ExpressionException? = null
            private set

        fun reset() {
            latest = null
        }
    }

    override fun toString() = "ExpressionException $message for ${expression.debugInfo()}"
}


class RecursionLimitExceeded(expression: Expression<*>) :
    ExpressionException(expression, "Max depth exceeded. Maybe there is a cyclic dependency?") {

    override fun toString() = "$message for ${expression.debugInfo()}"

}


