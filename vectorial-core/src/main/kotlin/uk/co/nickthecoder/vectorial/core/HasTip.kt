package uk.co.nickthecoder.vectorial.core

/**
 * Information about this expression.
 * e.g. the ratio for [RatioAlongLine] and the distance for [DistanceAlongLine].
 *
 * Note, in the GUI, text within square brackets are shown in bold.
 */
interface HasTip {
    fun tip(): String = ""
}
