package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.expression.*

internal class ShapeStyleImpl(
    override val document: Document,
    thicknessExpression: FloatExpression,
    strokeExpression: ColorExpression,
    fillExpression: ColorExpression
) : ShapeStyle {

    constructor(document: Document, thickness: Float, stroke: Color, fill: Color) : this(
        document,
        FloatConstant(thickness),
        ColorConstant(stroke),
        ColorConstant(fill)
    )

    // Attributes
    override val attributes = AttributesImpl(this)
    override val thickness = createAttribute(attributes, ShapeStyle.THICKNESS, thicknessExpression)
    override val stroke = createAttribute(attributes, ShapeStyle.STROKE, strokeExpression)
    override val fill = createAttribute(attributes, ShapeStyle.FILL, fillExpression)

}
