package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.Handle
import uk.co.nickthecoder.vectorial.core.Vector2

/**
 *
 */
interface ConnectorToHandle : Connector {
    /**
     * The [Handle] that we are connected to.
     * Note, that this may be free, or connected to something else.
     */
    val handle: Handle
}


internal class ConnectorToHandleImpl(
    override val handle: Handle
) : CachedExpression<Vector2>(), Vector2Expression, ConnectorToHandle {

    init {
        addDependencies(* handle.dependsOn().toTypedArray())
    }

    override fun isFixed() = true

    override fun recalculate() = handle.position()

}
