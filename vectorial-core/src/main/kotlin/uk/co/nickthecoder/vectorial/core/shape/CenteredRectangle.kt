package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.ChangeGeometryImpl
import uk.co.nickthecoder.vectorial.core.expression.*

internal class CenteredRectangle(
    centerExpression: Vector2Expression,
    sizeExpression: Vector2Expression

) : QuadrilateralImpl(), Rectangle {

    constructor(center: Vector2, size: Vector2) :
            this(Vector2Constant(center), Vector2Constant(size))

    override lateinit var shape: GeometryShape

    override val centered: Centered get() = Centered.CENTERED

    // Attributes
    override val attributes = AttributesImpl(this)
    override val center = createAttribute(attributes, "center", centerExpression)
    override val size = createAttribute(attributes, "size", sizeExpression)

    // Expressions
    override val corner0 = Corner(-0.5f, -0.5f)
    override val corner1 = Corner(0.5f, -0.5f)
    override val corner2 = Corner(0.5f, 0.5f)
    override val corner3 = Corner(-0.5f, 0.5f)


    // Continuous Geometry expressions
    override val start get() = corner0

    // End of expressions

    private val cachedControlPoints by lazy {
        listOf(
            SimpleAttributeHandle(shape, center),
            CenteredSizeHandle(shape, center, size),
            CenteredWidthHandle(shape, center, size),
            CenteredHeightHandle(shape, center, size),
        )
    }

    init {
        addCornerAttributes()
    }

    override fun handles(): List<Handle> = cachedControlPoints

    override fun changeCentered(centered: Centered) {
        if (centered == Centered.CORNER_BASED) {
            ChangeGeometryImpl(
                "Convert to a corner-based Diamond",
                shape,
                CornerBasedRectangle(Vector2Constant(corner0.eval()), Vector2Constant(corner2.eval()))
            ).now()
        }
    }

    override fun changeQuadrilateralType(type: QuadrilateralType) {

        when (type) {
            QuadrilateralType.RECTANGLE -> {}

            QuadrilateralType.PARALLELOGRAM -> {
                ChangeGeometryImpl(
                    "Convert to a parallelogram",
                    shape, CenteredParallelogram(center.expression, size.expression)
                ).now()
            }

            QuadrilateralType.DIAMOND -> {
                ChangeGeometryImpl(
                    "Convert to a diamond",
                    shape, CenteredDiamond(center.expression, size.expression)
                ).now()
            }
        }
    }

    override fun movableAttributes() = listOf(center)

    override fun toString() = "Rectangle center ${center.eval()} size ${size.eval()}}"

}
