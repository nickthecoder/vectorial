package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.Document

/**
 * Any change to the [Diagram] that will require redrawing.
 */
interface DiagramChange : Change {
    val diagram: Diagram

    override val document: Document
        get() = diagram.document
}
