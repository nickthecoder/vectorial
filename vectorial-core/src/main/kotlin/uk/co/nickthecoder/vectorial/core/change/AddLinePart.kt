package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.Vector2Constant
import uk.co.nickthecoder.vectorial.core.expression.Vector2Expression
import uk.co.nickthecoder.vectorial.core.expression.createAttribute
import uk.co.nickthecoder.vectorial.core.shape.LinePathPartImpl
import uk.co.nickthecoder.vectorial.core.shape.Path
import uk.co.nickthecoder.vectorial.core.shape.PathImpl

interface AddLinePart : PathChange {

    val endExpression: Vector2Expression

}

internal class AddLinePartImpl(

    override val path: PathImpl,
    override val endExpression: Vector2Expression

) : AddLinePart, InternalChange {

    constructor(path: PathImpl, point: Vector2) : this(path, Vector2Constant(point))

    override val label get() = "Add line"

    override val shape get() = path.shape

    override fun redo() {
        val start = path.end
        val end = createAttribute(path.attributes, "point${++ path.pointCounter}", endExpression)
        path.pathParts.add(LinePathPartImpl(path, start, end))
    }

    override fun undo() {
        path.pathParts.removeLast()
        path.attributes.remove("point${path.pointCounter --}")
    }

    override fun toString() = "$label to $endExpression"

}
