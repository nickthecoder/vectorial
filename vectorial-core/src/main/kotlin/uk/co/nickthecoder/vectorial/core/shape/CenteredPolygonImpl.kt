package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Handle
import uk.co.nickthecoder.vectorial.core.SimpleAttributeHandle
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.change.ChangeGeometryImpl
import uk.co.nickthecoder.vectorial.core.expression.Vector2Constant
import uk.co.nickthecoder.vectorial.core.expression.Vector2Expression

internal class CenteredPolygonImpl(

    sides: Int,
    centerExpression: Vector2Expression,
    vertexExpression: Vector2Expression

) : CenteredPolygonOrStarImpl(sides, centerExpression, vertexExpression), CenteredPolygon {

    constructor(sides: Int, center: Vector2, vertex: Vector2) :
        this(sides, Vector2Constant(center), Vector2Constant(vertex))

    override val polygonType: PolygonType get() = PolygonType.POLYGON

    override val centered: Centered get() = Centered.CENTERED


    override fun handles(): List<Handle> {
        var handles = cachedHandles
        if (handles == null) {
            handles = listOf(
                SimpleAttributeHandle(shape, center),
                RegularPolygonHandle(this, delta)
            )
            cachedHandles = handles
        }
        return handles
    }


    override fun calculateVertices(): List<Vector2Expression> {
        val vertices = mutableListOf<Vector2Expression>()
        vertices.add(center + delta)
        for (i in 1 until sides.eval()) {
            vertices.add(center + delta.rotateRadians(angle * i))
        }
        return vertices
    }

    override fun changeCentered(centered: Centered) {
        if (centered == Centered.CORNER_BASED) {
            val newGeometry = CornerBasedPolygonImpl(sides.eval(), start.eval(), vertices()[1].eval())
            ChangeGeometryImpl("Make Centered", shape, newGeometry).now()
            invalidateCache()
        }
    }

    override fun changePolygonType(type: PolygonType) {
        if (type == PolygonType.STAR) {
            val newGeometry = CenteredStarImpl(sides.eval(), center.expression, delta.expression)
            ChangeGeometryImpl("Change to star", shape, newGeometry).now()
            invalidateCache()
        }
    }

}
