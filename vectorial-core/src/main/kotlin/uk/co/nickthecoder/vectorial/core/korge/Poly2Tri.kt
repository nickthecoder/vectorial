/*
 * Copied and altered from https://github.com/korlibs/korge
 * I have attempted to make this more Kotlin-like, without changing functionality.
 *
 * The Korge version was Ported from .JS: https://github.com/r3mi/poly2tri.js/
 * This in turn was ported from a C++ version I believe :
 * https://github.com/greenm01/poly2tri
 *
 * Below is the copyright notice from the js version, which credits the C++ version.
 */

/*
 * Poly2Tri Copyright (c) 2009-2014, Poly2Tri Contributors
 * http://code.google.com/p/poly2tri/
 *
 * poly2tri.js (JavaScript port) (c) 2009-2017, Poly2Tri Contributors
 * https://github.com/r3mi/poly2tri.js
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 * * Redistributions of source code must retain the above copyright notice,
 *   this list of conditions and the following disclaimer.
 * * Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 * * Neither the name of Poly2Tri nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without specific
 *   prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package uk.co.nickthecoder.vectorial.core.korge

import uk.co.nickthecoder.vectorial.core.Vector2
import java.io.StringWriter
import kotlin.math.abs
import kotlin.math.min

/**
 * Converts polygon outlines into a mesh of [Triangle]s, suitable for filling.
 *
 * Note, there are a few constraints on the inputs.
 * There can be no duplicated points. No collinear straight lines, and the polygon
 * cannot overlap itself (e.g. a figure 8).
 */
class Poly2Tri {
    /**
     * Initial triangle factor, seed triangle will extend 30% of PointSet width to both left and right.
     */
    private var kAlpha = 0.3

    private val triangles = mutableListOf<Triangle>()
    private var map = mutableListOf<Triangle>()
    private var points = mutableListOf<Vector2>()
    private val edgeLists = mutableListOf<MutableList<Edge>?>()

    private var edgeList = mutableListOf<Edge>()

    private var front: AdvancingFront? = null

    /**
     * head of the advancing front
     */
    private var head: Vector2? = null

    /**
     * tail of the advancing front
     */
    private var tail: Vector2? = null

    private var basin = Basin()
    private var edgeEvent = EdgeEvent()

    fun addPolyline(polyline: List<Vector2>) {
        val len = polyline.size
        val points = this.points
        val start = points.size
        // Point addition
        points.addAll(polyline)

        while (edgeLists.size < points.size) edgeLists.add(null)

        // Edge initialization
        for (i in 0 until len) {
            val i1 = start + i
            val i2 = start + ((i + 1) % len)
            val p1 = points[i1]
            val p2 = points[i2]
            if (p1 != p2) {
                val edge = Edge(p1, p2)
                this.edgeList.add(edge)
                val index = if (edge.q == p1) i1 else i2

                if (edgeLists[index] == null) edgeLists[index] = mutableListOf()
                edgeLists[index]!!.add(edge)

            }
        }
    }

    /**
     * a Steiner point is a point that is not part of the input to a geometric optimization problem
     * but is added during the solution of the problem, to create a better solution than
     * would be possible from the original points alone.
     *
     * For example, you could choose to add the center of a circle as a Steiner Point, and then
     * the resulting mesh would contain triangles with one vertex at the center of the circle.
     * Without the Steiner Point, triangle vertices would only touch the circumference of the circle.
     */
    fun addSteinerPoint(point: Vector2) {
        this.points.add(point)
    }

    /**
     * Triangulate the polygon with holes and Steiner points.
     * Do this AFTER you've added the polyline, holes, and Steiner points
     */
    fun triangulate(): List<Triangle> {
        initTriangulation()
        createAdvancingFront()
        // Sweep points; build mesh
        sweepPoints()
        // Clean up
        finalizationPolygon()
        return triangles
    }

    // Private methods...

    fun pointCount() = points.size

    private fun initTriangulation() {
        var xMax = this.points[0].x
        var xMin = this.points[0].x
        var yMax = this.points[0].y
        var yMin = this.points[0].y

        // Calculate bounds
        val len = this.points.size
        for (i in 1 until len) {
            val p = this.points[i]
            if (p.x > xMax) xMax = p.x
            if (p.x < xMin) xMin = p.x
            if (p.y > yMax) yMax = p.y
            if (p.y < yMin) yMin = p.y
        }

        val dx = kAlpha * (xMax - xMin)
        val dy = kAlpha * (yMax - yMin)
        this.head = Vector2(xMax + dx, yMin - dy)
        this.tail = Vector2(xMin - dx, yMin - dy)

        // Sort points along y-axis
        genericSort(this, 0, this.points.size - 1, object : SortOps<Poly2Tri>() {
            override fun compare(subject: Poly2Tri, l: Int, r: Int): Int {
                return subject.points[l].compareTo(subject.points[r])
            }

            override fun swap(subject: Poly2Tri, indexL: Int, indexR: Int) {
                subject.points.swap(indexL, indexR)
                subject.edgeLists.swap(indexL, indexR)
            }
        })
    }

    private fun getPoint(index: Int): Vector2 {
        return this.points[index]
    }

    private fun addToMap(triangle: Triangle) {
        this.map.add(triangle)
    }

    private fun locateNode(point: Vector2): Node? {
        return this.front!!.locateNode(point.x)
    }

    private fun createAdvancingFront() {
        // Initial triangle
        val triangle = Triangle(this.points[0], this.tail!!, this.head!!)

        this.map.add(triangle)

        val head = Node(triangle.getPoint(1), triangle)
        val middle = Node(triangle.getPoint(0), triangle)
        val tail = Node(triangle.getPoint(2))

        this.front = AdvancingFront(head)

        head.next = middle
        middle.next = tail
        middle.prev = head
        tail.prev = middle
    }

    private fun mapTriangleToNodes(t: Triangle) {
        for (i in 0 until 3) {
            if (t.getNeighbor(i) == null) {
                val n = this.front!!.locatePoint(t.pointCW(t.getPoint(i))!!)
                if (n != null) {
                    n.triangle = t
                }
            }
        }
    }

    /**
     * Do a depth first traversal to collect triangles
     * @param triangle start
     */
    private fun meshClean(triangle: Triangle) {
        val triangles = mutableListOf(triangle)
        while (triangles.isNotEmpty()) {
            val t = triangles.removeLast()
            if (!t.isInterior()) {
                t.setInterior(true)
                this.triangles.add(t)
                for (i in 0 until 3) {
                    if (!t.constrainedEdge[i]) {
                        triangles.add(t.getNeighbor(i) ?: throw PointError("Null pointer", points))
                    }
                }
            }
        }
    }

    /**
     * Start sweeping the Y-sorted point set from bottom to top
     */
    private fun sweepPoints() {
        val len = pointCount()
        for (i in 1 until len) {
            val point = getPoint(i)
            val node = pointEvent(point)
            val edges = edgeLists.getOrNull(i)
            var j = 0
            while (edges != null && j < edges.size) {
                edgeEventByEdge(edges[j], node)
                ++j
            }
        }
    }

    private fun finalizationPolygon() {
        // Get an Internal triangle to start with
        var t = front!!.head.next!!.triangle!!
        val p = front!!.head.next!!.point
        while (!t.getConstrainedEdgeCW(p)) {
            t = t.neighborCCW(p)
                ?: error("Can't find neighborCCW($p) in $t")
        }

        // Collect interior triangles constrained by edges
        meshClean(t)
    }

    /**
     * Find closes node to the left of the new point and
     * create a new triangle. If needed new holes and basins
     * will be filled to.
     */
    private fun pointEvent(point: Vector2): Node {
        val node = locateNode(point)!!
        val newNode = newFrontTriangle(point, node)

        // Only need to check +epsilon since point never have smaller
        // x value than node due to how we fetch nodes from the front
        if (point.x <= node.point.x + (POLY2TRI_EPSILON)) {
            fill(node)
        }

        fillAdvancingFront(newNode)
        return newNode
    }

    private fun edgeEventByEdge(edge: Edge, node: Node) {

        edgeEvent.constrainedEdge = edge
        edgeEvent.right = (edge.p.x > edge.q.x)

        if (isEdgeSideOfTriangle(node.triangle!!, edge.p, edge.q)) {
            return
        }

        // For now, we will do all needed filling
        fillEdgeEvent(edge, node)
        edgeEventByPoints(edge.p, edge.q, node.triangle!!, edge.q)
    }

    private fun edgeEventByPoints(ep: Vector2, eq: Vector2, tri: Triangle, point: Vector2) {
        if (isEdgeSideOfTriangle(tri, ep, eq)) {
            return
        }

        var triangle = tri
        val p1 = triangle.pointCCW(point)!!
        val o1 = orient2d(eq, p1, ep)
        if (o1 === Orientation.COLLINEAR) {
            throw PointError("poly2tri EdgeEvent: Collinear not supported!", listOf(eq, p1, ep))
        }

        val p2 = triangle.pointCW(point)!!
        val o2 = orient2d(eq, p2, ep)
        if (o2 === Orientation.COLLINEAR) {
            throw PointError("poly2tri EdgeEvent: Collinear not supported!", listOf(eq, p2, ep))
        }

        if (o1 === o2) {
            // Need to decide if we are rotating CW or CCW to get to a triangle
            // that will cross edge
            triangle = if (o1 === Orientation.CW) {
                triangle.neighborCCW(point)!!
            } else {
                triangle.neighborCW(point)!!
            }
            edgeEventByPoints(ep, eq, triangle, point)
        } else {
            // This triangle crosses constraint so lets flippin start!
            flipEdgeEvent(ep, eq, triangle, point)
        }
    }

    /**
     * Creates a new front triangle and legalize it
     */
    private fun newFrontTriangle(point: Vector2, node: Node): Node {
        val triangle = Triangle(point, node.point, node.next!!.point)

        triangle.markNeighbor(node.triangle!!)
        addToMap(triangle)

        val newNode = Node(point)
        newNode.next = node.next
        newNode.prev = node
        node.next!!.prev = newNode
        node.next = newNode

        if (!legalize(triangle)) {
            mapTriangleToNodes(triangle)
        }

        return newNode
    }


    /**
     * Adds a triangle to the advancing front to fill a hole.
     */
    private fun fill(node: Node) {
        val triangle = Triangle(node.prev!!.point, node.point, node.next!!.point)

        triangle.markNeighbor(node.prev!!.triangle!!)
        triangle.markNeighbor(node.triangle!!)

        addToMap(triangle)

        // Update the advancing front
        node.prev!!.next = node.next
        node.next!!.prev = node.prev


        // If it was legalized the triangle has already been mapped
        if (!legalize(triangle)) {
            mapTriangleToNodes(triangle)
        }
    }

    /**
     * Fills holes in the Advancing Front
     */
    private fun fillAdvancingFront(n: Node) {
        // Fill right holes
        var node: Node? = n.next
        while (node!!.next != null) {
            if (isAngleObtuse(node.point, node.next!!.point, node.prev!!.point)) {
                break
            }
            fill(node)
            node = node.next
        }

        // Fill left holes
        node = n.prev
        while (node!!.prev != null) {
            if (isAngleObtuse(node.point, node.next!!.point, node.prev!!.point)) {
                break
            }
            fill(node)
            node = node.prev
        }

        // Fill right basins
        if (n.next != null && n.next!!.next != null) {
            if (isBasinAngleRight(n)) {
                fillBasin(n)
            }
        }
    }

    /**
     * Returns true if triangle was legalized
     */
    private fun legalize(t: Triangle): Boolean {
        // To legalize a triangle we start by finding if any of the three edges
        // violate the Delaunay condition
        for (i in 0 until 3) {
            if (t.delaunayEdge[i]) {
                continue
            }
            val ot = t.getNeighbor(i)
            if (ot != null) {
                val p = t.getPoint(i)
                val op = ot.oppositePoint(t, p)!!
                val oi = ot.index(op)

                // If this is a Constrained Edge or a Delaunay Edge(only during recursive legalization)
                // then we should not try to legalize
                if (ot.constrainedEdge[oi] || ot.delaunayEdge[oi]) {
                    t.constrainedEdge[i] = ot.constrainedEdge[oi]
                    continue
                }

                val inside = inCircle(p, t.pointCCW(p)!!, t.pointCW(p)!!, op)
                if (inside) {
                    // Let's mark this shared edge as Delaunay
                    t.delaunayEdge[i] = true
                    ot.delaunayEdge[oi] = true

                    // Let's rotate shared edge one vertex CW to legalize it
                    rotateTrianglePair(t, p, ot, op)

                    // We now got one valid Delaunay Edge shared by two triangles
                    // This gives us 4 new edges to check for Delaunay

                    // Make sure that triangle to node mapping is done only one time for a specific triangle
                    var notLegalized = !legalize(t)
                    if (notLegalized) {
                        mapTriangleToNodes(t)
                    }

                    notLegalized = !legalize(ot)
                    if (notLegalized) {
                        mapTriangleToNodes(ot)
                    }
                    // Reset the Delaunay edges, since they only are valid Delaunay edges
                    // until we add a new triangle or point.
                    t.delaunayEdge[i] = false
                    ot.delaunayEdge[oi] = false

                    // If triangle have been legalized no need to check the other edges since
                    // the recursive legalization will handle those, so we can end here.
                    return true
                }
            }
        }
        return false
    }

    /**
     * Fills a basin that has formed on the Advancing Front to the right
     * of given node.<br>
     * First we decide a left,bottom and right node that forms the
     * boundaries of the basin. Then we do a recursive fill.
     *
     * @param node - starting node, this or next node will be left node
     */
    private fun fillBasin(node: Node) {
        if (orient2d(node.point, node.next!!.point, node.next!!.next!!.point) === Orientation.CCW) {
            basin.leftNode = node.next!!.next
        } else {
            basin.leftNode = node.next
        }

        // Find the bottom and right node
        basin.bottomNode = basin.leftNode
        while (basin.bottomNode!!.next != null && basin.bottomNode!!.point.y >= basin.bottomNode!!.next!!.point.y) {
            basin.bottomNode = basin.bottomNode!!.next
        }
        if (basin.bottomNode === basin.leftNode) {
            // No valid basin
            return
        }

        basin.rightNode = basin.bottomNode
        while (basin.rightNode!!.next != null && basin.rightNode!!.point.y < basin.rightNode!!.next!!.point.y) {
            basin.rightNode = basin.rightNode!!.next
        }
        if (basin.rightNode === basin.bottomNode) {
            // No valid basins
            return
        }

        basin.width = basin.rightNode!!.point.x - basin.leftNode!!.point.x
        basin.leftHighest = basin.leftNode!!.point.y > basin.rightNode!!.point.y

        fillBasinReq(basin.bottomNode!!)
    }

    /**
     * Recursive algorithm to fill a Basin with triangles
     *
     * @param node - bottom_node
     */
    private fun fillBasinReq(node: Node) {
        // if shallow stop filling
        if (isShallow(node)) {
            return
        }

        fill(node)

        var currentNode: Node = node

        when {
            currentNode.prev === basin.leftNode && currentNode.next === basin.rightNode -> {
                return
            }

            currentNode.prev === basin.leftNode -> {
                val o = orient2d(currentNode.point, currentNode.next!!.point, currentNode.next!!.next!!.point)
                if (o === Orientation.CW) {
                    return
                }
                currentNode = currentNode.next!!
            }

            currentNode.next === basin.rightNode -> {
                val o = orient2d(currentNode.point, currentNode.prev!!.point, currentNode.prev!!.prev!!.point)
                if (o === Orientation.CCW) {
                    return
                }
                currentNode = currentNode.prev!!
            }

            else -> {
                // Continue with the neighbor node with lowest Y value
                currentNode = if (currentNode.prev!!.point.y < currentNode.next!!.point.y) {
                    currentNode.prev!!
                } else {
                    currentNode.next!!
                }
            }
        }

        fillBasinReq(currentNode)
    }

    private fun isShallow(node: Node): Boolean {
        val height = if (basin.leftHighest) {
            basin.leftNode!!.point.y - node.point.y
        } else {
            basin.rightNode!!.point.y - node.point.y
        }

        // if shallow stop filling
        if (basin.width > height) {
            return true
        }
        return false
    }

    private fun fillEdgeEvent(edge: Edge, node: Node) {
        if (edgeEvent.right) {
            fillRightAboveEdgeEvent(edge, node)
        } else {
            fillLeftAboveEdgeEvent(edge, node)
        }
    }

    private fun fillRightAboveEdgeEvent(edge: Edge, node: Node) {
        var currentNode: Node = node
        while (currentNode.next!!.point.x < edge.p.x) {
            // Check if next node is below the edge
            if (orient2d(edge.q, currentNode.next!!.point, edge.p) === Orientation.CCW) {
                fillRightBelowEdgeEvent(edge, currentNode)
            } else {
                currentNode = currentNode.next!!
            }
        }
    }

    private fun fillRightBelowEdgeEvent(edge: Edge, node: Node) {
        if (node.point.x < edge.p.x) {
            if (orient2d(node.point, node.next!!.point, node.next!!.next!!.point) === Orientation.CCW) {
                // Concave
                fillRightConcaveEdgeEvent(edge, node)
            } else {
                // Convex
                fillRightConvexEdgeEvent(edge, node)
                // Retry this one
                fillRightBelowEdgeEvent(edge, node)
            }
        }
    }

    private fun fillRightConcaveEdgeEvent(edge: Edge, node: Node) {
        fill(node.next!!)
        if (node.next!!.point !== edge.p) {
            // Next above or below edge?
            if (orient2d(edge.q, node.next!!.point, edge.p) === Orientation.CCW) {
                // Below
                if (orient2d(node.point, node.next!!.point, node.next!!.next!!.point) === Orientation.CCW) {
                    // Next is concave
                    fillRightConcaveEdgeEvent(edge, node)
                } else {
                    // Next is convex
                    /* jshint noempty:false */
                }
            }
        }
    }

    private fun fillRightConvexEdgeEvent(edge: Edge, node: Node) {
        // Next concave or convex?
        if (orient2d(
                node.next!!.point,
                node.next!!.next!!.point,
                node.next!!.next!!.next!!.point
            ) === Orientation.CCW
        ) {
            // Concave
            fillRightConcaveEdgeEvent(edge, node.next!!)
        } else {
            // Convex
            // Next above or below edge?
            if (orient2d(edge.q, node.next!!.next!!.point, edge.p) === Orientation.CCW) {
                // Below
                fillRightConvexEdgeEvent(edge, node.next!!)
            } else {
                // Above
                /* jshint noempty:false */
            }
        }
    }

    private fun fillLeftAboveEdgeEvent(edge: Edge, node: Node) {
        var currentNode = node
        while (currentNode.prev!!.point.x > edge.p.x) {
            // Check if next node is below the edge
            if (orient2d(edge.q, currentNode.prev!!.point, edge.p) === Orientation.CW) {
                fillLeftBelowEdgeEvent(edge, currentNode)
            } else {
                currentNode = currentNode.prev!!
            }
        }
    }

    private fun fillLeftBelowEdgeEvent(edge: Edge, node: Node) {
        if (node.point.x > edge.p.x) {
            if (orient2d(node.point, node.prev!!.point, node.prev!!.prev!!.point) === Orientation.CW) {
                // Concave
                fillLeftConcaveEdgeEvent(edge, node)
            } else {
                // Convex
                fillLeftConvexEdgeEvent(edge, node)
                // Retry this one
                fillLeftBelowEdgeEvent(edge, node)
            }
        }
    }

    private fun fillLeftConvexEdgeEvent(edge: Edge, node: Node) {
        // Next concave or convex?
        if (orient2d(node.prev!!.point, node.prev!!.prev!!.point, node.prev!!.prev!!.prev!!.point)
            === Orientation.CW
        ) {
            // Concave
            fillLeftConcaveEdgeEvent(edge, node.prev!!)
        } else {
            // Convex
            // Next above or below edge?
            if (orient2d(edge.q, node.prev!!.prev!!.point, edge.p) === Orientation.CW) {
                // Below
                fillLeftConvexEdgeEvent(edge, node.prev!!)
            } else {
                // Above
            }
        }
    }

    private fun fillLeftConcaveEdgeEvent(edge: Edge, node: Node) {
        fill(node.prev!!)
        if (node.prev!!.point !== edge.p) {
            // Next above or below edge?
            if (orient2d(edge.q, node.prev!!.point, edge.p) === Orientation.CW) {
                // Below
                if (orient2d(node.point, node.prev!!.point, node.prev!!.prev!!.point) === Orientation.CW) {
                    // Next is concave
                    fillLeftConcaveEdgeEvent(edge, node)
                } else {
                    // Next is convex
                }
            }
        }
    }

    private fun flipEdgeEvent(ep: Vector2, eq: Vector2, t: Triangle, p: Vector2) {
        var triangle = t
        val ot = triangle.neighborAcross(p) ?: throw PointError("FLIP failed due to missing triangle!", emptyList())

        val op = ot.oppositePoint(triangle, p)!!

        // Additional check from Java version (see issue #88)
        if (triangle.getConstrainedEdgeAcross(p)) {
            val index = triangle.index(p)
            throw PointError(
                "poly2tri Intersecting Constraints",
                listOf(p, op, triangle.getPoint((index + 1) % 3), triangle.getPoint((index + 2) % 3))
            )
        }

        if (inScanArea(p, triangle.pointCCW(p)!!, triangle.pointCW(p)!!, op)) {
            // Let's rotate shared edge one vertex CW
            rotateTrianglePair(triangle, p, ot, op)
            mapTriangleToNodes(triangle)
            mapTriangleToNodes(ot)

            // XXX: in the original C++ code for the next 2 lines, we are
            // comparing point values (and not pointers). In this JavaScript
            // code, we are comparing point references (pointers). This works
            // because we can't have 2 different points with the same values.
            // But to be really equivalent, we should use "Point.equals" here.
            if (p === eq && op === ep) {
                if (eq === edgeEvent.constrainedEdge!!.q && ep === edgeEvent.constrainedEdge!!.p) {
                    triangle.markConstrainedEdgeByPoints(ep, eq)
                    ot.markConstrainedEdgeByPoints(ep, eq)
                    legalize(triangle)
                    legalize(ot)
                } else {
                    // XXX: I think one of the triangles should be legalized here?
                    /* jshint noempty:false */
                }
            } else {
                val o = orient2d(eq, op, ep)
                triangle = nextFlipTriangle(o, triangle, ot, p, op)
                flipEdgeEvent(ep, eq, triangle, p)
            }
        } else {
            val newP = nextFlipPoint(ep, eq, ot, op)
            flipScanEdgeEvent(ep, eq, triangle, ot, newP!!)
            edgeEventByPoints(ep, eq, triangle, p)
        }
    }

    /**
     * Scan part of the FlipScan algorithm<br>
     * When a triangle pair isn't flippable we will scan for the next
     * point that is inside the flip triangle scan area. When found
     * we generate a new flipEdgeEvent
     *
     * @param ep - last point on the edge we are traversing
     * @param eq - first point on the edge we are traversing
     * @param flip_triangle - the current triangle sharing the point eq with edge
     */
    private fun flipScanEdgeEvent(ep: Vector2, eq: Vector2, flip_triangle: Triangle, t: Triangle, p: Vector2) {
        val ot = t.neighborAcross(p) ?: error("FLIP failed due to missing triangle")

        val op = ot.oppositePoint(t, p)

        if (inScanArea(eq, flip_triangle.pointCCW(eq)!!, flip_triangle.pointCW(eq)!!, op!!)) {
            // flip with new edge op.eq
            flipEdgeEvent(eq, op, ot, op)
        } else {
            val newP = nextFlipPoint(ep, eq, ot, op)
            flipScanEdgeEvent(ep, eq, flip_triangle, ot, newP!!)
        }
    }

    /**
     * After a flip we have two triangles and know that only one will still be
     * intersecting the edge. So decide which to contiune with and legalize the other
     *
     * @param o - should be the result of an orient2d( eq, op, ep )
     * @param t - triangle 1
     * @param ot - triangle 2
     * @param p - a point shared by both triangles
     * @param op - another point shared by both triangles
     * @return returns the triangle still intersecting the edge
     */
    private fun nextFlipTriangle(o: Orientation, t: Triangle, ot: Triangle, p: Vector2, op: Vector2): Triangle {
        if (o === Orientation.CCW) {
            // ot is not crossing edge after flip
            ot.delaunayEdge[ot.edgeIndex(p, op)] = true
            legalize(ot)
            ot.clearDelaunayEdges()
            return t
        }

        // t is not crossing edge after flip
        t.delaunayEdge[t.edgeIndex(p, op)] = true
        legalize(t)
        t.clearDelaunayEdges()
        return ot
    }


    companion object {

        private fun isEdgeSideOfTriangle(triangle: Triangle, ep: Vector2, eq: Vector2): Boolean {
            val index = triangle.edgeIndex(ep, eq)
            if (index != -1) {
                triangle.markConstrainedEdgeByIndex(index)
                triangle.getNeighbor(index)?.markConstrainedEdgeByPoints(ep, eq)
                return true
            }
            return false
        }

        /**
         * The basin angle is decided against the horizontal line [1,0].
         * @return true if angle < 3*π/4
         */
        private fun isBasinAngleRight(node: Node): Boolean {
            val ax = node.point.x - node.next!!.next!!.point.x
            val ay = node.point.y - node.next!!.next!!.point.y
            check(ay >= 0) { "unordered y" }
            return (ax >= 0 || abs(ax) < ay)
        }

        /**
         * Requirement
         *
         * 1. a,b and c form a triangle.<br>
         * 2. a and d is known to be on opposite side of bc<br>
         *
         *                a
         *                +
         *               / \
         *              /   \
         *            b/     \c
         *            +-------+
         *           /    d    \
         *          /           \
         *
         * Fact: d has to be in area B to have a chance to be inside the circle formed by
         *  a,b and c<br>
         *  d is outside B if orient2d(a,b,d) or orient2d(c,a,d) is CW<br>
         *  This preknowledge gives us a way to optimize the incircle test
         * @param pa - triangle point, opposite d
         * @param pb - triangle point
         * @param pc - triangle point
         * @param pd - point opposite a
         * @return true if d is inside circle, false if on circle edge
         */
        private fun inCircle(pa: Vector2, pb: Vector2, pc: Vector2, pd: Vector2): Boolean {
            val adx = pa.x - pd.x
            val ady = pa.y - pd.y
            val bdx = pb.x - pd.x
            val bdy = pb.y - pd.y

            val adxbdy = adx * bdy
            val bdxady = bdx * ady
            val oabd = adxbdy - bdxady
            if (oabd <= 0) {
                return false
            }

            val cdx = pc.x - pd.x
            val cdy = pc.y - pd.y

            val cdxady = cdx * ady
            val adxcdy = adx * cdy
            val ocad = cdxady - adxcdy
            if (ocad <= 0) {
                return false
            }

            val bdxcdy = bdx * cdy
            val cdxbdy = cdx * bdy

            val alift = adx * adx + ady * ady
            val blift = bdx * bdx + bdy * bdy
            val clift = cdx * cdx + cdy * cdy

            val det = alift * (bdxcdy - cdxbdy) + blift * ocad + clift * oabd
            return det > 0
        }

        /**
         * Rotates a triangle pair one vertex CW
         *
         *           n2                    n2
         *      P +-----+             P +-----+
         *        | t  /|               |\  t |
         *        |   / |               | \   |
         *      n1|  /  |n3           n1|  \  |n3
         *        | /   |    after CW   |   \ |
         *        |/ oT |               | oT \|
         *        +-----+ oP            +-----+
         *           n4                    n4
         *
         */
        private fun rotateTrianglePair(t: Triangle, p: Vector2, ot: Triangle, op: Vector2) {
            val n1 = t.neighborCCW(p)
            val n2 = t.neighborCW(p)
            val n3 = ot.neighborCCW(op)
            val n4 = ot.neighborCW(op)

            val ce1 = t.getConstrainedEdgeCCW(p)
            val ce2 = t.getConstrainedEdgeCW(p)
            val ce3 = ot.getConstrainedEdgeCCW(op)
            val ce4 = ot.getConstrainedEdgeCW(op)

            val de1 = t.getDelaunayEdgeCCW(p)
            val de2 = t.getDelaunayEdgeCW(p)
            val de3 = ot.getDelaunayEdgeCCW(op)
            val de4 = ot.getDelaunayEdgeCW(op)

            t.legalize(p, op)
            ot.legalize(op, p)

            // Remap delaunay_edge
            ot.setDelaunayEdgeCCW(p, de1)
            t.setDelaunayEdgeCW(p, de2)
            t.setDelaunayEdgeCCW(op, de3)
            ot.setDelaunayEdgeCW(op, de4)

            // Remap constrained_edge
            ot.setConstrainedEdgeCCW(p, ce1)
            t.setConstrainedEdgeCW(p, ce2)
            t.setConstrainedEdgeCCW(op, ce3)
            ot.setConstrainedEdgeCW(op, ce4)

            // Remap neighbors
            // XXX: might optimize the markNeighbor by keeping track of
            //      what side should be assigned to what neighbor after the
            //      rotation. Now mark neighbor does lots of testing to find
            //      the right side.
            t.clearNeighbors()
            ot.clearNeighbors()
            if (n1 != null) {
                ot.markNeighbor(n1)
            }
            if (n2 != null) {
                t.markNeighbor(n2)
            }
            if (n3 != null) {
                t.markNeighbor(n3)
            }
            if (n4 != null) {
                ot.markNeighbor(n4)
            }
            t.markNeighbor(ot)
        }


        /**
         * When we need to traverse from one triangle to the next we need
         * the point in current triangle that is the opposite point to the next
         * triangle.
         */
        private fun nextFlipPoint(ep: Vector2, eq: Vector2, ot: Triangle, op: Vector2): Vector2? {
            val o2d = orient2d(eq, op, ep)
            return when {
                o2d === Orientation.CW -> ot.pointCCW(op) // Right
                o2d === Orientation.CCW -> ot.pointCW(op) // Left
                else -> throw PointError(
                    "poly2tri [Unsupported] nextFlipPoint: opposing point on constrained edge!",
                    listOf(eq, op, ep)
                )
            }
        }

        private fun inScanArea(pa: Vector2, pb: Vector2, pc: Vector2, pd: Vector2): Boolean {
            val oadb = (pa.x - pb.x) * (pd.y - pb.y) - (pd.x - pb.x) * (pa.y - pb.y)
            if (oadb >= -POLY2TRI_EPSILON) return false

            val oadc = (pa.x - pc.x) * (pd.y - pc.y) - (pd.x - pc.x) * (pa.y - pc.y)
            if (oadc <= POLY2TRI_EPSILON) return false
            return true
        }

        /**
         * Check if the angle between (pa,pb) and (pa,pc) is obtuse i.e. (angle > π/2 || angle < -π/2)
         */
        private fun isAngleObtuse(pa: Vector2, pb: Vector2, pc: Vector2): Boolean {
            val ax = pb.x - pa.x
            val ay = pb.y - pa.y
            val bx = pc.x - pa.x
            val by = pc.y - pa.y
            return (ax * bx + ay * by) < 0
        }

        /**
         * Precision to detect repeated or collinear points
         */
        private const val POLY2TRI_EPSILON = 1e-12


        /**
         * Formula to calculate signed area
         *
         *     A[P1,P2,P3]  =  (x1*y2 - y1*x2) + (x2*y3 - y2*x3) + (x3*y1 - y3*x1)
         *                  =  (x1-x3)*(y2-y3) - (y1-y3)*(x2-x3)
         *
         */
        private fun orient2d(pa: Vector2, pb: Vector2, pc: Vector2): Orientation {
            val detleft = (pa.x - pc.x) * (pb.y - pc.y)
            val detright = (pa.y - pc.y) * (pb.x - pc.x)
            val v = detleft - detright
            return when {
                v > -(POLY2TRI_EPSILON) && v < (POLY2TRI_EPSILON) -> Orientation.COLLINEAR
                v > 0 -> Orientation.CCW
                else -> Orientation.CW
            }
        }
    }

}

// END OF Poly2Tri

class Triangle(val a: Vector2, val b: Vector2, val c: Vector2) {

    private val points = arrayOf(a, b, c)

    override fun toString() = "Triangle $a $b $c" // : ${points_.toList()}"

    val p1 get() = points[1] // TODO Replace p1 with b

    private val neighbors = arrayOfNulls<Triangle>(3)

    /**
     * Has this triangle been marked as an interior triangle?
     */
    private var interior = false

    /**
     * Flags to determine if an edge is a Constrained edge
     */
    val constrainedEdge = booleanArrayOf(false, false, false)

    /**
     * Flags to determine if an edge is a Delauney edge
     */
    val delaunayEdge = booleanArrayOf(false, false, false)


    /**
     * Get one vertex of the triangle.
     */
    fun getPoint(index: Int): Vector2 = this.points[index]

    /**
     * Get all 3 vertices of the triangle as an array
     * @public
     * @return {Array.<Vector2>}
     */

    fun getPoints(): Array<Vector2> = this.points

    fun getNeighbor(index: Int): Triangle? = this.neighbors[index]

    /**
     * Test if this Triangle contains the Vector2 object given as parameter as one of its vertices.
     * References are compared, not values.
     */
    // Here we are comparing point references, not values
    private fun containsPoint(point: Vector2): Boolean {
        val points = this.points
        return (point === points[0] || point === points[1] || point === points[2])
    }

    /**
     * Test if this Triangle contains the two Vector2 objects given as parameters among its vertices.
     * References are compared, not values.
     */
    private fun containsPoints(p1: Vector2, p2: Vector2): Boolean = this.containsPoint(p1) && this.containsPoint(p2)

    // TODO Use a var!
    /**
     * Has this triangle been marked as an interior triangle?
     */
    fun isInterior(): Boolean = this.interior

    /**
     * Mark this triangle as an interior triangle
     */
    fun setInterior(interior: Boolean) {
        this.interior = interior
    }

    /**
     * @throws PointError if we can't find objects
     */
    private fun markNeighbor(p1: Vector2, p2: Vector2, t: Triangle) {
        val points = this.points
        // Here we are comparing references, not values
        when {
            p1 === points[2] && p2 === points[1] || p1 === points[1] && p2 === points[2] -> this.neighbors[0] = t
            p1 === points[0] && p2 === points[2] || p1 === points[2] && p2 === points[0] -> this.neighbors[1] = t
            p1 === points[0] && p2 === points[1] || p1 === points[1] && p2 === points[0] -> this.neighbors[2] = t
            else -> throw PointError("poly2tri Invalid Triangle.markNeighborPointers() call", emptyList())
        }
    }

    /**
     * Exhaustive search to update neighbor pointers
     */
    fun markNeighbor(t: Triangle) {
        val points = this.points
        when {
            t.containsPoints(points[1], points[2]) -> {
                neighbors[0] = t
                t.markNeighbor(points[1], points[2], this)
            }

            t.containsPoints(points[0], points[2]) -> {
                neighbors[1] = t
                t.markNeighbor(points[0], points[2], this)
            }

            t.containsPoints(points[0], points[1]) -> {
                neighbors[2] = t
                t.markNeighbor(points[0], points[1], this)
            }
        }
    }


    fun clearNeighbors() {
        neighbors[0] = null
        neighbors[1] = null
        neighbors[2] = null
    }

    fun clearDelaunayEdges() {
        delaunayEdge[0] = false
        delaunayEdge[1] = false
        delaunayEdge[2] = false
    }

    /**
     * Returns the point clockwise to the given point.
     */
    fun pointCW(p: Vector2?): Vector2? {
        val points = this.points
        // Here we are comparing point references, not values
        return when {
            p === points[0] -> points[2]
            p === points[1] -> points[0]
            p === points[2] -> points[1]
            else -> null
        }
    }

    /**
     * Returns the point counter-clockwise to the given point.
     */
    fun pointCCW(p: Vector2): Vector2? {
        val points = this.points
        // Here we are comparing point references, not values
        return when {
            p === points[0] -> points[1]
            p === points[1] -> points[2]
            p === points[2] -> points[0]
            else -> null
        }
    }

    /**
     * Returns the neighbor clockwise to given point.
     */
    fun neighborCW(p: Vector2): Triangle? = when {
        // Here we are comparing point references, not values
        p === this.points[0] -> this.neighbors[1]
        p === this.points[1] -> this.neighbors[2]
        else -> this.neighbors[0]
    }

    /**
     * Returns the neighbor counter-clockwise to given point.
     */
    fun neighborCCW(p: Vector2): Triangle? = when {
        // Here we are comparing point references, not values
        p === this.points[0] -> this.neighbors[2]
        p === this.points[1] -> this.neighbors[0]
        else -> this.neighbors[1]
    }

    fun getConstrainedEdgeCW(p: Vector2): Boolean = when {
        // Here we are comparing point references, not values
        p === this.points[0] -> this.constrainedEdge[1]
        p === this.points[1] -> this.constrainedEdge[2]
        else -> this.constrainedEdge[0]
    }

    // Here we are comparing point references, not values
    fun getConstrainedEdgeCCW(p: Vector2): Boolean = when {
        p === this.points[0] -> this.constrainedEdge[2]
        p === this.points[1] -> this.constrainedEdge[0]
        else -> this.constrainedEdge[1]
    }

    // Additional check from Java version (see issue #88)
    // Here we are comparing point references, not values
    fun getConstrainedEdgeAcross(p: Vector2): Boolean = when {
        p === this.points[0] -> this.constrainedEdge[0]
        p === this.points[1] -> this.constrainedEdge[1]
        else -> this.constrainedEdge[2]
    }

    // Here we are comparing point references, not values
    fun setConstrainedEdgeCW(p: Vector2, ce: Boolean) = when {
        p === this.points[0] -> this.constrainedEdge[1] = ce
        p === this.points[1] -> this.constrainedEdge[2] = ce
        else -> this.constrainedEdge[0] = ce
    }

    // Here we are comparing point references, not values
    fun setConstrainedEdgeCCW(p: Vector2, ce: Boolean) = when {
        p === this.points[0] -> this.constrainedEdge[2] = ce
        p === this.points[1] -> this.constrainedEdge[0] = ce
        else -> this.constrainedEdge[1] = ce
    }

    // Here we are comparing point references, not values
    fun getDelaunayEdgeCW(p: Vector2): Boolean = when {
        p === this.points[0] -> this.delaunayEdge[1]
        p === this.points[1] -> this.delaunayEdge[2]
        else -> this.delaunayEdge[0]
    }

    // Here we are comparing point references, not values
    fun getDelaunayEdgeCCW(p: Vector2): Boolean = when {
        p === this.points[0] -> this.delaunayEdge[2]
        p === this.points[1] -> this.delaunayEdge[0]
        else -> this.delaunayEdge[1]
    }

    // Here we are comparing point references, not values
    fun setDelaunayEdgeCW(p: Vector2, e: Boolean) = when {
        p === this.points[0] -> this.delaunayEdge[1] = e
        p === this.points[1] -> this.delaunayEdge[2] = e
        else -> this.delaunayEdge[0] = e
    }

    // Here we are comparing point references, not values
    fun setDelaunayEdgeCCW(p: Vector2, e: Boolean) = when {
        p === this.points[0] -> this.delaunayEdge[2] = e
        p === this.points[1] -> this.delaunayEdge[0] = e
        else -> this.delaunayEdge[1] = e
    }

    /**
     * The neighboring triangle across from a given point.
     * @private
     * @param {Vector2} p - point object with {x,y}
     * @returns {Triangle}
     */
    // Here we are comparing point references, not values
    fun neighborAcross(p: Vector2): Triangle? = when {
        p === this.points[0] -> this.neighbors[0]
        p === this.points[1] -> this.neighbors[1]
        else -> this.neighbors[2]
    }

    fun oppositePoint(t: Triangle, p: Vector2): Vector2? = pointCW(t.pointCW(p))

    /**
     * Legalize triangle by rotating clockwise around [oPoint]
     * @throws PointError if [oPoint] can not be found
     */
    fun legalize(oPoint: Vector2, nPoint: Vector2) {
        val points = this.points
        // Here we are comparing point references, not values
        when {
            oPoint === points[0] -> {
                points[1] = points[0]
                points[0] = points[2]
                points[2] = nPoint
            }

            oPoint === points[1] -> {
                points[2] = points[1]
                points[1] = points[0]
                points[0] = nPoint
            }

            oPoint === points[2] -> {
                points[0] = points[2]
                points[2] = points[1]
                points[1] = nPoint
            }

            else -> {
                throw PointError("poly2tri Invalid Triangle.legalize() call", emptyList())
            }
        }
    }

    /**
     * Returns the index of a point in the triangle.
     * The point *must* be a reference to one of the triangle's vertices.
     * @returns index 0, 1 or 2
     * @throws PointError if [p] is not in this [Triangle]
     */
    fun index(p: Vector2): Int {
        val points = this.points
        // Here we are comparing point references, not values
        return when {
            p === points[0] -> 0
            p === points[1] -> 1
            p === points[2] -> 2
            else -> throw PointError("poly2tri Invalid Triangle.index() call", emptyList())
        }
    }

    /**
     * @return index 0, 1 or 2, or -1 if error
     */
    fun edgeIndex(p1: Vector2, p2: Vector2): Int {
        val points = this.points
        // Here we are comparing point references, not values
        if (p1 === points[0]) {
            if (p2 === points[1]) {
                return 2
            } else if (p2 === points[2]) {
                return 1
            }
        } else if (p1 === points[1]) {
            if (p2 === points[2]) {
                return 0
            } else if (p2 === points[0]) {
                return 2
            }
        } else if (p1 === points[2]) {
            if (p2 === points[0]) {
                return 1
            } else if (p2 === points[1]) {
                return 0
            }
        }
        return -1
    }

    /**
     * Mark an edge of this triangle as constrained.
     * @param index - edge index
     */
    fun markConstrainedEdgeByIndex(index: Int) {
        this.constrainedEdge[index] = true
    }

    /**
     * Mark an edge of this triangle as constrained.
     * This method takes two Vector2 instances defining the edge of the triangle.
     */
    fun markConstrainedEdgeByPoints(p: Vector2, q: Vector2) {
        val points = this.points
        // Here we are comparing point references, not values
        when {
            q === points[0] && p === points[1] || q === points[1] && p === points[0] -> this.constrainedEdge[2] = true
            q === points[0] && p === points[2] || q === points[2] && p === points[0] -> this.constrainedEdge[1] = true
            q === points[1] && p === points[2] || q === points[2] && p === points[1] -> this.constrainedEdge[0] = true
        }
    }
}

class PointError(message: String, points: Iterable<Vector2>) : Exception(message + " " + points.toList())

// Private helper classes / functions

/**
 * Advancing front node
 */
private class Node(var point: Vector2, var triangle: Triangle? = null) {
    var next: Node? = null
    var prev: Node? = null
    var value: Float = point.x
    override fun toString() = "$point tri : $triangle"
}

private class AdvancingFront(var head: Node) {
    private var search: Node = head

    override fun toString(): String {
        return StringWriter().apply {
            var node: Node? = head
            while (node != null) {
                append(node.point.toString())
                append(" ")
                node = node.next
            }
        }.toString()
    }

    private fun findSearchNode(): Node {
        return this.search
    }

    fun locateNode(x: Float): Node? {
        var node: Node? = this.search

        /* jshint boss:true */
        if (x < node!!.value) {
            while (true) {
                node = node!!.prev
                if (node == null) break
                if (x >= node.value) {
                    this.search = node
                    return node
                }
            }
        } else {
            while (true) {
                node = node!!.next
                if (node == null) break
                if (x < node.value) {
                    this.search = node.prev!!
                    return node.prev
                }
            }
        }
        return null
    }

    fun locatePoint(point: Vector2): Node? {
        val px = point.x
        var node: Node? = this.findSearchNode()
        val nx = node!!.point.x

        when {
            px == nx -> {
                // Here we are comparing point references, not values
                if (point !== node.point) {
                    // We might have two nodes with same x value for a short time
                    node = if (point === node.prev!!.point) {
                        node.prev
                    } else if (point === node.next!!.point) {
                        node.next
                    } else {
                        throw PointError("poly2tri Invalid AdvancingFront.locatePoint() call", emptyList())
                    }
                }
            }

            px < nx -> {
                /* jshint boss:true */
                while (true) {
                    node = node!!.prev
                    if (node == null) break
                    if (point === node.point) {
                        break
                    }
                }
            }

            else -> {
                while (true) {
                    node = node!!.next
                    if (node == null) break
                    if (point === node.point) {
                        break
                    }
                }
            }
        }

        if (node != null) {
            this.search = node
        }
        return node
    }

}

private enum class Orientation(val value: Int) {
    CW(+1), CCW(-1), COLLINEAR(0)
}

/**
 * Represents a simple polygon's edge
 * @throw PointError if p1 is the same as p2
 */
private class Edge(p1: Vector2, p2: Vector2) {

    var p: Vector2 = p1
    var q: Vector2 = p2

    init {
        if (p1.y > p2.y) {
            this.q = p1
            this.p = p2
        } else if (p1.y == p2.y) {
            if (p1.x > p2.x) {
                this.q = p1
                this.p = p2
            } else if (p1.x == p2.x) {
                throw PointError("poly2tri Invalid Edge constructor: repeated points!", listOf(p1))
            }
        }
    }

    override fun toString() = "Edge $p -> $q"
}

private class Basin {
    var leftNode: Node? = null
    var bottomNode: Node? = null
    var rightNode: Node? = null
    var width = 0f
    var leftHighest = false
}

private class EdgeEvent {
    var constrainedEdge: Edge? = null
    var right = false
}

private fun compare(lx: Float, ly: Float, rx: Float, ry: Float): Int {
    val ret = ly.compareTo(ry)
    return if (ret == 0) lx.compareTo(rx) else ret
}

private fun Vector2.compareTo(other: Vector2): Int = compare(this.x, this.y, other.x, other.y)

private fun <T> MutableList<T>.swap(lIndex: Int, rIndex: Int) {
    val temp = this[lIndex]
    this[lIndex] = this[rIndex]
    this[rIndex] = temp
}

// Sorting.
// This code annoys the hell out of me.
// And the sooner I get rid of the call to genericSort, and replace it with a "regular" sort the better!

private fun <T> genericSort(subject: T, left: Int, right: Int, ops: SortOps<T>): T =
    subject.also {
        timSort(subject, left, right, ops)
    }

private fun <T> insertionSort(arr: T, left: Int, right: Int, ops: SortOps<T>) {
    for (n in left + 1..right) {
        var m = n - 1

        while (m >= left) {
            if (ops.compare(arr, m, n) <= 0) break
            m--
        }
        m++

        if (m != n) ops.shiftLeft(arr, m, n)
    }
}

private fun <T> merge(arr: T, start: Int, mid: Int, end: Int, ops: SortOps<T>) {
    var s = start
    var m = mid
    var s2 = m + 1

    if (ops.compare(arr, m, s2) <= 0) return

    while (s <= m && s2 <= end) {
        if (ops.compare(arr, s, s2) <= 0) {
            s++
        } else {
            ops.shiftLeft(arr, s, s2)
            s++
            m++
            s2++
        }
    }
}

private fun <T> timSort(arr: T, l: Int, r: Int, ops: SortOps<T>, RUN: Int = 32) {
    val n = r - l + 1
    for (i in 0 until n step RUN) {
        insertionSort(arr, l + i, l + min((i + RUN - 1), (n - 1)), ops)
    }
    var size = RUN
    while (size < n) {
        for (left in 0 until n step (2 * size)) {
            val rize = min(size, n - left - 1)
            val mid = left + rize - 1
            val right = min((left + 2 * rize - 1), (n - 1))
            merge(arr, l + left, l + mid, l + right, ops)
        }
        size *= 2
    }
}

private abstract class SortOps<T> {
    abstract fun compare(subject: T, l: Int, r: Int): Int
    abstract fun swap(subject: T, indexL: Int, indexR: Int)
    fun shiftLeft(subject: T, indexL: Int, indexR: Int) {
        for (n in indexR downTo indexL + 1) swap(subject, n - 1, n)
    }
}
