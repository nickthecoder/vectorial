package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.ShapeParent
import uk.co.nickthecoder.vectorial.core.ShapeParentImpl
import uk.co.nickthecoder.vectorial.core.shape.Shape
import uk.co.nickthecoder.vectorial.core.shape.ShapeImpl

interface AddShape : DiagramChange {
    val index: Int
    val parent: ShapeParent
    val shape: Shape
}

internal class AddShapeImpl(
    index: Int,
    override val parent: ShapeParent,
    override val shape: Shape

) : AddShape, InternalChange {

    override val index = if (index < 0 || index > parent.shapes.size) {
        parent.shapes.size
    } else {
        index
    }

    override val diagram: Diagram
        get() = parent.diagram

    override val label
        get() = "New ${shape.name}"

    override fun redo() {
        (shape as ShapeImpl).parent = parent
        (shape.parent as ShapeParentImpl).shapes.add(index, shape)
    }

    override fun undo() {
        (shape.parent as ShapeParentImpl).shapes.remove(shape)
        // TODO, We cannot reset parent to null!
    }
}
