package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.ChangeConnectorImpl

/**
 * A [Connection] is the [Expression] that a [Handle] assigns to an [Attribute],
 * when the [Handle] is connected to something, such as another [Handle], or an [Edge].
 *
 * Most of the time, the type [V] is [Vector2], and has the same value as [Handle.position].
 * This is not always the case though.
 * For example, a [Handle] can be used to define an [Angle], relative to a central point.
 *
 * There is a middle-man, [Connector], which implements [Vector2Expression], and
 * hold extra information describing what we are connected to.
 *
 * ## Class Diagram
 *                                                     ┌───────────────────┐
 *                                                     │   Expression<V>   │
 *                                                     │                   │
 *                                                     └───────────────────┘
 *                                                               △
 *                                                               │
 *                                         ┌─────────────────────┴───────┐
 *                                         │                             │
 *                                         │                   ┌─────────┴─────────┐
 *                                         │                   │ Vector2Expression │
 *                                         │                   │                   │
 *                                         │                   └───────────────────┘
 *                              ┌──────────┴──────────┐                  △
 *                              │    Connection<V>    │                  │
 *                              │                     │       ┌──────────┴────────┐
 *      ┌────────────────┐      │ connector           ├───────┤     Connector     │
 *      │ PointConvertor ├──────┤ convertor           │       │                   │
 *      │                │      └─────────────────────┘       │ isFixed()         │
 *      │ fromVector2()  │             △                      │                   │
 *      │ toVector2()    │             │                      └───────────────────┘
 *      └────────────────┘             │                                △
 *                          ┌──────────┴──┐                             │
 *              ┌───────────┴───────┐    etc.        ┌──────────────────┴────┐
 *              │ Vector2Connection │                │                       │
 *              │                   │      ┌─────────┴────────┐   ┌──────────┴────────┐
 *              │                   │      │ MovableConnector │   │ ConnectorToHandle │
 *              └───────────────────┘      │                  │   │                   │     ┌──────────┐
 *                                         │ moveTowards()    │   │ handle            ├─────┤  Handle  │
 *                                         │ isFixed()=false  │   │ isFixed()=true    │     │          │
 *                                         └──────────────────┘   └───────────────────┘     └──────────┘
 *                                                   △
 *                                                   │
 *                                      ┌────────────┴───────────┐
 *                                      │    ConnectorToEdge     │
 *     ┌────────────────┐               │                        │                 ┌───────────┐
 *     │     <enum>     │               │ edge                   ├─────────────────┤   Edge    │
 *     │ ConnectorType  ├───────────────┤ connectorType          │                 │           │
 *     │                │               │ isBounded              │                 └───────────┘
 *     └────────────────┘               │                        │                       △
 *                                      └────────────────────────┘                       │
 *                                                  △                                    │
 *                                                  │                                    │
 *                                                  │                                    │
 *              ┌──────────────────────┬────────────┴───────┐ etc.                       │
 *              │                      │                    │                     ┌──────┴───────┐ etc.
 *     ┌────────┴──────────┐  ┌────────┴───────┐  ┌─────────┴────────┐            │              │
 *     │ DistanceAlongLine │  │ RatioAlongLine │  │ RatioAlongBezier │    ┌───────┴─────┐   ┌────┴───────┐
 *     │                   │  │                │  │ edge             ├────┤ BezierEdge  │   │  LineEdge  │
 *     └───────────────────┘  └────────────────┘  └──────────────────┘    │             │   │            │
 *                                                                        └─────────────┘   └────────────┘
 *
 * Created with [Blokart](https://gitlab.com/nickthecoder/blokart)
 */
interface Connection<V : Any> : Expression<V> {

    /**
     * A [Vector2Expression], which also holds additional information about what we are connected to.
     * For example, if we are connected to an [Edge], then this will be an [ConnectorToEdge],
     * which hold details of which edge, and whether we are a fixed distance from the start/end or
     * as a ratio along the line.
     */
    val connector: Connector

    val converter: PointConverter<V>

    fun createConstant(value: V): Constant<V>

    fun changeConnector(connectorType: ConnectorType, isBounded: Boolean)

    /**
     * A convenience method which returns [Connector.isFixed].
     */
    fun isFixed(): Boolean = connector.isFixed()
}

/**
 * The base class for all implementations of [Connection].
 *
 * There are subclasses for each `value` type supported by Vectorial
 * (`Double`, `Float`, `String`, [Vector2] etc.).
 * See the description in [Expression]
 */
abstract class ConnectionImpl<V : Any>(

    final override var connector: Connector,
    final override val converter: PointConverter<V>

) : CachedExpression<V>(), Connection<V> {

    init {
        addDependencies(connector)
    }

    final override fun recalculate(): V {
        val value = converter.fromVector2(connector.eval())
        return value
    }

    override fun changeConnector(connectorType: ConnectorType, isBounded: Boolean) {
        (connector as? ConnectorToEdge)?.let { connector ->
            val newConnector = connector.edge.createConnector(connector.eval(), connectorType, isBounded)
            ChangeConnectorImpl(this, newConnector).now()
        }
    }

    internal fun changeConnectorImpl(newConnector: Connector) {
        removeDependencies(connector)
        addDependencies(newConnector)
        connector = newConnector
        dirty()
    }

}
