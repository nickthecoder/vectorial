package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.change.*
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.shape.Shape

/**
 * A point which the user can drag to change the shape (or other properties) of a shape.
 *
 * The simplest variety is [SimpleAttributeHandle] (which just changes a [Vector2Attribute]).
 * However, there are more specialised varieties which are not directly related to a [Vector2Attribute].
 * For example, a [Handle] could be used to specify the radius of a circle, or the radius of a corner etc.
 *
 * This used to be called a `ControlPoint`. However, that was confusing, because Bezier Curves have control points
 * ([Vector2Attribute]s). There may be some code/comments which use the old terminology.
 */
interface Handle : HasTip {

    val shape: Shape
    val name: String

    fun dependsOn(): List<Expression<*>>

    fun position(): Vector2

    /**
     * Is this handle free to move anywhere?
     */
    fun isFree(): Boolean

    /**
     * Is this control point fixed? i.e. it has no degrees of freedom.
     */
    fun isFixed(): Boolean

    override fun tip() = "Position ${position().max2DPs()}"

    /**
     * Can this [Handle] be connected to other shapes (along their [Edge]s, or to other [Handle]s)?
     */
    fun isConnectable(): Boolean

    /**
     * Attempts to make a connection.
     *
     * @return true iff a connection was made.
     */
    fun makeConnection(shape: Shape, connector: Connector): Boolean

    fun breakConnection(shape: Shape, to: Vector2)

    fun connect(): Connector = ConnectorToHandleImpl(this)

    /**
     * @param handleThreshold2 The _square_ of the threshold distance.
     */
    fun isNear(world: Vector2, handleThreshold2: Float): Boolean {
        val point = position()
        val dx = world.x - point.x
        val dy = world.y - point.y
        val d2 = dx * dx + dy * dy
        return d2 < handleThreshold2
    }

    /**
     * The GUI may ask for a "snapped" position (e.g. dragging while holding down a modifier).
     * @return [position] unchanged if snapping is not supported, otherwise a `snapped` point near to [position].
     */
    fun snap(position: Vector2) = position

    /**
     * Selects this [Handle].
     */
    fun select() {
        SelectControlPoint.selectControlPoint(this)?.now()
    }

    /**
     * Move this [Handle] towards [position].
     *
     * NOTE, the final position of the [Handle] is only [position] if it is free to move anywhere.
     * However, many [Handle]s are constrained. For example, it may be constrained to a point along a line.
     */
    fun moveTo(position: Vector2)

    companion object {
        // Labels used by `ControlPoint.tip()` and `Geometry.tip()`
        // These values are set by `DiagramView.Companion` (which may rename them on platforms which do not
        // have the standard Ctrl,Shift,Alt modifier keys; i.e. macOS).
        var snapControlMod = "Ctrl"
    }
}
