package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.shape.AngleEdge
import uk.co.nickthecoder.vectorial.core.shape.LineEdge

enum class ConnectorType {

    /**
     * e.g. A point connected to the middle of an [Edge] will stay at the middle,
     * even when the edge grows/shrinks.
     */
    RATIO,

    /**
     * A fixed distance from the start of the [LineEdge]
     */
    FROM_START,

    /**
     * A fixed distance from the end of a [LineEdge]
     */
    FROM_END,

    /**
     * Currently only used by [AngleEdge]
     */
    DISTANCE
}
