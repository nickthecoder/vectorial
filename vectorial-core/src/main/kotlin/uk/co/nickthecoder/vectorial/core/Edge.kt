package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.shape.Shape

/**
 * Represents an edges of part of a [Shape].
 *
 * We can find out how close a point is to this [Edge] using [isNear].
 * If we are withing a given threshold, we can make a connection to this edge using
 * [createConnector], which returns an [ConnectorToEdge].
 *
 * Once we have the [ConnectorToEdge] we can use it as the [Vector2Expression] for the point that we
 * dragged to this [Edge].
 * If we move the shape that we connected to, then the connected point will automatically move too.
 *
 * Later, we can change the [ConnectorType] (See [ConnectorToEdge.connectorType]).
 * The default [ConnectorType] is [ConnectorType.RATIO].
 */
interface Edge {

    val shape: Shape

    /**
     * Used to highlight this edge when dragging a [Handle].
     */
    val points: List<Vector2>

    /**
     * Is [point] near this edge?
     *
     * param [threshold2] The square of the distance
     */
    fun isNear(point: Vector2, threshold2: Float): Boolean

    val supportedConnectorTypes: List<ConnectorType>

    fun createConnector(point: Vector2): ConnectorToEdge =
        createConnector(point, supportedConnectorTypes.first(), true)

    fun createConnector(point: Vector2, connectorType: ConnectorType, isBounded: Boolean): ConnectorToEdge

}
