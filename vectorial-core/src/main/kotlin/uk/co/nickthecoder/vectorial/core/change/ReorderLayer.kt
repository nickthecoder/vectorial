package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.LayerParentImpl

interface ReorderLayer : LayerChange {
    val layer: Layer
    val reorder: Reorder
}

internal class ReorderLayerImpl(

    override val layer: Layer,
    override val reorder: Reorder

) : ReorderLayer, InternalChange {

    private val oldIndex = layer.parent.layers.indexOf(layer)

    override val label: String
        get() = "Layer " + when (reorder) {
            Reorder.TOP -> "Top"
            Reorder.RAISE -> "Raise"
            Reorder.LOWER -> "Lower"
            Reorder.BOTTOM -> "Bottom"
        }
    override val diagram: Diagram get() = layer.diagram

    override fun redo() {
        val parent = layer.parent as LayerParentImpl
        val newIndex = when (reorder) {
            Reorder.TOP -> parent.layers.size -1
            Reorder.RAISE -> oldIndex + 1
            Reorder.LOWER -> oldIndex - 1
            Reorder.BOTTOM -> 0
        }
        if (newIndex >= 0 && newIndex < parent.layers.size) {
            parent.layers.remove(layer)
            parent.layers.add(newIndex, layer)
        }
    }

    override fun undo() {
        val parent = layer.parent as LayerParentImpl
        parent.layers.remove(layer)
        parent.layers.add(oldIndex, layer)
    }
}
