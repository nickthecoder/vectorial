package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.change.*
import uk.co.nickthecoder.vectorial.core.shape.Shape
import uk.co.nickthecoder.vectorial.core.shape.ShapeFilter

interface Layer : ShapeParent, LayerParent, Item {

    override val parent: LayerParent

    val isVisible: Boolean

    val isLocked: Boolean

    /**
     * How deeply nested is this layer? Zero for layers of a diagram.
     */
    val depth: Int

    fun reorder(reorder: Reorder): ReorderLayer = ReorderLayerImpl(this, reorder)
    fun changeVisibility(visible: Boolean): AdjustLayer = AdjustLayerImpl(this, visible, isLocked)
    fun changeLock(lock: Boolean): AdjustLayer = AdjustLayerImpl(this, isVisible, lock)

    fun delete(): DeleteLayer? {
        return if (diagram.layers.size > 1) {
            DeleteLayerImpl(this)
        } else {
            null
        }
    }

    companion object {
        fun create(name: String): Layer = LayerImpl(name)
    }
}

internal class LayerImpl(

    override val name: String

) : Layer, LayerParentImpl, ShapeParentImpl() {

    override val depth: Int
        get() {
            var result = 0
            var ancestor = parent
            while (ancestor !is Diagram) {
                result++
                ancestor = parent.parent as LayerParent
            }
            return result
        }

    override lateinit var parent: LayerParent

    override val diagram: Diagram get() = parent.diagram

    override val layers = mutableListOf<Layer>()

    override var isVisible: Boolean = true

    override var isLocked: Boolean = false


    override fun findShapesAt(point: Vector2, threshold: Float,threshold2: Float, into: MutableList<Shape>, filter: ShapeFilter) {
        if (! isVisible || isLocked) return

        super.findShapesAt(point, threshold, threshold2, into, filter)
        for (layer in layers) {
            layer.findShapesAt(point, threshold, threshold2, into, filter)
        }
    }

    override fun findTopShapeAt(point: Vector2, threshold: Float, threshold2: Float, filter: ShapeFilter): Shape? {
        if (! isVisible || isLocked) return null

        for (shape in shapes.asReversed()) {
            if (filter.acceptShape(shape) && shape.touching(point, threshold, threshold2)) {
                return shape
            }
        }
        return null
    }

    override fun addLayer(newLayer: Layer, position: NewLayerPosition) {
        AddLayerImpl(newLayer, diagram, this, position).now()
    }

    override fun toString() = ".".repeat(depth * 4) + "Layer $name"

}
