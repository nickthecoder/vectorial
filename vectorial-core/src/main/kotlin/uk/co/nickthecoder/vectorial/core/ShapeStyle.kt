package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.expression.Attributes
import uk.co.nickthecoder.vectorial.core.expression.ColorAttribute
import uk.co.nickthecoder.vectorial.core.expression.FloatAttribute
import uk.co.nickthecoder.vectorial.core.shape.CapStyle
import uk.co.nickthecoder.vectorial.core.shape.JoinStyle
import uk.co.nickthecoder.vectorial.core.shape.ShapeStyleImpl

interface ShapeStyle : AttributesOwner {

    val document: Document

    val attributes: Attributes

    val thickness: FloatAttribute

    val fill: ColorAttribute

    val stroke: ColorAttribute


    fun thickness() = thickness.eval()

    // TODO Use attributes
    fun capStyle() = CapStyle.ROUND
    fun joinStyle() = JoinStyle.ROUND

    fun fill() = fill.eval()
    fun stroke() = stroke.eval()

    companion object {
        const val FILL = "fill"
        const val STROKE = "stroke"
        const val THICKNESS = "thickness"

        fun create(
            document: Document,
            thickness: Float = 3f,
            stroke: Color = Colors.BLACK,
            fill: Color = Colors.LIGHTBLUE
        ): ShapeStyle = ShapeStyleImpl(document, thickness, stroke, fill)

        fun guideLineStyle(document: Document) = create(document, 1f, Colors["#449"])
    }
}
