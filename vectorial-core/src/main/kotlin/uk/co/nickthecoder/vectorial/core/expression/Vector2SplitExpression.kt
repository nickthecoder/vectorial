package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.Vector2

/**
 * A [Vector2Expression] which is evaluated from separate [x] and [y] expressions.
 */
class Vector2SplitExpression(val x: FloatExpression, val y: FloatExpression) :
    CachedVector2Expression() {

    init {
        addDependencies(x, y)
    }

    override fun recalculate() = Vector2(x.eval(), y.eval())

    override fun x() = x
    override fun y() = x

    override fun xFrom(other: Vector2Expression) = create(other.x(), y)
    override fun xFrom(x: FloatExpression) = create(x, y)

    override fun yFrom(other: Vector2Expression) = create(this.x, other.y())
    override fun yFrom(y: FloatExpression) = create(x, y)

    override fun toString() = "Vector2Split from ${x.debugInfo()} and ${y.debugInfo()} = ${eval()}"

    companion object {
        fun create(xExpression: FloatExpression, yExpression: FloatExpression): Vector2Expression {
            return if (xExpression is FloatConstant && yExpression is FloatConstant) {
                Vector2Constant(Vector2(xExpression.eval(), yExpression.eval()))
            } else {
                Vector2SplitExpression(xExpression, yExpression)
            }
        }
    }
}