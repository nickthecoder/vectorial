package uk.co.nickthecoder.vectorial.core.expression

abstract class WhenExpression<V : Any, E : Expression<V>>(
    val subject: Expression<*>,
    val matchLists: List<Pair<List<Expression<*>>, Expression<V>>>,
    val els: Expression<V>
) : CachedExpression<V>() {

    init {
        addDependencies(subject, els)
        for (matchList in matchLists) {
            for (match in matchList.first) {
                addDependencies(match)
            }
            addDependencies(matchList.second)
        }
    }

    override fun recalculate(): V {
        val subjectV = subject.eval()
        for (matchList in matchLists) {
            for (match in matchList.first) {
                if (match.eval() == subjectV) {
                    return matchList.second.eval()
                }
            }
        }
        return els.eval()
    }
}

abstract class When<V : Any, E : Expression<V>>(val subject: Expression<*>) {

    protected val matchLists = mutableListOf<Pair<List<Expression<*>>, E>>()

    fun result(result: E, vararg exp: Expression<*>) {
        matchLists.add(Pair(exp.toList(), result))
    }

    abstract fun els(result: E): WhenExpression<V, E> // return When(subject, matchLists, result)
}
