package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.expression.Attribute
import uk.co.nickthecoder.vectorial.core.expression.Connection
import uk.co.nickthecoder.vectorial.core.expression.Constant
import uk.co.nickthecoder.vectorial.core.expression.Expression
import uk.co.nickthecoder.vectorial.core.shape.Shape

/**
 * This implementation is identical to [SetShapeAttribute], but having a separate interface allows
 * listeners to distinguish between a simple change of ([Constant]) values, and a transition from
 * a [Constant] to a calculated expression.
 *
 * Note, this is badly named, because the new expression is not _always_ a [Connection],
 * it could be some other calculation, such as a reference to another [Attribute].
 *
 * See [BreakConnection]
 */
interface MakeConnection<V : Any, E : Expression<V>> : SetShapeAttribute<V, E>

internal class MakeConnectionImpl<V : Any, E : Expression<V>>(shape: Shape, attribute: Attribute<V, E>, connection: E) :
    SetShapeAttributeImpl<V, E>(shape, attribute, connection), MakeConnection<V, E>
