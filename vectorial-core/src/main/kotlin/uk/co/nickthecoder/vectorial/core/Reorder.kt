package uk.co.nickthecoder.vectorial.core

enum class Reorder {
    TOP, RAISE, LOWER, BOTTOM
}