package uk.co.nickthecoder.vectorial.core.expression

/*
 * Auto-generated by `generateBoilerPlate.sh`.  Do not edit!
 * The script uses `BooleanExpressions.kt.template` as the template (which you *can* edit - very carefully!).
 *
 * This file contains auto-generated boilerplate so that the vast majority of Vectorial's code doesn't need to use
 * the generic type `S` for each Expression, and instead can use e.g. FloatExpression, rather than Expression<Float>.
 * See [Expression] for a longer explanation.
 *
 * Vectorial supports many `value` types, such as `Double`, `Float`, `String`, `Vector2` etc.
 * It also has many types of expressions, such as [Constant], [Variable], [Attribute], [UnaryFunction] etc.
 * We need boilerplate for each combination.
 *
 * This file contains the boilerplate for the value type `Boolean`.
 */

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.PointConverter

class BooleanConstant(value : Boolean) : Constant<Boolean>(value), BooleanExpression

interface BooleanVariable : Variable<Boolean, BooleanExpression>, BooleanExpression

class BooleanVariableImpl(item: Item, name: String, value: BooleanExpression) :
    VariableImpl<Boolean, BooleanExpression>(item, name, value), BooleanVariable

interface BooleanAttribute : Attribute<Boolean, BooleanExpression>, BooleanExpression

internal class BooleanAttributeImpl(attributes : AttributesImpl, key: String, exp: BooleanExpression) :
    AttributeImpl<Boolean, BooleanExpression>(attributes, key, exp), BooleanAttribute

internal fun createAttribute( attributes: AttributesImpl, key: String, expression : BooleanExpression ) : BooleanAttributeImpl {
    val attribute = BooleanAttributeImpl( attributes, key, expression )
    attributes.add( attribute )
    return attribute
}

abstract class CachedBooleanExpression : CachedExpression<Boolean>(), BooleanExpression

class BooleanIf(condition: BooleanExpression, then: BooleanExpression, els: BooleanExpression) :
    If<Boolean, BooleanExpression>(condition, then, els), BooleanExpression

class BooleanWhenExpression(
    subject: Expression<*>, matchLists: List<Pair<List<Expression<*>>, BooleanExpression>>, ele: BooleanExpression
) : WhenExpression<Boolean, BooleanExpression>(subject, matchLists, ele), BooleanExpression

class BooleanWhen(subject: Expression<*>) : When<Boolean, BooleanExpression>(subject) {
    override fun els(result: BooleanExpression) =
        BooleanWhenExpression(subject, matchLists, result)
}


class UnaryBooleanFunction<A : Any, AE : Expression<A>>(
    override val label: String,
    override val a: AE,
    val lambda: (A) -> Boolean
) : CachedBooleanExpression(), UnaryFunction<AE> {
    init {
        addDependencies(a)
    }

    override fun recalculate() = lambda(a.eval())
    override fun debugInfo() = "${super.debugInfo()} $label"
}


class BinaryBooleanFunction<A : Any, B : Any, AE : Expression<A>, BE : Expression<B>>(
    override val label: String,
    override val a: AE,
    override val b: BE,
    val lambda: (A, B) -> Boolean
) : CachedBooleanExpression(), BinaryFunction<AE, BE> {
    init {
        addDependencies(a, b)
    }

    override fun recalculate() = lambda(a.eval(), b.eval())
    override fun debugInfo() = "${super.debugInfo()} $label"
}


class ParsedBooleanExpression(
    script: String
) : ParsedExpression<Boolean>(script), BooleanExpression {
    override val type: Class<Boolean>
        get() = Boolean::class.java
}


internal class BooleanConnection(connector: Connector, convertor: PointConverter<Boolean>) :
    ConnectionImpl<Boolean>(connector, convertor), BooleanExpression {
    override fun createConstant(value: Boolean) = BooleanConstant(value)
}
