package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.expression.Vector2AttributeImpl
import uk.co.nickthecoder.vectorial.core.shape.Shape
import uk.co.nickthecoder.vectorial.core.warn

/**
 * Sets [Vector2Attribute] to a different [Vector2Expression].
 *
 * See also [MoveTowards], which move a [Vector2Attribute], but without changing the [Vector2Expression].
 * However, [MoveTowards] will move towards the new position, within its current constraints.
 * [MoveTo] will break any existing constraints, and its new value will be whatever is passed in [newExpression].
 */
interface MoveTo : ShapeChange {
    val attribute: Vector2Attribute
    val newExpression: Vector2Expression
}

internal open class MoveToImpl(
    override val shape: Shape,
    final override val attribute: Vector2Attribute,
    final override var newExpression: Vector2Expression
) : MoveTo, InternalChange {

    private val oldExpression = attribute.expression

    override val label
        get() = "Move ${shape.name}.${attribute.key} to ${newExpression.eval()}"

    override fun redo() {
        try {
            (attribute as Vector2AttributeImpl).setFromHistory(newExpression)
            attribute.eval() // Check that the new value is OK.
        } catch (e: RecursionLimitExceeded) {
            attribute.debugEvalDirty("MoveTo failure")
            (attribute as Vector2AttributeImpl).setFromHistory(oldExpression)
            warn("MoveTo reverted due to : $e")
        }
    }

    override fun undo() {
        (attribute as Vector2AttributeImpl).setFromHistory(oldExpression)
    }

    override fun canMergeWith(previous: Change) =
        previous is MoveToImpl && previous.attribute === attribute

    override fun mergeWith(previous: Change) {
        previous as MoveToImpl
        previous.newExpression = newExpression
    }
}
