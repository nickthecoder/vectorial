package uk.co.nickthecoder.vectorial.core

/**
 * Angles are strongly typed, i.e. Vectorial does not use plain [Double]s to represent
 * angles, and instead uses [Angle] objects.
 * This is so that there is never any confusion between degrees and radians.
 * The GUI shows angles in degrees, but internally the maths is always done using radians.
 *
 * The convention used in Vectorial is for 0° to point east, and positive rotations
 * are clockwise. This is the same convention used by Inkscape, and is also
 * mathematically natural
 * (given that the Y axis is pointing downwards - i.e. Y=0 is at the top of the diagram).
 */
class Angle private constructor(val radians: Double) {

    val degrees: Double
        get() = Math.toDegrees(radians)

    /**
     * Allows feather to use the unary minus operator. e.g. myAngle = -otherAngle
     */
    operator fun unaryMinus() = radians(-radians)

    operator fun times(by: Int) = radians(radians * by)
    operator fun times(by: Float) = radians(radians * by)
    operator fun times(by: Double) = radians(radians * by)

    operator fun div(by: Int) = radians(radians / by)
    operator fun div(by: Float) = radians(radians / by)
    operator fun div(by: Double) = radians(radians / by)


    operator fun plus(other: Angle): Angle {
        return radians(radians + other.radians)
    }

    operator fun minus(other: Angle): Angle {
        return radians(radians - other.radians)
    }

    fun perpendicular() = radians(Math.PI / 2 - radians)

    fun toVector() = Vector2(Math.cos(radians).toFloat(), Math.sin(radians).toFloat())

    fun cos() = Math.cos(radians)
    fun sin() = Math.sin(radians)
    fun tan() = Math.tan(radians)

    override fun hashCode(): Int {
        return radians.hashCode() + 170
    }

    override fun equals(other: Any?): Boolean {
        return if (other is Angle) {
            other.radians == radians
        } else {
            false
        }
    }

    override fun toString() = "$degrees °"


    companion object {

        const val PI = Math.PI
        const val TAU = Math.PI * 2

        val ZERO_ANGLE = Angle.radians(0.0)
        val PI_ANGLE = Angle.radians(PI)
        val TAU_ANGLE = Angle.radians(TAU)
        val NINETY = Angle.radians(PI / 2)

        @JvmStatic
        fun toRadians(degrees: Double) = Math.toRadians(degrees)

        @JvmStatic
        fun toDegrees(radians: Double) = Math.toDegrees(radians)

        @JvmStatic
        fun degrees(degrees: Double) = Angle(toRadians(degrees))

        @JvmStatic
        fun radians(radians: Double) = Angle(radians)


        @JvmStatic
        fun radiansOf(vector: Vector2) = Math.atan2(vector.y.toDouble(), vector.x.toDouble())

        @JvmStatic
        fun of(vector: Vector2) = Angle(radiansOf(vector))


        @JvmStatic
        fun radiansOf(a: Vector2, from: Vector2) = Math.atan2((a.y - from.y).toDouble(), (a.x - from.x).toDouble())

        @JvmStatic
        fun of(a: Vector2, from: Vector2) = Angle(radiansOf(a, from))
    }
}

fun Double.radians() = Angle.radians(this)
fun Double.degrees() = Angle.degrees(this)
