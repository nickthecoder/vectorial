package uk.co.nickthecoder.vectorial.core

class Polar( val magnitude : Float, val angle : Angle ) {

    override fun toString() = "[$magnitude L ${angle.degrees}]"

}
