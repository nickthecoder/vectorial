package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.shape.GeometryShape
import uk.co.nickthecoder.vectorial.core.shape.Group
import uk.co.nickthecoder.vectorial.core.shape.Shape

/**
 * Moves a set of [Shape]s
 */
// TODO This only works for GeometryShapes, not other types of Shape
// Not a problem yet, as there are none, but e.g. Text will be a problem.
interface MoveShapes : DiagramChange {
    val by: Vector2
    val disconnect: Boolean

    companion object {
        fun moveShapes(shapes: List<Shape>, by: Vector2, disconnect: Boolean) {
            MoveShapesImpl(shapes, by, disconnect).now()
        }
    }
}

internal class MoveShapesImpl(

    shapes: List<Shape>,
    override var by: Vector2,
    override val disconnect: Boolean

) : MoveShapes, InternalChange {

    val shapes = shapes.toList()

    override val diagram: Diagram
        get() = shapes.first().diagram()

    override val label
        get() = if (shapes.size > 3) {
            "Move ${shapes.size} shapes"
        } else {
            "Move ${shapes.joinToString { it.name }}"
        }

    /**
     * Each [Shape] to be moved is the key of the outer map.
     * The key to the inner map is the [Attributes] key for a point to move.
     * The value is the expression of the point *before* the change was made (for undo).
     */
    private val initialExpressions = mutableMapOf<Shape, Map<Vector2Attribute, Vector2Expression>>()

    private fun rememberAttributes(shape: Shape) {
        when (shape) {
            is GeometryShape -> {
                initialExpressions[shape] = (shape.geometry.movableAttributes().associateWith { it.expression })
            }

            is Group -> {
                for (child in shape.shapes) {
                    rememberAttributes(child)
                }
            }
        }
    }

    init {
        for (shape in shapes) {
            rememberAttributes(shape)
        }
    }

    /**
     */
    private val initialValues: Map<Shape, Map<Vector2Attribute, Vector2>> =
        shapes.associateWith { shape ->
            if (shape is GeometryShape) {
                shape.geometry.movableAttributes().associateWith { it.eval() }
            } else {
                emptyMap()
            }
        }

    override fun redo() {
        // We do this in two stages.
        // First we move all the points which are Constants,
        // Then we move all the other points, if they need changing.
        // By doing it like this, points which are connected by the objects being moved will NOT
        // be disconnected.

        for (shape in shapes) {
            if (shape is GeometryShape) {
                val attributes = shape.geometry.attributes as AttributesImpl
                for (attribute in shape.geometry.movableAttributes()) {
                    val currentExpression: Vector2Expression = attribute.expression
                    val initialValue = initialValues[shape]!![attribute]!!
                    if (currentExpression is Vector2Constant) {
                        attributes[attribute.key] = Vector2Constant(initialValue + by)
                    }
                }
            }
        }

        if (disconnect) {
            for (shape in shapes) {
                if (shape is GeometryShape) {
                    val attributes = shape.geometry.attributes as AttributesImpl
                    for (attribute in shape.geometry.movableAttributes()) {
                        val currentExpression: Vector2Expression = attribute.expression
                        val initialValue = initialValues[shape]!![attribute]!!
                        val newValue = initialValue + by
                        if (currentExpression !is Vector2Constant) {
                            if (currentExpression.eval() != newValue) {
                                attributes[attribute.key] = Vector2Constant(newValue)
                            }
                        }
                    }
                }
            }
        }

    }

    override fun undo() {
        for ((shape, expressions) in initialExpressions) {
            if (shape is GeometryShape) {
                for ((attribute, value) in expressions) {
                    (attribute as Vector2AttributeImpl).setFromHistory(value)
                }
            }
        }
    }

    override fun canMergeWith(previous: Change) = previous is MoveShapesImpl && previous.shapes == shapes
    override fun mergeWith(previous: Change) {
        previous as MoveShapesImpl
        previous.by += this.by
    }

    override fun toString() = "MoveShapesChange by $by"
}
