package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.expression.Attributes
import uk.co.nickthecoder.vectorial.core.expression.Vector2Attribute
import uk.co.nickthecoder.vectorial.core.expression.Vector2Expression
import uk.co.nickthecoder.vectorial.core.shape.GeometryShape
import uk.co.nickthecoder.vectorial.core.shape.GeometryShapeImpl
import uk.co.nickthecoder.vectorial.core.shape.Path

/**
 * Defines the outline of a [GeometryShape].
 * There is a 1:1 relationship between [Geometry] and [GeometryShape].
 *
 *
 *      ┌─────────────────┐                    ┌─────────────────────┐            ┌────────────────┐
 *      │  GeometryShape  │                    │     Geometry        │◆───────────┤   Attributes   │
 *      │                 │ 1                1 │                     │            │                │
 *      │ geometry        ├────────────────────┤ shape               │    ┌───────┤                │
 *      └─────────────────┘                    │ start               │    │       └────────────────┘
 *                                             │                     │    │
 *       ┌───────────────────────────┐         │ movableAttributes() ├────┘
 *       │           Edge            ├─────────┤ findEdges()         │
 *       │                           │         │ isClosed()          │            ┌────────────────┐
 *       │ points                    │         │ handles()           ├────────────┤     Handle     │
 *       │                           │         └─────────────────────┘            │                │
 *       │ distanceTo(Vector2)       │                   △                        │                │
 *       │ connect(Vector2)          │                   │                        └────────────────┘
 *       │ connectionExpression(...) │                   │
 *       │                           │                   │
 *       └───────────────────────────┘                   │
 *                     △                                 │
 *                     │                                 │
 *                     │            ┌───────────┬────────┴───┬──────────────┬───────────┬──────────┐
 *                     │            │           │            │              │           │          │
 *                     │      ┌─────┴─────┐ ┌───┴──┐ ┌───────┴───────┐ ┌────┴────┐ ┌────┴────┐ ┌───┴──┐
 *                     │      │ GuideLine │ │ Line │ │ Quadrilateral │ │ Ellipse │ │ Polygon │ │ Path │
 *                     │      │           │ │      │ │               │ │         │ │         │ │      │
 *                     │      └───────────┘ └──────┘ └───────────────┘ └─────────┘ └─────────┘ └──────┘
 *            ┌────────┴──────┐                              △             ...         ...
 *            │   LineEdge    │                              │
 *            │               │              ┌───────────────┴─────────────────────┐
 *            │ start         │              │                                     │
 *            │ end           │         ┌────┴────┐                         ┌──────┴────────┐
 *            └───────────────┘         │ Diamond │                         │ Parallelogram │
 *      Other implementations of        │         │                         │               │
 *      Edge not shown.                 └─────────┘                         └───────────────┘
 *                                           △                                     △
 *                                           │                                     │
 *                                   ┌───────┴───────┐                   ┌─────────┴───────────┐
 *                                   │               │                   │                     │
 *                            ┌──────┴────┐   ┌──────┴──────┐    ┌───────┴───────┐     ┌───────┴───────┐
 *                            │ Centered  │   │ CornerBased │    │    Centered   │     │  CornerBased  │
 *                            │ Diamond   │   │  Diamond    │    │ Parallelogram │     │ Parallelogram │
 *                            └───────────┘   └─────────────┘    └───────────────┘     └───────────────┘
 *
 *
 */
interface Geometry : AttributesOwner {

    val shape: GeometryShape

    /**
     * The default base name for the [Shape], e.g. "rectangle" for [Rectangle]s.
     */
    val baseName: String

    val attributes: Attributes

    val start: Vector2Expression

    fun isClosed(): Boolean

    fun handles(): List<Handle>

    /**
     * The [Vector2Attribute]s which must be changed when moving (translating) a shape.
     *
     * NOTE. Despite the name, it is perfectly normal to include points which are locked to a particular
     * location.
     */
    fun movableAttributes(): List<Vector2Attribute>

    /**
     * Finds an [Edge] near a [point].
     *
     * @return An empty list if [point] is not near an edge.
     */
    fun touchingEdges(point: Vector2, threshold: Float,threshold2: Float, into: MutableList<Edge>) {}


    /**
     * Converts any [Geometry] into a [Polyline] (a sequence of line segments).
     * You can then use [Polyline.createStrokeMesh] to render the curve.
     *
     * @param distanceTolerance Used by [Bezier] and  [EllipticalArc].
     *     If the approximated point is less than this distance away from the actual point, then
     *     the curve is not refined anymore (i.e. we stop adding extra line segments).
     * @param angleTolerance Used by [Bezier].
     *     If three consecutive points form an angle which is less than this, then
     *     the curve is not refined anymore (i.e. we stop adding extra line segments).
     */
    fun toPolyline(
        thickness: Float,
        distanceTolerance: Float,
        angleTolerance: Double
    ): Polyline

    /**
     * The default implementation uses the shape's polyline, and then converts it into a series of triangles.
     * However, some shapes (e.g. rectangles) may override this for a more efficient algorithm.
     */
    fun calculateFillMesh() = shape.polyline.createFillMesh()

    fun createShape(name: String, style: ShapeStyle): GeometryShape = GeometryShapeImpl(name, this, style)

    /**
     * Creates a new [Path] which should look the same as this geometry.
     * A [Path] is a sequence of line-segments, bezier curves, and arcs.
     *
     * Note, this method does NOT change the diagram.
     * To convert a [GeometryShape], use [GeometryShape.convertToPath]
     */
    fun toPath(): Path

    companion object {

        /**
         * Used as the default value for [toPolyline].
         *
         * An arbitrary constant found by experimenting visually.
         *
         * (as the binary chop approach doesn't work very well).
         */
        const val DEFAULT_DISTANCE_TOLERANCE = 1.6f

        /**
         * Use as the default value for [toPolyline].
         *
         * An arbitrary constant found by experimenting visually.
         */
        const val DEFAULT_ANGLE_TOLERANCE = 0.1
    }
}
