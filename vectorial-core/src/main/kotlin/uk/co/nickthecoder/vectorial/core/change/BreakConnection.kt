package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.expression.Attribute
import uk.co.nickthecoder.vectorial.core.expression.Connection
import uk.co.nickthecoder.vectorial.core.expression.Constant
import uk.co.nickthecoder.vectorial.core.expression.Expression
import uk.co.nickthecoder.vectorial.core.shape.Shape

/**
 * This implementation is identical to [SetShapeAttribute], but having a separate interface allows
 * listeners to distinguish between a simple change of ([Constant]) values, and a transition from
 * a calculated expression to a [Constant].
 *
 * Note, this is badly named, because the previous expression is not _always_ a [Connection],
 * it could be some other calculation, such as a reference to another [Attribute].
 *
 * See [MakeConnection]
 */
interface BreakConnection<V : Any, E : Expression<V>> : SetShapeAttribute<V, E>

internal class BreakConnectionImpl<V : Any, E : Expression<V>>(shape: Shape, attribute: Attribute<V, E>, constant: E) :
    SetShapeAttributeImpl<V, E>(shape, attribute, constant), BreakConnection<V, E>
