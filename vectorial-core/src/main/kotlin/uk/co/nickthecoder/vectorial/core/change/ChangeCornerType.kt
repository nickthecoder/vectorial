package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Document
import uk.co.nickthecoder.vectorial.core.shape.PathPart
import uk.co.nickthecoder.vectorial.core.shape.PathPartImpl
import uk.co.nickthecoder.vectorial.core.shape.CornerType
import uk.co.nickthecoder.vectorial.core.shape.Shape

interface ChangeCornerType : ShapeChange {

    val pathPart: PathPart
    val newCorner: CornerType

}

internal class ChangeCornerTypeImpl(

    override val pathPart: PathPartImpl,
    override val newCorner: CornerType

) : ChangeCornerType, InternalChange {

    override val label: String
        get() = "Corner : ${newCorner.name.lowercase()}"

    override val shape: Shape
        get() = pathPart.path.shape

    override val document: Document
        get() = shape.diagram().document

    private val oldCorner = pathPart.endCornerType

    override fun redo() {
        pathPart.endCornerType = newCorner
        // TODO Change this pathPart and/or the previous pathPart to a curve if needed.
        // TODO Ensure the controlPoints are smooth/symmetric if newCorner is smooth/symmetric
    }

    override fun undo() {
        pathPart.endCornerType = oldCorner
        // TODO Change this pathPart and/or the previous pathPart to a curve if needed.
        // TODO Ensure the controlPoints are smooth/symmetric if newCorner is smooth/symmetric
    }

}

