package uk.co.nickthecoder.vectorial.core

import java.util.*

data class BoundingBox(
    val left: Float,
    val top: Float,
    val right: Float,
    val bottom: Float
) {

    val width
        get() = right - left
    val height
        get() = bottom - top

    fun plus(dx: Float, dy: Float) = BoundingBox(
        left + dx,
        top + dy,
        right + dx,
        bottom + dy
    )

    fun contains(other: BoundingBox): Boolean = left <= other.left && right >= other.right &&
            top <= other.top && bottom >= other.bottom

    fun contains(point: Vector2): Boolean {
        if (point.x < left) return false
        if (point.y < top) return false
        if (point.x > right) return false
        if (point.y > bottom) return false
        return true
    }

    fun contains(point: Vector2, threshold: Float): Boolean {
        if (point.x + threshold < left) return false
        if (point.y + threshold < top) return false
        if (point.x - threshold > right) return false
        if (point.y - threshold > bottom) return false
        return true
    }

    fun overlaps(other: BoundingBox): Boolean = left < other.right && right > other.left &&
            top < other.bottom && bottom > other.top

    fun combineWith(other: BoundingBox) = BoundingBox(
        Math.min(left, other.left),
        Math.min(top, other.top),
        Math.max(right, other.right),
        Math.max(bottom, other.bottom)
    )

    override fun equals(other: Any?): Boolean {
        if (other !is BoundingBox) {
            return false
        }
        return other.left == left && other.bottom == bottom && other.right == right && other.top == top
    }

    override fun hashCode() = Objects.hash(left, bottom, top, right)

    override fun toString(): String = "($left , $bottom , $right , $top)"

}
