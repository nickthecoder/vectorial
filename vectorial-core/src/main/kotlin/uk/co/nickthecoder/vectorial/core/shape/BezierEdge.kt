package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.expression.*

interface BezierEdge : Edge {
    val bezier: BezierPathPart
}

internal class BezierEdgeImpl(

    override val bezier: BezierPathPartImpl

) : BezierEdge {

    override val shape: Shape
        get() = bezier.path.shape

    override val supportedConnectorTypes: List<ConnectorType>
        get() = SUPPORTED_CONNECTION_TYPES

    // TODO Inefficient. No cache.
    override val points: List<Vector2>
        get() {
            val result = mutableListOf<Vector2>()
            result.add(bezier.start.eval())
            bezier.pointsForPolyline(result, 1f, 0.1)
            return result
        }

    override fun isNear(point: Vector2, threshold2: Float): Boolean {
        return bezier.isNear(point, threshold2)
    }

    override fun createConnector(point: Vector2, connectorType: ConnectorType, isBounded: Boolean): ConnectorToEdge {
        if (connectorType != ConnectorType.RATIO) throw IllegalStateException("RATIO expected, but found $connectorType")

        return RatioAlongBezierImpl(this, 0f).apply {
            performMoveTowards(point)
        }
    }

    override fun toString() = "BezierEdge of $bezier"

    companion object {
        private val SUPPORTED_CONNECTION_TYPES = listOf(ConnectorType.RATIO)
    }
}

interface RatioAlongBezier : ConnectorToEdge, HasTip {
    override val edge: BezierEdge
    val ratio: FloatVariable
}

internal class RatioAlongBezierImpl(
    override val edge: BezierEdgeImpl,
    ratioExpression: FloatExpression

) : RatioAlongBezier, MovableConnectorImpl, CachedExpression<Vector2>() {

    constructor(edge: BezierEdgeImpl, ratio: Float) :
        this(edge, FloatConstant(ratio))

    override val ratio = FloatVariableImpl(edge.bezier.path.shape, "ratio", ratioExpression)

    override val connectorType: ConnectorType get() = ConnectorType.RATIO

    override val isBounded: Boolean get() = true


    init {
        addDependencies(
            edge.bezier.start,
            edge.bezier.controlPoint1,
            edge.bezier.controlPoint2,
            edge.bezier.end,
            ratio
        )
    }


    override fun isFixed() = ratio.innerExpression !is FloatConstant

    override fun performMoveTowards(world: Vector2) {
        ratio.set(FloatConstant(edge.bezier.tNearPoint(world)))
    }

    fun ratio(): Float {
        val t = ratio.eval()
        return t.clamp(0f, 1f)
    }

    override fun recalculate(): Vector2 {
        val t = ratio()

        return edge.bezier.compute(t)
    }

    override fun tip() = "Ratio [${ratio()}]."

    override fun debugInfo() = super<CachedExpression>.debugInfo() + " ratio : $ratio"
    override fun toString() = super.toString() + " ${edge.bezier.start.eval()} -> ${edge.bezier.end.eval()}"
}
