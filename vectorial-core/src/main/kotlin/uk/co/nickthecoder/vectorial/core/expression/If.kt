package uk.co.nickthecoder.vectorial.core.expression

abstract class If<V : Any, E : Expression<V>>(
    val condition: BooleanExpression,
    val then: E,
    val els: E
) : CachedExpression<V>() {

    init {
        addDependencies(condition, then, els)
    }

    override fun recalculate(): V {
        return if (condition.eval()) then.eval() else els.eval()
    }
}
