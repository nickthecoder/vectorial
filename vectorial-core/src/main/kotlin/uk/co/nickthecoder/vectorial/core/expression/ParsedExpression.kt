package uk.co.nickthecoder.vectorial.core.expression

abstract class ParsedExpression<T : Any>(script: String) : CachedExpression<T>() {

    private var parsedPair: Pair<List<Expression<*>>, Expression<*>>? = null

    var script = script
        set(v) {
            field = v
            parsedPair?.let {
                for (usedExpression in it.first) {
                    removeDependencies(usedExpression)
                }
            }
            parsedPair = null
        }

    private fun parse(): Pair<List<Expression<*>>, Expression<*>> {
        parsedPair?.let { return it }
        val parsedPair = Parser.parse(script)
        for (usedExpression in parsedPair.first) {
            addDependencies(usedExpression)
        }
        this.parsedPair = parsedPair
        return parsedPair
    }

    override fun recalculate(): T {

        val result = parse().second.eval()

        if (result.javaClass.isAssignableFrom(type)) {
            @Suppress("UNCHECKED_CAST")
            (result as T).apply {
                return result
            }
        }
        throw ClassCastException("Expected ${type.simpleName} found ${result.javaClass.simpleName}")
    }

}