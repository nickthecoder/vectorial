package uk.co.nickthecoder.vectorial.core

data class Mesh(
    val points: List<Vector2>,
    val uvs: List<Vector2>,
    val thickness: Float = 1f,
    val length: Float = 1f
)
