package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.expression.*

interface Line : Geometry {

    override val start: Vector2Attribute
    val end: Vector2Attribute

    /**
     * The direction of the line ([end] - [start])
     */
    val direction: Vector2Expression

    fun direction() = direction.eval()

    companion object {

        /**
         * Create a [Line].
         * Note this does not add the [Line] to the [Diagram], nor does it update [History].
         */
        fun create(start: Vector2Expression, end: Vector2Expression): Line = LineImpl(start, end)

        /**
         * Create a [Line] using `values`, rather than [Expression]s.
         */
        fun create(start: Vector2, end: Vector2): Line = create(Vector2Constant(start), Vector2Constant(end))
    }
}

internal class LineImpl(
    startExpression: Vector2Expression,
    endExpression: Vector2Expression

) : Line, MutableGeometry {

    override lateinit var shape: GeometryShape

    override val baseName: String get() = "line"

    override val attributes = AttributesImpl(this)

    override val start = createAttribute(attributes, "start", startExpression)
    override val end = createAttribute(attributes, "end", endExpression)

    override val direction get() = end - start


    private val cachedControlPoints by lazy {
        listOf(
            SimpleAttributeHandle(shape, start),
            SimpleAttributeHandle(shape, end)
        )
    }

    override fun isClosed() = false

    override fun handles() = cachedControlPoints

    override fun movableAttributes() = listOf(start, end)

    private val edge by lazy { LineEdgeImpl(shape, start, end) }

    override fun touchingEdges(point: Vector2, threshold: Float, threshold2: Float, into: MutableList<Edge>) {
        if (edge.isNear(point, threshold2)) {
            into.add(edge)
        }
    }

    override fun toPath(): Path {
        val path = PathImpl(start.eval())
        path.appendLineSegment(end.eval())
        return path
    }


    override fun toPolyline(thickness: Float, distanceTolerance: Float, angleTolerance: Double): Polyline {
        return Polyline(listOf(start.eval(), end.eval()), thickness, false)
    }

    override fun toString() = "Line ${start.eval()} -> ${end.eval()}"

}
