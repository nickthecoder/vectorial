package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.Vector2Constant
import uk.co.nickthecoder.vectorial.core.expression.createAttribute
import uk.co.nickthecoder.vectorial.core.shape.*

interface ClosePath : ShapeChange {

    val path: Path
    val withCurve: Boolean
}

internal class ClosePathImpl(

    override val path: PathImpl,
    override val withCurve: Boolean

) : ClosePath, InternalChange {

    override val label: String get() = "Close Path"

    override val shape: Shape get() = path.shape

    private val startPart = path.pathParts.first() as StartPathPartImpl

    private val newPart: PathPartImpl

    init {

        val start = path.start
        val last = path.pathParts.last()
        val firstCurve = startPart.nextPathPart() as? BezierPathPartImpl
        val lastCurve = last as? BezierPathPartImpl

        newPart = if (withCurve && (firstCurve != null || lastCurve != null)) {
            val cp1Value: Vector2 =
                if (lastCurve == null) last.end.eval() else last.end.eval() - (last.controlPoint2.eval() - last.end.eval())
            val cp2Value =
                if (firstCurve == null) start.eval() else firstCurve.start.eval() - (firstCurve.controlPoint1.eval() - firstCurve.start.eval())

            val cp1Attribute =
                createAttribute(path.attributes, "point${++ path.pointCounter}", Vector2Constant(cp1Value))
            val cp2Attribute =
                createAttribute(path.attributes, "point${++ path.pointCounter}", Vector2Constant(cp2Value))

            BezierPathPartImpl(path, last.end, cp1Attribute, cp2Attribute, start)
        } else {
            LinePathPartImpl(path, last.end, start)
        }

    }

    override fun redo() {
        path.pathParts.removeFirst()
        path.pathParts.add(newPart)
        path.invalidateCache()
    }

    override fun undo() {
        path.pathParts.add(0, startPart)
        path.pathParts.removeLast()
        path.invalidateCache()
    }

}
