package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.expression.Connection
import uk.co.nickthecoder.vectorial.core.expression.Connector


interface PointConverter<V : Any> {

    /**
     * Converts a value  into a position. This must be the inverse of [toVector2].
     * For many connections, this is a no-op. i.e. returns the same [Vector2] that it is given.
     *
     * For example, a [Connection] where [V] is type [Angle] would convert an [Angle] into a position ([Vector2]).
     */
    fun toVector2(value: V): Vector2


    /**
     * Converts the [Connector]'s [position] into this [Connection]'s `value`.
     * For many connections, this is a no-op. i.e. returns the same [Vector2] that it is given.
     *
     * For example, a [Connection] where [V] is type [Angle] would convert a position ([Vector2]) to an [Angle]
     */
    fun fromVector2(position: Vector2): V
}
