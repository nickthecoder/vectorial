package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.ChangeGeometryImpl
import uk.co.nickthecoder.vectorial.core.expression.*


internal class CenteredStarImpl(

    sides: Int,
    centerExpression: Vector2Expression,
    vertexExpression: Vector2Expression,
    ratioAndTurnExpression: PolarExpression = PolarConstant(Polar(0.5f, Angle.radians(Math.PI / sides)))

) : CenteredStar, CenteredPolygonOrStarImpl(sides, centerExpression, vertexExpression), MutableGeometry {

    constructor(
        sides: Int, center: Vector2, vertex: Vector2,
        ratioAndTurn: Polar = Polar(0.5f, Angle.radians(Math.PI / sides))
    ) : this(sides, Vector2Constant(center), Vector2Constant(vertex), PolarConstant(ratioAndTurn))

    override val polygonType: PolygonType get() = PolygonType.STAR

    override val centered: Centered get() = Centered.CENTERED

    override val ratioAndTurn = createAttribute(attributes, "ratioAndTurn", ratioAndTurnExpression)

    override fun handles(): List<Handle> {
        var handles = cachedHandles
        if (handles == null) {
            handles = listOf(
                SimpleAttributeHandle(shape, center),
                RegularPolygonHandle(this, delta),
                RatioAndTurnHandle(this, ratioAndTurn)
            )
            cachedHandles = handles
        }
        return handles
    }

    override fun calculateVertices(): List<Vector2Expression> {
        val vertices = mutableListOf<Vector2Expression>()
        for (i in 0 until sides.eval()) {
            vertices.add(center + delta.rotateRadians(angle * i))
            vertices.add(center + (delta * ratioAndTurn).rotateRadians(angle * i))
        }
        return vertices
    }


    override fun changeCentered(centered: Centered) {
        if (centered == Centered.CORNER_BASED) {
            val newGeometry = CornerBasedStarImpl(sides.eval(), start.eval(), vertices()[2].eval(), ratioAndTurn.eval())
            ChangeGeometryImpl("Make Centered", shape, newGeometry).now()
            invalidateCache()
        }
    }

    override fun changePolygonType(type: PolygonType) {
        if (type == PolygonType.POLYGON) {
            val newGeometry = CenteredPolygonImpl(sides.eval(), center.expression, delta.expression)
            ChangeGeometryImpl("Change to polygon", shape, newGeometry).now()
            invalidateCache()
        }
    }

    override fun movableAttributes() = listOf(center)
}

/**
 * The `vertex` [Handle] for a [PolygonOrStar].
 */
internal class RatioAndTurnHandle(
    val star: Star,
    ratioAndTurn: PolarAttribute
) : DerivedAttributeHandle<Polar, PolarExpression>(
    star.shape,
    ratioAndTurn,
    object : PointConverter<Polar> {
        override fun fromVector2(position: Vector2): Polar {
            val delta = star.delta.eval()
            val foo: Vector2 = (position - star.center.eval())
            val mag = foo.magnitude() / delta.magnitude()
            val angle = foo.angle() - delta.angle()
            return Polar(mag, angle)
        }

        override fun toVector2(value: Polar) = star.center.eval() + star.delta.eval() * value
    },
    star.center, star.delta
) {

    override fun createConnection(connector: Connector) = PolarConnection(connector, convertor)

}
