package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.NoOpPointConverter
import uk.co.nickthecoder.vectorial.core.PointConverter
import uk.co.nickthecoder.vectorial.core.Vector2

/**
 * Defines the destination point of a [Connection].
 * i.e. its position, as well as what it is connected to.
 */
interface Connector : Vector2Expression {
    fun isFixed(): Boolean
    fun createSimpleConnection(): Vector2Connection = Vector2ConnectionImpl(this, NoOpPointConverter)
}

interface MovableConnector : Connector, MovableVector2Expression

/**
 * Exposes [performMoveTowards], which must only be called from a Change, and is therefore not part of the
 * public API.
 */
internal interface MovableConnectorImpl : MovableConnector, MovableVector2ExpressionImpl


interface Vector2Connection : Connection<Vector2>, Vector2Expression {
    override val type: Class<Vector2>
        get() = Vector2::class.java

    override fun createConstant(value: Vector2) = Vector2Constant(value)
}

internal class Vector2ConnectionImpl(

    connector: Connector,
    convertor: PointConverter<Vector2>

) : ConnectionImpl<Vector2>(connector, convertor), Vector2Connection {
}

interface FloatConnection : Connection<Float>, FloatExpression {
    override val type: Class<Float>
        get() = Float::class.java

    override fun createConstant(value: Float) = FloatConstant(value)
}

internal class FloatConnectionImpl(

    connector: Connector,
    convertor: PointConverter<Float>

) : ConnectionImpl<Float>(connector, convertor), FloatConnection {

}
