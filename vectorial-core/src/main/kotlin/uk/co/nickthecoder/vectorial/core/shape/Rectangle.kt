package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.expression.*

/**
 * An axis-aligned rectangle.
 *
 * There are two implementations, as specified by [Centerable] : [CenteredRectangle] and [CornerBasedRectangle].
 */
interface Rectangle : Quadrilateral {

    override val quadrilateralType: QuadrilateralType get() = QuadrilateralType.RECTANGLE

    companion object {

        fun create(start: Vector2Expression, opposite: Vector2Expression): Rectangle =
            CornerBasedRectangle(start, opposite)

        fun create(start: Vector2, opposite: Vector2): Rectangle =
            CornerBasedRectangle(start, opposite)

        fun createCentered(center: Vector2Expression, size: Vector2Expression): Rectangle =
            CenteredRectangle(center, size)

        fun createCentered(center: Vector2, size: Vector2): Rectangle =
            CenteredRectangle(center, size)

    }

}

