package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.*

/**
 * A parallelogram, where the top and bottom edges are parallel to the `X` axis.
 * The amount that the left and right edges are tilted is governed by a [ratio]
 * of the parallelogram's height ([size].y).,
 *
 * There are two implementations, as specified by [Centerable] : [CenteredParallelogram] and [CornerBasedParallelogram].
 */
interface Parallelogram : Quadrilateral {

    override val quadrilateralType: QuadrilateralType get() = QuadrilateralType.PARALLELOGRAM

    /**
     * The top right and bottom left corners are moved right from square by `ratio * size.y`.
     */
    val ratio: FloatAttribute


    companion object {

        fun create(from: Vector2Expression, to: Vector2Expression, ratio: FloatExpression): Parallelogram =
            CornerBasedParallelogram(from, to, ratio)

        fun create(from: Vector2, to: Vector2, ratio: Float): Parallelogram =
            CornerBasedParallelogram(from, to, ratio)

        fun createCentered(center: Vector2Expression, size: Vector2Expression, ratio: FloatExpression): Parallelogram =
            CenteredParallelogram(center, size, ratio)

        fun createCentered(center: Vector2, size: Vector2, ratio: Float): Parallelogram =
            CenteredParallelogram(center, size, ratio)

    }
}