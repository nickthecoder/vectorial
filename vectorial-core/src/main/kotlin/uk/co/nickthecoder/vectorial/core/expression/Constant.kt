package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.Angle
import uk.co.nickthecoder.vectorial.core.Color
import uk.co.nickthecoder.vectorial.core.Polar
import uk.co.nickthecoder.vectorial.core.Vector2

/**
 * A [Constant] [Expression] has no dependencies, and cannot change.
 *
 * NOTE, [value] must be immutable.
 *
 * [V] is the `value` type, such as [Vector2].
 */
abstract class Constant<V : Any>(val value: V) : Expression<V> {

    override fun eval() = value
    override fun dirty() {
        // Do nothing
    }

    override fun toString() = "Constant $value"
}


fun Any.toConstant(): Constant<*> {
    return when (this) {
        is Double -> DoubleConstant(this)
        is Float -> FloatConstant(this)
        is Int -> IntConstant(this)
        is Boolean -> BooleanConstant(this)
        is String -> StringConstant(this)
        is Color -> ColorConstant(this)
        is Angle -> AngleConstant(this)
        is Vector2 -> Vector2Constant(this)
        is Polar -> PolarConstant(this)
        else -> throw Exception("Class $this is not a supported Vectorial `value` type")
    }
}
