package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Document

interface Change {

    val document: Document

    val label: String

    /**
     * Adds this [Change] to the current [Batch].
     *
     * @throw Exception if we are not within a [Batch].
     */
    fun addToCurrentBatch() {
        val batch = document.history.currentBatch() ?: throw Exception("Not within a Batch")
        batch.addChange(this)
    }

    /**
     * Adds this [Change] to the existing batch,
     * or creates a new [Batch], with this [Change] as its only item,
     *
     * @throw Exception if we are already within a [Batch].
     */
    fun now() {
        val batch = try {
            document.history.currentBatch()
        } catch (e: Exception) {
            // If this change isn't related to a document, we can just apply the change without using history.
            (this as? InternalChange)?.redo()
            return
        }

        if (batch == null) {
            document.history.batch(this)
        } else {
            batch.addChange(this)
        }
    }

}


internal interface InternalChange : Change {

    fun undo()

    fun redo()

    /**
     * When dragging the mouse, lots of [Change] objects will be created.
     * If we added all of them, the current [Batch] very unwieldy.
     * So instead, whenever each new Change is added [Batch] calls [canMergeWith] passing it the previous
     * [Change] in the batch. If the reply is `true`, [Batch] call [mergeWith].
     */
    fun canMergeWith(previous: Change): Boolean = false

    /**
     * Mutates [previous], so that it also encompasses the changes in `this`.
     */
    fun mergeWith(previous: Change) {}

}
