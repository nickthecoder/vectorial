package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.SimpleAttributeHandle
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.change.ChangeGeometryImpl
import uk.co.nickthecoder.vectorial.core.expression.AttributesImpl
import uk.co.nickthecoder.vectorial.core.expression.Vector2Constant
import uk.co.nickthecoder.vectorial.core.expression.Vector2Expression
import uk.co.nickthecoder.vectorial.core.expression.createAttribute

/**
 * A [Rectangle], which is based on a [center] position, and its [size].
 * This is in contrast to [CornerBasedRectangle], which is based on the position of
 * two opposite corners.
 */
internal class CornerBasedRectangle(
    startExpression: Vector2Expression,
    oppositeExpression: Vector2Expression

) : QuadrilateralImpl(), Rectangle {

    constructor(start: Vector2, opposite: Vector2) :
            this(Vector2Constant(start), Vector2Constant(opposite))

    override lateinit var shape: GeometryShape

    override val centered: Centered get() = Centered.CORNER_BASED

    // Attributes
    override val attributes = AttributesImpl(this)
    override val corner0 = createAttribute(attributes, "corner0", startExpression)
    override val corner2 = createAttribute(attributes, "corner2", oppositeExpression)

    // Expressions
    override val corner1 = corner0.xFrom(corner2)
    override val corner3 = corner0.yFrom(corner2)
    override val size = corner2 - corner0
    override val center: Vector2Expression = corner0.midpointTo(corner2)

    // Continuous Geometry expressions
    override val start get() = corner0

    // End of expressions

    private val cachedControlPoints by lazy {
        listOf(
            SimpleAttributeHandle(shape, corner0),
            SimpleAttributeHandle(shape, corner2)
        )
    }

    init {
        addCornerAttributes()
    }

    override fun handles() = cachedControlPoints

    override fun changeCentered(centered: Centered) {
        if (centered == Centered.CENTERED) {
            ChangeGeometryImpl(
                "Convert to a center-based Rectangle",
                shape,
                CenteredRectangle(center.eval(), size.eval())
            ).now()
        }
    }

    override fun changeQuadrilateralType(type: QuadrilateralType) {

        when (type) {
            QuadrilateralType.RECTANGLE -> {}

            QuadrilateralType.PARALLELOGRAM -> {
                ChangeGeometryImpl(
                    "Convert to a rectangle",
                    shape, CornerBasedParallelogram(corner0.expression, corner2.expression)
                ).now()
            }

            QuadrilateralType.DIAMOND -> {
                ChangeGeometryImpl(
                    "Convert to a diamond",
                    shape, CornerBasedDiamond(corner0.expression, corner2.expression)
                ).now()
            }

        }

    }

    override fun movableAttributes() = listOf(corner0, corner2)

    override fun toString() = "Rectangle from ${corner0.eval()} to ${corner2.eval()}"
}
