package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*

/**
 * All implementations of [Geometry] must extend this interface, so that [GeometryShape] can set
 * [shape].
 */
internal interface MutableGeometry : Geometry {
    override var shape: GeometryShape
}
