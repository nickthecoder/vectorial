package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.SetShapeAttributeImpl
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.expression.AttributesImpl
import kotlin.math.PI
import kotlin.math.tan

enum class PolygonType {
    POLYGON, STAR
}

interface PolygonOrStar : Geometry, Centerable {

    val polygonType: PolygonType
    override val baseName: String get() = polygonType.name.lowercase()

    override fun isClosed() = true

    val sides: IntAttribute
    val center: Vector2Expression

    /**
     * The position of the first corner relative to the center.
     */
    val delta: Vector2Expression

    fun changePolygonType(type: PolygonType)

    fun setSides(sides: Int)

}

interface CenteredPolygonOrStar : PolygonOrStar {
}

interface CornerBasedPolygonOrStar : PolygonOrStar {
    val corner1: Vector2Expression
    val corner2: Vector2Expression
}

interface Polygon : PolygonOrStar {

    companion object {

        fun createCentered(sides: Int, center: Vector2Expression, delta: Vector2Expression): CenteredPolygon =
            CenteredPolygonImpl(sides, center, delta)

        fun createCentered(sides: Int, center: Vector2, delta: Vector2): CenteredPolygon =
            CenteredPolygonImpl(sides, center, delta)

        fun createCornerBased(sides: Int, corner1: Vector2Expression, corner2: Vector2Expression): CornerBasedPolygon =
            CornerBasedPolygonImpl(sides, corner1, corner2)

        fun createCornerBased(sides: Int, corner1: Vector2, corner2: Vector2): CornerBasedPolygon =
            CornerBasedPolygonImpl(sides, corner1, corner2)

    }

}

interface Star : PolygonOrStar {

    val ratioAndTurn: PolarExpression

    companion object {

        fun createCentered(
            sides: Int, center: Vector2Expression, delta: Vector2Expression,
            ratioAndTurn: PolarExpression,
        ): CenteredStar = CenteredStarImpl(sides, center, delta, ratioAndTurn)

        fun createCentered(sides: Int, center: Vector2, delta: Vector2, ratioAndTurn: Polar): CenteredStar =
            CenteredStarImpl(sides, center, delta, ratioAndTurn)

        fun createCornerBased(
            sides: Int,
            corner1: Vector2Expression,
            corner2: Vector2Expression,
            ratioAndTurn: PolarExpression
        ): CornerBasedStar =
            CornerBasedStarImpl(sides, corner1, corner2, ratioAndTurn)

        fun createCornerBased(sides: Int, corner1: Vector2, corner2: Vector2, ratioAndTurn: Polar): CornerBasedStar =
            CornerBasedStarImpl(sides, corner1, corner2, ratioAndTurn)

    }
}

interface CenteredPolygon : PolygonOrStar, Polygon {
}

interface CornerBasedPolygon : PolygonOrStar, Polygon {
}

interface CenteredStar : PolygonOrStar, Star {
}

interface CornerBasedStar : PolygonOrStar, Star {
}

internal abstract class PolygonOrStarImpl(

    sides: IntExpression

) : PolygonOrStar, MutableGeometry {

    constructor(sides: Int) : this(IntConstant(sides))

    final override lateinit var shape: GeometryShape

    // Attributes

    final override val attributes = AttributesImpl(this)
    final override val sides = createAttribute(attributes, "sides", sides)

    // Expressions

    protected val angle: DoubleExpression = ((Math.PI * 2.0) / this.sides)

    private var cachedEdges: List<Edge>? = null
    protected var cachedVertices: List<Vector2Expression>? = null
    protected var cachedHandles: List<Handle>? = null

    fun invalidateCache() {
        cachedEdges = null
        cachedVertices = null
        cachedHandles = null
    }

    /**
     * All vertices as [Vector2Expression]s.
     * We use expressions, because these are used by [touchingEdges], which requires expressions, rather than constants.
     *
     * Implementations should cache these (see [cachedVertices]), as they are often used.
     */
    protected fun vertices(): List<Vector2Expression> {
        var vertices = cachedVertices
        if (vertices == null) {
            vertices = calculateVertices()
            cachedVertices = vertices
        }
        return vertices
    }

    protected abstract fun calculateVertices(): List<Vector2Expression>

    override fun setSides(sides: Int) {
        SetShapeAttributeImpl(shape, this.sides, IntConstant(sides)).now()
        invalidateCache()
    }

    override fun touchingEdges(point: Vector2, threshold: Float,  threshold2: Float, into: MutableList<Edge>) {
        val sides = sides.eval()

        var edges: List<Edge>? = cachedEdges
        if (edges == null) {
            val vertices = vertices()
            edges = (0 until sides).map { LineEdgeImpl(shape, vertices[it], vertices[(it + 1) % sides]) }
            cachedEdges = edges
        }

        for (edge in edges) {
            if (edge.isNear(point, threshold2)) {
                into.add(edge)
            }
        }
    }

    override fun toPolyline(thickness: Float, distanceTolerance: Float, angleTolerance: Double): Polyline {
        val vertices = vertices().map { it.eval() }
        return Polyline(vertices, thickness, true)
    }

    override fun toPath(): Path {
        val vertices = vertices().map { it.eval() }
        val path = PathImpl(vertices.firstOrNull() ?: Vector2(0f, 0f))
        for (index in 1 until vertices.size) {
            path.appendLineSegment(vertices[index])
        }
        return path
    }

}

internal abstract class CenteredPolygonOrStarImpl(

    sides: Int,
    centerExpression: Vector2Expression,
    vertexExpression: Vector2Expression

) : CenteredPolygonOrStar, PolygonOrStarImpl(sides), MutableGeometry {

    // Attributes

    final override val center = createAttribute(attributes, "center", centerExpression)
    final override val delta = createAttribute(attributes, "delta", vertexExpression)

    // Expressions

    final override val start: Vector2Expression get() = center + delta

    override fun movableAttributes() = listOf(center)
}


internal abstract class CornerBasedPolygonOrStarImpl(

    sides: Int,
    cornerExpression: Vector2Expression,
    corner2Expression: Vector2Expression

) : CornerBasedPolygonOrStar, PolygonOrStarImpl(sides), MutableGeometry {

    // Attributes

    final override val corner1 = createAttribute(attributes, "corner1", cornerExpression)
    final override val corner2 = createAttribute(attributes, "corner2", corner2Expression)

    // Expressions

    final override val start: Vector2Expression get() = corner1
    final override val center: Vector2Expression

    init {
        val mid = corner1.midpointTo(corner2)
        val halfSideLength = mid.distanceTo(corner1)
        val toCenter = halfSideLength / tan(PI.toFloat() / sides)
        center = mid + (corner2 - corner1).perpendicular().unit() * toCenter
    }

    final override val delta: Vector2Expression = corner1 - center


    override fun movableAttributes() = listOf(corner1, corner2)

}
