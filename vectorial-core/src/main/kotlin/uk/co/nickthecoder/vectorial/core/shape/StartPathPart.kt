package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Edge
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.Vector2Attribute

interface StartPathPart : PathPart

internal class StartPathPartImpl(

    path: Path,
    override val start: Vector2Attribute

) : PathPartImpl(path), StartPathPart {

    override val end: Vector2Attribute get() = start

    override fun findEdges(point: Vector2, threshold2: Float, into: MutableList<Edge>) {}

    override fun pointsForPolyline(into: MutableList<Vector2>, distanceTolerance: Float, angleTolerance: Double) {
        into.add(start.eval())
    }

    override fun previousPathPart() = null

}
