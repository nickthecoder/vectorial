package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.ConvertToPathImpl
import uk.co.nickthecoder.vectorial.core.expression.*

interface GeometryShape : Shape {

    val style: ShapeStyle
    val geometry: Geometry

    val fillMesh: Mesh
    val strokeMesh: Mesh
    val polyline: Polyline

    override fun touchingEdges(point: Vector2, threshold: Float, threshold2: Float, into: MutableList<Edge>) =
        geometry.touchingEdges(point, threshold, threshold2, into)

    fun convertToPath()

}


internal class GeometryShapeImpl(

    override val name: String,
    geometry: Geometry,
    override val style: ShapeStyle

) : ShapeImpl(), GeometryShape, AttributesListener {

    override var geometry = geometry
        set(v) {
            field.attributes.removeListener(this)
            field = v
            (v as MutableGeometry).shape = this
            v.attributes.addListener(this)
            invalidateCache()
        }

    init {
        (geometry as MutableGeometry).shape = this
        geometry.attributes.addListener(this)
    }

    private var cachedFillMesh: Mesh? = null
    private var cachedStrokeMesh: Mesh? = null
    private var cachedPolyline: Polyline? = null
    private var cachedBoundingBox: BoundingBox? = null


    /**
     * Calculates the bounding box by finding the mix and max of each point
     * in the [strokeMesh].
     *
     * For speed, this value is cached
     *
     * TODO If the stroke style is NONE (when implemented), then the [fillMesh] should be used instead.
     */
    override val boundingBox: BoundingBox
        get() {
            var cached = cachedBoundingBox
            if (cached == null) {

                var left = Float.MAX_VALUE
                var top = Float.MAX_VALUE
                var right = -Float.MAX_VALUE
                var bottom = -Float.MAX_VALUE

                for (point in strokeMesh.points) {
                    if (point.x < left) left = point.x
                    if (point.x > right) right = point.x
                    if (point.y < top) top = point.y
                    if (point.y > bottom) bottom = point.y
                }

                cached = BoundingBox(left, top, right, bottom)
            }

            return cached
        }

    override val handles: List<Handle> get() = geometry.handles()

    override val polyline: Polyline
        get() {
            var cached = cachedPolyline
            if (cached == null) {
                cached = geometry.toPolyline(
                    style.thickness(),
                    Geometry.DEFAULT_DISTANCE_TOLERANCE,
                    Geometry.DEFAULT_ANGLE_TOLERANCE
                )
                cachedPolyline = cached
            }
            return cached
        }

    override val fillMesh: Mesh
        get() {
            var cached = cachedFillMesh
            if (cached == null) {
                cached = calculateFillMesh()
                cachedFillMesh = cached
            }
            return cached
        }

    override val strokeMesh: Mesh
        get() {
            var cached = cachedStrokeMesh
            if (cached == null) {
                cached = calculateStrokeMesh()
                cachedStrokeMesh = cached
            }
            return cached
        }

    init {
        geometry.attributes.addListener(this)
        style.attributes.addListener(this)
    }

    override fun touching(point: Vector2, threshold: Float, threshold2: Float): Boolean {
        val styleThickness = style.thickness()
        val totalThreshold = styleThickness + threshold
        val totalThreshold2 = styleThickness * styleThickness + threshold2

        if (polyline.withinBoundingBox(point, totalThreshold)) {
            if (polyline.nearEdge(point, totalThreshold2)) return true
        }

        return false
    }

    override fun attributeChanged(key: String) {
        invalidateCache()
    }

    internal fun invalidateCache() {
        cachedStrokeMesh = null
        cachedFillMesh = null
        cachedPolyline = null
        cachedBoundingBox = null
    }

    private fun calculateStrokeMesh(): Mesh = polyline.createStrokeMesh(
        style.thickness(),
        style.capStyle(),
        style.joinStyle(),
        includeUVs = true,
        allowOverlap = true,
        mirroredAcross = false // TODO Base on the stroke's gradient
    )


    private fun calculateFillMesh() = geometry.calculateFillMesh()

    override fun convertToPath() {
        ConvertToPathImpl(geometry).now()
    }

    override fun toString() = "Shape $name : $geometry"

}
