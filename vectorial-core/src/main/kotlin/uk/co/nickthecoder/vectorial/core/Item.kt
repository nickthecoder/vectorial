package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.shape.Shape

// TODO Should Items have access to their [Document]?
/**
 * Every object in a [Document] is an [Item], which has a unique [name].
 *
 * Most items are [Shape]s, but there are other types or item, such as swatches in a palette.
 */
interface Item {
    /**
     * A unique identifier. These are initially auto-generated, such as "rect1".
     */
    val name: String

    val parent: Parent

}
