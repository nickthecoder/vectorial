package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Document
import uk.co.nickthecoder.vectorial.core.info
import uk.co.nickthecoder.vectorial.core.warn
import java.lang.IllegalStateException

class History(val document: Document) {

    private val batches = mutableListOf<BatchImpl>()

    /**
     * The index into [batches] where new [Batch]es will be added.
     * If the index == 0, then there is no more to undo.
     * If `index == history.size`, then there is nothing to redo.
     */
    private var currentIndex = 0

    /**
     * The index when the document was saved.
     * If currentIndex != savedIndex, then changes have not been saved.
     */
    private var savedIndex = 0

    private var currentBatch: BatchImpl? = null

    fun clear() {
        batches.clear()
        currentIndex = 0
        savedIndex = 0
    }

    fun currentBatch(): Batch? = currentBatch

    fun isBatchStarted() = currentBatch != null

    fun isDocumentSaved(): Boolean {
        return savedIndex == currentIndex && currentBatch == null
    }


    fun beginBatch(label: String = ""): Batch {
        if (currentBatch != null) {
            throw IllegalStateException("History is already within a batch")
        }

        val batch = BatchImpl(label)
        currentBatch = batch
        return batch
    }

    fun endBatch() {
        val currentBatch = currentBatch ?: return

        if (currentBatch.changes.isNotEmpty()) {
            while (batches.size > currentIndex) {
                batches.removeAt(batches.size - 1)
            }
            batches.add(currentIndex, currentBatch)
            currentIndex++
        }
        this.currentBatch = null
    }

    fun abortBatch() {
        currentBatch?.let {
            it.undo()
            this.currentBatch = null
        }
    }

    /**
     * Begins a new [Batch], adds [changes], and then ends the [Batch].
     */
    fun batch(label: String = "", vararg changes: Change?) {
        batch(label) {
            for (change in changes) {
                if (change != null) {
                    addChange(change)
                }
            }
        }
    }

    /**
     * Begins a new [Batch], adds [changes], and then ends the [Batch].
     */
    fun batch(vararg changes: Change?) = batch("", *changes)

    /**
     * Begins a new [Batch], runs the [block], and then closes the [Batch].
     */
    fun batch(label: String = "", block: Batch.() -> Unit) {
        if (currentBatch == null) {
            val batch = beginBatch(label)
            try {
                batch.block()
            } finally {
                endBatch()
            }
        } else {
            currentBatch !!.block()
        }
    }

    fun batch(block: Batch.() -> Unit) = batch("", block)


    fun renameCurrentBatch(renameBatch: String, block: Batch.() -> Unit) {
        val batch = currentBatch ?: throw IllegalStateException("A batch has not been started")
        batch.label = renameBatch
        batch.block()
    }

    fun canUndo() = currentIndex > 0

    fun canRedo() = currentIndex < batches.size

    fun undo(skipSelectionChanges: Boolean) {
        if (currentBatch == null) {
            while (canUndo()) {
                currentIndex--
                val batch = batches[currentIndex]
                batch.undo()
                if (!skipSelectionChanges || !batch.isSkippable()) {
                    break
                }
            }
        } else {
            throw IllegalStateException("Cannot undo while in the middle of a batch")
        }
    }

    fun redo(skipSelectionChanges: Boolean) {
        if (currentBatch == null) {
            while (canRedo()) {
                val batch = batches[currentIndex]
                currentIndex++
                batch.redo()
                if (!skipSelectionChanges || !batch.isSkippable()) {
                    break
                }
            }
        } else {
            throw IllegalStateException("Cannot undo while in the middle of a batch")
        }

    }

    fun undoBatches(): List<Batch> = batches.subList(0, currentIndex).reversed()

    fun redoBatches(): List<Batch> = batches.subList(currentIndex, batches.size)


    inner class BatchImpl(label: String) : Batch {

        override var label: String = label
            get() = field.ifEmpty {
                when (changes.size) {
                    0 -> "Nothing"
                    1 -> changes.first().label
                    else -> changes.first().label + " ..."
                }
            }

        val changes = mutableListOf<Change>()

        /**
         * Does this batch only [SkippableChange]s? These are selection-only changes.
         * This gives us the choice to automatically jump over selection-only batches when performing
         * undo/redo.
         */
        override fun isSkippable(): Boolean {
            for (change in changes) {
                if (change !is SkippableChange) {
                    return false
                }
            }
            return true
        }

        fun undo() {
            changes.reversed().forEach { change ->
                (change as? InternalChange)?.undo()
                document.fireChange(change, isUndo = true)
            }
        }

        fun redo() {
            changes.forEach { change ->
                (change as? InternalChange)?.redo()
                document.fireChange(change, isUndo = false)
            }
        }

        override fun addChange(change: Change) {
            if (change !is InternalChange) return

            val prevChange = changes.lastOrNull()

            change.redo()
            document.fireChange(change, isUndo = false)
            if (prevChange !== null && change.canMergeWith(prevChange)) {
                change.mergeWith(prevChange)
                return
            }
            changes.add(change)

            // TODO This is only here to help during development.
            // Warns me that Changes are not being merged.
            if (changes.size == 11) {
                warn("Batch size is getting large : ${changes.last().javaClass.simpleName} not merged with ${change.javaClass.simpleName}")
                info(changes.joinToString { it.javaClass.simpleName })
            }
        }

        override fun toString(): String {
            return "Batch $label {\n    ${changes.joinToString(separator = "\n    ") { it.label }}\n}"
        }

    }

}
