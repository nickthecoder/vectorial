package uk.co.nickthecoder.vectorial.core.change

/**
 * Changes which do not change the model's data are skippable.
 * i.e. changes to the current selection are [SkippableChange]s.
 */
interface SkippableChange : Change
