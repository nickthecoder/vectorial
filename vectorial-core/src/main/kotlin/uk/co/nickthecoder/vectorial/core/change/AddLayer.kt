package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.*

/**
 * Adds a new layer to a [Diagram], or as a sub-layer to an existing [Layer].
 */
interface AddLayer : LayerChange {

    val newLayer: Layer
    val position: NewLayerPosition
    val relativeTo: Layer?
}

internal class AddLayerImpl(
    override val newLayer: Layer,
    override val diagram: Diagram,
    override val relativeTo: Layer?,
    override val position: NewLayerPosition

) : AddLayer, InternalChange {

    override val label: String get() = "Add Layer : ${newLayer.name}"


    override fun redo() {
        diagram as DiagramImpl
        newLayer as LayerImpl
        if (relativeTo == null) {
            if (position == NewLayerPosition.ABOVE) {
                diagram.layers.add(newLayer)
            } else {
                diagram.layers.add(0, newLayer)
            }
            newLayer.parent = diagram
        } else {
            val parent = relativeTo.parent as LayerParentImpl
            val currentIndex = parent.layers.indexOf(relativeTo)
            when (position) {
                NewLayerPosition.ABOVE -> parent.layers.add(currentIndex + 1, newLayer)
                NewLayerPosition.BELOW -> parent.layers.add(currentIndex, newLayer)
                NewLayerPosition.SUB_LAYER -> (relativeTo as LayerImpl).layers.add(newLayer)
            }
            newLayer.parent = if (position == NewLayerPosition.SUB_LAYER) relativeTo else parent
        }
        diagram.currentLayerOrGroup = newLayer
    }

    override fun undo() {
        val parent = newLayer.parent as LayerParentImpl
        parent.layers.remove(newLayer)
    }

}
