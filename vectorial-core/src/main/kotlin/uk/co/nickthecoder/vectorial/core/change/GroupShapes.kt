package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.SelectionImpl
import uk.co.nickthecoder.vectorial.core.ShapeParentImpl
import uk.co.nickthecoder.vectorial.core.shape.Group
import uk.co.nickthecoder.vectorial.core.shape.GroupImpl
import uk.co.nickthecoder.vectorial.core.shape.Shape
import uk.co.nickthecoder.vectorial.core.shape.ShapeImpl

/**
 * Takes selected shapes, and removes them from their parent, placing them into a new [Group].
 * The [Group] is then added to diagram. The zOrder for the group is the lowest zOrder of the original shapes.
 *
 * The selection contains just the [Group] after redo.
 */
interface GroupShapes : DiagramChange, AltersSelection {
    fun shapes(): List<Shape>
}

internal class GroupShapesImpl(

    override val diagram: Diagram

) : GroupShapes, InternalChange {

    override val label: String get() = "Group"

    /**
     * We need to know the parent and the zOrder of each shape, so that we can put them
     * back in the correct place in [redo].
     */
    private val shapesParentsZOrders =
        diagram.selection.shapes.map { Triple(it as ShapeImpl, it.parent as ShapeParentImpl, it.zOrder()) }

    val group = GroupImpl(document.generateName("group"))

    override fun shapes() = shapesParentsZOrders.map { it.first }

    override fun redo() {
        if (shapesParentsZOrders.isEmpty()) return
        val selection = diagram.selection as SelectionImpl
        selection.handle = null
        selection.shapes.clear()

        val (_, lowestParent, zOrder) = shapesParentsZOrders.minBy { it.third }

        for ((shape, parent, _) in shapesParentsZOrders) {
            group.items.add(shape)
            parent.items.remove(shape)
            shape.parent = group
        }
        lowestParent.shapes.add(zOrder, group)
        group.parent = lowestParent
        selection.shapes.add(group)
    }

    override fun undo() {
        if (shapesParentsZOrders.isEmpty()) return

        val selection = (diagram.selection as SelectionImpl)
        selection.shapes.clear()
        selection.handle = null

        (group.parent as ShapeParentImpl).items.remove(group)
        for ((shape, parent, zOrder) in shapesParentsZOrders.sortedBy { it.third }) {
            parent.items.add(zOrder, shape)
            shape.parent = parent
            selection.shapes.add(shape)
        }

    }

}

