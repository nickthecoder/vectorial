package uk.co.nickthecoder.vectorial.core.shape

enum class EllipseType {
    /**
     * The [Ellipse] is whole, i.e. it forms a complete circle/ellipse.
     */
    WHOLE,

    /**
     * There is a chord ( a straight line from to [Ellipse.fromAngle] to [Ellipse.toAngle] ).
     */
    CHORD,

    /**
     * There are radial lines from [Ellipse.fromAngle] to [Ellipse.center] and from [Ellipse.toAngle] to [Ellipse.center].
     * Thus forming a `pac-man` shape (or the shape formed when you cut a slice of pie).
     *
     * NOTE. Inkscape used to call this `segment`, but that is not the correct mathematical term. So `slice` is now used.
     */
    SLICE,

    /**
     * An open path.
     */
    ARC
}