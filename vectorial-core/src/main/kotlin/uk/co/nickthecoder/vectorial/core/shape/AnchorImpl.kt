package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Handle
import uk.co.nickthecoder.vectorial.core.SimpleAttributeHandle
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.Vector2Attribute
import uk.co.nickthecoder.vectorial.core.expression.Vector2Constant

interface Anchor : Handle {

    val pathPart: PathPart

    val handleBefore: Handle?

    val handleAfter: Handle?
}


/**
 * A [Handle] used by [Path].
 * These are special, because when they are selected, additional control points may also be displayed.
 * i.e. for points [BezierPathPart.controlPoint1] and [BezierPathPart.controlPoint2].
 *
 * Most of the time, [attribute] is the _end_ of the [pathPart]. However, this class is also used
 * for the very first point in a [Path] ([attribute] == [Path.start]).
 *
 */
internal open class AnchorImpl(

    final override val pathPart: PathPartImpl

) : SimpleAttributeHandle(pathPart.path.shape, pathPart.end), Anchor {

    override val name: String get() = "Anchor"

    override val handleBefore: Handle? by lazy {

        // We are looking for cp2 of THIS partPart (if it is a BezierPathPart).
        // We optionally also refer to its (symmetric?) partner - cp1 of the NEXT partPart.
        if (pathPart is BezierPathPart) {
            val nextCurve = pathPart.nextPathPart() as? BezierPathPartImpl
            val otherControlPoint = nextCurve?.controlPoint1

            PathBezierHandleImpl(pathPart, this, pathPart.controlPoint2, otherControlPoint)
        } else {
            null
        }

    }

    override val handleAfter: Handle? by lazy {

        // We are looking for cp1 of the NEXT pathPart (if it is a BezierPathPart).
        // We optionally also refer to its (symmetric?) partner - cp2 of THIS partPart.
        val nextCurve = pathPart.nextPathPart() as? BezierPathPartImpl
        val otherControlPoint = nextCurve?.controlPoint1

        if (otherControlPoint != null) {
            val cp2 = (pathPart as? BezierPathPart)?.controlPoint2
            PathBezierHandleImpl(pathPart, this, otherControlPoint, cp2)
        } else {
            null
        }
    }

    override fun moveTo(position: Vector2) {
        val oldPosition: Vector2 = position()
        super.moveTo(position)
        val delta = position() - oldPosition

        val cp2 = (handleBefore as? SimpleAttributeHandle)?.attribute
        if (cp2 != null && cp2.expression is Vector2Constant) {
            cp2.set(Vector2Constant(cp2.eval() + delta))
        }

        val cp1 = (handleAfter as? SimpleAttributeHandle)?.attribute
        if (cp1 != null && cp1.expression is Vector2Constant) {
            cp1.set(Vector2Constant(cp1.eval() + delta))
        }
    }

    override fun toString() = "PathHandle for $attribute"
}
