package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Edge
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.Vector2Attribute
import uk.co.nickthecoder.vectorial.core.minimumDistanceSquare
import kotlin.math.abs
import kotlin.math.atan2
import kotlin.math.sqrt

interface BezierPathPart : PathPart {

    val controlPoint1: Vector2Attribute
    val controlPoint2: Vector2Attribute

    var endIsSymmetric: Boolean

}

// Some inspiration from this javascript library :
// https://github.com/Pomax/bezierjs/blob/master/src/utils.js
// In particular, the idea of caching a lookup table of points,
// which are used as a coarse first pass when finding a point or `t` near a given point.

internal class BezierPathPartImpl(

    path: Path,
    override val start: Vector2Attribute,
    override val controlPoint1: Vector2Attribute,
    override val controlPoint2: Vector2Attribute,
    override val end: Vector2Attribute

) : BezierPathPart, PathPartImpl(path) {

    override var endIsSymmetric: Boolean = true

    override fun onAttributeChanged(att: Vector2Attribute) {
        if (att === start || att === end || att === controlPoint1 || att === controlPoint2) {
            invalidateCache()
        }
    }

    private var cachedPointsForPolyline: List<Vector2>? = null

    /**
     * This is used as a "look up table" when calculating distances.
     * These are regularly spaced with respect to `t`.
     *
     * See [cachedPointsForPolyline] for the points which are used to _draw_ the bezier curve.
     */
    private var cachedPointsLookup: List<Vector2>? = null

    private val edge by lazy { BezierEdgeImpl( this) }

    override fun invalidateCache() {
        cachedPointsForPolyline = null
        cachedPointsLookup = null
    }

    override fun findEdges(point: Vector2, threshold2: Float, into: MutableList<Edge>) {
        if (edge.isNear(point, threshold2)) {
            into.add(edge)
        }
    }

    fun pointsLookup(): List<Vector2> {
        if (cachedPointsLookup == null) {
            cachePointsLookup()
        }
        return cachedPointsLookup !!
    }

    private fun cachePointsLookup() {
        val steps = 10
        val dt = 1f / steps
        var t = dt
        val result = mutableListOf<Vector2>(start.eval())
        for (i in 1 until steps) {
            result.add(compute(t))
            t += dt
        }
        result.add(end.eval())
        cachedPointsLookup = result
    }

    fun compute(t: Float): Vector2 {
        if (t == 0f) {
            return start.eval()
        }

        if (t == 1f) {
            return end.eval()
        }

        val p0 = start.eval()
        val p1 = controlPoint1.eval()
        val p2 = controlPoint2.eval()
        val p3 = end.eval()

        val mt = 1f - t

        val mt2 = mt * mt
        val t2 = t * t
        val a = mt2 * mt
        val b = mt2 * t * 3
        val c = mt * t2 * 3
        val d = t * t2

        return Vector2(
            a * p0.x + b * p1.x + c * p2.x + d * p3.x,
            a * p0.y + b * p1.y + c * p2.y + d * p3.y
        )
    }

    // TODO Inefficient. Test the bounding box first???
    /**
     * We do NOT use a fine-grained test, we only use the [pointsLookup].
     */
    fun isNear(point: Vector2, threshold2: Float): Boolean {
        val pointsLookup = pointsLookup()
        val from = pointsLookup[0]
        for (i in 1 until pointsLookup.size) {
            val to = pointsLookup[i]
            val dist = minimumDistanceSquare(from, to, point).toDouble()
            if (dist < threshold2) return true
        }
        return false
    }

    /**
     * Returns the `t` value given a point [nearPoint] near the curve.
     */
    fun tNearPoint(nearPoint: Vector2): Float {
        val points = pointsLookup()
        val lastIndex = points.size - 1
        var closestIndex = - 1
        var dist2 = Float.MAX_VALUE
        val thresholdT = 0.01f

        // Course check (using cached points)
        for (i in 1 until lastIndex) {
            val p = points[i]
            val newDist2 = nearPoint.distanceSquaredTo(p)
            if (newDist2 < dist2) {
                closestIndex = i
                dist2 = newDist2
            }
        }

        var dt = 1f / lastIndex
        var t = dt * closestIndex
        var dist2Prev = nearPoint.distanceSquaredTo(points[closestIndex - 1])
        var dist2Next = nearPoint.distanceSquaredTo(points[closestIndex + 1])

        //return t
        // We now have 3 (close-ish) points.
        // Throwing away either distPrev or distNext.
        // Choose a mid-way point (making three points again).
        // Continue to do this, homing in on the solution.

        while (dt > thresholdT) {
            dt /= 2f
            if (dist2Prev < dist2Next) {
                // Throw away `next`, and binary chop between `prevT` and `t`
                dist2Next = dist2
                t -= dt
                dist2 = nearPoint.distanceSquaredTo(compute(t))
            } else {
                // Throw away `prev`, and binary chop between `t` and `nextT`
                dist2Prev = dist2
                t += dt
                dist2 = nearPoint.distanceSquaredTo(compute(t))
            }
        }

        // We've reached the threshold, so now pick the closest of the 3 points.
        return if (dist2Prev < dist2Next) {
            if (dist2Prev < dist2) t - dt else t
        } else {
            if (dist2Next < dist2) t + dt else t
        }
    }

    override fun pointsForPolyline(into: MutableList<Vector2>, distanceTolerance: Float, angleTolerance: Double) {
        if (cachedPointsForPolyline == null) {
            cachePointsForPolyline(distanceTolerance, angleTolerance)
        }

        cachedPointsForPolyline?.let {
            into.addAll(it)
        }
    }

    private fun cachePointsForPolyline(distanceTolerance: Float, angleTolerance: Double) {
        val into = mutableListOf<Vector2>()

        val distanceToleranceSquare = distanceTolerance * distanceTolerance

        fun calcDistanceSquared(x1: Float, y1: Float, x2: Float, y2: Float): Float {
            val dx = x2 - x1
            val dy = y2 - y1
            return dx * dx + dy * dy
        }

        fun recursiveBezier(
            x1: Float, y1: Float,
            x2: Float, y2: Float,
            x3: Float, y3: Float,
            x4: Float, y4: Float,
            level: Int
        ) {
            if (level > RECURSION_LIMIT) {
                return
            }

            // Calculate all the mid-points of the line segments
            //----------------------
            val x12 = (x1 + x2) / 2
            val y12 = (y1 + y2) / 2
            val x23 = (x2 + x3) / 2
            val y23 = (y2 + y3) / 2
            val x34 = (x3 + x4) / 2
            val y34 = (y3 + y4) / 2
            val x123 = (x12 + x23) / 2
            val y123 = (y12 + y23) / 2
            val x234 = (x23 + x34) / 2
            val y234 = (y23 + y34) / 2
            val x1234 = (x123 + x234) / 2
            val y1234 = (y123 + y234) / 2


            // Try to approximate the full cubic curve by a single straight line
            //------------------
            val dx = x4 - x1
            val dy = y4 - y1

            var d2 = abs(((x2 - x4) * dy - (y2 - y4) * dx))
            var d3 = abs(((x3 - x4) * dy - (y3 - y4) * dx))
            var da1: Float
            var da2: Float
            var k: Float

            //when (case) {
            if (d2 < COLLINEARITY_EPSILON) {

                if (d3 < COLLINEARITY_EPSILON) {
                    // println("A $x1234, $y1234")

                    // All collinear OR p1==p4
                    //----------------------
                    k = dx * dx + dy * dy
                    if (k == 0f) {
                        d2 = calcDistanceSquared(x1, y1, x2, y2)
                        d3 = calcDistanceSquared(x4, y4, x3, y3)
                    } else {
                        k = 1 / k
                        da1 = x2 - x1
                        da2 = y2 - y1
                        d2 = k * (da1 * dx + da2 * dy)
                        da1 = x3 - x1
                        da2 = y3 - y1
                        d3 = k * (da1 * dx + da2 * dy)
                        if (d2 > 0 && d2 < 1 && d3 > 0 && d3 < 1) {
                            // Simple collinear case, 1---2---3---4
                            // We can leave just two endpoints
                            return
                        }
                        d2 = if (d2 <= 0) calcDistanceSquared(x2, y2, x1, y1)
                        else if (d2 >= 1) calcDistanceSquared(x2, y2, x4, y4)
                        else calcDistanceSquared(x2, y2, x1 + d2 * dx, y1 + d2 * dy)

                        d3 = if (d3 <= 0) calcDistanceSquared(x3, y3, x1, y1)
                        else if (d3 >= 1) calcDistanceSquared(x3, y3, x4, y4)
                        else calcDistanceSquared(x3, y3, x1 + d3 * dx, y1 + d3 * dy)
                    }
                    if (d2 > d3) {
                        if (d2 < distanceToleranceSquare) {
                            into.add(Vector2(x2, y2))
                            return
                        }
                    } else {
                        if (d3 < distanceToleranceSquare) {
                            into.add(Vector2(x3, y3))
                            return
                        }
                    }
                } else {
                    // println("B $x1234, $y1234")

                    // p1,p2,p4 are collinear, p3 is significant
                    //----------------------
                    if (d3 * d3 <= distanceToleranceSquare * (dx * dx + dy * dy)) {
                        if (angleTolerance < ANGLE_TOLERANCE_EPSILON) {
                            into.add(Vector2(x23, y23))
                            return
                        }

                        // Angle Condition
                        //----------------------
                        da1 = abs(
                            atan2((y4 - y3).toDouble(), (x4 - x3).toDouble()) -
                                atan2((y3 - y2).toDouble(), (x3 - x2).toDouble())
                        ).toFloat()
                        if (da1 >= PI) da1 = 2 * PI - da1

                        if (da1 < angleTolerance) {
                            into.add(Vector2(x2, y2))
                            into.add(Vector2(x3, y3))
                            return
                        }

                        if (cuspLimit != 0.0f) {
                            if (da1 > cuspLimit) {
                                into.add(Vector2(x3, y3))
                                return
                            }
                        }
                    }
                }

            } else {
                if (d3 < COLLINEARITY_EPSILON) {
                    // println("C $x1234, $y1234")

                    // p1,p3,p4 are collinear, p2 is significant
                    //----------------------
                    if (d2 * d2 <= distanceToleranceSquare * (dx * dx + dy * dy)) {
                        if (angleTolerance < ANGLE_TOLERANCE_EPSILON) {
                            into.add(Vector2(x23, y23))
                            return
                        }

                        // Angle Condition
                        //----------------------
                        da1 = abs(
                            atan2((y3 - y2).toDouble(), (x3 - x2).toDouble()) -
                                atan2((y2 - y1).toDouble(), (x2 - x1).toDouble())
                        ).toFloat()
                        if (da1 >= PI) da1 = 2 * PI - da1

                        if (da1 < angleTolerance) {
                            into.add(Vector2(x2, y2))
                            into.add(Vector2(x3, y3))
                            return
                        }

                        if (cuspLimit != 0.0f) {
                            if (da1 > cuspLimit) {
                                into.add(Vector2(x2, y2))
                                return
                            }
                        }
                    }

                } else {
                    // println("D $x1234, $y1234")

                    // Regular case
                    //-----------------
                    if ((d2 + d3) * (d2 + d3) <= distanceToleranceSquare * (dx * dx + dy * dy)) {
                        // If the curvature doesn't exceed the distance_tolerance value
                        // we tend to finish subdivisions.
                        //----------------------
                        if (angleTolerance < ANGLE_TOLERANCE_EPSILON) {
                            into.add(Vector2(x23, y23))
                            return
                        }

                        // Angle & Cusp Condition
                        //----------------------
                        k = atan2((y3 - y2).toDouble(), (x3 - x2).toDouble()).toFloat()
                        da1 = abs(k - atan2((y2 - y1).toDouble(), (x2 - x1).toDouble())).toFloat()
                        da2 = abs(atan2((y4 - y3).toDouble(), (x4 - x3).toDouble()) - k).toFloat()
                        if (da1 >= PI) da1 = 2 * PI - da1
                        if (da2 >= PI) da2 = 2 * PI - da2

                        if (da1 + da2 < angleTolerance) {
                            // Finally we can stop the recursion
                            //----------------------
                            into.add(Vector2(x23, y23))
                            return
                        }

                        if (cuspLimit != 0.0f) {
                            if (da1 > cuspLimit) {
                                into.add(Vector2(x2, y2))
                                return
                            }

                            if (da2 > cuspLimit) {
                                into.add(Vector2(x3, y3))
                                return
                            }
                        }
                    }
                }
            }

            // Continue subdivision
            //----------------------
            recursiveBezier(x1, y1, x12, y12, x123, y123, x1234, y1234, level + 1)
            recursiveBezier(x1234, y1234, x234, y234, x34, y34, x4, y4, level + 1)
        }

        //------------------------------------------------------------------------

        val start = start.eval()
        val end = end.eval()
        val c1 = controlPoint1.eval()
        val c2 = controlPoint2.eval()

        recursiveBezier(start.x, start.y, c1.x, c1.y, c2.x, c2.y, end.x, end.y, 0)
        into.add(end)
        cachedPointsForPolyline = into
    }

    companion object {
        const val PI = Math.PI.toFloat()
        const val COLLINEARITY_EPSILON = 1e-30f
        const val ANGLE_TOLERANCE_EPSILON = 0.01
        const val RECURSION_LIMIT = 30
        var cuspLimit = 0.0f
    }
}
