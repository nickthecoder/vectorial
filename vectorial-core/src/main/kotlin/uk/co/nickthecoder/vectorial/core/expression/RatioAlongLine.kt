package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.shape.LineEdge

interface RatioAlongLine : ConnectorToEdge, HasTip {
    override val edge: LineEdge
    val start: Vector2Expression
    val end: Vector2Expression
    val ratio: FloatVariable
}

class RatioAlongLineImpl(
    override val edge: LineEdge,
    ratioExpression: FloatExpression,
    override val isBounded: Boolean

) : RatioAlongLine, MovableConnectorImpl, CachedExpression<Vector2>() {

    constructor(edge: LineEdge, ratio: Float, isBounded: Boolean) :
        this(edge, FloatConstant(ratio), isBounded)

    override val start: Vector2Expression get() = edge.start

    override val end: Vector2Expression get() = edge.end

    override val ratio = FloatVariableImpl(edge.shape, "ratio", ratioExpression)

    override val connectorType: ConnectorType
        get() = ConnectorType.RATIO

    init {
        addDependencies(edge.start, edge.end, ratio)
    }

    override fun isFixed() = ratio.innerExpression !is FloatConstant

    override fun performMoveTowards(world: Vector2) {
        val startV = edge.start.eval()
        val endV = edge.end.eval()
        val diff = endV - startV
        val length = diff.magnitude()
        if (length == 0f) return // Cannot move along a line, when the line is zero long!
        val lengthSquared = length * length
        var newRatio = ((endV - world).dot(diff)) / lengthSquared
        if (isBounded) {
            newRatio = newRatio.clamp(0f, 1f)
        }
        ratio.set(FloatConstant(newRatio))
    }

    fun ratio(): Float {
        val t = ratio.eval()
        return if (isBounded) {
            t.clamp(0f, 1f)
        } else {
            t
        }
    }

    override fun recalculate(): Vector2 {
        val t = ratio()
        val from = edge.start.eval()
        val to = edge.end.eval()

        return Vector2(lerp(from.x, to.x, t), lerp(from.y, to.y, t))
    }

    override fun tip() = "Ratio [${ratio()}]."

    override fun debugInfo() = super<CachedExpression>.debugInfo() + " ratio : $ratio"
    override fun toString() = super.toString() + " ${edge.start.eval()} -> ${edge.end.eval()} isBounded: $isBounded"
}
