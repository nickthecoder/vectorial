package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Diagram

/**
 * Alters [Diagram.currentLayerOrGroup]
 */
interface ChangeCurrentShapeParent : Change

