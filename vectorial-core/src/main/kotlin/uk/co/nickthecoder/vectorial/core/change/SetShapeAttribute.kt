package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.Geometry
import uk.co.nickthecoder.vectorial.core.shape.Shape
import uk.co.nickthecoder.vectorial.core.ShapeStyle
import uk.co.nickthecoder.vectorial.core.warn

/**
 * Sets an attribute's expression.
 * Note, there are different sub-interfaces depending on the owner of the [Attribute].
 * See [SetShapeAttribute].
 */
interface SetAttribute<V : Any, E : Expression<V>> : Change {
    val attribute: Attribute<V, E>
    val newExpression: E
}

/**
 * Sets a [Shape]'s attribute. Note the [attribute] may not be directly owned by [shape], for example.
 * it could be in the [Shape]'s [Geometry].
 * You can find the direct owner (such as [Shape], or [Geometry]) using [Attributes.owner] via [Attribute.attributes].
 */
interface SetShapeAttribute<V : Any, E : Expression<V>> : SetAttribute<V, E>, ShapeChange

interface SetDiagramAttribute<V : Any, E : Expression<V>> : SetAttribute<V, E>, DiagramChange

interface SetStyleAttribute<V : Any, E : Expression<V>> : SetAttribute<V, E>, StyleChange

// Implementation

internal abstract class SetAttributeImpl<V : Any, E : Expression<V>>(
    final override val attribute: Attribute<V, E>,
    final override var newExpression: E
) : SetAttribute<V, E>, InternalChange {

    private val oldExpression = attribute.expression

    override val label
        get() = "Set ${attribute.key} to ${newExpression.eval()}"

    override fun redo() {
        try {
            (attribute as MutableAttribute<V, E>).setFromHistory(newExpression)
            attribute.eval() // Check that the new value is OK.
        } catch (e: RecursionLimitExceeded) {
            attribute.debugEvalDirty("${javaClass.simpleName} failure")
            (attribute as MutableAttribute<V, E>).setFromHistory(oldExpression)
            warn("${javaClass.simpleName} reverted due to : $e")
        }
    }

    override fun undo() {
        (attribute as MutableAttribute<V, E>).setFromHistory(oldExpression)
    }

    override fun canMergeWith(previous: Change) =
        previous is SetShapeAttributeImpl<*, *> && previous.attribute === attribute

    override fun mergeWith(previous: Change) {
        @Suppress("UNCHECKED_CAST")
        previous as SetShapeAttributeImpl<V, E>
        previous.newExpression = newExpression
    }
}

internal open class SetDiagramAttributeImpl<V : Any, E : Expression<V>>(

    final override val diagram: Diagram,
    attribute: Attribute<V, E>,
    newExpression: E

) : SetAttributeImpl<V, E>(attribute, newExpression), SetDiagramAttribute<V, E>

internal open class SetStyleAttributeImpl<V : Any, E : Expression<V>>(

    final override val style: ShapeStyle,
    attribute: Attribute<V, E>,
    newExpression: E

) : SetAttributeImpl<V, E>(attribute, newExpression), SetStyleAttribute<V, E>

internal open class SetShapeAttributeImpl<V : Any, E : Expression<V>>(
    final override val shape: Shape,
    attribute: Attribute<V, E>,
    newExpression: E
) : SetAttributeImpl<V, E>(attribute, newExpression), SetShapeAttribute<V, E>
