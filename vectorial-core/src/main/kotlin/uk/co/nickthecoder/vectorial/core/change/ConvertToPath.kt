package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Geometry
import uk.co.nickthecoder.vectorial.core.shape.*

interface ConvertToPath : ShapeChange {
}

internal class ConvertToPathImpl(

    val oldGeometry: Geometry

) : ConvertToPath, InternalChange {

    override val label: String get() = "Convert to Path"

    override val shape: Shape get() = oldGeometry.shape

    private var newPath = oldGeometry.toPath()

    override fun redo() {
        (oldGeometry.shape as GeometryShapeImpl).geometry = newPath
    }

    override fun undo() {
        (oldGeometry.shape as GeometryShapeImpl).geometry = oldGeometry
    }

}
