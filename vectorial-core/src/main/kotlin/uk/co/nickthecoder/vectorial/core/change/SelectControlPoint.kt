package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Handle
import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.DiagramImpl
import uk.co.nickthecoder.vectorial.core.Selection
import uk.co.nickthecoder.vectorial.core.Geometry
import uk.co.nickthecoder.vectorial.core.shape.Shape

/**
 * Changes [Selection.handle]
 *
 * When one [Shape] is selected, we can select one point within its [Geometry].
 *
 * If we try to select a point, when the selection size != 1, then [IllegalStateException] is thrown.
 */
interface SelectControlPoint : AltersSelection, SkippableChange {

    val newHandle: Handle?

    companion object {

        fun clearHandleSelection(diagram: Diagram): SelectControlPoint? =
            if (diagram.selection.handle == null) null else SelectControlPointImpl(diagram, null)

        fun selectControlPoint(handle: Handle): SelectControlPoint? {
            val shape = handle.shape
            val diagram = shape.diagram() as DiagramImpl
            val selection = diagram.selection
            return if (selection.shapes.size != 1 || selection.shapes.firstOrNull() !== shape) {
                throw IllegalStateException("The selection must be ${shape.name}")
            } else {
                if (selection.handle != handle) {
                    SelectControlPointImpl(handle.shape.diagram(), handle)
                } else {
                    null
                }
            }
        }

    }
}

internal class SelectControlPointImpl(
    override val diagram: Diagram,

    override var newHandle: Handle?
) : SelectControlPoint, InternalChange {

    override val label: String
        get() {
            val shape = newHandle?.shape
            return if (shape == null || newHandle == null) {
                "Clear Control Point Selection"
            } else {
                "Select Control Point ${shape.name}.${newHandle!!.name}"
            }
        }

    private var oldControlPoint = diagram.selection.handle

    init {
        if (newHandle != null && diagram.selection.shapes.size != 1) {
            throw IllegalStateException("Expected 1 shape selected, but found ${diagram.selection.shapes.size}")
        }
    }

    override fun redo() {
        (diagram as DiagramImpl).selection.apply {
            handle = this@SelectControlPointImpl.newHandle
        }
    }

    override fun undo() {
        (diagram as DiagramImpl).selection.apply {
            handle = oldControlPoint
        }
    }

    override fun canMergeWith(previous: Change) = previous is SelectControlPoint

    override fun mergeWith(previous: Change) {
        previous as SelectControlPointImpl
        previous.newHandle = newHandle
    }


}
