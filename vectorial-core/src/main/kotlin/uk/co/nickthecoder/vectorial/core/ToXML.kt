package uk.co.nickthecoder.vectorial.core

import org.w3c.dom.Element
import uk.co.nickthecoder.vectorial.core.expression.Attribute
import uk.co.nickthecoder.vectorial.core.expression.Attributes
import uk.co.nickthecoder.vectorial.core.shape.*
import javax.xml.parsers.DocumentBuilderFactory

import org.w3c.dom.Document as XMLDocument

class ToXML {

    private val xmlDocument = DocumentBuilderFactory.newInstance().newDocumentBuilder().newDocument()

    fun toXMLDocument(document: Document): XMLDocument {
        xmlDocument.appendChild(toXML(document))
        return xmlDocument
    }

    fun toXML(document: Document): Element {
        return element("document") {
            attribute("name", document.name)

            child(toXML(document.diagram))
        }
    }

    fun toXML(diagram: Diagram): Element {
        return element("diagram") {
            attribute("name", diagram.name)

            child(toXML(diagram.attributes))
            child("layers") {
                for (layer in diagram.layers) {
                    child(toXML(layer))
                }
            }
        }
    }

    fun toXML(layer: Layer): Element {
        return element("layer") {
            attribute("name", layer.name)

            if (layer.layers.isNotEmpty()) {
                child("layers") {
                    for (subLayer in layer.layers) {
                        child(toXML(subLayer))
                    }
                }
            }

            if (layer.shapes.isNotEmpty()) {
                child("shapes") {
                    for (shape in layer.shapes) {
                        child(toXML(shape))
                    }
                }
            }
        }
    }

    fun toXML(shape: Shape): Element {
        val shapeXML = when (shape) {

            is GeometryShape -> {
                val geometry = shape.geometry
                when (geometry) {
                    is Path -> toXML(geometry)
                    is Quadrilateral -> toXML(geometry)
                    is Ellipse -> toXML(geometry)

                    else -> throw IllegalStateException("Unexpected geometry type : ${geometry.javaClass}")
                }.apply {
                    child(toXML(shape.style))
                }

            }

            is Group -> {
                element("group") {
                }
            }

            else -> throw IllegalStateException("Unexpected shape type : ${shape.javaClass}")
        }

        shapeXML.apply {
            attribute("name", shape.name)
        }

        return shapeXML
    }

    fun toXML(style: ShapeStyle): Element {
        return element("style") {
            for (key in style.attributes.keys()) {
                child(toXML(style.attributes[key]))
            }
        }
    }

    fun toXML(path: Path): Element {
        return element("path") {
            for (part in path.pathParts) {
                child(toXML(part))
            }
        }
    }

    fun toXML(part: PathPart): Element {
        return when (part) {
            is StartPathPart -> toXML(part)
            is LinePathPart -> toXML(part)
            is BezierPathPart -> toXML(part)
            else -> throw IllegalStateException("Unexpected part type : ${part.javaClass}")
        }
    }

    fun toXML(part: StartPathPart): Element {
        return element("start") {
            attribute("corner", part.endCornerType.name)

            child(toXML(part.start))
        }
    }

    fun toXML(part: LinePathPart): Element {
        return element("line") {
            attribute("corner", part.endCornerType.name)

            child(toXML(part.end))
        }
    }

    fun toXML(part: BezierPathPart): Element {
        return element("bezier") {
            attribute("corner", part.endCornerType.name)

            child(toXML(part.controlPoint1))
            child(toXML(part.controlPoint2))
            child(toXML(part.end))
        }
    }

    fun toXML(quad: Quadrilateral): Element {
        return element("quadrilateral") {
            attribute("quadrilateralType", quad.quadrilateralType.name)
            attribute("centered", quad.isCentered())

            child(toXML(quad.attributes))
        }
    }

    fun toXML(ellipse: Ellipse): Element {
        return element("ellipse") {
            attribute("ellipseType", ellipse.ellipseType.eval().name)

            child(toXML(ellipse.attributes))
        }
    }


    fun toXML(attributes: Attributes): Element {
        return element("attributes") {
            for (key in attributes.keys()) {
                child(toXML(attributes[key]))
            }
        }
    }

    fun toXML(attribute: Attribute<*, *>): Element {
        return element("attribute") {
            attribute("key", attribute.key)
            attribute("type", attribute.type.simpleName)

            when (val value = attribute.eval()) {
                is Int -> attribute("value", value)
                is Boolean -> attribute("value", value)
                is Float -> attribute("value", value)
                is Double -> attribute("value", value)
                is Vector2 -> {
                    attribute("x", value.x)
                    attribute("y", value.y)
                }

                is Angle -> attribute("degrees", value.degrees)
                else -> throw IllegalStateException("Unexpected type : ${value.javaClass}")
            }
        }
    }

    private fun element(name: String, block: Element.() -> Unit) = xmlDocument.createElement(name).apply {
        block()
    }

    private fun Element.attribute(name: String, value: Boolean) {
        setAttribute(name, value.toString())
    }

    private fun Element.attribute(name: String, value: Int) {
        setAttribute(name, value.toString())
    }

    private fun Element.attribute(name: String, value: Float) {
        setAttribute(name, value.toString())
    }

    private fun Element.attribute(name: String, value: Double) {
        setAttribute(name, value.toString())
    }

    private fun Element.attribute(name: String, value: String) {
        setAttribute(name, value)
    }

    private fun Element.child(name: String, block: Element.() -> Unit) {
        val child = element(name) {
            block()
        }
        appendChild(child)
    }

    private fun Element.child(child: Element) {
        appendChild(child)
    }
}

