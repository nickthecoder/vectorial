package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.DeleteShapeImpl
import uk.co.nickthecoder.vectorial.core.change.SelectionChange
import java.util.*

interface Shape : Item {

    override val parent: ShapeParent

    val boundingBox: BoundingBox

    val handles: List<Handle>

    /**
     * Is [point] within the shape, or close to the shape.
     *
     * @param threshold2 The _square_ of the threshold distance.
     */
    fun touching(point: Vector2, threshold: Float, threshold2: Float): Boolean

    /**
     *
     * @param threshold2 The _square_ of the threshold distance.
     */
    fun touchingEdges(point: Vector2, threshold: Float, threshold2: Float, into: MutableList<Edge>)

    fun diagram() = parent.diagram

    fun zOrder() = parent.shapes.indexOf(this)

    /**
     * Lists ancestors ([Group]s, [Layer] and [Diagram]).
     * It does NOT include the [Document].
     *
     * if [excludeDiagram] == `true`, then the [Diagram] isn't included either.
     */
    fun ancestors(includeShape: Boolean = true, excludeDiagram: Boolean = true): Stack<Item> {
        val ancestors = Stack<Item>()
        if (includeShape) ancestors.add(this)
        var current: Parent = this.parent
        while (true) {
            if (current is Document) break
            if (current is Diagram && excludeDiagram) break
            ancestors.add(current)
            current = current.parent
        }
        return ancestors
    }

    fun select() {
        SelectionChange.select(this)?.now()
    }

    fun addToSelection() {
        SelectionChange.add(this)?.now()
    }

    fun removeFromSelection() {
        SelectionChange.remove(this)?.now()
    }

    fun delete() {
        diagram().document.history.batch {
            DeleteShapeImpl(this@Shape).now()
            SelectionChange.remove(this@Shape)?.now()
        }
    }

}

abstract class ShapeImpl : Shape {

    final override lateinit var parent: ShapeParent

}


fun summariseShapes(shapes: List<Shape>) = when (shapes.size) {
    0 -> "Nothing"
    1 -> shapes.first().name
    2 -> shapes.joinToString(separator = ", ") { it.name }
    else -> "${shapes.size} shapes"
}

fun ShapeParent.layer(): Layer {
    if (this is Layer) return this
    val p = parent
    if (p is Layer) {
        return p
    } else if (p is ShapeParent) {
        return p.layer()
    } else {
        throw IllegalStateException("No layer found : $this")
    }
}
