package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.Connection
import uk.co.nickthecoder.vectorial.core.expression.MovableConnectorImpl
import uk.co.nickthecoder.vectorial.core.shape.Shape

interface MoveConnection : ShapeChange {
    val connectionLabel: String
    val connection: Connection<*>
    val from: Vector2
    val to: Vector2
}

internal class MoveConnectionImpl(

    override val shape: Shape,
    override val connectionLabel: String,
    override val connection: Connection<*>,
    override var to: Vector2

) : MoveConnection, InternalChange {

    override val label: String = "Move ${shape.name}.$connectionLabel"

    override val from = connection.connector.eval()

    override fun redo() {
        (connection.connector as MovableConnectorImpl).performMoveTowards(to)
    }

    override fun undo() {
        (connection.connector as MovableConnectorImpl).performMoveTowards(from)
    }

    override fun canMergeWith(previous: Change) = previous is MoveConnectionImpl && connection === previous.connection

    override fun mergeWith(previous: Change) {
        previous as MoveConnectionImpl
        previous.to = this.to
    }
}
