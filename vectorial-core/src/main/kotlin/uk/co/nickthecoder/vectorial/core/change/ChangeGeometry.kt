package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Geometry
import uk.co.nickthecoder.vectorial.core.shape.*

/**
 * Changes the replaces the existing [Geometry] of a [GeometryShape] with another implementation.
 *
 * For example, `Shape<Rectangle>` can switch its geometry between [CenteredRectangle] or [CornerBasedRectangle].
 */
interface ChangeGeometry : ShapeChange {
    val newGeometry: Geometry
}

internal class ChangeGeometryImpl(

    override val label: String,
    override val shape: GeometryShape,
    override val newGeometry: Geometry

) : ChangeGeometry, InternalChange {

    private val oldGeometry = shape.geometry

    override fun redo() {
        (shape as GeometryShapeImpl).geometry = newGeometry
    }

    override fun undo() {
        (shape as GeometryShapeImpl).geometry = oldGeometry
    }

}
