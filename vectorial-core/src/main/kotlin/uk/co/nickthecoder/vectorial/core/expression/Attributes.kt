package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.AttributesOwner
import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.change.Change
import uk.co.nickthecoder.vectorial.core.change.SetDiagramAttributeImpl
import uk.co.nickthecoder.vectorial.core.change.SetShapeAttributeImpl
import uk.co.nickthecoder.vectorial.core.change.SetStyleAttributeImpl
import uk.co.nickthecoder.vectorial.core.Geometry
import uk.co.nickthecoder.vectorial.core.shape.Line
import uk.co.nickthecoder.vectorial.core.shape.Shape
import uk.co.nickthecoder.vectorial.core.ShapeStyle


interface Attributes {

    val owner: AttributesOwner

    /**
     * Listeners are informed when an [Attributes]' [Expression] re-assigned, and also when an [Expression]
     * changes value (due to one of the Expression's dependencies changing).
     * By listening to [Attributes] we do not need listeners on every single [Expression].
     */
    val listeners: List<AttributesListener>

    fun keys(): List<String>

    /**
     * Gets the [Attribute] for the named attribute.
     * A [Attribute] is mutable, but its mutability is hidden from the API.
     */
    operator fun get(key: String): Attribute<*, *>

    //operator fun <V : Any, E : Expression<V>> set(key: String, expression: E)

    /**
     * Evaluates the value for the named [Attribute].
     */
    fun eval(key: String) = get(key).eval()

    fun addListener(l: AttributesListener)
    fun removeListener(l: AttributesListener)
}

/**
 * An [Attribute] is owned by [Attributes], and are stored in a [Map] via their [key]
 * (which must be unique to their owning [Attributes]).
 *
 * When an attribute's value changes, the [Attributes] listeners are informed via [AttributesListener.attributeChanged].
 * This can happen when an [Attribute]'s [expression] is set to a new [Expression] and when the [Expression]'s value changes
 * due to a change in one of the [Expression]'s dependencies.
 *
 * NOTE, in some cases [expression] is also a [Attribute]. For example, if two [Line]s are joined end together, then
 * the second line's `start` value is the first line's `end` [Attribute].
 *
 * [V] is the `value` type, such as [Vector2].
 * [E] is the `expression` type, such as [Vector2Expression].
 */
interface Attribute<V : Any, E : Expression<V>> : Expression<V> {

    val attributes: Attributes
    val key: String
    val expression: E

    fun set(newExpression: E)
}

/**
 * Exposes [setFromHistory], which must only be called from [Change].undo and [Change].redo.
 */
interface MutableAttribute<V : Any, E : Expression<V>> : Attribute<V, E> {
    fun setFromHistory(newExpression: E)
}


internal class AttributesImpl(override val owner: AttributesOwner) : Attributes {

    private val map = mutableMapOf<String, AttributeImpl<*, *>>()

    override val listeners = mutableListOf<AttributesListener>()

    override fun keys() = map.keys.toList().sorted()

    fun values() = map.values

    override fun addListener(l: AttributesListener) {
        listeners.add(l)
    }

    override fun removeListener(l: AttributesListener) {
        listeners.remove(l)
    }

    fun add(attribute: AttributeImpl<*, *>) {
        map[attribute.key] = attribute
        for (l in listeners) {
            l.attributeChanged(attribute.key)
        }
    }

    fun remove(key: String) {
        map.remove(key)
        for (l in listeners) {
            l.attributeChanged(key)
        }
    }


    override fun get(key: String): AttributeImpl<*, *> =
        map[key] ?: throw Exception("Attribute `$key` not found")

    operator fun <V : Any, E : Expression<V>> set(key: String, newExpression: E) {
        @Suppress("UNCHECKED_CAST")
        val attribute: MutableAttribute<V, E> = get(key) as MutableAttribute<V, E>
        attribute.setFromHistory(newExpression)
    }
}

internal abstract class AttributeImpl<V : Any, E : Expression<V>>(
    final override val attributes: AttributesImpl,
    override val key: String,
    exp: E
) : ExpressionBase<V>(), MutableAttribute<V, E> {

    final override var expression: E = exp
        private set

    init {
        addDependencies(exp)
    }

    override fun dirty() {
        super.dirty()
        for (l in attributes.listeners) {
            l.attributeChanged(key)
        }
    }

    /**
     * Like [setFromHistory], but with an unspecified type parameter [E], and therefore this version can throw.
     *
     * @throws ClassCastException if [newExpression]'s type is not what was expected.
     */
    fun setUntyped(newExpression: Expression<*>) {
        if (newExpression.type == type) {
            @Suppress("UNCHECKED_CAST")
            (setFromHistory(newExpression as E))
        } else {
            throw ClassCastException("Expected $type, but found ${newExpression.type}")
        }
    }

    override fun set(newExpression: E) {
        when (val owner = attributes.owner) {
            is Shape -> SetShapeAttributeImpl(owner, this, newExpression).now()
            is Geometry -> SetShapeAttributeImpl(owner.shape, this, newExpression).now()
            is Diagram -> SetDiagramAttributeImpl(owner, this, newExpression).now()
            is ShapeStyle -> SetStyleAttributeImpl(owner, this, newExpression).now()
            //else -> throw IllegalArgumentException("Unexpected owner : ${owner.javaClass.name}")
        }
    }

    override fun setFromHistory(newExpression: E) {
        removeDependencies(expression)
        expression = newExpression
        addDependencies(expression)
        dirty()
    }

    override fun eval() = expression.eval()

    override fun toString() =
        "Attribute $key = ${eval()}   Type : ${expression.javaClass.simpleName} Value : ${expression.eval()}"
}
