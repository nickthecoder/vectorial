package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.Handle
import uk.co.nickthecoder.vectorial.core.Item
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.change.Change
import uk.co.nickthecoder.vectorial.core.change.SetShapeVariableImpl
import uk.co.nickthecoder.vectorial.core.shape.Shape

/**
 * Most of the time that we need to assign a new [Expression], [Attribute] is used.
 * However, sometimes we want to alter a variable, which is _not_ an [Attribute],
 * in which case we use a [Variable].
 *
 * For example, when we make a [Connection] from a [Handle] to a point along a
 * line segment, the ratio along that line segment is a [Variable].
 *
 * The [name] of a variable does not have to be unique, and is for _informational_
 * purposes only (not functional).
 *
 * [V] is the `value` type, such as [Vector2].
 * [E] is the `expression` type, such as [Vector2Expression].
 *
 */
interface Variable<V : Any, E : Expression<V>> : Expression<V> {
    val name: String
    val innerExpression: E
    fun assignment(newExpression: E): Change?
}

abstract class VariableImpl<V : Any, E : Expression<V>>(

    val owner: Item,
    final override val name: String,
    final override var innerExpression: E

) : ExpressionBase<V>(), Variable<V, E> {

    init {
        addDependencies(innerExpression)
    }

    fun set(newExpression: E) {
        removeDependencies(innerExpression)
        innerExpression = newExpression
        addDependencies(innerExpression)
        dirty()
    }

    override fun eval() = innerExpression.eval()

    override fun assignment(newExpression: E): Change {
        return when (owner) {
            is Shape -> SetShapeVariableImpl(name, owner, this, newExpression)
            else -> throw Exception("Variable `$name` owner is not a Shape")
        }
    }

    override fun toString() = "Variable $innerExpression"
}
