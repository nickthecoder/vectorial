package uk.co.nickthecoder.vectorial.core.shape


enum class JoinStyle {
    /**
     * Corners are drawn with sharp joints.
     * If the joint's outer angle is too large, the joint is drawn as beveled instead,
     * to avoid the miter extending too far out.
     */
    MITER,

    /**
     * Corners are flattened.
     */
    BEVEL,

    /**
     * Corners are rounded off.
     */
    ROUND
}
