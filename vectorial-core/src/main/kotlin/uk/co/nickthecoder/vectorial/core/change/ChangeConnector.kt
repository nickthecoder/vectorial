package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.expression.Connection
import uk.co.nickthecoder.vectorial.core.expression.ConnectionImpl
import uk.co.nickthecoder.vectorial.core.expression.ConnectorToEdge


interface ChangeConnector : DiagramChange {
    val connection: Connection<*>
    val newConnector: ConnectorToEdge
}

internal class ChangeConnectorImpl(
    override val connection: ConnectionImpl<*>,
    override val newConnector: ConnectorToEdge,

    ) : ChangeConnector, InternalChange {

    private val oldConnector = connection.connector

    override val diagram: Diagram
        get() = newConnector.edge.shape.diagram()

    override val label: String
        get() = "Change Connector : ${connection.type.name}"

    override fun redo() {
        connection.changeConnectorImpl(newConnector)
    }

    override fun undo() {
        connection.changeConnectorImpl(oldConnector)
    }
}
