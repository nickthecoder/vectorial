package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.SetShapeAttributeImpl
import uk.co.nickthecoder.vectorial.core.expression.*

/**
 * A construction line, which can be dragged from the rulers (above and to the left of the diagram).
 * [GuideLine]s are often horizontal or vertical, but can be at any [angle].
 */
interface GuideLine : Geometry {

    val distance: FloatAttribute

    val angle: AngleAttribute

    fun distance() = distance.eval()
    fun angle() = angle.eval()

    fun moveTo(position: Vector2)

    companion object {

        fun create(distance: FloatExpression, angle: AngleExpression): GuideLine = GuideLineImpl(distance, angle)

        fun create(distance: Float, angle: Angle): GuideLine =
            GuideLineImpl(FloatConstant(distance), AngleConstant(angle))

        const val DISTANCE = "distance"
        const val ANGLE = "angle"
    }
}

internal class GuideLineImpl(
    distanceExpression: FloatExpression,
    angleExpression: AngleExpression

) : GuideLine, MutableGeometry {

    override val baseName: String get() = "guide"

    override lateinit var shape: GeometryShape

    // Attributes
    override val attributes = AttributesImpl(this)

    override val distance = createAttribute(attributes, GuideLine.DISTANCE, distanceExpression)
    override val angle = createAttribute(attributes, GuideLine.ANGLE, angleExpression)
    val point = createAttribute(attributes, "point", fromPolar(distance, angle.perpendicular()))

    // Expressions
    val delta: Vector2Expression = angle.toVector()

    override val start: Vector2Expression = point - delta * 1000f
    val end: Vector2Expression = point + delta * 1000f

    override fun isClosed() = false

    override fun handles() = emptyList<Handle>()

    override fun movableAttributes() = listOf(point)

    private val edge by lazy { AngleEdgeImpl(shape, point, angle) }

    override fun moveTo(position: Vector2) {
        val newDist = position.dot(angle.eval().perpendicular().toVector())
        SetShapeAttributeImpl(shape, distance, FloatConstant(newDist)).now()
    }

    override fun touchingEdges(point: Vector2, threshold: Float,threshold2: Float, into: MutableList<Edge>) {
        if (edge.isNear(point, threshold2)) {
            into.add(edge)
        }
    }

    override fun toPolyline(thickness: Float, distanceTolerance: Float, angleTolerance: Double): Polyline {
        return Polyline(listOf(start.eval(), end.eval()), 1f, false)
    }

    override fun toPath(): Path = PathImpl(start.eval()).apply {
        appendLineSegment(end.eval())
    }

    override fun toString() = "GuideLine distance ${distance.eval()} angle ${angle.eval().degrees}°"

}

