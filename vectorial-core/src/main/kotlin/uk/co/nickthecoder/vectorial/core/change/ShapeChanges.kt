package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.Geometry
import uk.co.nickthecoder.vectorial.core.shape.Shape

/**
 * [Change]s which alter a single [Shape].
 *
 * Note [AddShape] nor [DeleteShape] implement this interface, as it is designed for changes which modify an existing [Shape].
 */
interface ShapeChange : DiagramChange {
    val shape: Shape

    override val diagram: Diagram
        get() = shape.diagram()
}

/**
 * If a [ShapeChange] also changes the [Geometry]'s [Handle]s, then implement this interface,
 * which give a hint that the GUI representations of the [Handle]s should be rebuilt.
 */
interface ControlPointsChange : ShapeChange
