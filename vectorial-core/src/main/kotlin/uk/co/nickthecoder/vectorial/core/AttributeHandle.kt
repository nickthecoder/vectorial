package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.shape.Rectangle

/**
 * A [Handle] which alters an [Attribute] when it is moved.
 *
 */
interface AttributeHandle<V : Any, E : Expression<V>> : Handle {

    /**
     * The attribute which will be adjusted by this [Handle].
     * e.g. it will be `size` for [Rectangle]'s size, width and height [Handle]s.
     */
    val attribute: Attribute<V, E>

    override val name: String
        get() = attribute.key

    override fun dependsOn() = listOf(attribute)

    override fun tip() = (attribute.expression as? HasTip)?.tip() ?: super.tip()

    override fun isFree() = attribute.expression is Constant<*>

    override fun isConnectable() = true

    override fun isFixed(): Boolean {
        return when (val exp = attribute.expression) {
            is Constant<*> -> false
            is Connection<*> -> exp.isFixed()
            else -> true
        }
    }

}
