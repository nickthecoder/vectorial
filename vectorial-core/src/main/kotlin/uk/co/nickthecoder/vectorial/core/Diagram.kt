package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.change.*
import uk.co.nickthecoder.vectorial.core.change.GroupShapesImpl
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.expression.AttributesImpl
import uk.co.nickthecoder.vectorial.core.shape.AnyShapeFilter
import uk.co.nickthecoder.vectorial.core.shape.Group
import uk.co.nickthecoder.vectorial.core.shape.Shape
import uk.co.nickthecoder.vectorial.core.shape.ShapeFilter


/**
 *                           ┌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┐
 *                           ┆     Item        ┆
 *                           ┆                 ┆
 *                           ┆ name : String   ┆
 *                     ┌─────┤ parent          ┆
 *                     │     └╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┘
 *                     │             △
 *                     │             │
 *                     │     ┌───────┴─────────┐
 *                     │     │     Parent      │
 *                     │     │                 │
 *                     └─────┤ items           │
 *                           └─────────────────┘
 *                                    △
 *                                    │
 *                  ┌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┴╌╌╌╌╌╌╌╌╌┬╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┐
 *                  ┆                           ┆                               ┆
 *                  ┆                  ┌────────┴────────┐                      ┆
 *          ┌╌╌╌╌╌╌╌┴╌╌╌╌╌╌╌╌┐         │   Document      │                      ┆
 *          ┆  LayerParent   ┆         │                 │                      ┆
 *          ┆                ┆         │ diagram(s)      │                      ┆
 *     ┌────┤ layers         ┆         │                 │                      ┆
 *     │    ┆                ┆         └────────┬────────┘                      ┆
 *     │    └╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┘                  │                               ┆
 *     │            △                           │                               ┆
 *     │            ┊                           │                               ┆
 *     │            ├╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┐     │                               ┆
 *     │            ┊                     ┆     │                      ┌╌╌╌╌╌╌╌╌┴╌╌╌╌╌╌╌╌┐
 *     │            ┊                  ┌──┴─────┴────────┐             ┆   ShapeParent   ┆
 *     │            ┊                  │     Diagram     │             ┆                 ┆
 *     │            ┊                  │                 │             ┆ shapes          ├──────┐
 *     │            ┊                  │ document        │             ┆ layer           ┆      │
 *     │            ┊                  │                 │             └╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌╌┘      │
 *     │            ┊                  └────────┬────────┘                     △                │
 *     │            ┊                           │                              ┆                │
 *     │            ┊                           │                              ┆                │
 *     │            └┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┄┐    │     ┌╌╌╌╌╌╌╌╌╌╌╌╌┬╌╌╌╌╌╌╌╌╌╌╌┘                │
 *     │                                   ┊    │     ┆            ┆                            │
 *     │                               ┌───┴────┴─────┴──┐         ┆                            │
 *     │                               │      Layer      │         ┆                            │
 *     │                               │                 │         ┆                            │
 *     └───────────────────────────────┤ parent          │         ┆                            │
 *                                     │ shapes          │         ┆                            │
 *                                     └────────┬────────┘         ┆                            │
 *                                              │                  ┆                            │
 *                                              │                  ┆                            │
 *                                              │                  ┆                            │
 *                                              │                  ┆                            │
 *                                    ┌─────────┴───────┐          ┆                            │
 *                                    │      Shape      │          ┆                            │
 *                                    │                 │          ┆                            │
 *                                    │ parent          ├──────────┼────────────────────────────┘
 *                                    │                 │          ┆
 *                                    └─────────────────┘          ┆
 *                                             △                   ┆
 *                                             │                   ┆
 *                                             │                   ┆
 *                              ┌──────────────┴──────────────┐    ┆
 *                              │                             │    ┆
 *                     ┌────────┴──────────┐       ┌──────────┴────┴──┐
 *                     │  GeometryShape    │       │      Group       │
 *                     │                   │       │                  │
 *                     │ layer             │       │ layer            │
 *                     │ style             │       │ shapes           │
 *                     │ geometry          │       └──────────────────┘
 *                     │                   │
 *                     │                   │
 *                     └───────────────────┘
 *
 *
 *
 *
 *
 *
 *
 */
interface Diagram : LayerParent, AttributesOwner {

    val document: Document

    val attributes: Attributes

    val size: Vector2Attribute

    val currentLayerOrGroup: ShapeParent

    val selection: Selection

    fun size() = size.eval()
    fun width() = size.eval().x
    fun height() = size.eval().y

    fun addDiagramListener(dl: DocumentListener)

    fun clearSelection() {
        SelectionChange.clear(this)?.now()
    }

    fun select(shape: Shape) {
        SelectionChange.select(shape)?.now()
    }

    fun select(shapes: List<Shape>) {
        SelectionChange.select(diagram, shapes)?.now()
    }

    fun clearControlPointSelection() {
        SelectControlPoint.clearHandleSelection(this)?.now()
    }

    fun selectControlPoint(handle: Handle) {
        SelectControlPoint.selectControlPoint(handle)?.now()
    }

    fun findTopShapeAt(point: Vector2, threshold: Float, threshold2: Float, filter: ShapeFilter): Shape?

    /**
     * Returns a list of all [Shape]s which are at [point] (or close enough based on [threshold2]).
     * The shapes are ordered, such that the top-most one is first in the list, and the bottom-most is last.
     */
    fun findShapesAt(point: Vector2, threshold: Float,threshold2: Float, into: MutableList<Shape>, filter: ShapeFilter = AnyShapeFilter)

    fun findShapesAt(point: Vector2, threshold: Float,threshold2: Float, filter: ShapeFilter = AnyShapeFilter): List<Shape> {
        val into = mutableListOf<Shape>()
        findShapesAt(point, threshold, threshold2, into, filter)
        return into
    }

    /**
     * A [Change] which will create a [Group] consisting of the selected shapes.
     */
    fun group(): GroupShapes? = if (selection.isEmpty()) null else GroupShapesImpl(this)

    /**
     * A [Change] which will create ungroup all the selected [Group]s.
     */
    fun ungroup(): UngroupShapes? = if (selection.shapes.filterIsInstance<Group>().isEmpty()) {
        null
    } else {
        UngroupShapesImpl(this)
    }

    companion object {
        const val SIZE = "size"
    }
}


internal class DiagramImpl(override val document: Document) : LayerParentImpl, Diagram {

    override val layers = mutableListOf<Layer>()

    override val parent get() = document

    override val name get() = "diagram"

    override val diagram: Diagram get() = this

    override val items get() = layers

    override val attributes = AttributesImpl(this)
    override val size = createAttribute(attributes, Diagram.SIZE, Vector2Constant(Vector2(600f, 800f)))

    override lateinit var currentLayerOrGroup: ShapeParent

    override val selection = SelectionImpl()

    private val documentListeners = mutableListOf<DocumentListener>()

    override fun addDiagramListener(dl: DocumentListener) {
        documentListeners.add(dl)
    }

    /**
     * Adds a top-level Layer [newLayer].
     */
    override fun addLayer(newLayer: Layer, position: NewLayerPosition) {
        AddLayerImpl(newLayer, diagram, null, position).now()
    }

    override fun findShapesAt(point: Vector2, threshold: Float,threshold2: Float, into: MutableList<Shape>, filter: ShapeFilter) {
        for (layer in layers) {
            layer.findShapesAt(point, threshold, threshold2, into, filter)
        }
    }

    override fun findTopShapeAt(point: Vector2, threshold: Float, threshold2: Float, filter: ShapeFilter): Shape? {
        for (layer in layers.asReversed()) {
            val result = layer.findTopShapeAt(point, threshold, threshold2, filter)
            if (result != null) return result
        }
        return null
    }

    override fun toString() = "Diagram ${width()}x${height()}"
}
