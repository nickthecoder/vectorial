package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.korge.PointError
import uk.co.nickthecoder.vectorial.core.korge.Poly2Tri
import uk.co.nickthecoder.vectorial.core.shape.CapStyle
import uk.co.nickthecoder.vectorial.core.shape.JoinStyle
import uk.co.nickthecoder.vectorial.core.shape.Shape

/**
 * Used internally by Vectorial during rendering, and also when checking if a point is touching the
 * edge of a [Shape].
 *
 * [Shape]s cache their [Polyline]s, and rebuild them whenever the [Shape]'s [Geometry] changes.
 *
 * [Mesh]es are similar to [Polyline] in that they are used during rendering, but for filling, rather than
 * drawing the edge (fill vs stroke).
 *
 * Note, a [Polyline] is not a [Shape], nor a [Geometry].
 * If you wish to draw a polygon (or an open path composed of straight lines),
 * then use a [Path]. (However, at time of writing, [Path] hasn't been written yet!).
 */
class Polyline(val vertices: List<Vector2>, val thickness: Float, val isClosed: Boolean = false) {

    val boundingBox: BoundingBox

    init {
        var left = Float.MAX_VALUE
        var top = Float.MAX_VALUE
        var right = -Float.MAX_VALUE
        var bottom = -Float.MAX_VALUE
        for (vertex in vertices) {
            if (vertex.x < left) left = vertex.x
            if (vertex.y < top) top = vertex.y
            if (vertex.x > right) right = vertex.x
            if (vertex.y > bottom) bottom = vertex.y
        }
        boundingBox = BoundingBox(
            left - thickness / 2,
            top - thickness / 2,
            right + thickness / 2,
            bottom + thickness / 2
        )
    }

    fun withinBoundingBox(point: Vector2, threshold : Float = 0f) = boundingBox.contains(point, threshold)

    fun contains(point: Vector2): Boolean {
        // TODO Check if it is actually in the shape.
        return true
    }

    fun nearEdge(point: Vector2, thresholdSquared: Float): Boolean {
        val first = vertices.first()
        var from = first
        for (i in 1 until vertices.size) {
            val to = vertices[i]
            if (minimumDistanceSquare(from, to, point) < thresholdSquared) return true

            from = to
        }
        if (isClosed) {
            if (minimumDistanceSquare(from, first, point) < thresholdSquared) return true
        }
        return false
    }

    fun length(): Float {
        var result = 0f
        var from = vertices.first()
        for (i in 1 until vertices.size) {
            val to = vertices[i]
            result += (to - from).magnitude()
            from = to
        }
        return result
    }

    override fun toString() = "Polyline \n    " +
            vertices.subList(1, vertices.size - 1).joinToString(separator = "\n    ")

    /**
     * @param index The index into the [vertices] list.
     *        This may be negative, or greater than the size of the list, and still gives sensible results.
     *        e.g. -1 is the last point, -2 is the penultimate point etc.
     * @return The nth point of the polyline.
     */
    operator fun get(index: Int): Vector2 {
        return vertices[index.mod(vertices.size)]
    }

    /**
     * Used by [createStrokeMesh]
     */
    private class PolySegment(val center: LineSegment, halfThickness: Float) {
        val midPoint = (center.start + center.end) / 2f

        // calculate the segment's outer edges by offsetting
        // the central line by the normal vector
        // multiplied with the thickness
        val edge1 = center + center.normal() * halfThickness
        val edge2 = center - center.normal() * halfThickness

        override fun toString() = "PolySegment e1 : ${edge1}  e2 : ${edge2}"
    }

    /**
     * Ported from https://github.com/CrushedPixel/Polyline2D
     *
     * @param joinStyle When gradient filling across the line, [JoinStyle.MITER] doesn't look very good.
     * @param allowOverlap
     * @param miterMinAngle The threshold for mitered joints.
     *     If the joint's angle is smaller than this angle, the joint will be drawn beveled instead.
     *     The default value is ~10°
     * @param roundMinAngle The minimum angle of a round joint's triangles.
     *     The default value is ~20°
     * @param mirroredAcross When using GradientAcross, the gradient should bend round,
     *     instead of going straight ahead.
     * @return A list of points, where each 3 points in turn form a triangle
     *     A list of UVs, where U is the length along the path (range 0..1)
     *     and V is the distance from the center line (range -1..1)
     *
     * Note, in the C++ original, there was an extra [JoinStyle] to join the end to the start.
     * I've implemented this using [isClosed] instead.
     * The C++ original didn't include UVs.
     *
     */
    fun createStrokeMesh(
        thickness: Float, capStyle: CapStyle, joinStyle: JoinStyle,
        allowOverlap: Boolean = true, miterMinAngle: Float = 0.349066f, roundMinAngle: Float = 0.174533f,
        includeUVs: Boolean, mirroredAcross: Boolean
    ): Mesh {
        // operate on half the thickness to make our lives easier
        val halfThickness = thickness / 2

        val points = mutableListOf<Vector2>()
        val uvs = mutableListOf<Vector2>()

        val totalLength = if (includeUVs) {
            length() + if (capStyle == CapStyle.BUTT) 0f else thickness
        } else {
            1f
        }
        val endCapTravel = if (capStyle == CapStyle.BUTT) 0f else halfThickness / totalLength


        val segments = mutableListOf<PolySegment>()
        for (i in 0 until vertices.size - 1) {
            val point1 = vertices[i]
            val point2 = vertices[i + 1]

            // to avoid division-by-zero errors,
            // only create a line segment for non-identical points
            if (point1 != point2) {
                segments.add(PolySegment(LineSegment(point1, point2), halfThickness))
            }
        }

        if (segments.isEmpty()) {
            // handle the case of insufficient input points
            return Mesh(emptyList(), emptyList(), length = 0f, thickness = thickness)
        }

        if (isClosed) {
            // create a connecting segment from the last to the first point

            // to avoid division-by-zero errors, only create a line segment for non-identical points
            if (vertices.first() != vertices.last()) {
                segments.add(PolySegment(LineSegment(vertices.last(), vertices.first()), halfThickness))
            }
        }

        var nextStart1 = Vector2(0f, 0f)
        var nextStart2 = Vector2(0f, 0f)
        var start1 = Vector2(0f, 0f)
        var start2 = Vector2(0f, 0f)
        var end1 = Vector2(0f, 0f)
        var end2 = Vector2(0f, 0f)


        fun createTriangleFan(
            connectTo: Vector2, origin: Vector2,
            start: Vector2, end: Vector2, clockwise: Boolean,
            travelledFrom: Float, isEndCap: Boolean
        ) {

            val point1 = start - origin
            val point2 = end - origin

            // calculate the angle between the two points
            var angle1 = Math.atan2(point1.y.toDouble(), point1.x.toDouble())
            var angle2 = Math.atan2(point2.y.toDouble(), point2.x.toDouble())

            // ensure the outer angle is calculated
            if (clockwise) {
                if (angle2 > angle1) {
                    angle2 -= 2 * Math.PI
                }
            } else {
                if (angle1 > angle2) {
                    angle1 -= 2 * Math.PI
                }
            }

            val jointAngle = angle2 - angle1

            // calculate the amount of triangles to use for the joint
            val numTriangles = Math.max(1, Math.floor(Math.abs(jointAngle) / roundMinAngle).toInt())

            // calculate the angle of each triangle
            val triAngle = jointAngle / numTriangles

            var startPoint = start
            for (t in 1..numTriangles) {
                val rot = t * triAngle

                val cos = Math.cos(rot).toFloat()
                val sin = Math.sin(rot).toFloat()

                val endPoint = if (t == numTriangles) {
                    // it's the last triangle - ensure it perfectly connects to the next line
                    end
                } else {
                    // re-add the rotation origin to the target point
                    origin.plus(
                        (cos * point1.x - sin * point1.y),
                        (sin * point1.x + cos * point1.y)
                    )
                }

                // Add the triangle
                points.add(startPoint)
                points.add(endPoint)
                points.add(connectTo)
                startPoint = endPoint

                if (includeUVs) {
                    if (isEndCap) {

                        if (mirroredAcross) {
                            if (travelledFrom == 0f) {
                                // The cap for the start of the polyline when mirroredAcross==true
                                uvs.add(Vector2(0f, 0f))
                                uvs.add(Vector2(0f, 0f))
                                uvs.add(Vector2(endCapTravel, 0.5f))
                            } else {
                                // The cap for the end of the polyline when mirroredAcross==true
                                uvs.add(Vector2(1f, 0f))
                                uvs.add(Vector2(1f, 0f))
                                uvs.add(Vector2(1f - endCapTravel, 0.5f))
                            }
                        } else {
                            // TODO, This isn't quite right, should be bases on angles, but doesn't look bad.
                            val foo1 = (t - 1).toFloat() / numTriangles
                            val foo2 = t.toFloat() / numTriangles

                            // TODO UV.x isn't quite right. The outer points of the fan have the same UV.x value,
                            // but they should be ever so slightly different - based on the start and end angles
                            // of the triangle.
                            if (travelledFrom == 0f) {
                                // The cap for the start of the polyline when mirroredAcross==false
                                uvs.add(Vector2(endCapTravel * Math.abs(cos), foo1))
                                uvs.add(Vector2(endCapTravel * Math.abs(cos), foo2))
                                uvs.add(Vector2(endCapTravel, 0.5f))
                            } else {
                                // The cap for the end of the polyline when mirroredAcross==false
                                uvs.add(Vector2(1f - endCapTravel * Math.abs(cos), foo1))
                                uvs.add(Vector2(1f - endCapTravel * Math.abs(cos), foo2))
                                uvs.add(Vector2(1f - endCapTravel, 0.5f))
                            }
                        }
                    } else {

                        if (clockwise) {
                            // JoinStyle.ROUND (clockwise)
                            uvs.add(Vector2(travelledFrom, 0f))
                            uvs.add(Vector2(travelledFrom, 0f))
                            uvs.add(Vector2(travelledFrom, 1f))
                        } else {
                            // JoinStyle.ROUND (anticlockwise)
                            uvs.add(Vector2(travelledFrom, 1f))
                            uvs.add(Vector2(travelledFrom, 1f))
                            uvs.add(Vector2(travelledFrom, 0f))
                        }

                    }
                }
            }
        }

        fun createJoint(segment1: PolySegment, segment2: PolySegment, travelled: Float) {

            // calculate the angle between the two line segments
            val dir1 = segment1.center.direction()
            val dir2 = segment2.center.direction()

            val angle = dir1.angleRadians(dir2)
            // wrap the angle around the 180° mark if it exceeds 90° for minimum angle detection
            var wrappedAngle = angle
            if (wrappedAngle > Math.PI / 2) {
                wrappedAngle = Math.PI - wrappedAngle
            }

            val actualJoinStyle = if (joinStyle == JoinStyle.MITER && wrappedAngle < miterMinAngle) {
                // The joint is too pointy (the angle is too small).
                // To avoid the intersection point being extremely far out, we render the joint beveled instead.
                JoinStyle.BEVEL
            } else {
                joinStyle
            }

            when (actualJoinStyle) {

                JoinStyle.MITER -> {
                    // calculate both edges' intersection point with the next segment's edges
                    val sec1 = LineSegment.intersection(segment1.edge1, segment2.edge1, true)
                    val sec2 = LineSegment.intersection(segment1.edge2, segment2.edge2, true)

                    end1 = sec1 ?: segment1.edge1.end
                    end2 = sec2 ?: segment1.edge2.end

                    nextStart1 = end1
                    nextStart2 = end2
                }

                JoinStyle.BEVEL, JoinStyle.ROUND -> {

                    // find out which are the inner edges for this joint
                    val x1 = dir1.x
                    val x2 = dir2.x
                    val y1 = dir1.y
                    val y2 = dir2.y

                    val clockwise = x1 * y2 - x2 * y1 < 0

                    val inner1: LineSegment
                    val inner2: LineSegment
                    val outer1: LineSegment
                    val outer2: LineSegment

                    // as the normal vector is rotated counter-clockwise, the first edge lies to the left
                    // from the central line's perspective, and the second one to the right.
                    if (clockwise) {
                        outer1 = segment1.edge1
                        outer2 = segment2.edge1
                        inner1 = segment1.edge2
                        inner2 = segment2.edge2
                    } else {
                        outer1 = segment1.edge2
                        outer2 = segment2.edge2
                        inner1 = segment1.edge1
                        inner2 = segment2.edge1
                    }

                    // calculate the intersection point of the inner edges
                    val innerSecOpt = LineSegment.intersection(inner1, inner2, allowOverlap)

                    // for parallel lines, simply connect them directly
                    val innerSec = innerSecOpt ?: inner1.end

                    // if there's no inner intersection, flip the next start position for near-180° turns
                    val innerStart = if (innerSecOpt != null) {
                        innerSec
                    } else if (angle > Math.PI / 2) {
                        outer1.end
                    } else {
                        inner1.end
                    }

                    if (clockwise) {
                        end1 = outer1.end
                        end2 = innerSec

                        nextStart1 = outer2.start
                        nextStart2 = innerStart

                    } else {
                        end1 = innerSec
                        end2 = outer1.end

                        nextStart1 = innerStart
                        nextStart2 = outer2.start
                    }

                    // connect the intersection points according to the joint style

                    if (actualJoinStyle == JoinStyle.BEVEL) {
                        // simply connect the intersection points
                        points.add(outer1.end)
                        points.add(outer2.start)
                        points.add(innerSec)

                        if (includeUVs) {
                            if (clockwise) {
                                uvs.add(Vector2(travelled, 0f))
                                uvs.add(Vector2(travelled, 0f))
                                uvs.add(Vector2(travelled, 1f))
                            } else {
                                uvs.add(Vector2(travelled, 1f))
                                uvs.add(Vector2(travelled, 1f))
                                uvs.add(Vector2(travelled, 0f))
                            }
                        }

                    } else {
                        // draw a circle between the ends of the outer edges,
                        // centered at the actual point
                        // with half the line thickness as the radius
                        createTriangleFan(
                            innerSec, segment1.center.end, outer1.end, outer2.start,
                            clockwise, travelled, false
                        )
                    }
                }
            }
        }

        val firstSegment = segments.first()
        val lastSegment = segments.last()
        var pathStart1 = firstSegment.edge1.start
        var pathStart2 = firstSegment.edge2.start
        var pathEnd1: Vector2 = lastSegment.edge1.end
        var pathEnd2: Vector2 = lastSegment.edge2.end

        if (!isClosed) {

            when (capStyle) {
                CapStyle.SQUARE -> {
                    // extend the start/end points by half the thickness
                    pathStart1 -= firstSegment.edge1.normalisedDirection() * halfThickness
                    pathStart2 -= firstSegment.edge2.normalisedDirection() * halfThickness
                    pathEnd1 += lastSegment.edge1.normalisedDirection() * halfThickness
                    pathEnd2 += lastSegment.edge2.normalisedDirection() * halfThickness
                }

                CapStyle.ROUND -> {
                    // Draw half circle end caps.
                    // Note, isEndCap=false when symmetric==true. This causes a circular gradient at the ends
                    // (which only looks nice when the gradient is made symmetrical).
                    createTriangleFan(
                        firstSegment.center.start, firstSegment.center.start,
                        firstSegment.edge1.start, firstSegment.edge2.start, false,
                        0f, true
                    )
                    createTriangleFan(
                        lastSegment.center.end, lastSegment.center.end,
                        lastSegment.edge1.end, lastSegment.edge2.end, true,
                        1f, true
                    )
                }

                CapStyle.BUTT -> {
                    // Do nothing.
                }
            }
        } else {
            createJoint(lastSegment, firstSegment, 1f)
            // createJoint only updates nextStart and nextEnd. Update the segments starts/ends.
            // The original didn't need this extra "bodge"!
            firstSegment.edge1.start = nextStart1
            firstSegment.edge2.start = nextStart2
            lastSegment.edge1.end = end1
            lastSegment.edge2.end = end2
        }

        var travelled = 0f

        for (i in 0 until segments.size) {
            val segment = segments[i]

            val segmentLength = if (includeUVs) segment.center.length() else 0f
            var nextTravelled = travelled + segmentLength / totalLength

            if (i == 0) {
                start1 = pathStart1
                start2 = pathStart2
                if (capStyle != CapStyle.BUTT) {
                    travelled += endCapTravel
                    nextTravelled += endCapTravel
                }
            }

            if (i + 1 == segments.size) {
                // this is the last segment
                end1 = pathEnd1
                end2 = pathEnd2

            } else {
                createJoint(segment, segments[i + 1], nextTravelled)
            }

            // The two triangles for the line of segment.

            points.add(start1)
            points.add(start2)
            points.add(end1)

            points.add(end1)
            points.add(start2)
            points.add(end2)

            if (includeUVs) {
                uvs.add(Vector2(travelled, 0f))
                uvs.add(Vector2(travelled, 1f))
                uvs.add(Vector2(nextTravelled, 0f))

                uvs.add(Vector2(nextTravelled, 0f))
                uvs.add(Vector2(travelled, 1f))
                uvs.add(Vector2(nextTravelled, 1f))
            }

            start1 = nextStart1
            start2 = nextStart2
            travelled = nextTravelled
        }

        return Mesh(points, uvs, length = totalLength, thickness = thickness)
    }

    fun createFillMesh(): Mesh {
        if (vertices.isEmpty()) return Mesh(emptyList(), emptyList())

        // Poly2Tri throws a fit if there are two points which are the same,
        // so let's filter out successive points which are the same...
        val nonOverlappingPoints = mutableListOf<Vector2>()
        var prev = vertices.last()
        for (point in vertices) {
            if (point != prev) {
                nonOverlappingPoints.add(point)
                prev = point
            }
        }
        if (nonOverlappingPoints.size < 3) return Mesh(emptyList(), emptyList())

        return try {
            val triangles = Poly2Tri().apply {
                addPolyline(nonOverlappingPoints)
            }.triangulate()

            val resultPoints = mutableListOf<Vector2>()
            for (t in triangles) {
                resultPoints.addAll(t.getPoints())
            }

            Mesh(resultPoints, emptyList())

        } catch (e: PointError) {
            warn("PointError $e")
            Mesh(emptyList(), emptyList())
        }

    }
}

private class LineSegment(var start: Vector2, var end: Vector2) {

    fun length() = (end - start).magnitude()

    fun normal() = (end - start).perpendicular().unit()

    fun direction() = (end - start)

    fun normalisedDirection() = direction().unit()

    operator fun plus(a: Vector2) = LineSegment(start + a, end + a)
    operator fun minus(a: Vector2) = LineSegment(start - a, end - a)

    override fun toString() = "LineSegment $start -> $end"

    companion object {

        fun intersection(a: LineSegment, b: LineSegment, infiniteLines: Boolean): Vector2? {
            // calculate un-normalized direction vectors
            val r = a.direction()
            val s = b.direction()

            val originDist = b.start - a.start

            val uNumerator = originDist.cross(r)
            val denominator = r.cross(s)

            if (Math.abs(denominator) < 0.0001f) {
                // The lines are parallel
                return null
            }

            // solve the intersection positions
            val u = uNumerator / denominator
            val t = originDist.cross(s) / denominator

            if (!infiniteLines && (t < 0 || t > 1 || u < 0 || u > 1)) {
                // the intersection lies outside the line segments
                return null
            }

            // calculate the intersection point
            return a.start + r * t
        }
    }

}
