package uk.co.nickthecoder.vectorial.core.expression

/**
 * Run an [action] whenever [listenTo] Expression changes.
 * NOTE, you should call [Expression.eval] within [action], otherwise you may not get additional notifications
 * (though in practice, you almost certainly will).
 *
 * Call [removeListener] when you have finished listening to the expression.
 */
class ExpressionListener<V : Any>(val listenTo: Expression<V>, val action: () -> Unit) {

    private val fakeExpression = object : ExpressionBase<V>() {
        override val type: Class<V>
            get() = listenTo.type

        override fun eval() = listenTo.eval()

        override fun dirty() {
            action()
        }
    }

    init {
        if (listenTo is ChangeableExpression<*>) {
            listenTo.addDependent(fakeExpression)
        }
    }

    fun removeListener() {
        if (listenTo is ChangeableExpression<*>) {
            listenTo.removeDependent(fakeExpression)
        }
    }

}