package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.Vector2Expression

/**
 * An equilateral diamond shape.
 * There are two implementations, as specified by [Centerable] : [CornerBasedDiamond] and [CenteredDiamond].
 */
interface Diamond : Quadrilateral {

    override val quadrilateralType: QuadrilateralType get() = QuadrilateralType.DIAMOND

    companion object {

        fun create(from: Vector2Expression, to: Vector2Expression): Diamond =
            CornerBasedDiamond(from, to)

        fun create(from: Vector2, to: Vector2): Diamond =
            CornerBasedDiamond(from, to)

        fun createCentered(center: Vector2Expression, size: Vector2Expression): Diamond =
            CenteredDiamond(center, size)

        fun createCentered(center: Vector2, size: Vector2): Diamond =
            CenteredDiamond(center, size)

    }
}
