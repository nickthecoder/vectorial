package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.change.*
import uk.co.nickthecoder.vectorial.core.shape.Shape


/**
 * Which [Shape]s are selected, and if there is only one, then also which [Handle] of that shape is selected.
 *
 * NOTE. Changes to [Selection] go through the [History] mechanism via [SelectionChange] and [SelectControlPoint].
 */
interface Selection : Iterable<Shape> {

    /**
     * The shapes which make up the selection
     */
    val shapes: List<Shape>

    /**
     * If [shapes] is size 1, then we may also select a [Handle] from [Geometry.handles].
     */
    val handle: Handle?

    fun isEmpty() = shapes.isEmpty()
    fun isNotEmpty() = shapes.isNotEmpty()

    fun singleShape(): Shape? {
        if (shapes.size == 1) return shapes.firstOrNull()
        return null
    }

    override fun iterator() = shapes.iterator()


}

internal class SelectionImpl : Selection {

    override val shapes = mutableListOf<Shape>()
    override var handle: Handle? = null

}
