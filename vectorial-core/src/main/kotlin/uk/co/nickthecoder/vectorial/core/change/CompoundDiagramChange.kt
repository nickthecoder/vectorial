package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.Document

interface CompoundDiagramChange : CompoundChange<DiagramChange>, DiagramChange

internal open class CompoundDiagramChangeImpl(label: String, vararg changes: DiagramChange) :
    CompoundChangeImpl<DiagramChange>(label, *changes), CompoundDiagramChange {

    override val document: Document
        get() = diagram.document

    override val diagram: Diagram
        get() = changes[0].diagram
}
