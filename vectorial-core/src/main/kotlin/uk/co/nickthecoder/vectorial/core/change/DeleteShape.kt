package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.ShapeParentImpl
import uk.co.nickthecoder.vectorial.core.shape.Shape

interface DeleteShape : DiagramChange {
    val shape: Shape
}

internal class DeleteShapeImpl(
    override val shape: Shape
) : DeleteShape, InternalChange {

    override val diagram: Diagram
        get() = shape.diagram()

    override val label
        get() = "Delete ${shape.name}"

    override fun redo() {
        (shape.parent as ShapeParentImpl).shapes.remove(shape)
    }

    override fun undo() {
        (shape.parent as ShapeParentImpl).shapes.add(shape)
    }
}
