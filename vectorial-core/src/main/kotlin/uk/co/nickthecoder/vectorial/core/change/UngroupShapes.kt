package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.SelectionImpl
import uk.co.nickthecoder.vectorial.core.ShapeParentImpl
import uk.co.nickthecoder.vectorial.core.shape.Group
import uk.co.nickthecoder.vectorial.core.shape.GroupImpl
import uk.co.nickthecoder.vectorial.core.shape.Shape
import uk.co.nickthecoder.vectorial.core.shape.ShapeImpl

/**
 * Takes selected shapes, and removes them from their parent, placing them into a new [Group].
 * The [Group] is then added to diagram. The zOrder for the group is the lowest zOrder of the original shapes.
 *
 * The selection contains just the [Group] after redo.
 */
interface UngroupShapes : DiagramChange, AltersSelection {
    fun shapes(): List<Shape>
}

internal class UngroupShapesImpl(

    override val diagram: Diagram

) : UngroupShapes, InternalChange {

    override val label: String get() = "Ungroup"

    /**
     * We need to know the parent and the zOrder of each group, so that we can put them
     * back in the correct place in [redo].
     */
    private val groupsParentsZOrders =
        diagram.selection.shapes.filterIsInstance<GroupImpl>()
            .map { Triple(it, it.parent as ShapeParentImpl, it.zOrder()) }.sortedBy { it.third }

    override fun shapes() = groupsParentsZOrders.map { it.first }

    /**
     *  For each group, remove the Group from its parent, and move all the children into the group's
     *  parent at the same zIndex as the Group.
     */
    override fun redo() {
        if (groupsParentsZOrders.isEmpty()) return

        val selection = diagram.selection as SelectionImpl
        selection.shapes.clear()
        selection.handle = null

        for ((group, parent, zOrder) in groupsParentsZOrders) {

            selection.shapes.remove(group)
            parent.shapes.remove(group)
            var newZOrder = zOrder
            for (child in group.shapes) {
                parent.shapes.add(newZOrder, child)
                (child as ShapeImpl).parent = parent
                newZOrder++
                selection.shapes.add(child)
            }
        }

    }

    /**
     * For each old group, remove the children from their existing parent, put them
     * back into their original group, and place the group back into the parent.
     */
    override fun undo() {
        if (groupsParentsZOrders.isEmpty()) return

        val selection = (diagram.selection as SelectionImpl)
        selection.shapes.clear()
        selection.handle = null

        val byGroup = groupsParentsZOrders.groupBy { it.first }
        for (group: GroupImpl in byGroup.keys) {
            for (child in group.shapes.toList()) {
                (child.parent as ShapeParentImpl).shapes.remove(child)
                group.shapes.add(child)
            }
            // TODO group.parent =
        }

    }

}

