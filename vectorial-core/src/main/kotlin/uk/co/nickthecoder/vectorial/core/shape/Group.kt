package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*

interface Group : Shape, ShapeParent {
}


internal class GroupImpl(override val name: String) : Group, ShapeParentImpl() {

    override lateinit var parent: ShapeParent
    override val diagram: Diagram get() = parent.diagram

    override val boundingBox: BoundingBox
        get() {
            if (items.isEmpty()) return BoundingBox(0f, 0f, 0f, 0f)
            var left = Float.MAX_VALUE
            var top = Float.MAX_VALUE
            var right = -Float.MAX_VALUE
            var bottom = -Float.MAX_VALUE
            for (child in shapes) {
                val bb = child.boundingBox
                if (bb.left < left) left = bb.left
                if (bb.top < top) top = bb.top
                if (bb.right > right) right = bb.right
                if (bb.bottom > bottom) bottom = bb.bottom
            }
            return BoundingBox(left, top, right, bottom)
        }

    override val handles: List<Handle> get() = emptyList()

    override fun findTopShapeAt(point: Vector2, threshold: Float, threshold2: Float, filter: ShapeFilter): Shape? {

        for (shape in shapes.asReversed()) {
            if (filter.acceptShape(shape) && shape.touching(point, threshold, threshold2)) {
                return shape
            }
        }
        return null
    }

    override fun touching(point: Vector2, threshold: Float, threshold2: Float): Boolean {
        for (child in shapes) {
            if (child.touching(point, threshold, threshold2)) return true
        }
        return false
    }

    override fun touchingEdges(point: Vector2, threshold: Float, threshold2: Float, into: MutableList<Edge>) {
        for (child in shapes) {
            child.touchingEdges(point, threshold, threshold2, into)
        }
    }

}
