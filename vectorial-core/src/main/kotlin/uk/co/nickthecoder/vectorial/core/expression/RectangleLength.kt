package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.shape.Rectangle

class RectangleLength(val rectangle: Rectangle) : CachedFloatExpression() {

    init {
        addDependencies(rectangle.size)
    }

    override fun recalculate(): Float {
        val s = rectangle.size.eval()
        return (s.x + s.y) * 2
    }
}
