package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.Vector2

/**
 * Unary functions (i.e. takes 1 parameter).
 *
 * Implementing classes should use a type parameter `A` as the `value` type, such as [Vector2].
 * [AE] is the corresponding [Expression] type, such as [Vector2Expression].
 */
interface UnaryFunction<AE : Expression<*>> {
    val label: String
    val a: AE
}
