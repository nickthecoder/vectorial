package uk.co.nickthecoder.vectorial.core

import java.util.Objects
import kotlin.math.acos
import kotlin.math.max
import kotlin.math.min
import kotlin.math.sqrt

class Vector2(val x: Float, val y: Float) {

    constructor() : this(0f,0f)
    constructor(x: Double, y: Double) : this(x.toFloat(), y.toFloat())

    operator fun unaryMinus() = Vector2(-x, -y)

    operator fun plus(other: Vector2) = Vector2(x + other.x, y + other.y)
    operator fun minus(other: Vector2) = Vector2(x - other.x, y - other.y)

    fun plus(dx: Float, dy: Float) = Vector2(x + dx, y + dy)
    fun minus(dx: Float, dy: Float) = Vector2(x - dx, y - dy)

    operator fun times(scale: Float) = Vector2(x * scale, y * scale)
    operator fun times(other: Vector2) = Vector2(x * other.x, y * other.y)
    operator fun times(polar: Polar): Vector2 = (this * polar.magnitude).rotate(polar.angle)

    operator fun div(scale: Float) = Vector2(x / scale, y / scale)
    operator fun div(other: Vector2) = Vector2(x / other.x, y / other.y)

    operator fun rem(other: Float) = Vector2(x % other, y % other)
    operator fun rem(other: Vector2) = Vector2(x % other.x, y % other.y)

    fun translate(dx: Float, dy: Float) = Vector2(x + dx, y + dy)

    fun rotate(angle: Angle) = rotateRadians(angle.radians)

    fun rotateRadians(radians: Double): Vector2 {
        val sin = Math.sin(radians)
        val cos = Math.cos(radians)
        return Vector2(
            (cos * x - sin * y).toFloat(),
            (sin * x + cos * y).toFloat()
        )
    }

    fun rotateDegrees(degrees: Double) = rotateRadians(Angle.toRadians(degrees))

    fun unit() = this / magnitude()

    fun magnitude() = sqrt(x * x + y * y)
    fun magnitudeSquared() = x * x + y * y

    /**
     * @return x + y
     */
    fun hamiltonian() = x + y

    fun angleRadians() = Angle.radiansOf(this)
    fun angleDegrees() = Angle.toDegrees(Angle.radiansOf(this))
    fun angle() = Angle.of(this)

    fun distanceTo(to: Vector2) = sqrt(distanceSquaredTo(to))

    fun distanceSquaredTo(to: Vector2): Float {
        val dx = x - to.x
        val dy = y - to.y
        return dx * dx + dy * dy
    }

    fun dot(other: Vector2) = x * other.x + y * other.y
    fun cross(other: Vector2) = x * other.y - y * other.x
    fun perpendicular() = Vector2(-y, x)

    fun angleRadians(other: Vector2) = acos((this.dot(other) / this.magnitude() / other.magnitude()).toDouble())
    fun angleDegrees(other: Vector2) =
        Angle.toDegrees(acos((this.dot(other) / this.magnitude() / other.magnitude()).toDouble()))

    fun angle(other: Vector2) = Angle.radians(angleRadians(other))

    override fun equals(other: Any?): Boolean {
        return other is Vector2 && other.x == x && other.y == y
    }

    override fun hashCode() = Objects.hash(x, y)

    override fun toString() = "[$x , $y]"
}

/**
 * Find the distance squared from [point] to the line segment [v] .. [w]
 */
fun minimumDistanceSquare(v: Vector2, w: Vector2, point: Vector2): Float {
    // Return minimum distance between line segment vw and point p
    val l2 = v.distanceSquaredTo(w)
    if (l2 == 0f) return point.distanceSquaredTo(v)
    // Consider the line extending the segment, parameterized as v + t (w - v).
    // We find projection of point onto the line.
    // It falls where t = [(p-v) . (w-v)] / |w-v|^2
    // We clamp t from [0,1] to handle points outside the segment vw.
    val t = max(0f, min(1f, (point - v).dot(w - v) / l2))
    val projection = v + (w - v) * t // Projection falls on the segment
    return point.distanceSquaredTo(projection)
}
