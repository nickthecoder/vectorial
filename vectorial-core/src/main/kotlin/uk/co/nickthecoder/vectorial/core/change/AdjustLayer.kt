package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.Layer
import uk.co.nickthecoder.vectorial.core.LayerImpl

interface AdjustLayer : LayerChange {
    val layer: Layer
    val isVisible: Boolean
    val isLocked: Boolean
}

internal class AdjustLayerImpl(
    override val layer: Layer,
    override val isVisible: Boolean,
    override val isLocked: Boolean
) : AdjustLayer, InternalChange {

    override val label: String get() = "Adjust Layer"

    override val diagram: Diagram get() = layer.diagram

    private val wasLocked = layer.isLocked
    private val wasVisible = layer.isVisible

    override fun redo() {
        layer as LayerImpl
        layer.isLocked = isLocked
        layer.isVisible = isVisible
    }

    override fun undo() {
        layer as LayerImpl
        layer.isLocked = wasLocked
        layer.isVisible = wasVisible
    }
}
