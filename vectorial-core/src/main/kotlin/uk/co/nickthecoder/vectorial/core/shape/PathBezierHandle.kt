package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Handle
import uk.co.nickthecoder.vectorial.core.SimpleAttributeHandle
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.Vector2Attribute
import uk.co.nickthecoder.vectorial.core.expression.Vector2Constant

/**
 * A [Handle] for a [BezierPathPart].
 * [attribute] is the attribute that changes when we move the control point.
 * It is usually [BezierPathPart.end], but can alo be [BezierPathPart.start] for the very first part in a Path.
 *
 * [otherAttribute] is an attribute from a different [PathPart]
 *
 * * The next part's `cp1` when [attribute] is `cp2`
 * * The prev part's `cp2` when [attribute] is `cp1`
 *
 * If the node is `symmetric`, then [otherAttribute] is adjusted to be in to opposite direction, and the same
 * distance from [anchor].
 *+
 * If the node is `smooth`, then [otherAttribute] is adjusted to be in the opposite direction, but its distance is unchanged.
 */
interface PathBezierHandle : Handle {
    val pathPart: PathPart
    val anchor: Anchor
    val attribute: Vector2Attribute
    val otherAttribute: Vector2Attribute?
}


internal class PathBezierHandleImpl(
    override val pathPart: PathPartImpl,
    override val anchor: AnchorImpl,
    attribute: Vector2Attribute,
    override val otherAttribute: Vector2Attribute?
) : SimpleAttributeHandle(pathPart.path.shape, attribute), PathBezierHandle {

    override fun moveTo(position: Vector2) {
        super.moveTo(position)

        if (otherAttribute != null && otherAttribute.expression is Vector2Constant) {
            val delta = attribute.eval() - anchor.position()
            when (pathPart.endCornerType) {

                CornerType.SYMMETRIC -> {
                    otherAttribute.set(Vector2Constant(anchor.position() - delta))
                }

                CornerType.SMOOTH -> {
                    val mag = (otherAttribute.eval() - anchor.position()).magnitude()
                    otherAttribute.set(Vector2Constant(anchor.position() - delta.unit() * mag))
                }

                else -> {}
            }
        }
    }

}
