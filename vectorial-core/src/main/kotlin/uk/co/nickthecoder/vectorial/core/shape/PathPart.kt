package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Edge
import uk.co.nickthecoder.vectorial.core.Handle
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.change.ChangeCornerTypeImpl
import uk.co.nickthecoder.vectorial.core.expression.Vector2Attribute

/**
 * Part of a [Path], such as a [LinePathPart], [BezierPathPart].
 * Each [PathPart] has [start] and [end] points, which are joined together.
 * i.e. the [start] of one [PathPart] is the [end] of the previous [PathPart].
 *
 */
sealed interface PathPart {

    val start: Vector2Attribute
    val end: Vector2Attribute

    val endCornerType: CornerType

    /**
     * Find any [Edge]s which are close to [point] (using distance squared : [threshold2]),
     * adding them to the list [into].
     */
    fun findEdges(point: Vector2, threshold2: Float, into: MutableList<Edge>)

    fun previousPathPart(): PathPart?

    fun nextPathPart(): PathPart?

    fun changeCornerType(value: CornerType)
}


abstract class PathPartImpl(

    val path: Path

) : PathPart {

    override var endCornerType: CornerType = CornerType.ANGLED

    open fun invalidateCache() {}

    /**
     * Add points to [into] (not including this part's starting point).
     */
    abstract fun pointsForPolyline(into: MutableList<Vector2>, distanceTolerance: Float, angleTolerance: Double)

    fun handles(into: MutableList<Handle>) {
        into.add(AnchorImpl(this))
    }

    open fun onAttributeChanged(att: Vector2Attribute) {}

    override fun previousPathPart(): PathPartImpl? {
        val myIndex = path.pathParts.indexOf(this)
        return if (myIndex == 0) {
            if (path.isClosed()) {
                path.pathParts.lastOrNull()
            } else {
                null
            }
        } else {
            path.pathParts[myIndex - 1]
        } as? PathPartImpl
    }

    override fun nextPathPart(): PathPartImpl? {
        val myIndex = path.pathParts.indexOf(this)
        return if (myIndex == path.pathParts.size - 1) {
            if (path.isClosed()) {
                path.pathParts.firstOrNull()
            } else {
                null
            }
        } else {
            path.pathParts[myIndex + 1]
        } as? PathPartImpl
    }

    override fun changeCornerType(value: CornerType) {
        if (endCornerType != value) {
            ChangeCornerTypeImpl(this, value).now()
        }
    }

}
