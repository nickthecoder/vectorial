package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.shape.Line
import uk.co.nickthecoder.vectorial.core.Vector2
import kotlin.math.abs

class LineIntersection(
    val a: Line,
    val b: Line
) : CachedVector2Expression(), Vector2Expression {

    init {
        addDependencies(a.start, a.end, b.start, b.end)
    }

    override fun recalculate(): Vector2 {

        // calculate un-normalized direction vectors
        val r = a.direction()
        val s = b.direction()

        val originDist = b.start.eval() - a.start.eval()

        val denominator = r.cross(s)

        // Are the lines parallel?
        if (abs(denominator) < 0.0001f) {
            throw ExpressionException(this, "Lines do not intersect : $a, $b")
        }

        val t = originDist.cross(s) / denominator

        // calculate the intersection point
        return a.start.eval() + r * t
    }

}
