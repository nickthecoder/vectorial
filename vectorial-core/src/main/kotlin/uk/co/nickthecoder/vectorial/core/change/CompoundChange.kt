package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Document

interface CompoundChange<T : Change> : Change

internal abstract class CompoundChangeImpl<T : Change>(

    override val label: String,
    protected vararg val changes: T

) : CompoundChange<T>, InternalChange {

    override val document: Document
        get() = changes[0].document

    override fun redo() {
        for (change in changes) {
            (change as? InternalChange)?.redo()
        }
    }

    override fun undo() {
        for (i in changes.size - 1 downTo 0) {
            (changes[i] as? InternalChange)?.undo()
        }
    }

    override fun canMergeWith(previous: Change): Boolean {
        // We check the javaClass, so that a subclasses is not considered equal.
        // We check using "is" too, for smart casting.
        if (previous.javaClass === this.javaClass && previous is CompoundChangeImpl<*> && previous.changes.size == changes.size) {
            for (i in changes.indices) {
                val myChange = changes[i] as? InternalChange ?: return false
                val otherChange = (previous.changes[i] as? InternalChange) ?: return false
                if (!myChange.canMergeWith(otherChange)) {
                    return false
                }
            }
            return true
        }
        return false
    }

    override fun mergeWith(previous: Change) {
        previous as CompoundChangeImpl<*>
        for (i in changes.indices) {
            val myChange = changes[i] as? InternalChange ?: return
            val otherChange = (previous.changes[i] as? InternalChange) ?: return

            myChange.mergeWith(otherChange)
        }
    }
}
