package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.Vector2

/**
 * Binary functions (i.e. take 2 parameters).
 *
 * Implementing classes should use type parameters` A` and `B`,
 * which are `value` types, such as [Vector2].
 * [AE] and [BE] are the corresponding [Expression] types, such as [Vector2Expression].
 */
interface BinaryFunction<AE : Expression<*>, BE : Expression<*>> {
    val label: String
    val a: AE
    val b: BE
}
