#!/bin/sh

# Generates Boiler-plate code for each `value` type supported by Vectorial.
# It uses a template file `TemplateExpressions.kt.template`, performing a simple search and replace.
# of `Template` to ${type}.
# The generated files are included in git, and no attempt is made to automate calling this
# from the gradle build script.

# To add support for a new `value` type, include the type name here,
# and also create a new interface in ExpressionTypes.kt

for type in "Double" "Float" "Int" "Boolean" "String" "Color" "Angle" "Vector2" "Polar"
do
    echo "Type : $type"
    sed -e 's/Template/'$type'/g' TemplateExpressions.kt.template > ${type}Expressions.kt
done
