package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.change.Change
import uk.co.nickthecoder.vectorial.core.change.History
import java.util.concurrent.atomic.AtomicInteger

/**
 * The top-most item in the model's hierarchy.
 */
interface Document : Parent {

    // TODO Document doesn't have any attributes??

    override val parent get() = this

    override val name: String get() = "document"

    override val items: List<Item> get() = listOf(diagram)

    val history: History
    val diagram: Diagram
    val itemsByName: Map<String, Item>

    /**
     * Is the current state of the document the same as that saved to disk?
     */
    fun isSaved() = history.isDocumentSaved()
    fun generateName(baseName: String): String

    fun addListener(listener: DocumentListener)
    fun removeListener(listener: DocumentListener)
    fun fireChange(change: Change, isUndo: Boolean)

    companion object {
        fun create(): Document = DocumentImpl()
    }
}

internal class DocumentImpl : Document {

    override val history = History(this)

    val listeners = mutableListOf<DocumentListener>() // TODO Use WeakReferences?

    val idGenerators = mutableMapOf<String, AtomicInteger>()

    override val diagram = DiagramImpl(this)

    override val itemsByName = mutableMapOf<String, Item>()


    override fun addListener(listener: DocumentListener) {
        listeners.add(listener)
    }

    override fun removeListener(listener: DocumentListener) {
        listeners.remove(listener)
    }

    override fun fireChange(change: Change, isUndo: Boolean) {
        for (l in listeners) {
            l.documentChanged(this, change, isUndo)
        }
    }

    override fun generateName(baseName: String): String {

        if (baseName.isBlank() || baseName.last().isDigit()) {
            throw IllegalArgumentException("Generated names must be non-blank, and cannot end with a digit")
        }

        fun createGenerator(): AtomicInteger {
            val result = AtomicInteger()
            result.getAndIncrement()
            idGenerators[baseName] = result
            return result
        }

        val generator = idGenerators[baseName] ?: createGenerator()
        return "${baseName}${generator.getAndIncrement()}"
    }
}
