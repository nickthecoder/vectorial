package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.shape.EllipseType
import kotlin.math.*

/*
 * For each `value` type that Vectorial supports, we define an interface for Expressions of that type.
 * e.g. DoubleExpression. This allows the nasty `generic` types to vanish, at the expense of a small amount
 * of boilerplate (which is auto-generated. See DoubleExpressions.kt for details).
 *
 * NOTE, all expressions must use the non-generic version where possible.
 * For example, use DoubleAttribute, and not Attribute<Double>.
 * Note that Attribute<Double> is NOT assignable to DoubleAttribute,
 * so for implementations this is NOT a style issue, it is REQUIRED!
 */

/*
 * NOTE, anywhere you see UnaryXXXFunction or BinaryXXXFunction, the result is an Expression, not a `value`.
 * e.g. `myVector2Expression + otherVector2Expression` returns a Vector2Expression, which evaluates to the sum.
 * The evaluation will change whenever myVector2Expression or otherVector2Expression changes.
 */

/*
 * Boilerplate code which used to reside here is now auto-generated across multiple files.
 * See DoubleExpressions.kt for details.
 */

interface DoubleExpression : Expression<Double> {
    override val type: Class<Double> get() = Double::class.java

    operator fun unaryMinus() = UnaryDoubleFunction("-", this) { -it }

    operator fun plus(other: DoubleExpression) = BinaryDoubleFunction("+", this, other) { va, vb -> va + vb }

    operator fun minus(other: DoubleExpression) = BinaryDoubleFunction("-", this, other) { va, vb -> va - vb }

    operator fun times(other: DoubleExpression) = BinaryDoubleFunction("*", this, other) { va, vb -> va * vb }
    operator fun times(other: IntExpression) = BinaryDoubleFunction("*", this, other) { va, vb -> va * vb }
    operator fun times(other: Int) = UnaryDoubleFunction("*", this) { va -> va * other }

    operator fun div(other: DoubleExpression) = BinaryDoubleFunction("/", this, other) { va, vb -> va / vb }

    operator fun rem(other: DoubleExpression) = BinaryDoubleFunction("%", this, other) { va, vb -> va % vb }

    fun abs() = UnaryDoubleFunction("abs", this) { a -> abs(a) }

    fun sign() = UnaryDoubleFunction("sign", this) { a -> sign(a) }
}

fun Double.div(other: DoubleExpression) = UnaryDoubleFunction("/", other) { a -> this / a }
fun Double.div(other: FloatExpression) = UnaryDoubleFunction("/", other) { a -> this / a }
operator fun Double.div(other: IntExpression) = UnaryDoubleFunction("/", other) { a -> this / a }

interface FloatExpression : Expression<Float> {
    override val type: Class<Float> get() = Float::class.java

    operator fun unaryMinus() = UnaryFloatFunction("-", this) { -it }

    operator fun plus(other: FloatExpression) = BinaryFloatFunction("+", this, other) { va, vb -> va + vb }
    operator fun plus(other: Float) = UnaryFloatFunction("+", this) { va -> va + other }

    operator fun minus(other: FloatExpression) = BinaryFloatFunction("-", this, other) { va, vb -> va - vb }
    operator fun minus(other: Float) = UnaryFloatFunction("-", this) { va -> va - other }

    operator fun times(other: FloatExpression) = BinaryFloatFunction("*", this, other) { va, vb -> va * vb }
    operator fun times(other: Float) = UnaryFloatFunction("*", this) { va -> va * other }
    operator fun times(other: Int) = UnaryFloatFunction("*", this) { va -> va * other }

    operator fun div(other: FloatExpression) = BinaryFloatFunction("/", this, other) { va, vb -> va / vb }
    operator fun div(other: Float) = UnaryFloatFunction("/", this) { va -> va / other }

    operator fun rem(other: FloatExpression) = BinaryFloatFunction("%", this, other) { va, vb -> va % vb }
    operator fun rem(other: Float) = UnaryFloatFunction("%", this) { va -> va % other }

    fun abs() = UnaryFloatFunction("abs", this) { a -> abs(a) }
    fun sign() = UnaryFloatFunction("sign", this) { a -> sign(a) }

    /**
     * A [Vector2Expression] from two [FloatExpression]s.
     * `this` [FloatExpression] is the `y` coordinate, and `x` is the function argument.
     */
    fun x(x: FloatExpression) = BinaryVector2Function("x", this, x) { yValue, xValue -> Vector2(xValue, yValue) }

    /**
     * A [Vector2Expression] from two [FloatExpression]s.
     * `this` [FloatExpression] is the `x` coordinate and `y` is the function argument.
     */
    fun y(y: FloatExpression) = BinaryVector2Function("y", this, y) { xValue, yValue -> Vector2(xValue, yValue) }
}


interface IntExpression : Expression<Int> {
    override val type: Class<Int> get() = Int::class.java

    operator fun unaryMinus() = UnaryIntFunction("-", this) { -it }

    operator fun plus(other: IntExpression) = BinaryIntFunction("+", this, other) { a, b -> a + b }
    operator fun minus(other: IntExpression) = BinaryIntFunction("-", this, other) { a, b -> a - b }
    operator fun times(other: IntExpression) = BinaryIntFunction("*", this, other) { a, b -> a * b }
    operator fun div(other: IntExpression) = BinaryIntFunction("/", this, other) { a, b -> a / b }
    operator fun rem(other: IntExpression) = BinaryIntFunction("%", this, other) { a, b -> a % b }
    fun abs() = UnaryIntFunction("abs", this) { a -> abs(a) }
    fun sign() = UnaryIntFunction("sign", this) { a -> if (a == 0) 0 else if (a < 0) -1 else 1 }

}

interface BooleanExpression : Expression<Boolean> {
    override val type: Class<Boolean> get() = Boolean::class.java

    operator fun not() = UnaryBooleanFunction("!", this) { !it }

    infix fun and(other: BooleanExpression) = BinaryBooleanFunction("and", this, other) { a, b -> a && b }
    infix fun or(other: BooleanExpression) = BinaryBooleanFunction("or", this, other) { a, b -> a || b }
    infix fun xor(other: BooleanExpression) = BinaryBooleanFunction("xor", this, other) { a, b -> a xor b }
}

interface StringExpression : Expression<String> {
    override val type: Class<String> get() = String::class.java

    operator fun plus(other: StringExpression) = BinaryStringFunction("+", this, other) { a, b -> a + b }

}

interface ColorExpression : Expression<Color> {
    override val type: Class<Color> get() = Color::class.java
}

interface AngleExpression : Expression<Angle> {
    override val type: Class<Angle> get() = Angle::class.java

    fun toVector() = UnaryVector2Function("toVector", this) { it.toVector() }

    operator fun plus(other: AngleExpression) = BinaryAngleFunction("+", this, other) { a, b -> a + b }
    operator fun plus(b: Angle) = UnaryAngleFunction("+", this) { a -> a + b }
    operator fun minus(other: AngleExpression) = BinaryAngleFunction("-", this, other) { a, b -> a - b }
    operator fun minus(b: Angle) = UnaryAngleFunction("-", this) { a -> a - b }

    operator fun times(other: DoubleExpression) = BinaryAngleFunction("*", this, other) { a, b -> a * b }
    operator fun times(other: FloatExpression) = BinaryAngleFunction("*", this, other) { a, b -> a * b }
    operator fun times(other: IntExpression) = BinaryAngleFunction("*", this, other) { a, b -> a * b }

    operator fun times(b: Double) = UnaryAngleFunction("*", this) { a -> a * b }
    operator fun times(b: Float) = UnaryAngleFunction("*", this) { a -> a * b }
    operator fun times(b: Int) = UnaryAngleFunction("*", this) { a -> a * b }

    operator fun div(other: DoubleExpression) = BinaryAngleFunction("/", this, other) { a, b -> a / b }
    operator fun div(other: FloatExpression) = BinaryAngleFunction("/", this, other) { a, b -> a / b }
    operator fun div(other: IntExpression) = BinaryAngleFunction("/", this, other) { a, b -> a / b }

    operator fun div(b: Double) = UnaryAngleFunction("/", this) { a -> a / b }
    operator fun div(b: Float) = UnaryAngleFunction("/", this) { a -> a / b }
    operator fun div(b: Int) = UnaryAngleFunction("/", this) { a -> a / b }

    fun perpendicular() = UnaryAngleFunction("perpendicular", this) { a -> a.perpendicular() }
}


interface Vector2Expression : Expression<Vector2> {
    override val type: Class<Vector2> get() = Vector2::class.java

    operator fun unaryMinus() = UnaryVector2Function("-", this) { a -> -a }

    operator fun plus(vector2: Vector2Expression) = BinaryVector2Function("+", this, vector2) { a, b -> a + b }
    operator fun plus(vector2: Vector2) = UnaryVector2Function("+", this) { a -> a + vector2 }

    operator fun minus(vector2: Vector2Expression) = BinaryVector2Function("-", this, vector2) { a, b -> a - b }
    operator fun minus(vector2: Vector2) = UnaryVector2Function("-", this) { a -> a - vector2 }

    operator fun times(vector2: Vector2Expression) = BinaryVector2Function("*", this, vector2) { a, b -> a * b }
    operator fun times(float: FloatExpression) = BinaryVector2Function("*", this, float) { a, b -> a * b }
    operator fun times(polar: PolarExpression) = BinaryVector2Function("*", this, polar) { a, b -> a * b }

    operator fun times(vector2: Vector2) = UnaryVector2Function("*", this) { a -> a * vector2 }
    operator fun times(float: Float) = UnaryVector2Function("*", this) { a -> a * float }
    operator fun times(polar: Polar) = UnaryVector2Function("*", this) { a -> a * polar }

    operator fun div(float: FloatExpression) = BinaryVector2Function("/", this, float) { a, b -> a / b }
    operator fun div(vector2: Vector2Expression) = BinaryVector2Function("/", this, vector2) { a, b -> a / b }

    operator fun div(float: Float) = UnaryVector2Function("/", this) { a -> a / float }
    operator fun div(vector2: Vector2) = UnaryVector2Function("/", this) { a -> a / vector2 }

    fun plusX(float: FloatExpression) = BinaryVector2Function("+x", this, float) { a, b -> Vector2(a.x + b, a.y) }
    fun plusY(float: FloatExpression) = BinaryVector2Function("+y", this, float) { a, b -> Vector2(a.x, a.y + b) }
    fun minusX(float: FloatExpression) = BinaryVector2Function("-x", this, float) { a, b -> Vector2(a.x - b, a.y) }
    fun minusY(float: FloatExpression) = BinaryVector2Function("-y", this, float) { a, b -> Vector2(a.x, a.y - b) }
    fun timesX(float: FloatExpression) = BinaryVector2Function("*x", this, float) { a, b -> Vector2(a.x * b, a.y) }
    fun timeY(float: FloatExpression) = BinaryVector2Function("*y", this, float) { a, b -> Vector2(a.x, a.y * b) }
    fun divX(float: FloatExpression) = BinaryVector2Function("/x", this, float) { a, b -> Vector2(a.x / b, a.y) }
    fun divY(float: FloatExpression) = BinaryVector2Function("/y", this, float) { a, b -> Vector2(a.x, a.y / b) }

    operator fun rem(float: FloatExpression) = BinaryVector2Function("%", this, float) { a, b -> a % b }
    operator fun rem(vector2: Vector2Expression) = BinaryVector2Function("%", this, vector2) { a, b -> a % b }

    operator fun rem(float: Float) = UnaryVector2Function("%", this) { a -> a % float }
    operator fun rem(vector2: Vector2) = UnaryVector2Function("%", this) { a -> a % vector2 }

    fun midpointTo(other: Vector2Expression) = BinaryVector2Function("mid", this, other) { a, b -> (a + b) / 2f }

    fun x(): FloatExpression = UnaryFloatFunction("x", this) { a -> a.x }
    fun y(): FloatExpression = UnaryFloatFunction("y", this) { a -> a.y }

    fun angle() = UnaryAngleFunction("angle", this) { a -> a.angle() }
    fun angle(to: Vector2Expression) = BinaryAngleFunction("angle", this, to) { a, b -> a.angle(b) }
    fun magnitude() = UnaryFloatFunction("magnitude", this) { a -> a.magnitude() }
    fun distanceTo(other: Vector2Expression) =
        BinaryFloatFunction("distanceTo", this, other) { a, b -> a.distanceTo(b) }

    fun perpendicular() = UnaryVector2Function("perpendicular", this) { a -> Vector2(- a.y, a.x) }
    fun unit() = UnaryVector2Function("unit", this) { a -> a.unit() }

    fun hamiltonian() = UnaryFloatFunction("hamiltonian", this) { a -> a.hamiltonian() }

    /**
     * @Return A Vector2Expression evaluating to Vector2(this.x + other.x, this.y)
     */
    fun plusX(other: Vector2Expression) =
        BinaryVector2Function("+X", this, other) { a, b -> Vector2(a.x + b.x, a.y) }

    /**
     * @Return A Vector2Expression evaluating to Vector2(this.x, this.y + other.y)
     */
    fun plusY(other: Vector2Expression) =
        BinaryVector2Function("+Y", this, other) { a, b -> Vector2(a.x, a.y + b.y) }

    /**
     * @Return A Vector2Expression evaluating to Vector2(this.x - other.x, this.y)
     */
    fun minusX(other: Vector2Expression) =
        BinaryVector2Function("+X", this, other) { a, b -> Vector2(a.x - b.x, a.y) }

    /**
     * @Return A Vector2Expression evaluating to Vector2(this.x, this.y - other.y)
     */
    fun minusY(other: Vector2Expression) =
        BinaryVector2Function("+Y", this, other) { a, b -> Vector2(a.x, a.y - b.y) }

    /**
     * @Return A [Vector2SplitExpression] evaluating to Vector2(other.x, this.y)
     * If both x and y expressions are [FloatConstant]s, then the result is a [Vector2Constant].
     * Otherwise, it is a Vector2SplitExpression.
     */
    fun xFrom(other: Vector2Expression) = Vector2SplitExpression.create(other.x(), y())
    fun xFrom(x: FloatExpression) = Vector2SplitExpression.create(x, y())

    /**
     * @Return A [Vector2SplitExpression] evaluating to Vector2(this.x, other.y)
     * If both x and y expressions are [FloatConstant]s, then the result is a [Vector2Constant].
     * Otherwise, it is a [Vector2SplitExpression].
     */
    fun yFrom(other: Vector2Expression) = Vector2SplitExpression.create(x(), other.y())
    fun yFrom(y: FloatExpression) = Vector2SplitExpression.create(x(), y)

    fun rotate(angle: AngleExpression) = BinaryVector2Function("rotate", this, angle) { a, b -> a.rotate(b) }
    fun rotateRadians(radians: DoubleExpression) =
        BinaryVector2Function("rotate", this, radians) { a, b -> a.rotateRadians(b) }
}

fun fromPolar(distance: FloatExpression, angle: AngleExpression): Vector2Expression {
    return BinaryVector2Function("From Polar", distance, angle) { d, a ->
        Vector2(d, 0f).rotate(a)
    }
}

interface PolarExpression : Expression<Polar> {
    override val type: Class<Polar> get() = Polar::class.java
}

interface EllipseTypeExpression : Expression<EllipseType> {
    override val type: Class<EllipseType> get() = EllipseType::class.java
}
