package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Document
import uk.co.nickthecoder.vectorial.core.shape.Shape

interface CompoundShapeChange : CompoundChange<ShapeChange>, ShapeChange

internal open class CompoundShapeChangeImpl(label: String, vararg changes: ShapeChange) :
    CompoundChangeImpl<ShapeChange>(label, *changes), CompoundShapeChange, InternalChange {

    override val document: Document
        get() = diagram.document

    override val shape: Shape
        get() = changes[0].shape
}
