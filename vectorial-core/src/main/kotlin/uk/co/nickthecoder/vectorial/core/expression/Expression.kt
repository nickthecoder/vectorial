package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.shape.Line
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.debug


/**
 * Most [Expression]s in `Vectorial` can depend on other [Expression]s.
 * For example, a [Line] has a [Line.start] and [Line.end] vectors.
 *
 * These may be simple [Constant]s, or instead are calculated from other [Expression]s.
 * For example, the start may be tied to the center of a circle, and the end tied to a point on
 * the circle's circumference.
 *
 * In this case, we cannot move the line, but when we move the circle, the line will automatically move.
 *
 * [V] is the type of the `value` type, returned by [eval], such as [Vector2].
 * We also store type [V] in [type], so that we can inspect the type at runtime, despite Java's `Type Erasure`.
 *
 * Note, it is a PITA to use the generic parameter [V] everywhere, so instead, we
 * have an interface for each type, such as [Vector2Expression].
 * There is a downside to this : `Expression<Vector2>` cannot be safely cast to a `Vector2Expression`.
 * This leads to extra boilerplate. This boilerplate is auto-generated though.
 * See [DoubleExpression]
 *
 * However, this design choice has a big advantage, we can declare methods such as
 * `operator fun plus( other : Vector2Expression )` on `Vector2Expression`.
 *
 * This wouldn't be possible otherwise, due to `type erasure`.
 * (e.g. At the byte code level `Expression<Int>.unaryMinus()` would have the same signature as
 * `Expression<Float>.unaryMinus()`)
 *
 * Many [Expression] subclasses/sub-interfaces also introduce a type parameter `E`,
 * which denotes the type of the [Expression], such as [Vector2Expression].
 *
 */
interface Expression<V : Any> {

    val type: Class<V>

    /**
     * @return The value held/calculated by this [Expression].
     * @throws ExpressionException
     */
    fun eval(): V

    /**
     * Ca
     */
    fun dirty()

    /**
     * Calls [eval] and [dirty] with debug logging turned on, and turns logging off afterwards.
     * NOTE. Only [CachedExpression] support logging,
     * but as all complex [Expression]s extend it, that it usually sufficient.
     */
    fun debugEvalDirty(label: String) {
        logExpressions(label) {
            eval()
            dirty()
            eval()
        }
    }

    fun addDependencies(vararg dependencies: Expression<*>) {
        for (dependency in dependencies) {
            if (dependency is ChangeableExpression<*>) {
                dependency.addDependent(this)
            }
        }
    }

    fun removeDependencies(vararg dependencies: Expression<*>) {
        for (dependency in dependencies) {
            if (dependency is ChangeableExpression<*>) {
                dependency.removeDependent(this)
            }
        }
    }


    /**
     * Return basic information.
     *
     * If you override this :
     * Do NOT use [eval]. Do not access any other [Expression]s.
     */
    fun debugInfo() = "${javaClass.simpleName} (${type.simpleName}) #${hashCode()}"

    fun eq(other: Expression<*>) = BinaryBooleanFunction("eq", this, other) { a, b -> a == b }
    fun ne(other: Expression<*>) = BinaryBooleanFunction("eq", this, other) { a, b -> a != b }


    companion object {
        var log = false
    }

}

/**
 * All [Expression]s except [Constant] should implement this interface.
 * `Changeable` in this context means that the result of `eval` can change over time.
 * Either by changes to themselves, or because another [Expression] that they depend on changes.
 *
 * [ChangeableExpression]s need to keep track of `other` [Expression]s which depend on them, so
 * that when the [ChangeableExpression] changes, it can inform the `other` [Expression]s that they are
 * [dirty], i.e. if they have cached the results of [eval], then the cache is no longer valid.
 * The `other` Expressions must then recurse, informing all that those are used by, that they are [dirty].
 */
interface ChangeableExpression<V : Any> : Expression<V> {

    /**
     * After calling this, whenever this [ChangeableExpression] is made [dirty], then [dependent] will also be
     * made [dirty].
     */
    fun addDependent(dependent: Expression<*>)

    /**
     * After calling this, [dependent] will no longer be informed when this [ChangeableExpression] is made [dirty].
     */
    fun removeDependent(dependent: Expression<*>)
}


fun logExpressions(label: String, block: () -> Unit) {
    try {
        debug("")
        debug("BEGIN $label")
        Expression.log = true
        block()
    } catch (e: Exception) {
        debug("FAILED : $e")
        throw e
    } finally {
        Expression.log = false
        debug("END")
        debug("")
    }
}

fun Expression<*>.evalToString() = try {
    eval().toString()
} catch (e: Exception) {
    e.message
} ?: ""
