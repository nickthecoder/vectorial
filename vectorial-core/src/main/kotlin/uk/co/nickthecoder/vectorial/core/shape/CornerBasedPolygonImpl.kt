package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Handle
import uk.co.nickthecoder.vectorial.core.SimpleAttributeHandle
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.change.ChangeGeometryImpl
import uk.co.nickthecoder.vectorial.core.expression.Vector2Constant
import uk.co.nickthecoder.vectorial.core.expression.Vector2Expression

internal class CornerBasedPolygonImpl(

    sides: Int,
    corner1Expression: Vector2Expression,
    corner2Expression: Vector2Expression

) : CornerBasedPolygonOrStarImpl(sides, corner1Expression, corner2Expression), CornerBasedPolygon {

    constructor(sides: Int, corner1: Vector2, corner2: Vector2) :
        this(sides, Vector2Constant(corner1), Vector2Constant(corner2))

    override val polygonType: PolygonType get() = PolygonType.POLYGON

    override val centered: Centered get() = Centered.CORNER_BASED

    override fun handles(): List<Handle> {
        var handles = cachedHandles
        if (handles == null) {
            handles = listOf(
                SimpleAttributeHandle(shape, corner1),
                SimpleAttributeHandle(shape, corner2)
            )
            cachedHandles = handles
        }
        return handles
    }


    override fun calculateVertices(): List<Vector2Expression> {
        val vertices = mutableListOf<Vector2Expression>()
        vertices.add(corner1)
        vertices.add(corner2)
        for (i in 2 until sides.eval()) {
            vertices.add(center + delta.rotateRadians(angle * i))
        }
        return vertices
    }


    override fun changeCentered(centered: Centered) {
        if (centered == Centered.CENTERED) {
            val newGeometry =
                CenteredPolygonImpl(sides.eval(), center.eval(), corner1.eval() - center.eval())
            ChangeGeometryImpl("Make Centered", shape, newGeometry).now()
        }
    }

    override fun changePolygonType(type: PolygonType) {
        if (type == PolygonType.STAR) {
            val newGeometry = CornerBasedStarImpl(sides.eval(), corner1.eval(), corner2.eval())
            ChangeGeometryImpl("Change to star", shape, newGeometry).now()
        }
    }

}
