package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.change.*
import uk.co.nickthecoder.vectorial.core.change.BreakConnectionImpl
import uk.co.nickthecoder.vectorial.core.change.MakeConnectionImpl
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.shape.CenteredRectangle
import uk.co.nickthecoder.vectorial.core.shape.Shape

/**
 * A [Handle] which changes an [Attribute], which does not have the same value as the [Handle]'s
 * [Handle.position], but the two are related to each other.
 *
 * For example, [CenteredRectangle] has a `size` [Vector2Attribute], which we need to control via a
 * [Handle], which is positioned at `center` + `size` / 2.
 *
 */
abstract class DerivedAttributeHandle<V : Any, E : Expression<V>>(
    override val shape: Shape,
    override val attribute: Attribute<V, E>,
    val convertor: PointConverter<V>,
    vararg additionalDependencies: Expression<*>

) : AttributeHandle<V, E> {

    private val alsoDependsOn = additionalDependencies

    /**
     * Creates an instance of [Connection], such as [Vector2Connection] etc.
     *
     * @return null if a connection cannot be made, otherwise a [Connection] using the [connector].
     */
    protected abstract fun createConnection(connector: Connector): E?


    override fun position() = convertor.toVector2(attribute.eval())

    override fun makeConnection(shape: Shape, connector: Connector): Boolean {
        val connection = createConnection(connector) ?: return false
        // TODO BUG?!? Mutating state outside a Change.
        connection.addDependencies(* alsoDependsOn)
        val change = MakeConnectionImpl(shape, attribute, connection)
        change.now()
        return true
    }

    override fun breakConnection(shape: Shape, to: Vector2) {
        @Suppress("UNCHECKED_CAST")
        val currentConnection: Connection<V> = attribute.expression as Connection<V>
        val constant: Constant<V> = currentConnection.createConstant(currentConnection.eval())
        @Suppress("UNCHECKED_CAST")
        BreakConnectionImpl(shape, attribute, constant as E).now()
    }

    override fun moveTo(position: Vector2) {
        val exp = attribute.expression

        @Suppress("UNCHECKED_CAST")
        val newExp = convertor.fromVector2(position).toConstant() as E

        val change = when (exp) {
            is Connection<*> -> MoveConnectionImpl(shape, attribute.key, exp, position)
            is Constant<*> -> SetShapeAttributeImpl(shape, attribute, newExp)
            else -> BreakConnectionImpl(shape, attribute, newExp)
        }
        change.now()
    }

    override fun toString() = "DerivedAttributeControlPoint for $attribute"

}
