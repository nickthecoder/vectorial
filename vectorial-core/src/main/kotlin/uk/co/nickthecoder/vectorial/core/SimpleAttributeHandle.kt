package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.change.*
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.shape.Shape

/**
 * A [Handle] whose position is given by a [Vector2Attribute].
 *
 * Note, if the attribute is a *relative* value, such as an offset, or a size,
 * then [DerivedAttributeHandle] must be used instead.
 * [DerivedAttributeHandle] must also be used when the attribute is not a [Vector2],
 * for example when a [Handle] controls an [Angle].
 */
open class SimpleAttributeHandle(
    override val shape: Shape,
    override val attribute: Vector2Attribute

) : AttributeHandle<Vector2, Vector2Expression> {

    override fun position() = attribute.eval()

    override fun makeConnection(shape: Shape, connector: Connector) : Boolean {
        MakeConnectionImpl(shape, attribute, Vector2ConnectionImpl(connector, NoOpPointConverter)).now()
        return true
    }

    override fun breakConnection(shape: Shape, to: Vector2) {
        val currentConnection: Vector2Connection = attribute.expression as Vector2Connection
        val constant: Vector2Constant = currentConnection.createConstant(currentConnection.eval())
        BreakConnectionImpl(shape, attribute, constant).now()
    }

    override fun moveTo(position: Vector2) {
        val change = when (val exp = attribute.expression) {
            is Connection<*> -> MoveConnectionImpl(shape, attribute.key, exp, position)
            is Constant<*> -> SetShapeAttributeImpl(shape, attribute, Vector2Constant(position))
            else -> BreakConnectionImpl(shape, attribute, Vector2Constant(position))
        }
        change.now()
    }

    override fun toString() = "SimpleAttributeControlPoint for $attribute"
}
