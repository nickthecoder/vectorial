package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.Change
import uk.co.nickthecoder.vectorial.core.change.ControlPointsChange
import uk.co.nickthecoder.vectorial.core.change.InternalChange
import uk.co.nickthecoder.vectorial.core.change.ShapeChange
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.expression.AttributesImpl


interface Ellipse : Geometry {

    val ellipseType: EllipseTypeExpression

    val center: Vector2Expression
    val radius: Vector2Expression

    val radiusX: FloatExpression
    val radiusY: FloatExpression

    // NOTE These angles are not true angles when elliptical.
    // They are angles assuming the shape is a circle, but will get distorted for an ellipse.
    val fromAngle: AngleExpression
    val toAngle: AngleExpression

    val end: Vector2Expression

    /**
     * Returns the [Change] required to adjust this ellipse, or null if this ellipse doesn't need to be changed.
     */
    fun changeEllipseType(newType: EllipseType): ShapeChange?

    companion object {
        fun createCenterEdge(center: Vector2Expression, edgePoint: Vector2Expression): Ellipse =
            CenterCircle(center, edgePoint)

        fun createCenterEdge(center: Vector2, edgePoint: Vector2): Ellipse =
            CenterCircle(center, edgePoint)

        fun createCenterRadius(center: Vector2Expression, radius: FloatExpression): Ellipse =
            CenterRadiusEllipse(center, radius)

        fun createCenterRadius(center: Vector2, radius: Float): Ellipse =
            CenterRadiusEllipse(center, radius)
    }

}

internal abstract class EllipseImpl : Ellipse, MutableGeometry {

    override val baseName: String get() = "ellipse"

    protected var cachedHandles: List<Handle>? = null

    override fun isClosed() = ellipseType.eval() != EllipseType.ARC

    protected fun pointForAngle(radians: Double): Vector2 {
        val center = center.eval()
        val radiusX = radiusX.eval()
        val radiusY = radiusY.eval()
        val cos = Math.cos(radians)
        val sin = Math.sin(radians)
        return Vector2(center.x + cos * radiusX, center.y + sin * radiusY)
    }

    abstract fun fromAngleRadiansForBuildPoints(): Double
    abstract fun toAngleRadiansForBuildPoints(): Double

    override fun toPolyline(thickness: Float, distanceTolerance: Float, angleTolerance: Double): Polyline {
        val into = mutableListOf<Vector2>()

        val ellipseType = ellipseType.eval()
        val fromAngleRadians = fromAngleRadiansForBuildPoints()
        var toAngleRadians = toAngleRadiansForBuildPoints()
        if (toAngleRadians <= fromAngleRadians) toAngleRadians += Angle.TAU

        val sides = 10 // TODO Base this off of tolerances and (toAngle - fromAngle)
        val deltaAngleRadians = (toAngleRadians - fromAngleRadians) / sides

        val center = center.eval()
        // We have not included `size`, because for `WHOLE` and `ARC`, this is the final point,
        // and I want this to be EXACTLY `end`, and doing so here would introduce rounding errors.
        for (side in 0 until sides) {
            val radians = fromAngleRadians + side * deltaAngleRadians
            into.add(pointForAngle(radians))
        }
        when (ellipseType) {
            EllipseType.CHORD -> {
                into.add(pointForAngle(toAngleRadians))
            }

            EllipseType.SLICE -> {
                into.add(pointForAngle(toAngleRadians))
                into.add(center)
            }

            EllipseType.WHOLE -> {}
            EllipseType.ARC -> {}

        }
        // By including `end` here, we guarantee buildPoints DOES end where it should,
        // without rounding errors.
        into.add(end.eval())

        return Polyline(into, thickness, isClosed())
    }

    internal inner class AngleHandle(override val name: String) :
        DerivedAttributeHandle<Angle, AngleExpression>(
            shape,
            attributes[name] as AngleAttribute,
            object : PointConverter<Angle> {
                override fun fromVector2(position: Vector2) = (position - center.eval()).angle()

                override fun toVector2(value: Angle): Vector2 {
                    return pointForAngle(value.radians)
                }
            },
            center
        ) {
        override fun isFixed() = !isFree()
        override fun isConnectable() = false
        override fun createConnection(connector: Connector) = null
    }

    inner class PointForAngle(val angle: AngleExpression) : CachedVector2Expression() {
        init {
            addDependencies(center, radius, angle)
        }

        override fun recalculate(): Vector2 {
            return pointForAngle(angle.eval().radians)
        }
    }

    // TODO We should LISTEN to ellipseType, and invalidate the cache whenever it changes,
    // and also change the
    // This class can then be deleted, and replaced with a simple SetShapeAttribute.
    override fun changeEllipseType(newType: EllipseType): ShapeChange? {
        if (newType == ellipseType.eval()) return null
        return object : ControlPointsChange, InternalChange {
            override val shape: GeometryShape get() = this@EllipseImpl.shape
            override val label: String get() = "Ellipse type ; $newType"
            val oldType = ellipseType.eval()

            override fun redo() {
                (ellipseType as EllipseTypeAttributeImpl).setFromHistory(EllipseTypeConstant(newType))
                cachedHandles = null
                (shape as GeometryShapeImpl).invalidateCache()
            }

            override fun undo() {
                (ellipseType as EllipseTypeAttributeImpl).setFromHistory(EllipseTypeConstant(oldType))
                cachedHandles = null
                (shape as GeometryShapeImpl).invalidateCache()
            }
        }
    }

    override fun toPath(): Path {
        TODO("Not implemented")
    }

}

/**
 * A Circle defined by its [center] and a [start] point on the circumference.
 *
 */
internal class CenterCircle(

    centerExpression: Vector2Expression,
    startExpression: Vector2Expression

) : EllipseImpl() {

    constructor(center: Vector2, edgePoint: Vector2) :
            this(Vector2Constant(center), Vector2Constant(edgePoint))

    override lateinit var shape: GeometryShape

    // Attributes
    override val attributes = AttributesImpl(this)
    override val ellipseType = createAttribute(attributes, "ellipseType", EllipseTypeConstant(EllipseType.WHOLE))
    override val start = createAttribute(attributes, "start", startExpression)
    override val center = createAttribute(attributes, "center", centerExpression)
    override val toAngle = createAttribute(attributes, "toAngle", AngleConstant(Angle.ZERO_ANGLE))

    // Expressions
    override val radiusX = (start - center).magnitude()
    override val radiusY = radiusX
    override val radius = radiusX.y(radiusX)

    override var fromAngle: AngleExpression = (start - center).angle()
    override val end = Vector2If(
        condition = ellipseType.eq(EllipseTypeConstant(EllipseType.ARC)),
        then = PointForAngle(toAngle),
        els = start
    )
    // End of expressions

    override fun fromAngleRadiansForBuildPoints() = fromAngle.eval().radians
    override fun toAngleRadiansForBuildPoints() =
        if (ellipseType.eval() == EllipseType.WHOLE) fromAngle.eval().radians + Angle.TAU else toAngle.eval().radians

    override fun movableAttributes() = listOf(center, start)

    override fun handles(): List<Handle> {
        cachedHandles?.let { return it }
        if (ellipseType.eval() == EllipseType.WHOLE) {
            cachedHandles = listOf(
                SimpleAttributeHandle(shape, center),
                SimpleAttributeHandle(shape, start)
            )
        } else {
            cachedHandles = listOf(
                SimpleAttributeHandle(shape, center),
                SimpleAttributeHandle(shape, start),
                AngleHandle("toAngle")
            )
        }
        return cachedHandles!!
    }

}

/**
 * An axis-aligned [Ellipse] defined by its [center], and radius as two floats [radiusX] and [radiusY].
 *
 * If no [radiusY] expression is given in the constructor, then [radiusY] is connected to [radiusX], i.e.
 * we have a circle (until the [radiusY] expression is broken away from [radiusX]).
 */
internal class CenterRadiusEllipse(

    centerInit: Vector2Expression,
    radiusXInit: FloatExpression,
    radiusYInit: FloatExpression = radiusXInit

) : EllipseImpl() {

    constructor(center: Vector2, radiusX: Float, radiusY: Float = radiusX) :
            this(Vector2Constant(center), FloatConstant(radiusX), FloatConstant(radiusY))

    constructor(center: Vector2, radius: Float) :
            this(Vector2Constant(center), FloatConstant(radius))

    override lateinit var shape: GeometryShape

    // Attributes
    override val attributes = AttributesImpl(this)

    override val ellipseType = createAttribute(attributes, "ellipseType", EllipseTypeConstant(EllipseType.WHOLE))
    override val center = createAttribute(attributes, "center", centerInit)
    override val radiusX = createAttribute(attributes, "radiusX", radiusXInit)
    override val radiusY = createAttribute(
        attributes, "radiusY", if (radiusYInit === radiusXInit) radiusX else radiusYInit
    )
    override val fromAngle = createAttribute(attributes, "fromAngle", AngleConstant(Angle.degrees(0.0)))
    // -15.0 Is an arbitrary non-zero angle, so that when we start dragging non-WHOLE ellipse, it doesn't look "weird"
    // If it were zero, it would *look* like a WHOLE ellipse, but with an extra radius line.
    override val toAngle = createAttribute(attributes, "toAngle", AngleConstant(Angle.degrees(-15.0)))

    // Expressions

    override val radius = radiusX.y(radiusY)
    override val start = Vector2If(
        condition = ellipseType.eq(EllipseTypeConstant(EllipseType.WHOLE)),
        then = PointForAngle(AngleConstant(Angle.ZERO_ANGLE)),
        els = PointForAngle(fromAngle)
    )
    override val end = Vector2If(
        condition = ellipseType.eq(EllipseTypeConstant(EllipseType.ARC)),
        then = PointForAngle(toAngle),
        els = start
    )

    // End of expressions

    override fun fromAngleRadiansForBuildPoints() =
        if (ellipseType.eval() == EllipseType.WHOLE) 0.0 else fromAngle.eval().radians

    override fun toAngleRadiansForBuildPoints() =
        if (ellipseType.eval() == EllipseType.WHOLE) Angle.TAU else toAngle.eval().radians

    override fun movableAttributes() = listOf(center)

    override fun handles(): List<Handle> {
        cachedHandles?.let { return it }

        cachedHandles = if (ellipseType.eval() == EllipseType.WHOLE) {
            listOf(
                SimpleAttributeHandle(shape, center),
                RadiusXHandle(shape, center, radiusX),
                RadiusYHandle(shape, center, radiusY)
            )
        } else {
            listOf(
                SimpleAttributeHandle(shape, center),
                RadiusXHandle(shape, center, radiusX),
                RadiusYHandle(shape, center, radiusY),
                AngleHandle("fromAngle"),
                AngleHandle("toAngle")
            )
        }
        return cachedHandles!!
    }

}

internal class RadiusXHandle(
    shape: Shape,
    center: Vector2Attribute,
    radius: FloatAttribute
) : DerivedAttributeHandle<Float, FloatExpression>(
    shape,
    radius,
    object : PointConverter<Float> {
        override fun fromVector2(position: Vector2): Float {
            return (center.eval() - position).magnitude()
        }

        override fun toVector2(value: Float): Vector2 {
            return center.eval() + Vector2(-value, 0f)
        }
    },
    center
) {
    override val name: String get() = "radiusX"
    override fun isFixed() = !isFree()
    override fun isConnectable() = false
    override fun createConnection(connector: Connector) = null
}

internal class RadiusYHandle(
    shape: Shape,
    center: Vector2Attribute,
    radius: FloatAttribute
) : DerivedAttributeHandle<Float, FloatExpression>(
    shape,
    radius,
    object : PointConverter<Float> {
        override fun fromVector2(position: Vector2): Float {
            return (center.eval() - position).magnitude()
        }

        override fun toVector2(value: Float): Vector2 {
            return center.eval() + Vector2(0f, -value)
        }
    },
    center
) {
    override val name: String get() = "radiusY"
    override fun isFixed() = !isFree()
    override fun isConnectable() = false
    override fun createConnection(connector: Connector) = null
}


