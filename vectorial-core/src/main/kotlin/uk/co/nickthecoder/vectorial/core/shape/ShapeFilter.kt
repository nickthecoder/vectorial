package uk.co.nickthecoder.vectorial.core.shape

interface ShapeFilter {
    fun acceptShape(shape: Shape): Boolean
}

object AnyShapeFilter : ShapeFilter {
    override fun acceptShape(shape: Shape) = true
}
