package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Handle
import uk.co.nickthecoder.vectorial.core.SimpleAttributeHandle
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.change.ChangeGeometryImpl
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.expression.AttributesImpl
import uk.co.nickthecoder.vectorial.core.expression.createAttribute

/**
 * A [Diamond] (aka Rhombus) is defined by an axis-aligned box with corners [from] and [to].
 *
 * [size] will only have positive `x` and `y` values, if [from] is in the top left
 * (and [to] is in the bottom right).
 *
 * Note that the corners of the [Diamond] do not touch either [from] or [to].
 */
internal class CornerBasedDiamond(
    fromExpression: Vector2Expression,
    toExpression: Vector2Expression
) : QuadrilateralImpl(), Diamond, MutableGeometry {

    constructor(from: Vector2, to: Vector2) :
            this(Vector2Constant(from), Vector2Constant(to))

    override lateinit var shape: GeometryShape

    override val centered: Centered get() = Centered.CORNER_BASED

    // Attributes
    override val attributes = AttributesImpl(this)
    val from = createAttribute(attributes, "from", fromExpression)
    val to = createAttribute(attributes, "to", toExpression)

    override val center = (from + to) / 2f
    override val size = to - from

    // Expressions
    override val corner0 = from.xFrom(center)
    override val corner1 = to.yFrom(center)
    override val corner2 = to.xFrom(center)
    override val corner3 = from.yFrom(center)

    // Continuous Geometry expressions
    override val start get() = corner0

    // End of expressions

    private val cachedControlPoints by lazy {
        listOf(
            SimpleAttributeHandle(shape, from),
            SimpleAttributeHandle(shape, to),
        )
    }

    init {
        addCornerAttributes()
    }

    override fun handles(): List<Handle> = cachedControlPoints

    override fun changeCentered(centered: Centered) {
        if (centered == Centered.CENTERED) {
            ChangeGeometryImpl(
                "Convert to a center-based Diamond",
                shape,
                CenteredDiamond(Vector2Constant(center.eval()), Vector2Constant(size.eval()))
            ).now()
        }
    }

    override fun changeQuadrilateralType(type: QuadrilateralType) {

        val c0 = center.eval() - size.eval() / 2f
        val c2 = center.eval() + size.eval() / 2f

        when (type) {

            QuadrilateralType.DIAMOND -> {}

            QuadrilateralType.RECTANGLE -> {
                ChangeGeometryImpl(
                    "Convert to a rectangle",
                    shape, CornerBasedRectangle(c0, c2)
                ).now()
            }

            QuadrilateralType.PARALLELOGRAM -> {
                ChangeGeometryImpl(
                    "Convert to a parallelogram",
                    shape, CornerBasedParallelogram(c0, c2)
                ).now()
            }
        }
    }

    override fun movableAttributes() = listOf(from, to)

    override fun toString() = "Rectangle center ${center.eval()} size ${size.eval()}}"

}
