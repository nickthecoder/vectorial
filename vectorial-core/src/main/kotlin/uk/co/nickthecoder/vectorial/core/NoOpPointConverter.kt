package uk.co.nickthecoder.vectorial.core

object NoOpPointConverter : PointConverter<Vector2> {
    override fun fromVector2(position: Vector2) = position
    override fun toVector2(value: Vector2) = value
}
