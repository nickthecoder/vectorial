package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.AddBezierPartImpl
import uk.co.nickthecoder.vectorial.core.change.AddLinePartImpl
import uk.co.nickthecoder.vectorial.core.change.ClosePathImpl
import uk.co.nickthecoder.vectorial.core.expression.*

/**
 * A [Path] is composed of a series of [PathPart]s (such as a [LinePathPart] or [BezierPathPart]).
 * The start of each [PathPart] is at the end of the previous one, thus forming a single continuous path.
 *
 * A degenerate path, with no [pathParts] is valid.
 *
 * A [Path] may be open or closed. For a closed path, `end` === `start`. However, an `open` path _may_
 * have its start at the same point as the end without being closed (i.e. `end` == `start`, but `end` !== `start`).
 * In this case, the start and end of the [Path] will use the shape's [CapStyle],
 * whereas a closed path will not have caps.
 *
 * ## Construction
 *
 * See [buildPath], [PathBuilder] or [create].
 *
 * ## Limitations
 *
 * ### Holes
 *
 * Unlike SVG, Vectorial's [Path] cannot have `holes`, or be comprised of `compound` paths.
 * i.e. SVG's `move` command can only appear at the start of a Vectorial [Path].
 *
 * If you wish to draw shapes with holes, or to combine paths in other ways, then create 2 or more [Shape]s,
 * each with a single [Path], and combine them using any of boolean clipping operations :
 * `intersection`, `union`, `difference` or `xor`. (At time of writing, this hasn't been implemented).
 *
 * This gives Vectorial greater flexibility, and ease of use, because non-path shapes can use clipping operations
 * without being converted to [Path]s. (This limitation of SVG is a major bugbear for me!)
 *
 * ### Self-Intersection
 *
 * Filled [Path] should NOT be self-intersecting. This constraint isn't enforced hy the UI.
 * A self-intersecting path will not appear filled (regardless of the fill-style).
 *
 * ## Attribute Names
 *
 * [attributes] contain keys in the form `pointN`, where `N` is an integer.
 * These integers are NOT sequential.
 * When a point is deleted a hole in the sequence remains.
 * When a point is inserted, the inserted point's integer is larger than all other points.
 * e.g. If we have line segments (_point0_, _point1_, _point2_), and then insert a new node after _point1_,
 * the attributes will be : _point0_, _point1_, _point3_, _point2_.
 *
 * In general, you should NOT make any assumptions about the attribute names.
 */
interface Path : Geometry {

    override val baseName: String get() = "path"

    val pathParts: List<PathPart>

    val end: Vector2Expression

    fun endPoint(): Vector2

    fun appendLineSegment(point: Vector2): LinePathPart
    fun appendBezierCurve(cp1: Vector2, cp2: Vector2, point: Vector2): BezierPathPart

    /**
     * Adds a line segment (or bezier curve) from the end of the path to the start, forming a closed path.
     *
     * @param withCurve If true, and the start or end parts are bezier curves, then close using a bezier curve.
     * Otherwise, close with a line segment.
     */
    fun closePath(withCurve: Boolean = false)

    override fun toPath() = this

    companion object {

        /**
         * Creates a [Path] with no [PathPart]s.
         * Use [Path.appendLineSegment] etc. to grow the [Path].
         *
         * Alternatively, use [buildPath] or [PathBuilder].
         */
        fun create(start: Vector2): Path = PathImpl(start)

    }

}

internal class PathImpl(

    start: Vector2Expression

) : Path, MutableGeometry {

    constructor(start: Vector2) : this(Vector2Constant(start))


    override lateinit var shape: GeometryShape

    override val pathParts = mutableListOf<PathPartImpl>()

    // Attributes
    override val attributes = AttributesImpl(this)

    override val start: Vector2Attribute = createAttribute(attributes, "point0", start)

    override val end: Vector2Attribute get() = pathParts.lastOrNull()?.end ?: start

    internal var pointCounter = 0

    private var cachedHandles: List<Handle>? = null

    // init

    init {
        pathParts.add(StartPathPartImpl(this, this.start))
        attributes.addListener(object : AttributesListener {
            override fun attributeChanged(key: String) {
                val value = attributes[key]
                if (value is Vector2Attribute) {
                    for (part in pathParts) {
                        part.onAttributeChanged(value)
                    }
                }
            }
        })
    }

    override fun endPoint(): Vector2 = end.eval()

    override fun handles(): List<Handle> = cachedHandles ?: cacheControlPoints()

    fun invalidateCache() {
        cachedHandles = null
        for (part in pathParts) {
            part.invalidateCache()
        }
        (shape as GeometryShapeImpl).invalidateCache()
    }

    private fun cacheControlPoints(): List<Handle> {
        val result = mutableListOf<Handle>()
        for (path in pathParts) {
            path.handles(result)
        }
        return result
    }


    override fun toPolyline(thickness: Float, distanceTolerance: Float, angleTolerance: Double): Polyline {
        val points = mutableListOf<Vector2>()
        for (part in pathParts) {
            part.pointsForPolyline(points, distanceTolerance, angleTolerance)
        }
        return Polyline(points, thickness, isClosed())
    }

    override fun touchingEdges(point: Vector2, threshold: Float, threshold2: Float, into: MutableList<Edge>) {
        for (part in pathParts) {
            part.findEdges(point, threshold2, into)
        }
    }

    override fun isClosed() = pathParts.firstOrNull() !is StartPathPart

    override fun closePath(withCurve: Boolean) {
        if (! isClosed()) {
            ClosePathImpl(this, withCurve).now()
        }
    }

    override fun movableAttributes() = attributes.values().filterIsInstance<Vector2Attribute>()

    override fun appendLineSegment(point: Vector2): LinePathPart {
        AddLinePartImpl(this, point).now()
        cachedHandles = null
        return pathParts.last() as LinePathPart
    }

    override fun appendBezierCurve(cp1: Vector2, cp2: Vector2, point: Vector2): BezierPathPart {
        AddBezierPartImpl(this, cp1, cp2, point).now()
        cachedHandles = null
        return pathParts.last() as BezierPathPart
    }

    override fun toString() = "Path start=${start.eval()} part count ${pathParts.size}"
}
