package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Edge
import uk.co.nickthecoder.vectorial.core.ConnectorType
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.minimumDistanceSquare

interface LineEdge : Edge {
    val start: Vector2Expression
    val end: Vector2Expression
}

internal class LineEdgeImpl(

    override val shape: Shape,
    override val start: Vector2Expression,
    override val end: Vector2Expression

) : LineEdge {

    override val points: List<Vector2>
        get() = listOf(start.eval(), end.eval())

    override val supportedConnectorTypes: List<ConnectorType> get() = SUPPORTED_CONNECTION_TYPES


    override fun isNear(point: Vector2, threshold2: Float): Boolean {
        return minimumDistanceSquare(start.eval(), end.eval(), point) < threshold2
    }

    override fun createConnector(point: Vector2, connectorType: ConnectorType, isBounded : Boolean): ConnectorToEdge {
        return when (connectorType) {
            ConnectorType.RATIO -> RatioAlongLineImpl(this, 0.5f, isBounded)
            ConnectorType.FROM_START -> DistanceAlongLineImpl(this, 0f, isFromStart = true, isBounded)
            ConnectorType.FROM_END -> DistanceAlongLineImpl(this, 0f, isFromStart = false, isBounded)
            else -> throw IllegalArgumentException("Expected RATIO, FROM_START or FROM_END, but found $connectorType")
        }.apply {
            performMoveTowards(point)
        }
    }

    private fun ratio(point: Vector2): Float {
        val startV = start.eval()
        val endV = end.eval()
        val diff = endV - startV
        val length = diff.magnitude()
        return if (length == 0f) {
            0.5f
        } else {
            val lengthSquared = length * length
            ((endV - point).dot(diff)) / lengthSquared
        }
    }

    override fun toString() = "LineEdge ${start.eval()} .. ${end.eval()}"

    companion object {
        private val SUPPORTED_CONNECTION_TYPES =
            listOf(ConnectorType.RATIO, ConnectorType.FROM_START, ConnectorType.FROM_END)
    }

}
