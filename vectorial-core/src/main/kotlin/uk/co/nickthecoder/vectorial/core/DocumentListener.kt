package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.change.Change

interface DocumentListener {
    fun documentChanged(document: Document, change: Change, isUndo: Boolean)
}
