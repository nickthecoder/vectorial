package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Item
import uk.co.nickthecoder.vectorial.core.expression.Expression
import uk.co.nickthecoder.vectorial.core.expression.Variable
import uk.co.nickthecoder.vectorial.core.expression.VariableImpl
import uk.co.nickthecoder.vectorial.core.shape.Shape

interface SetVariable<V : Any, E : Expression<V>> : Change {
    val variable: Variable<V, E>
    val newExpression: E
}

interface SetShapeVariable<V : Any, E : Expression<V>> : SetVariable<V, E>, ShapeChange

// Implementations

abstract class SetVariableImpl<V : Any, E : Expression<V>>(

    final override val label: String,
    val owner: Item,
    final override val variable: Variable<V, E>,
    final override var newExpression: E

) : SetShapeVariable<V, E>, ShapeChange, InternalChange {

    private val oldExpression = variable.innerExpression

    override fun redo() {
        (variable as VariableImpl<V, E>).set(newExpression)
    }

    override fun undo() {
        (variable as VariableImpl<V, E>).set(oldExpression)
    }

    override fun canMergeWith(previous: Change) =
        previous is SetShapeVariableImpl<*, *> && variable === previous.variable

    override fun mergeWith(previous: Change) {
        @Suppress("UNCHECKED_CAST")
        (previous as SetShapeVariableImpl<V, E>).newExpression = newExpression
    }

}

class SetShapeVariableImpl<V : Any, E : Expression<V>>(
    label: String,
    shape: Shape,
    variable: Variable<V, E>,
    newExpression: E
) : SetVariableImpl<V, E>(label, shape, variable, newExpression) {
    override val shape: Shape get() = owner as Shape
}
