package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.Handle.Companion.snapControlMod
import uk.co.nickthecoder.vectorial.core.expression.*
import kotlin.math.abs

enum class QuadrilateralType {
    RECTANGLE, DIAMOND, PARALLELOGRAM
}

/**
 * The base interface for [Rectangle], [Parallelogram] and [Diamond].
 * Each of these have two implementations, one which is based on a `center` and `size`,
 * and another which is based on a pair of opposite corners. See [Centerable].
 *
 * Note that [size] may have negative values.
 */
interface Quadrilateral : Geometry, Centerable {

    val quadrilateralType: QuadrilateralType

    override val baseName: String get() = quadrilateralType.name.lowercase()

    val size: Vector2Expression
    val center: Vector2Expression

    /**
     * Another name for [start], which is an [Expression] common to all [Geometry].
     */
    val corner0: Vector2Expression
    val corner1: Vector2Expression
    val corner2: Vector2Expression
    val corner3: Vector2Expression

    override fun isClosed() = true

    fun changeQuadrilateralType(type: QuadrilateralType)
}

/**
 * A base class containing common functionality across all [Quadrilateral] implementations.
 */
internal abstract class QuadrilateralImpl : Quadrilateral, MutableGeometry {

    abstract override val attributes: AttributesImpl

    protected fun addCornerAttributes() {
        createAttribute(attributes, "corner0", corner0)
        createAttribute(attributes, "corner1", corner1)
        createAttribute(attributes, "corner2", corner2)
        createAttribute(attributes, "corner3", corner3)
    }

    final override fun toPolyline(thickness: Float, distanceTolerance: Float, angleTolerance: Double): Polyline {
        return Polyline( listOf(corner1.eval(),corner2.eval(),corner3.eval(),start.eval()), thickness, true)
    }

    final override fun calculateFillMesh(): Mesh {

        val a = corner0.eval()
        val b = corner1.eval()
        val c = corner2.eval()
        val d = corner3.eval()

        return Mesh(listOf(a, b, c, c, d, a), emptyList())
    }

    private val edges by lazy {
        listOf(
            LineEdgeImpl(shape, corner0, corner1),
            LineEdgeImpl(shape, corner1, corner2),
            LineEdgeImpl(shape, corner2, corner3),
            LineEdgeImpl(shape, corner3, corner0)
        )
    }

    final override fun touchingEdges(point: Vector2, threshold: Float, threshold2: Float, into: MutableList<Edge>) {
        for (edge in edges) {
            if (edge.isNear(point, threshold2)) {
                into.add(edge)
            }
        }
    }

    override fun toPath(): Path {
        return PathImpl(corner0.eval()).apply {
            appendLineSegment(corner1.eval())
            appendLineSegment(corner2.eval())
            appendLineSegment(corner3.eval())
            closePath()
        }
    }

    inner class Corner(private val scaleX: Float, private val scaleY: Float) : CachedVector2Expression() {
        init {
            addDependencies(center, size)
        }

        override fun recalculate(): Vector2 {
            val center = center.eval()
            val size = size.eval()
            return Vector2(
                center.x + scaleX * size.x,
                center.y + scaleY * size.y
            )
        }
    }

}


/**
 * The `size` [Handle] for a [Geometry] which has `center` and `size` [Attribute]s.
 */
internal class CenteredSizeHandle(
    shape: Shape,
    val center: Vector2Attribute,
    val size: Vector2Attribute
) : DerivedAttributeHandle<Vector2, Vector2Expression>(
    shape,
    size,
    object : PointConverter<Vector2> {
        override fun fromVector2(position: Vector2) = (position - center.eval()) * 2f
        override fun toVector2(value: Vector2) = center.eval() + (value / 2f)
    },
    center
) {
    /**
     * Snap the ratio to integer multiples.
     */
    override fun snap(position: Vector2): Vector2 {
        val center = center.eval()
        val diff = position - center
        val wider = Math.abs(diff.x) > abs(diff.y)
        var ratio = if (wider) {
            diff.x / diff.y
        } else {
            diff.y / diff.x
        }
        // TODO Inkscape also snaps to the golden ratio!
        ratio = Math.round(ratio).toFloat()

        return center + if (wider) {
            Vector2(diff.y * ratio, diff.y)
        } else {
            Vector2(diff.x, diff.x * ratio)
        }
    }

    override fun tip(): String {
        val size = size.eval()
        val arString = if (size.y == 0f) {
            "flat"
        } else {
            val aspectRatio = abs(size.x / size.y)
            if (aspectRatio >= 1) {
                "${aspectRatio.max2DPs()} : 1"
            } else {
                "1 : ${(1 / aspectRatio).max2DPs()}"
            }
        }
        val sizeStr = size.sizeMax2DPs()
        return "Size : $sizeStr Aspect Ratio : [$arString]. [$snapControlMod] to snap aspect ratio."
    }

    override fun createConnection(connector: Connector) : Vector2Connection = Vector2ConnectionImpl(connector, convertor)
}

/**
 * The `width` [Handle] for a [CenteredRectangle].
 */
internal class CenteredWidthHandle(
    shape: Shape,
    centerAtt: Vector2Attribute,
    sizeAtt: Vector2Attribute
) : DerivedAttributeHandle<Vector2, Vector2Expression>(
    shape,
    sizeAtt,
    object : PointConverter<Vector2> {
        override fun fromVector2(position: Vector2): Vector2 {
            val center = centerAtt.eval()
            return Vector2((position.x - center.x) * 2f, sizeAtt.eval().y)
        }

        override fun toVector2(value: Vector2): Vector2 {
            val center = centerAtt.eval()
            return Vector2(center.x + (value.x / 2f), center.y)
        }
    },
    centerAtt
) {
    override val name: String get() = "width"
    override fun isFixed() = !isFree()
    override fun isConnectable() = false
    override fun createConnection(connector: Connector) = null
}

/**
 * The `height` [Handle] for a [CenteredRectangle].
 */
internal class CenteredHeightHandle(
    shape: Shape,
    centerAtt: Vector2Attribute,
    sizeAtt: Vector2Attribute
) : DerivedAttributeHandle<Vector2, Vector2Expression>(
    shape,
    sizeAtt,
    object : PointConverter<Vector2> {
        override fun fromVector2(position: Vector2): Vector2 {
            val center = centerAtt.eval()
            return Vector2(sizeAtt.eval().x, (position.y - center.y) * 2f)
        }

        override fun toVector2(value: Vector2): Vector2 {
            val center = centerAtt.eval()
            return Vector2(center.x, center.y + (value.y / 2f))
        }
    },
    centerAtt
) {
    override val name: String get() = "height"
    override fun isFixed() = !isFree()
    override fun isConnectable() = false
    override fun createConnection(connector: Connector) = null
}
