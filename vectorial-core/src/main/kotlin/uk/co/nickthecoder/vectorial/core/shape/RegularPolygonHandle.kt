package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.expression.Connector
import uk.co.nickthecoder.vectorial.core.expression.Vector2Attribute
import uk.co.nickthecoder.vectorial.core.expression.Vector2ConnectionImpl
import uk.co.nickthecoder.vectorial.core.expression.Vector2Expression

/**
 * The `vertex` [Handle] for a [PolygonOrStar].
 */
internal class RegularPolygonHandle(
    val polygon: CenteredPolygonOrStarImpl,
    vertex: Vector2Attribute
) : DerivedAttributeHandle<Vector2, Vector2Expression>(
    polygon.shape,
    vertex,
    object : PointConverter<Vector2> {
        override fun fromVector2(position: Vector2) = position - polygon.center.eval()
        override fun toVector2(value: Vector2) = polygon.center.eval() + value
    },
    polygon.center
) {

    override fun createConnection(connector: Connector) = Vector2ConnectionImpl(connector, convertor)

    override fun tip(): String {
        val center = polygon.center.eval()
        val diff = position() - center
        val radius = diff.magnitude().max2DPs()
        val angle = diff.angleDegrees().max2DPs()

        return "Radius $radius. Angle $angle °. [${Handle.snapControlMod}] to snap angle."
    }

    override fun snap(position: Vector2): Vector2 {
        val center = polygon.center.eval()
        val delta = position - center
        var degrees: Double = delta.angleDegrees()
        degrees += 7.5 * Math.signum(degrees)
        degrees -= degrees % 15
        return center + Vector2(delta.magnitude(), 0f).rotateDegrees(degrees)
    }
}
