package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.Angle
import uk.co.nickthecoder.vectorial.core.ConnectorType
import uk.co.nickthecoder.vectorial.core.HasTip
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.shape.AngleEdge


interface DistanceAlongAngleEdge : ConnectorToEdge, HasTip {
    override val edge: AngleEdge
    val start: Vector2Expression
    val distance: FloatVariable
}

class DistanceAlongAngleEdgeImpl(
    override val edge: AngleEdge,
    distanceExpression: FloatExpression,
) : CachedExpression<Vector2>(), DistanceAlongAngleEdge, MovableConnectorImpl {

    constructor(
        edge: AngleEdge,
        distance: Float
    ) : this(edge, FloatConstant(distance))

    override val isBounded: Boolean get() = false

    override val start: Vector2Expression get() = edge.point

    override val connectorType: ConnectorType get() = ConnectorType.DISTANCE

    override val distance = FloatVariableImpl(edge.shape, "distance", distanceExpression)


    init {
        addDependencies(edge.point, edge.angle, distance)
    }

    override fun isFixed() = distance.innerExpression !is FloatConstant

    override fun performMoveTowards(world: Vector2) {
        val startV = edge.point.eval()
        val direction = edge.angle.eval().toVector()

        val delta = world - startV
        val newValue = delta.dot(direction)

        distance.set(FloatConstant(newValue))
        dirty()
    }

    override fun recalculate(): Vector2 {

        val startV = edge.point.eval()
        val direction = edge.angle.eval().toVector()

        return startV + direction * distance.eval()
    }

    override fun tip() = "Distance [${distance.eval()}]."

    override fun toString() =
        super.toString() + " ${edge.point.eval()} angle=${edge.angle.eval().degrees}} distance=${distance.eval()}"

}
