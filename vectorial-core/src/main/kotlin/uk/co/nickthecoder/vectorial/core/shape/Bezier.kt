package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.expression.*
import kotlin.math.abs
import kotlin.math.atan2
import kotlin.math.pow

/**
 * [Geometry] is a 2D path, without "visible" attribute, such as thickness or color
 */
interface Bezier : Geometry {

    override val start: Vector2Attribute

    val c1: Vector2Attribute

    val c2: Vector2Attribute

    val end: Vector2Attribute

    fun c1() = c1.eval()

    fun c2() = c2.eval()

    fun along(t: Float): Vector2

    fun pointsForPolyline(distanceTolerance: Float, angleTolerance: Double): List<Vector2>

    companion object {

        const val C1 = "c1"
        const val C2 = "c2"

        fun create(
            start: Vector2Expression, c1: Vector2Expression, c2: Vector2Expression, end: Vector2Expression
        ): Bezier = BezierImpl(start, c1, c2, end)

        fun create(
            start: Vector2, c1: Vector2, c2: Vector2, end: Vector2
        ) = create(Vector2Constant(start), Vector2Constant(c1), Vector2Constant(c2), Vector2Constant(end))

    }
}

/**
 * A Cubic [Bezier] curve has a [start] and [end] points, as well as two control points [c1] and [c2].
 * The tangent of the curve at [start] is along the line [start]..[c1] and the tangent of the curve
 * at [end] is [c2]..[end].
 * Therefore, we can stitch together a sequence of bezier curves, which are smooth, if the
 * `a.end == b.start` and if `a.c2`, `a.end` and `b.c1` form a straight line.
 *
 * In many cases the distance `a.c2 .. a.end` and `a.end .. b.c2` are made equal.
 */
internal class BezierImpl(
    startExpression: Vector2Expression,
    c1Expression: Vector2Expression,
    c2Expression: Vector2Expression,
    endExpression: Vector2Expression
) : Bezier, MutableGeometry {

    override lateinit var shape: GeometryShape

    override val baseName: String get() = "bezier"

    override val attributes = AttributesImpl(this)
    override val start = createAttribute(attributes, "start", startExpression)
    override val end = createAttribute(attributes, "end", endExpression)
    override val c1 = createAttribute(attributes, Bezier.C1, c1Expression)
    override val c2 = createAttribute(attributes, Bezier.C2, c2Expression)

    private val cachedControlPoints by lazy {
        val startCP = BezierEndPoint(shape, start)
        val endCP = BezierEndPoint(shape, end)

        listOf(
            startCP,
            BezierHandle(shape, c1, startCP),
            BezierHandle(shape, c2, endCP),
            endCP
        )
    }

    override fun handles(): List<Handle> = cachedControlPoints


    override fun isClosed() = false

    /**
     * Approximate the length, by splitting the curve into 10 straight lines.
     */
    fun length(): Float {
        var length = 0f
        var prev = start.eval()
        var next: Vector2
        var tmp: Vector2
        for (i in 1..10) {
            next = along(i.toFloat() / 10f)
            tmp = next
            tmp -= prev
            length += tmp.magnitude()
            prev = next
        }
        return length
    }

    override fun along(t: Float): Vector2 {
        val start = start.eval()
        val end = end.eval()
        val c1 = c1.eval()
        val c2 = c2.eval()
        return Vector2(
            pointAlongBezier(start.x, c1.x, c2.x, end.x, t.toDouble()),
            pointAlongBezier(start.y, c1.y, c2.y, end.y, t.toDouble())
        )
    }

    override fun movableAttributes() = listOf(start, c1, c2, end)

    /**
     * Ported from C++
     * Many thanks.
     * https://web.archive.org/web/20180307160123/http://antigrain.com/research/adaptive_bezier/index.html
     */
    override fun pointsForPolyline(distanceTolerance: Float, angleTolerance: Double): List<Vector2> {

        val into = mutableListOf<Vector2>()

        val distanceToleranceSquare = distanceTolerance * distanceTolerance

        fun calcDistanceSquared(x1: Float, y1: Float, x2: Float, y2: Float): Float {
            val dx = x2 - x1
            val dy = y2 - y1
            return dx * dx + dy * dy
        }

        fun recursiveBezier(
            x1: Float, y1: Float,
            x2: Float, y2: Float,
            x3: Float, y3: Float,
            x4: Float, y4: Float,
            level: Int
        ) {
            if (level > RECURSION_LIMIT) {
                return
            }

            // Calculate all the mid-points of the line segments
            //----------------------
            val x12 = (x1 + x2) / 2
            val y12 = (y1 + y2) / 2
            val x23 = (x2 + x3) / 2
            val y23 = (y2 + y3) / 2
            val x34 = (x3 + x4) / 2
            val y34 = (y3 + y4) / 2
            val x123 = (x12 + x23) / 2
            val y123 = (y12 + y23) / 2
            val x234 = (x23 + x34) / 2
            val y234 = (y23 + y34) / 2
            val x1234 = (x123 + x234) / 2
            val y1234 = (y123 + y234) / 2


            // Try to approximate the full cubic curve by a single straight line
            //------------------
            val dx = x4 - x1
            val dy = y4 - y1

            var d2 = abs(((x2 - x4) * dy - (y2 - y4) * dx))
            var d3 = abs(((x3 - x4) * dy - (y3 - y4) * dx))
            var da1: Float
            var da2: Float
            var k: Float

            //when (case) {
            if (d2 < COLLINEARITY_EPSILON) {

                if (d3 < COLLINEARITY_EPSILON) {
                    // println("A $x1234, $y1234")

                    // All collinear OR p1==p4
                    //----------------------
                    k = dx * dx + dy * dy
                    if (k == 0f) {
                        d2 = calcDistanceSquared(x1, y1, x2, y2)
                        d3 = calcDistanceSquared(x4, y4, x3, y3)
                    } else {
                        k = 1 / k
                        da1 = x2 - x1
                        da2 = y2 - y1
                        d2 = k * (da1 * dx + da2 * dy)
                        da1 = x3 - x1
                        da2 = y3 - y1
                        d3 = k * (da1 * dx + da2 * dy)
                        if (d2 > 0 && d2 < 1 && d3 > 0 && d3 < 1) {
                            // Simple collinear case, 1---2---3---4
                            // We can leave just two endpoints
                            return
                        }
                        d2 = if (d2 <= 0) calcDistanceSquared(x2, y2, x1, y1)
                        else if (d2 >= 1) calcDistanceSquared(x2, y2, x4, y4)
                        else calcDistanceSquared(x2, y2, x1 + d2 * dx, y1 + d2 * dy)

                        d3 = if (d3 <= 0) calcDistanceSquared(x3, y3, x1, y1)
                        else if (d3 >= 1) calcDistanceSquared(x3, y3, x4, y4)
                        else calcDistanceSquared(x3, y3, x1 + d3 * dx, y1 + d3 * dy)
                    }
                    if (d2 > d3) {
                        if (d2 < distanceToleranceSquare) {
                            into.add(Vector2(x2, y2))
                            return
                        }
                    } else {
                        if (d3 < distanceToleranceSquare) {
                            into.add(Vector2(x3, y3))
                            return
                        }
                    }
                } else {
                    // println("B $x1234, $y1234")

                    // p1,p2,p4 are collinear, p3 is significant
                    //----------------------
                    if (d3 * d3 <= distanceToleranceSquare * (dx * dx + dy * dy)) {
                        if (angleTolerance < ANGLE_TOLERANCE_EPSILON) {
                            into.add(Vector2(x23, y23))
                            return
                        }

                        // Angle Condition
                        //----------------------
                        da1 = abs(
                            atan2((y4 - y3).toDouble(), (x4 - x3).toDouble()) -
                                atan2((y3 - y2).toDouble(), (x3 - x2).toDouble())
                        ).toFloat()
                        if (da1 >= PI) da1 = 2 * PI - da1

                        if (da1 < angleTolerance) {
                            into.add(Vector2(x2, y2))
                            into.add(Vector2(x3, y3))
                            return
                        }

                        if (cuspLimit != 0.0f) {
                            if (da1 > cuspLimit) {
                                into.add(Vector2(x3, y3))
                                return
                            }
                        }
                    }
                }

            } else {
                if (d3 < COLLINEARITY_EPSILON) {
                    // println("C $x1234, $y1234")

                    // p1,p3,p4 are collinear, p2 is significant
                    //----------------------
                    if (d2 * d2 <= distanceToleranceSquare * (dx * dx + dy * dy)) {
                        if (angleTolerance < ANGLE_TOLERANCE_EPSILON) {
                            into.add(Vector2(x23, y23))
                            return
                        }

                        // Angle Condition
                        //----------------------
                        da1 = abs(
                            atan2((y3 - y2).toDouble(), (x3 - x2).toDouble()) -
                                atan2((y2 - y1).toDouble(), (x2 - x1).toDouble())
                        ).toFloat()
                        if (da1 >= PI) da1 = 2 * PI - da1

                        if (da1 < angleTolerance) {
                            into.add(Vector2(x2, y2))
                            into.add(Vector2(x3, y3))
                            return
                        }

                        if (cuspLimit != 0.0f) {
                            if (da1 > cuspLimit) {
                                into.add(Vector2(x2, y2))
                                return
                            }
                        }
                    }

                } else {
                    // println("D $x1234, $y1234")

                    // Regular case
                    //-----------------
                    if ((d2 + d3) * (d2 + d3) <= distanceToleranceSquare * (dx * dx + dy * dy)) {
                        // If the curvature doesn't exceed the distance_tolerance value
                        // we tend to finish subdivisions.
                        //----------------------
                        if (angleTolerance < ANGLE_TOLERANCE_EPSILON) {
                            into.add(Vector2(x23, y23))
                            return
                        }

                        // Angle & Cusp Condition
                        //----------------------
                        k = atan2((y3 - y2).toDouble(), (x3 - x2).toDouble()).toFloat()
                        da1 = abs(k - atan2((y2 - y1).toDouble(), (x2 - x1).toDouble())).toFloat()
                        da2 = abs(atan2((y4 - y3).toDouble(), (x4 - x3).toDouble()) - k).toFloat()
                        if (da1 >= PI) da1 = 2 * PI - da1
                        if (da2 >= PI) da2 = 2 * PI - da2

                        if (da1 + da2 < angleTolerance) {
                            // Finally we can stop the recursion
                            //----------------------
                            into.add(Vector2(x23, y23))
                            return
                        }

                        if (cuspLimit != 0.0f) {
                            if (da1 > cuspLimit) {
                                into.add(Vector2(x2, y2))
                                return
                            }

                            if (da2 > cuspLimit) {
                                into.add(Vector2(x3, y3))
                                return
                            }
                        }
                    }
                }
            }

            // Continue subdivision
            //----------------------
            recursiveBezier(x1, y1, x12, y12, x123, y123, x1234, y1234, level + 1)
            recursiveBezier(x1234, y1234, x234, y234, x34, y34, x4, y4, level + 1)
        }

        //------------------------------------------------------------------------

        val start = start.eval()
        val end = end.eval()
        val c1 = c1.eval()
        val c2 = c2.eval()

        recursiveBezier(start.x, start.y, c1.x, c1.y, c2.x, c2.y, end.x, end.y, 0)
        into.add(end)

        return into
    }

    override fun toPolyline(thickness: Float, distanceTolerance: Float, angleTolerance: Double): Polyline {
        return Polyline(pointsForPolyline(distanceTolerance, angleTolerance), thickness, isClosed())
    }

    override fun toPath(): Path = PathImpl(start.eval()).apply {
        appendBezierCurve(c1.eval(), c2.eval(), end.eval())
    }

    override fun toString() = "Bezier ${start.eval()} -> ${end.eval()} c1=${c1.eval()} c2=${c2.eval()}"

    companion object {
        const val PI = Math.PI.toFloat()
        const val COLLINEARITY_EPSILON = 1e-30f
        const val ANGLE_TOLERANCE_EPSILON = 0.01
        const val RECURSION_LIMIT = 30
        var cuspLimit = 0.0f
    }
}

private fun bez03(u: Double) = (1 - u).pow(3.0)
private fun bez13(u: Double) = 3.0 * u * ((1.0 - u).pow(2.0))
private fun bez23(u: Double) = 3.0 * (u.pow(2.0)) * (1.0 - u)
private fun bez33(u: Double) = u.pow(3.0)

fun pointAlongBezier(p0: Float, p1: Float, p2: Float, p3: Float, u: Double) =
    (bez03(u) * p0 + bez13(u) * p1 + bez23(u) * p2 + bez33(u) * p3).toFloat()


class BezierHandle(shape: Shape, attribute: Vector2Attribute, val startEnd: BezierEndPoint) :
    SimpleAttributeHandle(shape, attribute) {
    init {
        startEnd.handle = this
    }
}

class BezierEndPoint(shape: Shape, attribute: Vector2Attribute) :
    SimpleAttributeHandle(shape, attribute) {

    lateinit var handle: BezierHandle

}
