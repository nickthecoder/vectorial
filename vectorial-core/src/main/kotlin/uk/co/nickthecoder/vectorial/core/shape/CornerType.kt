package uk.co.nickthecoder.vectorial.core.shape

/**
 * How one [PathPart] transitions to the next one.
 */
enum class CornerType {
    ANGLED,

    /**
     * The tangent is the same, but the _distance_ of the handles may vary
     */
    SMOOTH,

    /**
     * The two handles are symmetric about the anchor point.
     */
    SYMMETRIC
}
