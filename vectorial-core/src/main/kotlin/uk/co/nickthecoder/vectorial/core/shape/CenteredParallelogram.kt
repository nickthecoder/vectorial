package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.ChangeGeometryImpl
import uk.co.nickthecoder.vectorial.core.expression.*
import kotlin.math.sign

internal class CenteredParallelogram(
    centerExpression: Vector2Expression,
    sizeExpression: Vector2Expression,
    ratioExpression: FloatExpression = FloatConstant(0.25f)

) : QuadrilateralImpl(), Parallelogram, MutableGeometry {

    constructor(center: Vector2, size: Vector2, ratio: Float = 0.25f) :
            this(Vector2Constant(center), Vector2Constant(size), FloatConstant(ratio))

    override lateinit var shape: GeometryShape

    override val centered: Centered get() = Centered.CENTERED

    // Attributes
    override val attributes = AttributesImpl(this)
    override val center = createAttribute(attributes, "center", centerExpression)
    override val size = createAttribute(attributes, "size", sizeExpression)
    override val ratio = createAttribute(attributes, "ratio", ratioExpression)

    // Expressions
    override val corner0 = ParallelogramCorner(this, -0.5f, -0.5f)
    override val corner1 = ParallelogramCorner(this, 0.5f, -0.5f)
    override val corner2 = ParallelogramCorner(this, 0.5f, 0.5f)
    override val corner3 = ParallelogramCorner(this, -0.5f, 0.5f)

    // Continuous Geometry expressions
    override val start get() = corner0

    // End of expressions

    private val cachedControlPoints by lazy {
        listOf(
            SimpleAttributeHandle(shape, center),
            CenteredSizeHandle(shape, center, size),
            ParallelogramOffsetHandle(this),
        )
    }

    init {
        addCornerAttributes()
    }

    override fun handles() = cachedControlPoints

    override fun changeCentered(centered: Centered) {
        if (centered == Centered.CORNER_BASED) {
            ChangeGeometryImpl(
                "Convert to a corner-based Diamond",
                shape,
                CornerBasedParallelogram(
                    Vector2Constant(corner0.eval()),
                    Vector2Constant(corner2.eval()),
                    ratio.expression
                )
            ).now()
        }
    }


    override fun changeQuadrilateralType(type: QuadrilateralType) {

        when (type) {
            QuadrilateralType.PARALLELOGRAM -> {}

            QuadrilateralType.RECTANGLE -> {
                ChangeGeometryImpl(
                    "Convert to a rectangle",
                    shape, CenteredRectangle(center.expression, size.expression)
                ).now()
            }

            QuadrilateralType.DIAMOND -> {
                ChangeGeometryImpl(
                    "Convert to a diamond",
                    shape, CenteredDiamond(center.expression, size.expression)
                ).now()
            }
        }
    }

    override fun movableAttributes() = listOf(center)

    override fun toString() = "Parallelogram center ${center.eval()} size ${size.eval()}} offset ${ratio.eval()}"


}


internal class ParallelogramCorner(
    private val parallelogram: Parallelogram,
    private val scaleX: Float,
    private val scaleY: Float

) : CachedVector2Expression() {

    init {
        addDependencies(parallelogram.center, parallelogram.size, parallelogram.ratio)
    }

    override fun recalculate(): Vector2 {
        val center = parallelogram.center.eval()
        val size = parallelogram.size.eval()
        val foo = if (scaleX.sign != scaleY.sign) size.y * parallelogram.ratio.eval() * scaleX.sign else 0f
        return Vector2(
            center.x + scaleX * size.x + foo,
            center.y + scaleY * size.y
        )
    }
}


/**
 * The `width` [Handle] for a [CenteredRectangle].
 */
internal class ParallelogramOffsetHandle(val parallelogram: Parallelogram) :
    DerivedAttributeHandle<Float, FloatExpression>(
        parallelogram.shape,
        parallelogram.ratio,
        object : PointConverter<Float> {
            override fun fromVector2(position: Vector2) =
                (position.x - parallelogram.center.eval().x - parallelogram.size.eval().x / 2) / parallelogram.size.eval().y

            override fun toVector2(value: Float): Vector2 {
                val center = parallelogram.center.eval()
                val size = parallelogram.size.eval()
                return Vector2(center.x + size.x / 2 + size.y * value, center.y - size.y / 2)
            }
        },
        parallelogram.center, parallelogram.size
    ) {
    override val name: String get() = "offset"
    override fun isFixed() = !isFree()
    override fun isConnectable() = false
    override fun createConnection(connector: Connector) = null
}
