package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.shape.LineEdge

interface ConnectorToEdge : MovableConnector, HasTip {

    /**
     * The [Edge] that is being connected to, e.g. a [LineEdge] when connection to a line-segment.
     */
    val edge: Edge

    /**
     *
     */
    val connectorType: ConnectorType

    /**
     * Is the connection allowed to extend before the start or beyond the end of the edge?
     * For bezier curve edges, this should always be false, but for [LineEdge]s, it is often useful
     * to exceed the bounds of the line-segment.
     */
    val isBounded: Boolean

}
