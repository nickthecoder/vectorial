package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.Vector2

/**
 * Exposes [performMoveTowards], which must only be called from [Change]'s `undo` and `redo` methods
 * (which are hidden from the public API, and are found in [InternalChange] and [InternalChange]).
 *
 * Used by expression such as [RatioAlongLine], [DistanceAlongLine] to change their
 * internal values so that they evaluate as close to the given `world` point.
 * Most [Connection] implementations also implement this interface, and forward to
 * the underlying expression, such as [RatioAlongLine] etc.
 *
 * [AttributesImpl.Vector2AttributeImpl] also implements this interface, and forwards
 * to its expression (assuming it is a [MovableVector2Expression]).
 */
interface MovableVector2Expression : Vector2Expression {

    /**
     * If this is a fixed point (with no degrees of freedom), then return true.
     */
    fun isFixed(): Boolean

}

internal interface MovableVector2ExpressionImpl : MovableVector2Expression {

    /**
     * Move towards [world], while maintaining all connections.
     * If there are no constraints, then move exactly to [world].
     *
     * Only call this from [Change].undo() or [Change].redo(). Otherwise, the [History] mechanism
     * will break (because changes to the model will take place outside of [History]).
     */
    fun performMoveTowards(world: Vector2)

}
