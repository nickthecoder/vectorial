package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Diagram
import uk.co.nickthecoder.vectorial.core.DiagramImpl
import uk.co.nickthecoder.vectorial.core.Selection
import uk.co.nickthecoder.vectorial.core.shape.Shape

/**
 * Changes [Selection.shapes].
 */
interface SelectionChange : AltersSelection, SkippableChange {

    companion object {

        fun clear(diagram: Diagram): SelectionChange? {
            return if (diagram.selection.shapes.isNotEmpty()) {
                SelectionChangeImpl(diagram as DiagramImpl, emptyList())
            } else {
                null
            }
        }

        fun remove(shape: Shape): SelectionChange? {
            return if (shape.diagram().selection.shapes.contains(shape)) {
                val newShapes = shape.diagram().selection.shapes.toMutableList().apply {
                    remove(shape)
                }
                SelectionChangeImpl(shape.diagram() as DiagramImpl, newShapes)
            } else {
                null
            }
        }


        fun add(shape: Shape): SelectionChange? {
            return if (!shape.diagram().selection.shapes.contains(shape)) {
                val newShapes = shape.diagram().selection.shapes.toMutableList().apply {
                    add(shape)
                }
                SelectionChangeImpl(shape.diagram() as DiagramImpl, newShapes)
            } else {
                null
            }
        }

        fun select(shape: Shape): SelectionChange? {
            val si = shape.diagram().selection.shapes
            return if (si.size == 1 && si.first() === shape) {
                null
            } else {
                SelectionChangeImpl(shape)
            }
        }

        fun select(diagram: Diagram, shapes: List<Shape>): SelectionChange? {
            val si = diagram.selection.shapes
            return if (si.size == shapes.size && si == shapes) {
                null
            } else {
                SelectionChangeImpl(diagram, shapes)
            }
        }
    }
}

internal class SelectionChangeImpl(
    override val diagram: Diagram,
    newShapes: List<Shape>
) : SelectionChange, InternalChange {

    constructor(newShape: Shape) : this(newShape.diagram(), listOf(newShape))

    var newShapes = newShapes.toList()

    private val oldShapes = diagram.selection.shapes.toList()
    private val oldHandleKey = diagram.selection.handle

    override val label: String
        get() {
            return if (newShapes.isEmpty()) {
                "Clear selection"
            } else {
                if (oldShapes.isEmpty()) {
                    if (newShapes.size == 1) {
                        "Select ${newShapes.first().name}"
                    } else {
                        "Select ${newShapes.size} shapes"
                    }
                } else {
                    // TODO Either Add $newShape or Remove $oldShape
                    "Adjust selection"
                }
            }
        }

    override fun redo() {
        (diagram as DiagramImpl).selection.apply {
            shapes.clear()
            shapes.addAll(newShapes)
            handle = null
        }
    }

    override fun undo() {
        (diagram as DiagramImpl).selection.apply {
            shapes.clear()
            shapes.addAll(oldShapes)
            handle = oldHandleKey
        }
    }

    override fun canMergeWith(previous: Change) = previous is SelectionChangeImpl

    override fun mergeWith(previous: Change) {
        previous as SelectionChangeImpl
        previous.newShapes = newShapes
    }

}
