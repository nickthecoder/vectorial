package uk.co.nickthecoder.vectorial.core.shape


enum class CapStyle {

    /**
     * Path ends are drawn flat, and don't exceed the actual end point.
     */
    BUTT,

    /**
     * Path ends are drawn flat, but extended beyond the end point by half the line thickness.
     */
    SQUARE,

    /**
     * Path ends are rounded off.
     */
    ROUND
}
