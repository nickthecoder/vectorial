package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Document
import uk.co.nickthecoder.vectorial.core.ShapeStyle

interface StyleChange : Change {
    val style: ShapeStyle

    override val document: Document
        get() = style.document
}
