package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Handle
import uk.co.nickthecoder.vectorial.core.SimpleAttributeHandle
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.change.ChangeGeometryImpl
import uk.co.nickthecoder.vectorial.core.expression.*

internal class CenteredDiamond(
    centerExpression: Vector2Expression,
    sizeExpression: Vector2Expression

) : QuadrilateralImpl(), Diamond, MutableGeometry {

    constructor(center: Vector2, size: Vector2) :
            this(Vector2Constant(center), Vector2Constant(size))

    override lateinit var shape: GeometryShape

    override val centered: Centered get() = Centered.CENTERED

    // Attributes
    override val attributes = AttributesImpl(this)
    override val center = createAttribute(attributes, "center", centerExpression)
    override val size = createAttribute(attributes, "size", sizeExpression)

    // Expressions
    override val corner0 = Corner(0f, -0.5f)
    override val corner1 = Corner(0.5f, 0f)
    override val corner2 = Corner(0f, 0.5f)
    override val corner3 = Corner(-0.5f, 0f)

    // Continuous Geometry expressions
    override val start get() = corner0

    // End of expressions

    private val cachedHandles: List<Handle> by lazy {
        listOf(
            SimpleAttributeHandle(shape, center),
            CenteredSizeHandle(shape, center, size),
            CenteredWidthHandle(shape, center, size),
            CenteredHeightHandle(shape, center, size),
        )
    }

    init {
        addCornerAttributes()
    }

    override fun handles() = cachedHandles

    override fun changeCentered(centered: Centered) {
        val change = when (centered) {
            Centered.CENTERED -> null
            else -> ChangeGeometryImpl(
                "Convert to a corner-based Diamond",
                shape,
                CornerBasedDiamond(
                    Vector2Constant(center.eval() - size.eval() / 2f),
                    Vector2Constant(center.eval() + size.eval() / 2f)
                )
            )
        }
        change?.now()
    }

    override fun changeQuadrilateralType(type: QuadrilateralType) {

        val change = when (type) {
            QuadrilateralType.DIAMOND -> null

            QuadrilateralType.PARALLELOGRAM -> {
                ChangeGeometryImpl(
                    "Convert to a parallelogram",
                    shape, CenteredParallelogram(center.expression, size.expression)
                )
            }

            QuadrilateralType.RECTANGLE -> {
                ChangeGeometryImpl(
                    "Convert to a rectangle",
                    shape, CenteredRectangle(center.expression, size.expression)
                )
            }
        }
        change?.now()
    }

    override fun movableAttributes() = listOf(center)

    override fun toString() = "Rectangle center ${center.eval()} size ${size.eval()}}"

    inner class Corner(private val scaleX: Float, private val scaleY: Float) : CachedVector2Expression() {

        init {
            addDependencies(center, size)
        }

        override fun recalculate(): Vector2 {
            val center = center.eval()
            val size = size.eval()
            return Vector2(
                center.x + scaleX * size.x,
                center.y + scaleY * size.y
            )
        }
    }

}
