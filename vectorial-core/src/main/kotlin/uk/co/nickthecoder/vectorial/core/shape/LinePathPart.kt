package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Edge
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.Vector2Attribute

interface LinePathPart : PathPart


internal class LinePathPartImpl(

    path: Path,
    override val start: Vector2Attribute,
    override val end: Vector2Attribute

) : LinePathPart, PathPartImpl(path) {

    private val edge by lazy { LineEdgeImpl(path.shape, start, end) }

    override fun findEdges(point: Vector2, threshold2: Float, into: MutableList<Edge>) {
        if (edge.isNear(point, threshold2)) {
            into.add(edge)
        }
    }

    override fun pointsForPolyline(into: MutableList<Vector2>, distanceTolerance: Float, angleTolerance: Double) {
        into.add(end.eval())
    }

}
