package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.change.ChangeGeometryImpl
import uk.co.nickthecoder.vectorial.core.expression.*

internal class CornerBasedStarImpl(

    sides: Int,
    corner1Expression: Vector2Expression,
    corner2Expression: Vector2Expression,
    ratioAndTurnExpression: PolarExpression = PolarConstant(Polar(0.5f, Angle.radians(Math.PI / sides)))

) : CornerBasedPolygonOrStarImpl(sides, corner1Expression, corner2Expression), CornerBasedStar {

    constructor(
        sides: Int,
        corner1: Vector2,
        corner2: Vector2,
        ratioAndTurn: Polar = Polar(0.5f, Angle.radians(Math.PI / sides))
    ) : this(sides, Vector2Constant(corner1), Vector2Constant(corner2), PolarConstant(ratioAndTurn))

    override val polygonType: PolygonType get() = PolygonType.STAR

    override val ratioAndTurn = createAttribute(attributes, "ratioAndTurn", ratioAndTurnExpression)

    override val centered: Centered get() = Centered.CORNER_BASED

    override fun handles(): List<Handle> {
        var handles = cachedHandles
        if (handles == null) {
            handles = listOf(
                SimpleAttributeHandle(shape, corner1),
                SimpleAttributeHandle(shape, corner2),
                RatioAndTurnHandle(this, ratioAndTurn)
            )
            cachedHandles = handles
        }
        return handles
    }

    override fun calculateVertices(): List<Vector2Expression> {
        val vertices = mutableListOf<Vector2Expression>()
        for (i in 0 until sides.eval()) {
            vertices.add(center + delta.rotateRadians(angle * i))
            vertices.add(center + (delta * ratioAndTurn).rotateRadians(angle * i))
        }
        return vertices
    }


    override fun changeCentered(centered: Centered) {
        if (centered == Centered.CENTERED) {
            val newGeometry =
                CenteredStarImpl(sides.eval(), center.eval(), corner1.eval() - center.eval(), ratioAndTurn.eval())
            ChangeGeometryImpl("Make Centered", shape, newGeometry).now()
        }
    }

    override fun changePolygonType(type: PolygonType) {
        if (type == PolygonType.POLYGON) {
            val newGeometry = CornerBasedPolygonImpl(sides.eval(), corner1.eval(), corner2.eval())
            ChangeGeometryImpl("Change to polygon", shape, newGeometry).now()
        }
    }

    override fun movableAttributes() = listOf(corner1, corner2)
}
