package uk.co.nickthecoder.vectorial.core

import org.joml.Matrix3x2f
import org.joml.Matrix3x2fc
import java.text.DecimalFormat

internal val identityMatrix: Matrix3x2fc = Matrix3x2f()

fun ignoreErrors(action: () -> Any?) {
    try {
        action()
    } catch (e: Exception) {
        e.printStackTrace()
    }
}

fun Float.clamp(min: Float, max: Float) = coerceIn(min, max)
fun Float.clamp() = coerceIn(0f, 1f)

fun lerp(a: Float, b: Float, t: Float) = a * t + (1f - t) * b
fun lerp(a: Color, b: Color, t: Float) = a.lerp(b, t)


// Format numbers with a maximum of 2 decimal places
val max2DecimalPlaces = DecimalFormat("0.##")
fun Double.max2DPs(): String = max2DecimalPlaces.format(this)
fun Float.max2DPs(): String = max2DecimalPlaces.format(this.toDouble())
fun Vector2.max2DPs() = "[${x.max2DPs()} , ${y.max2DPs()}]"
fun Vector2.sizeMax2DPs() = "[${x.max2DPs()} x ${y.max2DPs()}]"
fun Matrix3x2f.max2DPs() = """[ ${m00.max2DPs()} , ${m10.max2DPs()} , ${m20.max2DPs()}
${m01.max2DPs()} , ${m11.max2DPs()} , ${m21.max2DPs()} ]"""
fun Matrix3x2fc.max2DPs() = """[ ${m00().max2DPs()} , ${m10().max2DPs()} , ${m20().max2DPs()}
  ${m01().max2DPs()} , ${m11().max2DPs()} , ${m21().max2DPs()} ]"""

// Format numbers with exactly 1 decimal place
val with1DecimalPlace = DecimalFormat("0.0")
fun Double.with1DPs(): String = with1DecimalPlace.format(this)
fun Float.with1DPs(): String = with1DecimalPlace.format(this.toDouble())
fun Vector2.with1DPs() = "[${x.with1DPs()} , ${y.with1DPs()}]"
fun Matrix3x2f.with1DPs() = """[ ${m00.with1DPs()} , ${m10.with1DPs()} , ${m20.with1DPs()}
${m01.with1DPs()} , ${m11.with1DPs()} , ${m21.with1DPs()} ]"""
fun Matrix3x2fc.with1DPs() = """[ ${m00().with1DPs()} , ${m10().with1DPs()} , ${m20().with1DPs()}
  ${m01().with1DPs()} , ${m11().with1DPs()} , ${m21().with1DPs()} ]"""

// Format numbers with exactly 2 decimal places
val with2DecimalPlaces = DecimalFormat("0.00")
fun Double.with2DPs(): String = max2DecimalPlaces.format(this)
fun Float.with2DPs(): String = max2DecimalPlaces.format(this.toDouble())
fun Vector2.with2DPs() = "[${x.with2DPs()} , ${y.with2DPs()}]"
fun Matrix3x2f.with2DPs() = """[ ${m00.with2DPs()} , ${m10.with2DPs()} , ${m20.with2DPs()}
${m01.with2DPs()} , ${m11.with2DPs()} , ${m21.with2DPs()} ]"""
fun Matrix3x2fc.with2DPs() = """[ ${m00().with2DPs()} , ${m10().with2DPs()} , ${m20().with2DPs()}
  ${m01().with2DPs()} , ${m11().with2DPs()} , ${m21().with2DPs()} ]"""

fun indent(depth: Int) = "    ".repeat(depth)


// TODO Replace with better logging.

fun severe(e: Exception) {
    System.err.println("SEVERE : $e")
    e.printStackTrace()
}

fun severe(message: String) {
    System.err.println("SEVERE : $message")
}

fun warn(message: String) {
    System.err.println("WARNING : $message")
}

fun info(message: String) {
    System.err.println("INFO : $message")
}

fun debug(message: String) {
    System.err.println("DEBUG : $message")
}
