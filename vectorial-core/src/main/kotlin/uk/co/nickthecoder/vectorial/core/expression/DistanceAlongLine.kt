package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.shape.LineEdge

interface DistanceAlongLine : ConnectorToEdge, HasTip {
    override val edge: LineEdge
    val start: Vector2Expression
    val end: Vector2Expression
    val distance: FloatVariable
    val isFromStart: Boolean
}

class DistanceAlongLineImpl(
    override val edge: LineEdge,
    distanceExpression: FloatExpression,
    override var isFromStart: Boolean,
    override val isBounded: Boolean
) : CachedExpression<Vector2>(), DistanceAlongLine, MovableConnectorImpl {

    constructor(
        edge: LineEdge,
        distance: Float,
        isFromStart: Boolean, isBounded: Boolean
    ) : this(edge, FloatConstant(distance), isFromStart, isBounded)

    override val start: Vector2Expression get() = edge.start

    override val end: Vector2Expression get() = edge.end

    override val connectorType: ConnectorType
        get() = if (isFromStart) ConnectorType.FROM_START else ConnectorType.FROM_END

    override val distance = FloatVariableImpl(edge.shape, "distance", distanceExpression)


    init {
        addDependencies(edge.start, edge.end, distance)
    }

    override fun isFixed() = distance.innerExpression !is FloatConstant

    override fun performMoveTowards(world: Vector2) {
        val startV = edge.start.eval()
        val endV = edge.end.eval()
        val diff = endV - startV
        val length = diff.magnitude()
        if (length == 0f) return // Cannot move along a line, when the line is zero long!
        val lengthSquared = length * length
        val ratio = ((endV - world).dot(diff)) / lengthSquared

        var newValue = if (isFromStart) length * (1 - ratio) else length * ratio
        if (isBounded) {
            newValue = newValue.clamp(0f, length)
        }
        distance.set(FloatConstant(newValue))
        dirty()
    }

    fun distance(): Float {
        return if (isBounded) {
            val startV = edge.start.eval()
            val endV = edge.end.eval()
            val diff = endV - startV
            distance.eval().clamp(0f, diff.magnitude())
        } else {
            distance.eval()
        }
    }

    override fun recalculate(): Vector2 {

        val startV = edge.start.eval()
        val endV = edge.end.eval()
        val diff = endV - startV
        val distance = if (isBounded) {
            distance.eval().clamp(0f, diff.magnitude())
        } else {
            distance.eval()
        }

        return if (isFromStart) {
            startV + diff.unit() * distance
        } else {
            endV + diff.unit() * -distance
        }
    }

    override fun tip() = "Distance [${distance()}]."

    override fun toString() =
        super.toString() + " ${edge.start.eval()} -> ${edge.end.eval()} distance ${distance.eval()} isBounded $isBounded"

}
