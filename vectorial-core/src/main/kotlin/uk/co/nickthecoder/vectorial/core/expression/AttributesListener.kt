package uk.co.nickthecoder.vectorial.core.expression

interface AttributesListener {
    fun attributeChanged(key: String)
}
