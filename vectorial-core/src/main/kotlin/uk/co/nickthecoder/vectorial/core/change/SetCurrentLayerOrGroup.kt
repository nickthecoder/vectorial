package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.DiagramImpl
import uk.co.nickthecoder.vectorial.core.Document
import uk.co.nickthecoder.vectorial.core.ShapeParent

interface SetCurrentLayerOrGroup : Change {
    val newLayerOrGroup: ShapeParent
}

internal class SetCurrentLayerOrGroupImpl(

    override val newLayerOrGroup: ShapeParent

) : SetCurrentLayerOrGroup, InternalChange {

    override val document: Document get() = newLayerOrGroup.diagram.document
    override val label: String get() = "Select Layer : ${newLayerOrGroup.name}"

    private val oldLayerOrGroup = newLayerOrGroup.diagram.currentLayerOrGroup

    override fun redo() {
        (newLayerOrGroup.diagram as DiagramImpl).currentLayerOrGroup = newLayerOrGroup
    }

    override fun undo() {
        (newLayerOrGroup.diagram as DiagramImpl).currentLayerOrGroup = oldLayerOrGroup
    }

}
