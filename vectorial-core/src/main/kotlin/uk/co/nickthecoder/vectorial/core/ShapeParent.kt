package uk.co.nickthecoder.vectorial.core

import org.joml.Matrix3x2f
import org.joml.Matrix3x2fc
import uk.co.nickthecoder.vectorial.core.change.*
import uk.co.nickthecoder.vectorial.core.change.AddShapeImpl
import uk.co.nickthecoder.vectorial.core.shape.AnyShapeFilter
import uk.co.nickthecoder.vectorial.core.shape.Group
import uk.co.nickthecoder.vectorial.core.shape.Shape
import uk.co.nickthecoder.vectorial.core.shape.ShapeFilter

/**
 * Any container which contains [Shape]s.
 * This includes [Diagram], and will include [Group] and [Layer] when they are implemented.
 *
 * NOTE. When [Layer] is implemented, [Diagram] will still be a [ShapeParent], because [Layer]
 * will be a subclass of [Group], which is itself a [Shape].
 */
interface ShapeParent : Parent {

    val diagram: Diagram
    val shapes: List<Shape>

    /**
     * The matrix which converts from local coordinates to the coordinates
     * of the parent.
     * For example, if we have a [Group] at (100,100), containing a Rectangle at (10,10),
     * then the rectangle's position in the group's coordinates is [110,110],
     * and this matrix is a translation by (100,100).
     *
     * [parentToLocal] is the inverse of this (which, in our example would be a translation of (-100,-100))
     */
    val localToParent: Matrix3x2fc

    /**
     * The inverse of [localToParent].
     */
    val parentToLocal: Matrix3x2fc


    /**
     * Returns a list of all [Shape]s which are at [point] (or close enough based on [threshold2]).
     * The shapes are ordered, such that the top-most one is first in the list, and the bottom-most is last.
     *
     * @param threshold2 The _square_ of the minimum distance threshold.
     */
    fun findShapesAt(
        point: Vector2,
        threshold: Float,
        threshold2: Float,
        into: MutableList<Shape>,
        filter: ShapeFilter = AnyShapeFilter
    )

    /**
     *
     * @param threshold2 The _square_ of the minimum distance threshold.
     */
    fun findTopShapeAt(
        point: Vector2,
        threshold: Float,
        threshold2: Float,
        filter: ShapeFilter = AnyShapeFilter
    ): Shape?

    fun addShape(index: Int, shape: Shape)

    fun addShape(shape: Shape)

    fun makeCurrentLayerOrGroup(): SetCurrentLayerOrGroup = SetCurrentLayerOrGroupImpl(this)

}

internal abstract class ShapeParentImpl : ShapeParent {

    final override val shapes: MutableList<Shape> = mutableListOf()
    final override val items get() = shapes

    final override val localToParent = Matrix3x2f()

    final override val parentToLocal: Matrix3x2f = localToParent.invert()

    override fun findShapesAt(
        point: Vector2,
        threshold: Float,
        threshold2: Float,
        into: MutableList<Shape>,
        filter: ShapeFilter
    ) {
        for (shape in shapes.asReversed()) {
            if (filter.acceptShape(shape) && shape.touching(point, threshold, threshold2)) {
                into.add(shape)
            }
        }
    }

    override fun addShape(index: Int, shape: Shape) {
        AddShapeImpl(index, this, shape).now()
    }

    override fun addShape(shape: Shape) {
        AddShapeImpl(- 1, this, shape).now()
    }
}
