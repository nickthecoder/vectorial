package uk.co.nickthecoder.vectorial.core

import uk.co.nickthecoder.vectorial.core.shape.Shape

/**
 * As layers are a hierarchical structure, [Diagram] and [Layer] can both be a [LayerParent].
 *
 * When a [Layer] has sub-layers, the [Shape]s in the layer are drawn first, then the sub-layers.
 */
interface LayerParent : Parent {
    /**
     * Layers are drawn in the order found in this last.
     * i.e. index 0 is the _lowest_ layer.
     */
    val layers: List<Layer>
    val diagram: Diagram

    fun addLayer(newLayer: Layer, position: NewLayerPosition = NewLayerPosition.ABOVE)
}

internal interface LayerParentImpl : LayerParent {
    override val layers: MutableList<Layer>
}
