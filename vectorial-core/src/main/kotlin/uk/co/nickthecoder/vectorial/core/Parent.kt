package uk.co.nickthecoder.vectorial.core

interface Parent : Item {

    val items: List<Item>

}
