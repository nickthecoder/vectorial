package uk.co.nickthecoder.vectorial.core.expression

import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.debug
import java.lang.Exception
import java.lang.ref.WeakReference

/**
 * The base class for [Expression] implementations, which don't need to use [CachedExpression].
 *
 * [V] is the `value` type, such as [Vector2].
 */
abstract class ExpressionBase<V : Any> : ChangeableExpression<V> {

    private val dependents = mutableListOf<WeakReference<Expression<*>>>()

    final override fun addDependencies(vararg dependencies: Expression<*>) {
        super.addDependencies(*dependencies)
    }

    final override fun addDependent(dependent: Expression<*>) {
        dependents.add(WeakReference(dependent))
    }

    final override fun removeDependent(dependent: Expression<*>) {
        dependents.removeIf { it.get() == null || it.get() === dependent }
    }

    override fun dirty() {
        try {
            for (u in dependents) {
                u.get()?.dirty()
            }
        } catch (e: Exception) {
            println("Dirty failed. this=$this")
            println("Dependents : ")
            for (u in dependents) {
                println("    ${u.get()}")
            }
            throw e
        }
    }
}

/**
 * Caches the results of [eval].
 * Whenever one of the dependencies change, it will call our [dirty] method,
 * which clears the cache.
 *
 * When [eval] is called, it should first check the cache. If the value is cached the value
 * can be returned without recalculating.
 * Otherwise, the calculation is performed, and the cache updated.
 *
 * [V] is the `value` type, such as [Vector2].
 */
abstract class CachedExpression<V : Any> : ExpressionBase<V>() {

    private var cachedValue: V? = null

    final override fun eval(): V {
        if (Expression.log) {
            debug("eval() for ${debugInfo()}")
        }
        try {
            depth++
            if (depth > MAX_DEPTH) {
                throw RecursionLimitExceeded(this)
            }
            cachedValue?.let { return it }
            recalculate().let {
                cachedValue = it
                return it
            }
        } finally {
            depth--
        }
    }

    abstract fun recalculate(): V

    override fun dirty() {
        depth++
        if (depth > MAX_DEBUG_DEPTH) {
            depth = 0
            throw RecursionLimitExceeded(this)
        }
        if (Expression.log) {
            debug("dirty() for ${debugInfo()}")
        }
        try {
            cachedValue = null
            super.dirty()
        } finally {
            depth--
        }
    }


    override fun toString(): String = debugInfo() + " = " + eval()

    companion object {

        // TODO Move to Expression?
        /**
         * The depth of recursion during [eval] or [dirty]
         */
        internal var depth = 0

        /**
         * The maximum recursion depth before [eval] or [dirty] will throw an [ExpressionException].
         * This should be greater than the maximum expected recursion needed for valid expressions,
         * but less than half of Java's stack limit.
         */
        const val MAX_DEPTH = 30

        const val MAX_DEBUG_DEPTH = 30
    }
}
