package uk.co.nickthecoder.vectorial.core
/*
import com.eclipsesource.json.Json
import com.eclipsesource.json.JsonArray
import com.eclipsesource.json.JsonObject
import com.eclipsesource.json.JsonValue
import uk.co.nickthecoder.vectorial.core.expression.Attribute
import uk.co.nickthecoder.vectorial.core.expression.Attributes
import uk.co.nickthecoder.vectorial.core.shape.Shape

object JSONWriter {

    fun Document.toJSON(): JsonObject {
        return obj {
            add("name", name)
            add("diagram", diagram.toJson())
        }
    }

    fun Diagram.toJson(): JsonObject {
        return obj {
            add("attributes", attributes.toJson())
            add("layers", array {
                for (layer in layers) {
                    add(layer.toJson())
                }
            })
        }
    }

    fun Layer.toJson(): JsonObject {
        return obj {
            add("name", name)
            add("shapes", array {
                for (shape in shapes) {
                    add(shape.toJson())
                }
            })
        }
    }

    fun Shape.toJson(): JsonObject {

    }

    fun Attributes.toJson(): JsonArray {
        return array {
            for (key in keys()) {
                val attribute = this@toJson[key]
                add(attribute.toJson())
            }
        }
    }

    fun Attribute<*, *>.toJson(): JsonValue {
        val jsonValue = when (val value = eval()) {
            is Int -> Json.value(value)
            is Boolean -> Json.value(value)
            is Float -> Json.value(value)
            is Double -> Json.value(value)
            is Vector2 -> value.toJson()
            is Angle -> value.toJson()
            else -> Json.NULL
        }
        return obj {
            add("key", key)
            add("value", jsonValue)
        }
    }

    fun Vector2.toJson(): JsonObject {
        return obj {
            add("x", x)
            add("y", y)
        }
    }

    fun Angle.toJson(): JsonObject {
        return obj {
            add("degrees", degrees)
        }
    }
}

private fun obj(block: JsonObject.() -> Unit) = JsonObject().apply { block() }
private fun array(block: JsonArray.() -> Unit) = JsonArray().apply { block() }

*/
