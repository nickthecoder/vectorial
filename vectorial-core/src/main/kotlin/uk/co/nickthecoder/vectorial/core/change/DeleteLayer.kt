package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.*
import uk.co.nickthecoder.vectorial.core.LayerParentImpl

/**
 * Deletes a [Layer].
 *
 * NOTE, This implements [ChangeCurrentShapeParent] because if the current layer is
 * deleted, then [Diagram.currentLayerOrGroup] will be changed in addition to deleting the layer.
 */
interface DeleteLayer : LayerChange, ChangeCurrentShapeParent {
    val layer: Layer
}

class DeleteLayerImpl(
    override val layer: Layer
) : DeleteLayer, InternalChange {

    override val diagram: Diagram get() = layer.diagram

    override val label: String get() = "Delete Layer ${layer.name}"

    private val index = layer.parent.layers.indexOf(layer)

    override fun redo() {
        val parent = (layer.parent) as LayerParentImpl
        val diagram = parent.diagram as DiagramImpl
        if (parent is Diagram && parent.layers.size == 1) {
            throw Exception("Cannot delete the last layer")
        } else {
            parent.layers.remove(layer)
            if (diagram.currentLayerOrGroup === layer) {
                diagram.currentLayerOrGroup = diagram.layers.first()
            }
        }
    }

    override fun undo() {
        (layer.parent as LayerParentImpl).layers.add(index, layer)
    }

}
