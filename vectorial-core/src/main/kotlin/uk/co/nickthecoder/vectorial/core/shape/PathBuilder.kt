package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.Vector2Constant
import uk.co.nickthecoder.vectorial.core.expression.Vector2Expression

class PathBuilder(startPoint: Vector2Expression) {

    private val path = PathImpl(startPoint)
    private var built = false

    constructor(startPoint: Vector2) : this(Vector2Constant(startPoint))

    fun build(): Path {
        if (built) throw IllegalStateException("Path has already been built")
        built = true
        return path
    }

    fun lineTo(point: Vector2) {
        path.appendLineSegment(point)
    }

    fun lineBy(delta: Vector2) {
        path.appendLineSegment(path.endPoint() + delta)
    }

}

inline fun buildPath(start: Vector2Expression, builderAction: PathBuilder.() -> Unit): Path {

    return PathBuilder(start).apply {
        builderAction()
    }.build()

}

inline fun buildPath(start: Vector2, builderAction: PathBuilder.() -> Unit) =
    buildPath(Vector2Constant(start), builderAction)
