package uk.co.nickthecoder.vectorial.core.change

interface Batch {
    var label: String

    fun addChange(change: Change)
    fun isSkippable(): Boolean
}
