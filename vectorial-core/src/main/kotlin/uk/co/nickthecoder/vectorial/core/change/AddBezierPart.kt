package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.Vector2Constant
import uk.co.nickthecoder.vectorial.core.expression.Vector2Expression
import uk.co.nickthecoder.vectorial.core.expression.createAttribute
import uk.co.nickthecoder.vectorial.core.shape.BezierPathPartImpl
import uk.co.nickthecoder.vectorial.core.shape.Path
import uk.co.nickthecoder.vectorial.core.shape.PathImpl

interface PathChange : ShapeChange {
    val path: Path
}

interface AddBezierPart : PathChange {

    val cp1Expression: Vector2Expression
    val cp2Expression: Vector2Expression
    val endExpression: Vector2Expression

}

internal class AddBezierPartImpl(

    override val path: PathImpl,
    override val cp1Expression: Vector2Expression,
    override val cp2Expression: Vector2Expression,
    override val endExpression: Vector2Expression

) : AddBezierPart, InternalChange {

    constructor(path: PathImpl, cp1: Vector2, cp2: Vector2, end: Vector2) : this(
        path,
        Vector2Constant(cp1), Vector2Constant(cp2), Vector2Constant(end)
    )

    override val label get() = "Add bezier curve"

    override val shape get() = path.shape

    override fun redo() {
        val start = path.end
        val cp1 = createAttribute(path.attributes, "point${++ path.pointCounter}", cp1Expression)
        val cp2 = createAttribute(path.attributes, "point${++ path.pointCounter}", cp2Expression)
        val end = createAttribute(path.attributes, "point${++ path.pointCounter}", endExpression)
        path.pathParts.add(BezierPathPartImpl(path, start, cp1, cp2, end))
    }

    override fun undo() {
        path.pathParts.removeLast()
        path.attributes.remove("point${path.pointCounter --}")
        path.attributes.remove("point${path.pointCounter --}")
        path.attributes.remove("point${path.pointCounter --}")
    }

    override fun toString() = "$label to $endExpression"

}
