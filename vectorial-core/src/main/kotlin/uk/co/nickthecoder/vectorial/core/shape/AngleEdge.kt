package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.ConnectorType
import uk.co.nickthecoder.vectorial.core.Edge
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.minimumDistanceSquare

/**
 * An [Edge] for a line going through [point] at a given [angle].
 *
 * Used by `GuideLine`, and may later be used by construction lines, such as tangents and normals to curves.
 */
interface AngleEdge : Edge {
    val point: Vector2Expression
    val angle: AngleExpression
}

internal class AngleEdgeImpl(

    override val shape: Shape,
    override val point: Vector2Expression,
    override val angle: AngleExpression,
    val length: FloatExpression = FloatConstant(1000f)

) : AngleEdge {

    private val start = point - angle.toVector() * length
    private val end = point + angle.toVector() * length

    override val points: List<Vector2> get() = listOf(start.eval(), end.eval())

    override val supportedConnectorTypes: List<ConnectorType> get() = SUPPORTED_CONNECTION_TYPES

    override fun isNear(point: Vector2, threshold2: Float): Boolean {
        return minimumDistanceSquare(start.eval(), end.eval(), point) < threshold2
    }

    override fun createConnector(point: Vector2, connectorType: ConnectorType, isBounded: Boolean): ConnectorToEdge {
        return DistanceAlongAngleEdgeImpl(this, 0f).apply {
            performMoveTowards(point)
        }
    }

    override fun toString() = "AngleEdge from ${point.eval()} angle ${angle.eval()}"

    companion object {
        val SUPPORTED_CONNECTION_TYPES = listOf(ConnectorType.DISTANCE)
    }

}

