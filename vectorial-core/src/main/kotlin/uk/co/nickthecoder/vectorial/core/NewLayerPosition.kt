package uk.co.nickthecoder.vectorial.core

enum class NewLayerPosition {
    ABOVE, BELOW, SUB_LAYER;

    override fun toString(): String {
        return when (this) {
            ABOVE -> "Above"
            BELOW -> "Below"
            SUB_LAYER -> "As a Sub-Layer"
        }
    }
}
