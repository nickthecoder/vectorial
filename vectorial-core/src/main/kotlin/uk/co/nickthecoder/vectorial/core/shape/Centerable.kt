package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.Geometry


enum class Centered {
    CENTERED,
    CORNER_BASED
}

// Make this part of Quadrilateral, so no need for a separate interface.
interface Centerable : Geometry {

    // val features : QuadFeatures
    val centered: Centered

    fun isCentered() = centered == Centered.CENTERED

    fun toggleCentered() =
        changeCentered(if (centered == Centered.CENTERED) Centered.CORNER_BASED else Centered.CENTERED)

    // changeFeatures
    fun changeCentered(centered: Centered)

}
