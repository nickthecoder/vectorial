package uk.co.nickthecoder.vectorial.core.change

import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.expression.*
import uk.co.nickthecoder.vectorial.core.shape.Shape
import uk.co.nickthecoder.vectorial.core.warn

/**
 * Moves a [Vector2Expression] to a new position.
 * Used when you drag a Handle.
 *
 * Note the new position will in general NOT be the value supplied as [newValue].
 * For example, [attribute] may be a calculated value, such as [RatioAlongLine],
 * and [newValue] only affects one degree of freedom (in this example, the ratio along the line).
 *
 * See also [MoveTo], which (unlike [MoveTowards]) will break any constraints on
 * the existing expression, and replace it with a different expression.
 */
interface MoveTowards : ShapeChange {
    val attribute: Vector2Attribute
    val newValue: Vector2
}

internal class MoveTowardsImpl(

    override val shape: Shape,
    override val attribute: Vector2Attribute,
    override var newValue: Vector2

) : MoveTowards, InternalChange {

    private val oldValue = attribute.eval()

    override val label: String
        get() = "Move ${shape.name}.${attribute.key} to $newValue"

    override fun redo() {
        try {
            if (attribute is MovableVector2ExpressionImpl && ! attribute.isFixed()) {
                attribute.performMoveTowards(newValue)
            } else {
                throw Exception("Move not supported : $attribute")
            }
        } catch (e: RecursionLimitExceeded) {
            attribute.debugEvalDirty("MoveTowards failure")
            (attribute as? MovableVector2ExpressionImpl)?.performMoveTowards(oldValue)
            warn("MoveTowards reverted due to : $e")
        }
    }

    override fun undo() {
        if (attribute is MovableVector2ExpressionImpl) {
            attribute.performMoveTowards(oldValue)
        } else {
            throw Exception("Move not supported on type ${attribute.javaClass}")
        }
    }

    override fun canMergeWith(previous: Change) = previous is MoveTowardsImpl && attribute === previous.attribute
    override fun mergeWith(previous: Change) {
        previous as MoveTowardsImpl
        previous.newValue = newValue
    }

}
