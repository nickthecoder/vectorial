package uk.co.nickthecoder.vectorial.core.shape

import uk.co.nickthecoder.vectorial.core.SimpleAttributeHandle
import uk.co.nickthecoder.vectorial.core.Vector2
import uk.co.nickthecoder.vectorial.core.change.ChangeGeometryImpl
import uk.co.nickthecoder.vectorial.core.expression.*

internal class CornerBasedParallelogram(
    corner0Exp: Vector2Expression,
    corner2Exp: Vector2Expression,
    ratioExpression: FloatExpression = FloatConstant(0.25f)

) : QuadrilateralImpl(), Parallelogram, MutableGeometry {

    constructor(corner0: Vector2, corner2: Vector2, ratio: Float = 0.25f) :
            this(Vector2Constant(corner0), Vector2Constant(corner2), FloatConstant(ratio))

    override lateinit var shape: GeometryShape

    override val centered: Centered get() = Centered.CORNER_BASED

    // Attributes
    override val attributes = AttributesImpl(this)
    override val corner0 = createAttribute(attributes, "corner0", corner0Exp)
    override val corner2 = createAttribute(attributes, "corner2", corner2Exp)
    override val ratio = createAttribute(attributes, "ratio", ratioExpression)


    // Expressions
    override val center = (corner0 + corner2) / 2f
    override val size = (corner2 - corner0)

    // NOTE These MUST be declared after [center] and [size].
    override val corner1 = ParallelogramCorner(this, 0.5f, -0.5f)
    override val corner3 = ParallelogramCorner(this, -0.5f, 0.5f)

    // Continuous Geometry expressions
    override val start get() = corner0

    // End of expressions

    private val cachedControlPoints by lazy {
        listOf(
            SimpleAttributeHandle(shape, corner0),
            SimpleAttributeHandle(shape, corner2),
            ParallelogramOffsetHandle(this),
        )
    }

    init {
        addCornerAttributes()
    }

    override fun handles() = cachedControlPoints

    override fun changeCentered(centered: Centered) {
        if (centered == Centered.CENTERED) {

            val geometry = CenteredParallelogram(
                Vector2Constant(center.eval()),
                Vector2Constant(size.eval()),
                ratio.expression
            )

            ChangeGeometryImpl("Convert to a center-based Parallelogram", shape, geometry).now()
        }
    }

    override fun changeQuadrilateralType(type: QuadrilateralType) {

        when (type) {
            QuadrilateralType.PARALLELOGRAM -> {}

            QuadrilateralType.RECTANGLE -> {
                ChangeGeometryImpl(
                    "Convert to a rectangle",
                    shape, CornerBasedRectangle(corner0.expression, corner2.expression)
                ).now()
            }

            QuadrilateralType.DIAMOND -> {
                ChangeGeometryImpl(
                    "Convert to a diamond",
                    shape, CornerBasedDiamond(corner0.expression, corner2.expression)
                ).now()
            }
        }
    }

    override fun movableAttributes() = listOf(corner0, corner2)

    override fun toString() = "Parallelogram center ${center.eval()} size ${size.eval()}} offset ${ratio.eval()}"


}
