# Module vectorial-core

The core vectorial classes, which does not include any rendering code, or GUI code.
Therefore, it has no external dependencies.

# Package uk.co.nickthecoder.vectorial.core

Core classes, such as [Document], [Diagram] as well as simple classes, such as [Vector2], [Angle], [Color] etc.

Note that `Shape` classes are contained in their own package (see below).

# Package uk.co.nickthecoder.vectorial.core.shape

Everything which implements [Shape], and related classes (such as shape specific `ControlPoints`).

# Package uk.co.nickthecoder.vectorial.core.expression

Unlike many drawing programs, vectorial's shapes do not directly use value types, such as `Vector2`, or `Color`.
Instead, they ae defined using [Expression]s.
An [Expression] can be a [Constant], or a calculation based upon other [Expression]s.

[Expression.eval] returns a value type, such as `Vector2`.

The supported value types are Double, Float, Int, Boolean, String, Angle, Color and Vector2.
These have corresponding expression types :
[DoubleExpression], [FloatExpression], [IntExpression], [BooleanExpression], [StringExpression], [AngleExpression],
[ColorExpression] and [Vector2Expression].

These hide [Expression]'s `generic` type, which makes Vectorial code easier to read, as well as give additional
type safety (which would otherwise by caused by Java's `Type Erasure`).
At no point, should you ever use a type such as Expression< Vector2 >, because that is NOT assignable to
[Vector2Expression].

# Package uk.co.nickthecoder.vectorial.core.change

Vectorial's model give read-only access to the data.
To change the model, you create a [Change] object, and pass it to [History].
[History] applies the change, and stores it in a list for the purposes of `undo/redo`.

This ensures that all changes to the `model` go through [History].

[Change]s are grouped into [Batch]es. Undo / Redo apply to batches, not individual changes.
However, batches often only contain one [Change].

Note, there are a few exceptions to the rule that only [Change] implementations can alter the model,
and these must be hidden in `internal` methods.
