// vectorial (root)
// Builds the GUI application.

plugins {
    kotlin("jvm") version "1.9.10"
    id("org.jetbrains.dokka") version "1.9.10"
    application
}

allprojects {
    apply(plugin = "org.jetbrains.kotlin.jvm")

    repositories {
        mavenCentral()
        // Glok is a GUI toolkit similar to JavaFX, but with low level access to OpenGL.
        maven {
            name = "glok"
            url = uri("https://gitlab.com/api/v4/projects/46354938/packages/maven")
        }
    }

    //kotlin {
    //    jvmToolchain(11)
    //}
}

application {
    mainClass.set("uk.co.nickthecoder.vectorial.Vectorial")
}

val vectorialVersion: String by project
val glokVersion: String by project
val jomlVersion: String by project
val lwjglVersion: String by project

dependencies {
    implementation(project(":vectorial-core"))

    implementation("uk.co.nickthecoder:glok-model:$glokVersion")
    implementation("uk.co.nickthecoder:glok-core:$glokVersion")
    implementation("uk.co.nickthecoder:glok-dock:$glokVersion")

    implementation("org.joml:joml:$jomlVersion")

    implementation("org.lwjgl:lwjgl:$lwjglVersion")
    implementation("org.lwjgl:lwjgl-glfw:$lwjglVersion")
    implementation("org.lwjgl:lwjgl-jemalloc:$lwjglVersion")
    implementation("org.lwjgl:lwjgl-opengl:$lwjglVersion")
    implementation("org.lwjgl:lwjgl-stb:$lwjglVersion")
    implementation("org.lwjgl:lwjgl-glfw:$lwjglVersion")
    implementation("org.lwjgl:lwjgl-nfd:$lwjglVersion")
}

task<Exec>("publishDokka") {
    dependsOn(":dokkaHtmlMultiModule")
    commandLine("./publishDokka.feathers", "build/dokka/htmlMultiModule/", "../api/vectorial-$vectorialVersion")
}

task<Exec>("publishZip") {
    dependsOn(":distZip")
    commandLine("cp", "build/distributions/vectorial-${vectorialVersion}.zip", "../download/")
}

task<Exec>("ntc") {
    dependsOn(":publishDokka", ":publishZip")
    commandLine("echo", "Done")
}

defaultTasks("installDist")
