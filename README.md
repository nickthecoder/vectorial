Vectorial
=========

A scalable vector image editor, similar, in spirit, to Inkscape.
However, `Vectorial` is targeted as a poor-man's 2D CAD program and diagram editor.

Still in the early stages of development, but is getting quite close to a usable prototype.

![screenshot](screenshot.jpeg)

Why?
----

Just for fun, and to try out something new.

Inkscape annoys me. Maybe that's because I don't know how to use it properly,
but at the moment, I don't like it.

There's one feature I'd really like in Inkscape though...

All "attributes" of a shape can be "expressions", which reference other shapes' attributes.
This gives us two features not seen in Inkscape :

1) Construction lines / Parametric shapes as seen in CAD programs
2) Shape geometries can be based on other shapes' attributes, as seen in Visio.

Expressions won't only be used for shapes' geometries. All attributes can be expression.
For example, we could define colors using expressions.
Have a "base" color, and then a lighter, and darker variation can be based on that.
Change the base color, and all the derived colors change too.

Technologies
------------

Rendering used `OpenGL` (with the help of `LWJGL`).
The GUI uses `JavaFX`.
The code is `Kotlin`.

See also [DesignChoices.md].

Build & Run
-----------

Vectorial uses gradle to build the project.
To compile (from the projects base directory), run the command :

    ./gradlew

This builds the complete application in ./build/install
So you can run it from the command line like so :

    cd ./build/install/vectorial/bin/
    ./vectorial

Or using gradle :

    ./gradlew run

To build the dokka API reference :

    ./gradle dokkaHtml
    
Progress
========

Completed
---------

* Application's main window, including toolbars, dockable windows.
* DiagramView, where you can edit the diagram.
* OpenGL rendering of a diagram onto a JavaFX ImageView
* Rectangles, Line, simple Bezier curves (not splines)
* Undo/Redo (The API only lets you modify the model via the "History" mechanism)
* Draggable Control Points
* Context-sensitive toolbars (i.e. a Selection toolbar, a Control Point toolbar etc.)

Unimplemented
-------------

* Grouping shapes
* Filled shapes (currently only shape's outlines are drawn)
* A GUI for choosing stroke and line colors
* Compound paths (Only single line segments, single bezier curves (not spline), and rectangles are implemented)
* Layers (which are a special kind of group)
* Regular polygons / stars
* Coordinate transformations (e.g. to rotate, scale, skew etc).
* A parser for Expressions (this is required for load/save, as the file format will be parsed expression during load/save) 
* User defined Document Attributes. e.g. to parameterize sizes etc.
* Multiple diagrams per document
* Loading/Saving files
* Line styles (dashed etc)
* Gradients (only flat color is supported at the moment)
* Exporting to PNG or other pixel-base image formats
* Text
* Embedded pixel-based images
* Macros/plug-ins
* Visio-style reusable "smart" shapes
* A GUI to edit preferences (though the VectorialPreferences object exists, and is being used)
* Sensible feedback to the user when cyclic dependencies occur within Expressions
* An "inspector" to view and edit Expressions (there is a "Debug" dockable, but it is limited)

I'm sure there's a lot more too!
