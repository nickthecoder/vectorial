# Vectorial API

If the left panel is blank, this is probably because you are viewing directly from files, rather than
via a web server (i.e. the url starts with `file:`, rather than `https:`).
This is an issue with `Dokka` itself. Sorry, I can't fix that!

## Project Structure

There are two `subprojects` ( referred to as `modules` by Dokka, which is confusing,
because the JVM uses that term to mean something very different! FYI, Vectorial doesn't use JVM `modules` ) :

### vectorial-core

Contains the logic for the model. No rendering code and no GUI, therefore it has no external dependencies.

### vectorial-graphics

`JavaFX` Controls. Primarily `DiagramView`, which lets you embed Vectorial diagrams into your own
JavaFX application.

Rendering is done using `OpenGL`, which is then transferred to a JavaFX `Image`.

There are quite a lot of 3rd party dependencies!
 
### The root project

The Vectorial application, which depends on `vectorial-core` and `vectorial-graphics`, as well as two of my own
libraries :

* `fxessentials` - Makes life using JavaFX a little easier!
* `harbourfx` - Dockable panes, which can be dragged to any edge of the main window, or detached within their own window.

The application offers no public API, and is therefore absent from this documentation.
