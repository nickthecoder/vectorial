/**
 * Version numbers for each 3rd party library/gradle plugin.
 */
object Version {
    // This project
    const val vectorial = "0.1"

    // Other projects, written by me
    const val fxessentials = "0.3"
    const val harbourfx = "0.3"

    // 3rd Party libraries
    const val javaFX = "13"
    const val lwjgl = "3.3.1"
    const val joml = "1.10.4"
    const val junit = "4.11"

    // Gradle plugins
    const val kotlin = "1.7.20"
    const val dokka = "1.8.10"
    const val javaFXPlugin = "0.0.14"
}
